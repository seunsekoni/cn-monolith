# Connect Nigeria
---

# Getting Started
Leggo 🚀
## Installation
1. Clone repository
2. Install dependencies
   > **Local**
    >> `composer install`

   > **Staging & Production**
    >> `composer install --no-dev`
3. Create `.env` file (if unavailable)
   > `composer run-script post-root-package-install`
4. Complete setup
   > `composer run-script post-create-project-cmd`
## Configuration
Configure your environment variables in `env`:
- Database information.
- Preferred filesystem in `FILESYSTEM_DRIVER=`. *I would recommend `=public`, so that your uploads will be accessible*.
- Preferred mailing driver and credentials.

*Note: By default, the queuer uses the database driver.*

## Migrations & Seeds
Run `php artisan migrate --seed`

# Important Notes
## Routing
1. Try as much as possible to avoid Closures in routes, so that it is easy to cache routes when we go live.

# Search Drivers
Basically, you can choose which Laravel Scout you want to use from the following:
1. [MeiliSearch](https://laravel.com/docs/8.x/scout#meilisearch)

## MeiliSearch
For proper optimization, use **Docker**. Following [this link](https://docs.meilisearch.com/learn/getting_started/installation.html) to configure your local machine.
- When implementation Geocode, I used [Geohash](https://www.programmersought.com/article/36821617059/). This is to enable geocode search (location).
  Geohash is used as an alias in the project, hence, you can use it by `\Geocode::...`.

