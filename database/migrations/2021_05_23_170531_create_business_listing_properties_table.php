<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessListingPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_listing_properties', function (Blueprint $table) {
            $table->unsignedBigInteger('business_listing_id');
            $table->unsignedBigInteger('category_property_id');
            $table->string('category_property_value');
            $table->timestamps();
            $table->primary(['business_listing_id', 'category_property_id'], 'business_listing_property_index_primary');
            $table->unique(['business_listing_id', 'category_property_id'], 'business_listing_property_index_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_listing_properties');
    }
}
