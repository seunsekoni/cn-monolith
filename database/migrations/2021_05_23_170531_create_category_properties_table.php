<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id');
            $table->string('name');
            $table->string('hint')->nullable()->comment('Short description on what users should enter');
            $table->boolean('status')->default(1);
            $table->boolean('optional')->default(1)
                ->comment('Can user input or pick from available category property values?');
            $table->boolean('filterable')->default(0)->comment('Should appear in the webpage for the category?');
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['category_id', 'name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_properties');
    }
}
