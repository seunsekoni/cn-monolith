<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToCategoryPropertyValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_property_values', function (Blueprint $table) {
            $table->foreign('category_property_id')
                ->references('id')
                ->on('category_properties')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_property_values', function (Blueprint $table) {
            $table->dropForeign(['category_property_id']);
        });
    }
}
