<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('business_type_id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->text('profile')->nullable();
            $table->boolean('featured')->default(0);
            $table->boolean('verified')->default(0);
            $table->timestamp('verified_at')->nullable();
            $table->unsignedInteger('views')->default(0);
            $table->unsignedInteger('likes')->default(0);
            $table->unsignedInteger('bookmarks')->default(0);
            $table->tinyInteger('shares')->default(0);
            $table->unsignedInteger('sms_credit')->default(0);
            $table->boolean('changed_logo')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
