<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToBusinessListingPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_listing_properties', function (Blueprint $table) {
            $table->foreign('business_listing_id')
                ->references('id')
                ->on('business_listings')
                ->onDelete('CASCADE');
            $table->foreign('category_property_id')
                ->references('id')
                ->on('category_properties')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_listing_properties', function (Blueprint $table) {
            $table->dropForeign(['business_listing_id']);
            $table->dropForeign(['category_property_id']);
        });
    }
}
