<?php

namespace Database\Factories;

use App\Models\Business;
use App\Models\BusinessType;
use Illuminate\Database\Eloquent\Factories\Factory;

class BusinessFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Business::class;

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterMaking(function (Business $business) {
            $business->businessType()->associate(BusinessType::inRandomOrder()->first());
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->company,
            'profile' => $this->faker->sentence,
        ];
    }

    /**
     * Indicate that the user is verified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function verified()
    {
        return $this->state(function () {
            return [
                'verified' => true,
                'verified_at' => now(),
            ];
        });
    }

    /**
     * Indicate that the business is featured.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function featured()
    {
        return $this->state(function () {
            return [
                'featured' => true,
            ];
        });
    }
}
