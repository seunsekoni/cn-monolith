<?php

namespace Database\Factories;

use App\Models\Business;
use App\Models\BusinessListing;
use App\Models\Review;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReviewFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Review::class;

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        $models = [Business::class, BusinessListing::class];

        return $this->afterMaking(function (Review $review) use ($models) {
            $i = rand(0, (count($models) - 1));

            $review->reviewable()->associate($models[$i]::inRandomOrder()->first());
            $review->user()->associate(User::inRandomOrder()->first());
            $review->parent()->associate(Review::inRandomOrder()->first());
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'comment' => $this->faker->sentence,
            'rating' => rand(0, 5),
        ];
    }

    /**
     * Indicate that the comment is approved.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function approved()
    {
        return $this->state(function () {
            return [
                'approved' => true,
            ];
        });
    }
}
