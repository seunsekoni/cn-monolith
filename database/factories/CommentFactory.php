<?php

namespace Database\Factories;

use App\Models\Business;
use App\Models\BusinessListing;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        $models = [Business::class, BusinessListing::class];

        return $this->afterMaking(function (Comment $comment) use ($models) {
            $i = rand(0, (count($models) - 1));

            $comment->commentable()->associate($models[$i]::inRandomOrder()->first());
            $comment->user()->associate(User::inRandomOrder()->first());
            $comment->parent()->associate(Comment::inRandomOrder()->first());
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'body' => $this->faker->sentence,
        ];
    }

    /**
     * Indicate that the comment is approved.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function approved()
    {
        return $this->state(function () {
            return [
                'approved' => true,
            ];
        });
    }
}
