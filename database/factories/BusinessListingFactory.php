<?php

namespace Database\Factories;

use App\Models\Business;
use App\Models\BusinessListing;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class BusinessListingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BusinessListing::class;

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterMaking(function (BusinessListing $businessListing) {
            $businessListing->business()->associate(Business::approved()->inRandomOrder()->first());
            $businessListing->category()->associate(Category::approved()->inRandomOrder()->first());
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->jobTitle,
            'description' => $this->faker->sentence,
            'geolocation' => "{$this->faker->latitude}, {$this->faker->longitude}",
            'price' => $this->faker->randomFloat(2, 1.00, 100.00),
        ];
    }

    /**
     * Indicate that the business listing is active.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function active()
    {
        return $this->state(function () {
            return [
                'status' => true,
            ];
        });
    }

    /**
     * Indicate that the business listing is featured.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function featured()
    {
        return $this->state(function () {
            return [
                'featured' => true,
            ];
        });
    }
}
