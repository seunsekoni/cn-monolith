<?php

namespace Database\Seeders;

use App\Models\BusinessListing;
use Illuminate\Database\Seeder;

class BusinessListingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BusinessListing::factory(100)->active()->create();
    }
}
