<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'id' => 1,
                'name' => 'admin',
                'guard_name' => 'admin'
            ],
            [
                'id' => 2,
                'name' => 'super admin',
                'guard_name' => 'admin'
            ],
        ];

        foreach ($roles as $role) {
            Role::updateOrCreate([
                'id' => $role['id']
            ], $role);
        }
    }
}
