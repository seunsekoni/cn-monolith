<?php

namespace Database\Seeders;

use App\Imports\BusinessTypeImport;
use Illuminate\Database\Seeder;

class BusinessTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new BusinessTypeImport())->import(storage_path('existing-data/business types.xlsx'));
    }
}
