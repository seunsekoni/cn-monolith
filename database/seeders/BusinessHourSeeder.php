<?php

namespace Database\Seeders;

use App\Models\Business;
use Illuminate\Database\Seeder;

class BusinessHourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $businesses = Business::all();

        foreach ($businesses as $business) {
            $business->update([
                'business_hours' => file_get_contents(base_path() . '/default_business_hours.json')
            ]);
        }
    }
}
