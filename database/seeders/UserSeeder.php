<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate(
            [
                'email' => 'user@connectnigeria.com',
                'phone' => '08071718932',
            ],
            [
                'first_name' => 'System',
                'last_name' => 'User',
                'password' => Hash::make('password'),
            ]
        );
    }
}
