<?php

namespace Database\Seeders;

use App\Imports\StateImport;
use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new StateImport())->import(storage_path('existing-data/states.xlsx'));
    }
}
