<?php

namespace Database\Seeders;

use App\Imports\CityImport;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new CityImport())->import(storage_path('existing-data/cities.xlsx'));
    }
}
