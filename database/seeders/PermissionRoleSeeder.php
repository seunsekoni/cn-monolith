<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Admin::whereEmail('admin@connectnigeria.com')->first();

        // Assign role to admin
        $admin->assignRole('super admin');

        // Give role permission.
        $admin->givePermissionTo('view audit');
    }
}
