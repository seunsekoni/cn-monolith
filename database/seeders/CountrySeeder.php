<?php

namespace Database\Seeders;

use App\Imports\CountryImport;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new CountryImport())->import(storage_path('existing-data/countries.xlsx'));
    }
}
