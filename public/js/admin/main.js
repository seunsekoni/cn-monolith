String.prototype.ucwords = function () {
    value = this.toString();
    return value?.toLowerCase().replace(/(?: )[^\s]|^./g, a => a.toUpperCase())
};

$(function(){
    // Hiding sidebar on page load
    if($(window).width() < 480){
        $(".sidebar").attr("style", "display: none")
    }

    // Listing parent elements in the main content
    const mainContentElements = [$("#content"), $(".navbar")]

    // Styling sidebar when sidebar toggle button is toggled
    $("#sidebarToggle").click(function(){
        $(".sidebarToggleBg").attr("style", `width: ${$(".sidebar").width()}px !important`)

        if($(".sidebar").width() <= 110){
            mainContentElements.forEach(function(element){
                element.attr("style", "padding-left: 105px");
            });
            $(".navbar").attr("style", "width: calc(100% - 105px)");
        }else{
            mainContentElements.forEach(function(element)
            {
                element.attr("style", "padding-left: 250px");
            });
            $(".navbar").attr("style", "width: calc(100% - 250px)");
        }
    });

    $("#sidebarToggleTop").click(function(){
        if($(".sidebar").width() > 200){
            mainContentElements.forEach(function(element){
                element.attr("style", "padding-left: 0px !important");
            });
            $(".navbar").attr("style", "width: calc(100%) !important");
        }else{
            $(".sidebarToggleBg").attr("style", `width: ${$(".sidebar").width()}px !important`)

            mainContentElements.forEach(function(element){
                element.attr("style", `padding-left: ${$(".sidebar").width()}px !important`);
            });
            $(".navbar").attr("style", `width: calc(100% - ${$(".sidebar").width()}px) !important`);
            $(".sidebarToggleBg").attr("style", `width: ${$(".sidebar").width()}px !important`)
        }

    })

    // Increasing and decreasing content size for responsiveness
    $(window).on('resize', function(){
        mainContentElements.forEach(function(element){
            element.attr("style", `padding-left: ${$(".sidebar").width()}px !important`);
        });
        $(".navbar").attr("style", `width: calc(100% - ${$(".sidebar").width()}px) !important`);
        $(".sidebarToggleBg").attr("style", `width: ${$(".sidebar").width()}px !important`)
    });


    // Mixed javascript and jquery to use sidebar collapse buttons to open the sidebar when clicked
    const collapseBtns = [].slice.call($(".collapsed"));

    collapseBtns.forEach(function (element, index){
        element.addEventListener("click", function(){
            $(".sidebarToggleBg").attr("style", `width: ${$(".sidebar").width()}px !important`)

            if($(window).width() < 615){
                mainContentElements.forEach(function(element){
                    element.attr("style", `padding-left: 0px !important`);
                });
                $(".navbar").attr("style", `width: 100% !important`);
            }

            if($(".sidebar").width() < 110){
                document.getElementById("sidebarToggle").click();

                mainContentElements.forEach(function(element){
                    element.attr("style", "padding-left: 250px");
                });
                $(".navbar").attr("style", "width: calc(100% - 250px)");
            }else if ($(".sidebar").width() == 110){
                document.getElementById("sidebarToggle").click();
                if($(window).width() < 767){
                    mainContentElements.forEach(function(element){
                        element.attr("style", `padding-left: 0px !important`);
                    });
                    $(".navbar").attr("style", `width: 100% !important`);
                }

                if($(window).width() > 480){
                    $(".sidebar").attr("style", "width: 250px !important; overflow-x: hidden");
                }else{
                    $(".sidebar").attr("style", "width: 100% !important; overflow-x: hidden");
                };
            }

        });
    });

    // Handling sidebar when sidebar toggle is toggled
    $("#sidebarToggleTop").click(function(){
        let fired = false;

        if(!fired){
            $(".sidebar").attr("style", "display: block");
            fired = true;
        }else{
            $(".sidebar").attr("style", "display: none");
            fired = false;
        };
    })
});

// Sorry to use this, code became a little complicated
// Checkout for font-size every 10milliseconds
function fontCheckTimer() {
    const sideBarElements = [$(".sidebar .nav-item i"), $(".sidebar .nav-item span"), $(".sidebar .collapse-item")];

    if($(".sidebar").width() <= 110){
        sideBarElements.forEach(function(element){
            element.attr("style", "font-size: .85rem !important");
        });
    }else{
        sideBarElements.forEach(function(element){
            element.attr("style", "font-size: 1.05rem !important");
        });
    }
};

setInterval(fontCheckTimer, 10);