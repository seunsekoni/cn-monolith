
$(function() { 
    "use strict";

    // Append .background-image-holder <img>'s as CSS backgrounds
    // $('.background-image-holder').each(function() {
    //     var imgSrc = $(this).children('img').attr('src');
    //     $(this).css('background', 'url("' + imgSrc + '")');
    //     $(this).children('img').hide();
    //     $(this).css('background-position', 'initial');
    // });

    // CAROUSEL
    // * Available carousel data attributes:
    //     data-items="1".......................(items visible on desktop)
    //     data-items-mobile-portrait="1".......(items visible on mobiles)
    //     data-items-mobile-landscape="1"......(items visible on mobiles)
    //     data-items-tablet="1"................(items visible on tablets)
    //     data-loop="true".....................(true/false)
    //     data-margin="10".....................(space between items)
    //     data-center="false"..................(true/false)
    //     data-start-position="0"..............(item start position)
    //     data-animate-in="fadeIn".............(more animations: http://daneden.github.io/animate.css/)
    //     data-animate-out="fadeOut"...........(more animations: http://daneden.github.io/animate.css/)
    //     data-mouse-drag="true"...............(true/false)
    //     data-autoheight="false"..............(true/false)
    //     data-autoplay="true".................(true/false)
    //     data-autoplay-timeout="5000".........(milliseconds)
    //     data-autoplay-hover-pause="true".....(true/false)
    //     data-autoplay-speed="800"............(milliseconds)
    //     data-nav="true"......................(true/false)
    //     data-nav-speed="800".................(milliseconds)
    //     data-dots="true".....................(true/false)
    //     data-dots-speed="800"................(milliseconds)
    
     $('.owl-carousel').each(function(){
         var $carousel = $(this);
         $carousel.owlCarousel({

            items: $carousel.data("items"),
            loop: $carousel.data("loop"),
            margin: $carousel.data("margin"),
            center: $carousel.data("center"),
            startPosition: $carousel.data("start-position"),
            animateIn: $carousel.data("animate-in"),
            animateOut: $carousel.data("animate-out"),
            autoWidth: $carousel.data("autowidth"),
            autoHeight: $carousel.data("autoheight"),
            autoplay: $carousel.data("autoplay"),
            autoplayTimeout: $carousel.data("autoplay-timeout"),
            autoplayHoverPause: $carousel.data("autoplay-hover-pause"),
            autoplaySpeed: $carousel.data("autoplay-speed"),
            nav: $carousel.data("nav"),
            navText: ['', ''],
            navSpeed: $carousel.data("nav-speed"),
            dots: $carousel.data("dots"),
            dotsSpeed: $carousel.data("dots-speed"),
            mouseDrag: $carousel.data("mouse-drag"),
            touchDrag: $carousel.data("touch-drag"),
            dragEndSpeed: $carousel.data("drag-end-speed"),
            lazyLoad: $carousel.data("lazy-load"),
            video: true,
            responsive: {
               0: {
                  items: $carousel.data("items-mobile-portrait"),
                  center: false,
               },
               480: {
                  items: $carousel.data("items-mobile-portrait"),
                  center: false,
               },
               768: {
                  items: $carousel.data("items-mobile-portrait"),
                  center: false,
               },
               992: {
                  items: $carousel.data("items-tablet-landscape"),
               },
               1200: {
                  items: $carousel.data("items"),
               }
            }

         });

      });
     // END CAROUSEL

    $('[data-toggle="tooltip"]').tooltip();
});

function equal() {
  $(".equal").css("height", "auto"),
      $(window).width() >= 992 &&
          $(".equal-container").each(function () {
              var t = $(this),
                  i = !1,
                  n = !1;
              (i = t
                  .find(".equal")
                  .map(function () {
                      return $(this).outerHeight();
                  })
                  .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal").css("height", n),
                  (i = t
                      .find(".equal2")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal2").css("height", n),
                  (i = t
                      .find(".equal3")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal3").css("height", n),
                  (i = t
                      .find(".equal4")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal4").css("height", n),
                  (i = t
                      .find(".equal5")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal5").css("height", n),
                  (i = t
                      .find(".equal6")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal6").css("height", n);
          }),
      $(window).width() >= 768 &&
          $(window).width() <= 991 &&
          $(".equal-container.sm").each(function () {
              var t = $(this),
                  i = !1,
                  n = !1;
              (i = t
                  .find(".equal")
                  .map(function () {
                      return $(this).outerHeight();
                  })
                  .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal").css("height", n),
                  (i = t
                      .find(".equal2")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal2").css("height", n),
                  (i = t
                      .find(".equal3")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal3").css("height", n),
                  (i = t
                      .find(".equal4")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal4").css("height", n),
                  (i = t
                      .find(".equal5")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal5").css("height", n),
                  (i = t
                      .find(".equal6")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal6").css("height", n);
          }),
      $(window).width() <= 767 &&
          $(".equal-container.xs").each(function () {
              var t = $(this),
                  i = !1,
                  n = !1;
              (i = t
                  .find(".equal")
                  .map(function () {
                      return $(this).outerHeight();
                  })
                  .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal").css("height", n),
                  (i = t
                      .find(".equal2")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal2").css("height", n),
                  (i = t
                      .find(".equal3")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal3").css("height", n),
                  (i = t
                      .find(".equal4")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal4").css("height", n),
                  (i = t
                      .find(".equal5")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal5").css("height", n),
                  (i = t
                      .find(".equal6")
                      .map(function () {
                          return $(this).outerHeight();
                      })
                      .get()),
                  (n = Math.max.apply(null, i)),
                  t.find(".equal6").css("height", n);
          }),
      switch_contents();
}
function switch_contents() {
    $(window).width() >= 768 &&
        $(window).width() <= 991 &&
        ($(".switch-contents-xs").each(function () {
            $(this).find(".switch1").insertBefore($(this).find(".switch2"));
        }),
        $(".switch-contents-sm").each(function () {
            $(this).find(".switch1").insertAfter($(this).find(".switch2"));
        })),
        $(window).width() <= 767 &&
            ($(".switch-contents-sm").each(function () {
                $(this).find(".switch1").insertBefore($(this).find(".switch2"));
            }),
            $(".switch-contents-xs").each(function () {
                $(this).find(".switch1").insertAfter($(this).find(".switch2"));
            })),
        $(window).width() >= 992 &&
            $(".switch-contents-sm,.switch-contents-xs").each(function () {
                $(this).find(".switch1").insertBefore($(this).find(".switch2"));
            });
}
$(window).on("load", function (t) {
    equal();
}),
$(window).on("resize", function (t) {
    equal();
});


// Review modal popup
$('.showReviews').on('click', function(){
    $('#ratingModal').modal('show');
    // $('#reviewSubmit').prop('disabled', true)
})

$('.close-review-modal i').on('click', function(){
    $('#ratingModal').modal('hide');
})

// Set default value of rating to 1 by default
function setDefaultStar() {
    $('.rating-stars li')[0].click()
}

// Review Star Rating
/* 1. Visualizing things on Hover - See next part for action on click */
$('.rating-stars #stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10);
    $(this).parent().children('li.star').each(function(e){
        if (e < onStar) {
            $(this).addClass('hover');
        }
        else {
            $(this).removeClass('hover');
        }
    });
}).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
        $(this).removeClass('hover');
    });
});
  
  
/* 2. Action to perform on click */
$('.rating-stars #stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10);
    var stars = $(this).parent().children('li.star');

    for (i = 0; i < stars.length; i++) {
        $(stars[i]).removeClass('selected');
    }

    for (i = 0; i < onStar; i++) {
        $(stars[i]).addClass('selected');
    }

    var ratingValue = parseInt($('.rating-stars #stars li.selected').last().data('value'));

    // Converting rate to percentage and sending to HTML input
    var ratingPercentage = (ratingValue/stars.length)*100
    // $("#starRatingPercentage").val(ratingPercentage)
    $("#starRatingPercentage").val(ratingValue)
    
    // rating Response
    var msg = "";
    if (ratingValue > 1) {
        msg = "Thanks! You rated this " + ratingValue + " stars.";
    }
    else {
        msg = "We will improve ourselves. You rated this " + ratingValue + " star.";
    }

    // // disable submit button until all required fields are filled for the modal
    // $('#reviewText').on('keyup', function () {
    //     let reviewText = $('#reviewText').val();

    //     if(ratingValue >=0 && reviewText.trim().length != "") {
    //         console.log((ratingValue >= 0),  (reviewText != ""))
    //         $('#reviewSubmit').removeAttr('disabled');
    //     } else {
    //         $('#reviewSubmit').prop('disabled', true);
    //     }

    // })
    responseMessage(msg);

});



$(window).on('load', function(){
    $('.rating-widget .success-box div.text-message').html("<span>Give us a star!</span>");
})

function responseMessage(msg) {
  $('.rating-widget .success-box div.text-message').html("<span>" + msg + "</span>");
}


// Set height for listings card
$(window).on("load resize", function(){
    var cardWidth = $(".listing-card-img").width()
    $(".listing-card-img").css({"height": (cardWidth-20)+"px"})
})

// Action for reply
// $(".comment .card").each(function(i, element){
//     $(this).find('small').on('click', function(){
//         if(!$(this).parent().children('.reply-add').length > 0){
//             $(".comment .card").each(function(){
//                 $(this).find('.reply-add').remove()
//             })

//             if($(element).find('.reply').hasClass('head')){
//                 var commenter = $(element).children('div.d-flex').children('div.user').children('span').children('.commenter').text()
//                 $(element).find('.reply.head').append(
//                     `
//                     <div class="reply-add my-3 row">
//                         <form action="">
//                             <div class="col-12">
//                                 <input type="text" name="" id="" >
//                             </div>
//                             <div class="col-12 mt-2">
//                                 <button type="button" class="btn btn-secondary cancel">Cancel</button>
//                                 <button type="submit" class="btn btn-primary">Reply</button>
//                             </div>
//                         </form>
//                     </div>
//                     `
//                 )
//                 $(element).find('.reply.head').find("input").val(`@${commenter} `).focus()

//                 $(element).find('.reply.head').find('.cancel').on('click', function(){
//                     $(this).parent().parent().parent().remove()
//                 })
//             }
//         }
//     })
// })

// $('.comment .card .collapse').find('.reply.tail').each(function(i, element){
//     $(this).find('small').on('click', function(){
//         if(!$(this).parent().children('.reply-add').length > 0){
//             $(".comment .card").each(function(){
//                 $(this).find('.reply-add').remove()
//             })
            
//             var commenter = $(this).parent().parent().parent().find('.commenter').text()

//             $(element).append(
//                 `
//                 <div class="reply-add my-3 row">
//                     <form action="">
//                         <div class="col-12">
//                             <input type="text" name="" id="" >
//                         </div>
//                         <div class="col-12 mt-2">
//                             <button type="button" class="btn btn-secondary cancel">Cancelgggggg</button>
//                             <button type="submit" class="btn btn-primary">Reply</button>
//                         </div>
//                     </form>
//                 </div>
//                 `
//                 )
//             $(element).find("input").val(`@${commenter} `).focus()

//             $(element).find('.cancel').on('click', function(){
//                 $(this).parent().parent().parent().remove()
//             })
//         }
//     })
// })
    
