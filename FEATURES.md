## HOW TO USE THIRD SIDEBAR SUB-MENU

* Add a `d-flex justify-content-between` to the class of `<a></a>` tag wrapping the menu you wish to include a sub-menu.
* Replace url in the `href` attribute with a collapse name (staring with an id `#` sign) which you will be using as the value of `id` for your menu, example `href="https://example.com"` to `href="#collapseExample"`.
* Include the following attributes to the `<a></a>` tag - `data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"`
* Wrap the text inside the `a` tag with a `span` tag, e.g `Categories` to `<span>Categories</span>`. Go to the next line and add `<i class="fa fa-chevron-down chevron-icon"></i>`
* Create a `div` tag outside the previous `a` tag, giving the former the same id used inside the latter's `href` attribute, e.g `collapseExample`, and a class name of `collapse`.
* Open the `div` tag and include - `<div class="third-layer-menu border"></div>`.
* Open the above `div` tag and list your menu, each wrapped up in an `a` tag.
* After all these steps, you should have a block that looks like but may not be equal to:
```php
<a
    class="collapse-item active d-flex justify-content-between"
    data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"
>
    <span>Categories</span>
    <i class="fa fa-chevron-down chevron-icon"></i>
</a>
<div class="collapse" id="collapseExample">
    <div class="third-layer-menu border">
        <a href="https://example.com/all }}">List All</a>
        <a href="https://example.com?c=1 }}">Category 1</a>
        <a href="https://example.com?c=2 }}">Category 2</a>

    </div>
</div>
```