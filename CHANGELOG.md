# CHANGELOG
All major changes to application are here...

# Routing

## File Structure
The `routes/web.php` file can become very large as more routes are added for all kinds of users, hence, the need to separate routes based on major prefixes is important. One huge consideration of this is that the **admin** routes alone can be very many, and the main website routes can double. *think about it* 😏

Now, all web routes are placed in the `routes/web/` directory:
- `admin.php`: Contains all routes with the prefix of `/admin`.
  E.g.; `Route::get('login', fn () => view('admin.login'));` will resolve in the browser as `{{appUrl}}/admin/login`
- `generic.php`: Contains all general routes without any prefixes.
  E.g.; `Route::get('login', fn () => view('auth.login'));` will resolve in the browser as `{{appUrl}}/login`

*An expansion of this technique for routing is welcome...*

## Style
In [Laravel 8.x](https://laravel.com/docs/8.x/releases#routing-namespace-updates), a new technique for routing namespace update was introduced, which I like to call the `Class Structure`. I adopted this rule because;
1. It improves code readability.
2. Enhances code completion and referencing for IDEs. That is, you don't have to read the entire line of route to navigate which file was referenced, when your IDE can help with that.

**THIS IS NOW COMPLETELY ADOPTED!**

# Business Module
Run `php artisan migrate`
Run `php artisan db:seed --class=TagSeeder`

## New
- Tags:
  A new `Tag` model is introduced, to serve as a collection of tags for businesses.
  <br>
  Also, introduces a pivot table for businesses and tags.
- Gallery:
  A new `Gallery` model is introduced, basically, to replace the old `PhotoGallery`.
  This model will be used "polymorphically" across other models for storing gallery photos or URLs.
  <br>
  Its purpose is to utilize the media library, as well as keep track of URLs (video links) for any necessary model relation.
- Template:
  A new `Template` model is introduced.

## Notes
### Migrations
**These will take some time when you run it! So be patient 🙂**
- Businesses (`businesses` table)
  The `business_type_id` in this table should be a foreign key to the `business_types` table, hence, if there are businesses without a type, there is a migration that automatically converts those values to **`null`**.
  <br>
  The same applies to the `user_id` column in the same table.
- Business Locations (`business_locations` table)
  Some locations exist without a business, which is absurd. So, these data will be deleted automatically, thereby adding the foreign key constraint to the `business_id` column.
  <br>
  The deletion is necessary because there is no use of a location data without a business attached to it, hence, `business_id` is now a **cascade-on-delete** attribute.
