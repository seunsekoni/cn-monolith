<?php

return [
    // Set naming limits.
    'business_name_limit' => env('CN_BUSINESS_NAME_LIMIT', 50),
    'listing_name_limit' => env('CN_LISTING_NAME_LIMIT', 50),

    // Set default admin passwor
    'default_admin_password' => env('DEFAULT_ADMIN_PASSWORD', 'cn2021'),
];
