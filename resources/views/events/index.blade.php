@extends('layouts.app')

@section('title', 'Events')

@section('content')
<div class="nav-container">
  <div>
    @include('includes.top-menu')
    @include('includes.events-nav')
  </div>
</div>
<div class="main-container">
  <section class="imagebg height-40">
    <div class="background-image-holder">
      <img src="{{asset('img/events/hero-banner-1.jpg')}}" alt="CN Business" />
    </div>
    <div class="container pos-vertical-center">
      <div class="row align-items-center">
        <div class="col-md-12 col-lg-9 col-xl-9">
          <form class="search-bar mb-2">
            <div class="row">
              <div class="col-5 col-md-2 mb-2 mb-md-0 mb-lg-0 mb-xl-0">
                <div class="input-select">
                  <select class="form-control form-control-lg cars-search-dropdown">
                    <option>All Dates</option>
                    <option>Today</option>
                    <option>Tomorrow</option>
                    <option>This Week</option>
                    <option>This Month</option>
                    <option>Next Month</option>
                  </select>
                </div>
              </div>
              <div class="col-7 col-md-4 mb-2 mb-md-0 mb-lg-0 mb-xl-0">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">Search</span>
                  </div>
                  <input type="text" class="form-control" placeholder="events or categories">
                </div>
              </div>
              <div class="col-9 col-md-4 pr-xs-0">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">In</span>
                  </div>
                  <input type="text" class="form-control" placeholder="Lagos, Nigeria">
                </div>
              </div>
              <div class="col-3 col-md-1 pl-xs-0">
                <button type="submit" class="btn btn-primary h-100"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </form>

          <div class="popular-searches">
            <span class="color-primary">
              <small class="type-bold">Popular:</small>
            </span>
            <span>
              <small class="color-white">Forums in Lekki, Online Talks, Business Fair</small>
            </span>
          </div>
        </div>
        <div class="col-lg-3 col-xl-3 hidden-xs hidden-sm">
          <p class="mb-0"><small>Advertisements</small></p>
          <div class="medrec-carousel owl-carousel owl-theme" data-items="1" data-items-mobile-portrait="1"
            data-autoplay="true" data-autoplay-timeout="5000" data-loop="true">
            <div class="item">
              <img src="{{asset('img/events/ads-1.jpg')}}" width="240" class="text-center mb-3">
            </div>
            <div class="item">
              <img src="{{asset('img/events/ads-2.jpg')}}" width="240" class="text-center mb-3">
            </div>
            <div class="item">
              <img src="{{asset('img/events/ads-3.jpg')}}" width="240" class="text-center mb-3">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- categories -->
  <section class="pb-0">
    <div class="container">
      <object src="{{asset('svg/svg_cats.svg')}}" style="display: none;"></object>
      <ul class="nav nav-tabs browse">
        <li><span class="type-bold">Browse by</span></li>
        <li><a data-toggle="tab" href="#categories" class="active">Categories</a></li>
        <li><a data-toggle="tab" href="#location">Location</a></li>

      </ul>

      <div class="tab-content">
        <div id="categories" class="tab-pane fade in active show">
          <div class="category-carousel owl-carousel owl-theme" data-items="8" data-margin="20"
            data-items-mobile-portrait="3" data-autoplay="true" data-loop="true" data-autoplay-timeout="3000">
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#business')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Business</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#fashion')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Fashion</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#education')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Education</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#entertainment')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Entertainment</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#festival')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Festival</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#art')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Art</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#info-tech')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Technology</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#branding')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Branding</small></p>
              </a>
            </div>


          </div><!-- end carousel -->
        </div><!-- end tab-pane -->

        <div id="price" class="tab-pane fade">
          <div id="location" class="tab-pane fade in active show">
            <div class="category-carousel owl-carousel owl-theme" data-items="8" data-margin="20"
              data-items-mobile-portrait="3" data-autoplay="true" data-loop="true" data-autoplay-timeout="3000">
              <div class="item text-center">
                <a href="#">
                  <svg viewBox="0 0 100 100" class="icon mb-3">
                    <use xlink:href="{{asset('svg/svg_cats.svg#business')}}"></use>
                  </svg>

                  <p class="cat-name"><small class="type-bold">Business</small></p>
                </a>
              </div>
              <div class="item text-center">
                <a href="#">
                  <svg viewBox="0 0 100 100" class="icon mb-3">
                    <use xlink:href="{{asset('svg/svg_cats.svg#fashion')}}"></use>
                  </svg>

                  <p class="cat-name"><small class="type-bold">Fashion</small></p>
                </a>
              </div>
              <div class="item text-center">
                <a href="#">
                  <svg viewBox="0 0 100 100" class="icon mb-3">
                    <use xlink:href="{{asset('svg/svg_cats.svg#education')}}"></use>
                  </svg>

                  <p class="cat-name"><small class="type-bold">Education</small></p>
                </a>
              </div>
              <div class="item text-center">
                <a href="#">
                  <svg viewBox="0 0 100 100" class="icon mb-3">
                    <use xlink:href="{{asset('svg/svg_cats.svg#entertainment')}}"></use>
                  </svg>

                  <p class="cat-name"><small class="type-bold">Entertainment</small></p>
                </a>
              </div>
              <div class="item text-center">
                <a href="#">
                  <svg viewBox="0 0 100 100" class="icon mb-3">
                    <use xlink:href="{{asset('svg/svg_cats.svg#festival')}}"></use>
                  </svg>

                  <p class="cat-name"><small class="type-bold">Festival</small></p>
                </a>
              </div>
              <div class="item text-center">
                <a href="#">
                  <svg viewBox="0 0 100 100" class="icon mb-3">
                    <use xlink:href="{{asset('svg/svg_cats.svg#art')}}"></use>
                  </svg>

                  <p class="cat-name"><small class="type-bold">Art</small></p>
                </a>
              </div>
              <div class="item text-center">
                <a href="#">
                  <svg viewBox="0 0 100 100" class="icon mb-3">
                    <use xlink:href="{{asset('svg/svg_cats.svg#info-tech')}}"></use>
                  </svg>

                  <p class="cat-name"><small class="type-bold">Technology</small></p>
                </a>
              </div>
              <div class="item text-center">
                <a href="#">
                  <svg viewBox="0 0 100 100" class="icon mb-3">
                    <use xlink:href="{{asset('svg/svg_cats.svg#branding')}}"></use>
                  </svg>

                  <p class="cat-name"><small class="type-bold">Branding</small></p>
                </a>
              </div>


            </div><!-- end carousel -->
          </div><!-- end tab-pane -->
        </div><!-- end tab-pane -->

      </div>
    </div>
  </section>

  <!-- featured -->
  <section class="featured-biz pb-0">
    <div class="container">

      <div class="row">
        <div class="col-md-9 col-lg-9 col-xl-9">
          <div class="row justify-content-end">
            <div class="col-7 col-md-9">
              <h5 class="type-bold mb-4">Featured Events Listings</h5>
            </div>
            <div class="col-5 col-md-3 text-right">
              <a href="" class="color-primary">View All Events <span class="color-dark ml-2"><i
                    class="fas fa-angle-right"></i></span></a>
            </div>
          </div>
          <div class="row equal-container">
            @forelse ($events as $event)

            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
              <a href="#">
                <div class="featured card">
                  <img src="{{asset('img/events/featured-1.jpg')}}" class="card-img-top" alt="Featured 1">
                  <div class="card-body">

                    <div class="equal">
                      <p class="card-title type-bold mb-1">Power China 2020</p>
                    </div>
                    <p class="type-bold type-uppercase color-lime">By Registration</p>

                    <div class="listing-indicators mb-2">
                      <div class="listing-address">
                        <i class="far fa-calendar color-primary"></i>
                        <span>Aug 16 - 18, 2020</span>
                      </div>
                      <div class="listing-address">
                        <i class="fas fa-briefcase color-primary"></i>
                        <span>Business</span>
                      </div>

                    </div>
                    <div class="listing-address">
                      <i class="fas fa-map-marker-alt color-primary"></i>
                      <span>China import & Export Fair Complex</span>
                    </div>
                  </div>
                </div>
              </a>
            </div>

            @empty

            <p>There are currently no events.</p>

            @endforelse



          </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xl-3 text-center">
          <p class="mb-4"><small>Advertisements</small></p>
          <div class="hidden-xs hidden-sm">
            <img src="{{asset('img/events/ads-4.jpg')}}" width="240" class="text-center mb-3">
            <img src="{{asset('img/events/ads-3.jpg')}}" width="240" class="text-center mb-3">
            <img src="{{asset('img/events/ads-2.jpg')}}" width="240" class="text-center mb-3">
          </div>
          <div class="hidden-lg hidden-md">
            <div class="medrec-carousel owl-carousel owl-theme" data-items-mobile-portrait="1" data-autoplay="true"
              data-autoplay-timeout="5000" data-loop="true">
              <div class="item">
                <img src="{{asset('img/events/ads-4.jpg')}}" width="240" class="text-center mb-3">
              </div>
              <div class="item">
                <img src="{{asset('img/events/ads-3.jpg')}}" width="240" class="text-center mb-3">
              </div>
              <div class="item">
                <img src="{{asset('img/events/ads-2.jpg')}}" width="240" class="text-center mb-3">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- cta -->
  <section>
    <div class="container">
      <!-- CTAs -->
      <div class="row">
        <!-- Get a quote -->
        <div class="col-md-7 col-lg-7 col-xl-7 mb-3 mb-lg-0 mb-xl-0">
          <div class="card home-card-ctas ">
            <div class="row no-gutters">


              <div class="col-md-4 col-lg-4 col-xl-4">
                <div class="imagebg h-100 second">
                  <div class="background-image-holder border-tl-radius border-bl-radius">
                    <img src="{{asset('img/events/home-1.jpg')}}" alt="">
                  </div>
                </div>
              </div><!-- end col -->

              <div class="col-md-8 col-lg-8 col-xl-8">
                <div class="card-body border-tr-radius border-br-radius bg-primary-3">
                  <h3 class="card-title">International events and <span class="next-line">current happenings </span>
                  </h3>
                  <p>Connect Nigeria Events brings you recently held events, current happenings and upcoming events
                    anywhere in Nigeria. Browse through events blogs as well!</p>
                  <a href="#" class="btn btn-primary-1">Read Event Blogs</a>
                </div>
              </div><!-- end col -->
            </div><!-- end row -->
          </div><!-- end card -->
        </div><!-- end quote cta -->

        <!-- Confirm Businesses -->
        <div class="col-md-5 col-lg-5 col-xl-5">
          <div class="card home-card-ctas h-100 ">
            <div class="imagebg image-light">
              <div class="background-image-holder border-radius-5">
                <img src="{{asset('img/events/home-2.jpg')}}" class="card-img">
              </div>
              <div class="card-img-overlay card-body">
                <div class="row">
                  <div class="col-11 col-lg-7">
                    <h3 class="card-title">Need an Audience?</h3>
                    <p class="card-text">Are you looking for an audience for your upcoming event? Let everybody know and
                      gather interested goers!</p>
                    <a href="#" class="btn btn-black">Post an Event</a>
                  </div>
                </div>
              </div>
            </div>

          </div><!-- end card -->
        </div><!-- end confirm cta -->
      </div><!-- end row -->
    </div>


  </section>

  <!-- Articles -->
  <section class="bg-secondary home-news">
    <div class="container ">
      <div class="row mb-4">
        <div class="col-6 col-md-6 col-lg-6 col-xl-6">
          <h5 class="type-bold">In the news</h5>
        </div>
        <div class="col-6 col-md-6 col-lg-6 col-xl-6 text-right">
          <a href="">Browse Articles <span class="color-dark ml-2"><i class="fas fa-angle-right"></i></span></a>
        </div>
      </div>
      <div class="">
        <div class="home-news-carousel owl-carousel owl-theme" data-items="4" data-margin="15"
          data-items-mobile-portrait="1" data-items-tablet-portrait="2" data-loop="true" data-autoplay="true"
          data-autoplay-timeout="3000">

          <div class="item" style="">
            <a href="#" class="">
              <div class="card card-post">
                <img src="{{asset('img/events/articles-1.jpg')}}" class="card-img-top" alt="">

                <div class="card-body">

                  <p class="card-text meta-posted">
                    <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Alex Obigdu</span></small>
                  </p>
                  <p class="card-title">Meet The Boss: Andrea Kamara, CEO Broad Street</p>
                </div>

              </div>
            </a>
          </div>
          <div class="item" style="">
            <a href="#" class="">
              <div class="card card-post">
                <img src="{{asset('img/events/articles-2.jpg')}}" class="card-img-top" alt="">

                <div class="card-body">

                  <p class="card-text meta-posted">
                    <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Uzo Ajawi</span></small>
                  </p>
                  <p class="card-title">All Grace Events Concept Limited Commences </p>
                </div>

              </div>
            </a>
          </div>
          <div class="item" style="">
            <a href="#" class="">
              <div class="card card-post">
                <img src="{{asset('img/events/articles-3.jpg')}}" class="card-img-top" alt="">

                <div class="card-body">

                  <p class="card-text meta-posted">
                    <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Kelly Abudhi</span></small>
                  </p>
                  <p class="card-title">Business Profile: WeMove- Hire Buses For Events And Outings </p>
                </div>

              </div>
            </a>
          </div>
          <div class="item" style="">
            <a href="#" class="">
              <div class="card card-post">
                <img src="{{asset('img/events/articles-4.jpg')}}" class="card-img-top" alt="">

                <div class="card-body">

                  <p class="card-text meta-posted">
                    <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Martin Andrews</span></small>
                  </p>
                  <p class="card-title">10 Tips To MC Nigerian<br> Events</p>
                </div>

              </div>
            </a>
          </div>

        </div>
      </div>
    </div>
  </section>
  <!-- end Articles section -->

  <section class="p-0">
    <div class="row no-gutters">
      <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="imagebg height-50 image-dark">
          <div class="background-image-holder">
            <img src="{{asset('img/cars/footer-banners-1.jpg')}}">
          </div>
          <div class="container pos-vertical-center h-100">
            <div class="cta-banner-footer">
              <div class="row w-100 mb-2">
                <div class="col-md-10 col-lg-10 col-xl-10">
                  <h2 class="title type-bold mb-2">Reach your target market faster </h2>
                  <p>with just <strong>N10,000</strong></p>
                </div>
              </div>
              <div class="d-flex mb-3">
                <div class="cta-stats mr-3">
                  <img src="{{asset('img/businesses/window-pointer.png')}}">
                  <div>
                    <p class="type-medium mb-0">250,000+</p>
                    <small>Daily Visitors</small>
                  </div>
                </div>
                <div class="cta-stats">
                  <img src="{{asset('img/businesses/briefcase.png')}}">
                  <div>
                    <p class="type-medium mb-0">500,000+</p>
                    <small>Business Listings</small>
                  </div>
                </div>
              </div>
              <div class="d-flex">
                <a href="#" class="btn btn-lg btn-primary-1 mr-4">
                  Advertise with us
                </a>
                <div class="">
                  <p class="mb-0">Call <span class="type-medium">0700-800-5000</span></p>
                  <small>grow@connectnigeria.com</small>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="imagebg height-50 image-dark">
          <div class="background-image-holder">
            <img src="{{asset('img/events/footer-banners-2.jpg')}}">
          </div>
          <div class="container pos-vertical-center h-100">
            <div class="cta-banner-footer-2 d-flex">
              <div class="row align-items-center w-100 mb-2">
                <div class="col-md-8 col-lg-8 col-xl-8">
                  <h2 class="title type-bold mb-4">Be the first to know what’s happening</h2>
                  <p class="mb-4">Sign up to our newsletter get interesting news and updates delivered right to your
                    inbox</p>
                  <a href="#" class="btn btn-lg btn-black mr-4">
                    Subscribe Now
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('includes.footer')
</div>
@endsection