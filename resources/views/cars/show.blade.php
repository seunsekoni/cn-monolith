@extends('layouts.app')

@section('title', 'Cars Details')

@section('styles')
<link href="{{ asset('css/theme-cars.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')

<div class="nav-container">
    <div>
        @include('includes.top-menu')
        @include('includes.cars-nav')
    </div>
</div>

<div class="main-container">

    <!-- Left Sidebar -->
    <section class="bg-secondary">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item"><a href="/listings">Cars</a></li>
                            <li class="breadcrumb-item"><a href="#">Toyota</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$listing->name}}</li>
                        </ol>
                    </nav>

                    <!-- LISTING DETAILS CARDS -->
                    <div class="row  mt-3">
                        <div class="col-md-12 mb-3">
                            <div class="bordered card">
                                <div class="card-body p0">
                                    <div class="row p40">
                                        <div class="col-md-8">
                                            <h4 class="type-bold mb-2">{{$listing->name}}
                                                <!-- <span class="badge bg-orange h6-size color-white type-normal">CONFIRMED</span> -->
                                            </h4>
                                            <h6 class="color-lime type-medium mb-3 mr-2">N
                                                {{number_format($listing->price, 2)}}</h6>

                                            <div class="row mb-3">
                                                <div class="col-md-12 mb-2 pl-3">
                                                    <div class="listing-address">
                                                        <i class="fas fa-map-marker-alt color-lime mr-2"></i>
                                                        @if (is_null($listing->business))
                                                        <span class="font-small">Coming soon</span>
                                                        @else
                                                        <span class="font-small">Lekki, Lagos</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="container">
                                                    <div class="row mb-2">

                                                        <div class="col-6 pl-3">
                                                            <div>
                                                                <small class="type-medium">Condition:</small>
                                                                <small>Used</small>
                                                            </div>
                                                            <div>
                                                                <small class="type-medium">Year:</small>
                                                                <small>2015</small>
                                                            </div>

                                                        </div>
                                                        <div class="col-6">
                                                            <div>
                                                                <small class="type-medium">Color:</small>
                                                                <small>Black</small>
                                                            </div>
                                                            <div>
                                                                <small class="type-medium">Odo:</small>
                                                                <small>89343 kms</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <a href="#" class="btn btn-cta btn-gray d-inline-block mb-0 type-uppercase">
                                                Suggest an edit
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="{{asset('img/cars/feat-3.jpg')}}" class="image-inherit mb-0">
                                            <a href="#" class="btn btn-open mb-0">
                                                FOR SALE
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row plr-40 bg-secondary-1">

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-share-square mr-1"></i> Share
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-star-half-alt mr-1"></i> Rate & Review
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-comments mr-1"></i> Contact Agent
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-thumbs-up mr-1"></i> Recommend
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-bookmark mr-1"></i> Save
                                            </a>

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row p40">

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="card hours">
                                                        <div class="card-header type-bold">
                                                            Agent Information
                                                        </div>
                                                        <ul class="list-group list-group-flush">
                                                            <li class="list-group-item bg-secondary-1 font-small">
                                                                <dl class="row mb-0">
                                                                    <dt class="col-sm-5 ">Name</dt>
                                                                    <dd class="col-sm-7 mb-0">Jande Autos</dd>
                                                                </dl>
                                                            </li>
                                                            <li class="list-group-item bg-secondary-1 font-small">
                                                                <dl class="row mb-0">
                                                                    <dt class="col-sm-5 ">Address</dt>
                                                                    <dd class="col-sm-7 mb-0">Janded Big Autos Lekki Epe
                                                                        expressway, Lekki, Lagos</dd>
                                                                </dl>
                                                            </li>
                                                            <li class="list-group-item bg-secondary-1 font-small">
                                                                <dl class="row mb-0">
                                                                    <dt class="col-sm-5" style="padding-right: 10px;">
                                                                        Contact No.</dt>
                                                                    <dd class="col-sm-7 mb-0">07062673336, 07062673336
                                                                    </dd>
                                                                </dl>
                                                            </li>
                                                            <li class="list-group-item bg-secondary-1 font-small">
                                                                <dl class="row mb-0">
                                                                    <dt class="col-sm-5 ">Email</dt>
                                                                    <dd class="col-sm-7 mb-0">jandedautos@gmail.com</dd>
                                                                </dl>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="col-md-7">
                                                    <iframe
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.546441382114!2d3.385522114494949!3d6.452222595332086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b8b0fb5756d71%3A0x7c6a0510ac25ea80!2sStallion%20Plaza!5e0!3m2!1sen!2sph!4v1597047213715!5m2!1sen!2sph"
                                                        width="450" height="270" frameborder="0"
                                                        style="border:1px solid #bfc3c9;" allowfullscreen=""
                                                        aria-hidden="false" tabindex="0"></iframe>
                                                </div>
                                            </div>

                                            <div class="row pt-4">


                                                <div class="col-md-8">
                                                    <h6 class="type-bold mb-4">Description</h6>

                                                    <p><span class="type-medium color-dark">Date Listed:</span>
                                                        September 09, 2019 <i>(about 12 months ago)</i></p>

                                                    {!!$listing->body!!}

                                                    <hr>

                                                    <div class="row ">
                                                        <div class="col-6 pl-3 mt-3">
                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Maker/Model</dt>
                                                                <dd class="col-sm-7 mb-0">Make Name: Toyota<br> Model
                                                                    Name: Corolla</dd>
                                                            </dl>

                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Model Year</dt>
                                                                <dd class="col-sm-7 mb-0">2015</dd>
                                                            </dl>

                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Transmission</dt>
                                                                <dd class="col-sm-7 mb-0">Automatic</dd>
                                                            </dl>

                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Body Style</dt>
                                                                <dd class="col-sm-7 mb-0">Others</dd>
                                                            </dl>

                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Condition</dt>
                                                                <dd class="col-sm-7 mb-0">Used</dd>
                                                            </dl>

                                                        </div>

                                                        <div class="col-6 mt-3">
                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Mileage</dt>
                                                                <dd class="col-sm-7 mb-0">89343 kms</dd>
                                                            </dl>

                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Color</dt>
                                                                <dd class="col-sm-7 mb-0">Black</dd>
                                                            </dl>

                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Interior</dt>
                                                                <dd class="col-sm-7 mb-0">Keyless Entry, Push To Start,
                                                                    Ac, Fabric , Foglamps</dd>
                                                            </dl>

                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Door Count</dt>
                                                                <dd class="col-sm-7 mb-0">4</dd>
                                                            </dl>
                                                        </div>

                                                        <div class="col-12 mt-3 ml-2">
                                                            <p>
                                                                <a class="" data-toggle="collapse"
                                                                    href="#collapseExample" role="button"
                                                                    aria-expanded="false"
                                                                    aria-controls="collapseExample">
                                                                    More Specifications <i
                                                                        class="fas fa-chevron-down ml-2"></i>
                                                                </a></p>

                                                            <div class="collapse" id="collapseExample">
                                                                <div class="row">
                                                                    <div class="col-6 pl-3 mt-3">
                                                                        <dl class="row mb-3">
                                                                            <dt class="col-sm-5">Trim</dt>
                                                                            <dd class="col-sm-7 mb-0">CE</dd>
                                                                        </dl>

                                                                        <dl class="row mb-3">
                                                                            <dt class="col-sm-5">Horsepower</dt>
                                                                            <dd class="col-sm-7 mb-0">84/6000</dd>
                                                                        </dl>

                                                                        <dl class="row mb-3">
                                                                            <dt class="col-sm-5">Drivetrain</dt>
                                                                            <dd class="col-sm-7 mb-0">Automatic</dd>
                                                                        </dl>

                                                                        <dl class="row mb-3">
                                                                            <dt class="col-sm-5">Fuel</dt>
                                                                            <dd class="col-sm-7 mb-0">Diesel</dd>
                                                                        </dl>

                                                                        <dl class="row mb-3">
                                                                            <dt class="col-sm-5">Engine</dt>
                                                                            <dd class="col-sm-7 mb-0">3 Cylinder
                                                                                In-line, 12 Valve</dd>
                                                                        </dl>

                                                                    </div>

                                                                    <div class="col-6 mt-3">
                                                                        <dl class="row mb-3">
                                                                            <dt class="col-sm-5">Drive Setup</dt>
                                                                            <dd class="col-sm-7 mb-0">89343 kms</dd>
                                                                        </dl>

                                                                        <dl class="row mb-3">
                                                                            <dt class="col-sm-5">Seats</dt>
                                                                            <dd class="col-sm-7 mb-0">5</dd>
                                                                        </dl>

                                                                        <dl class="row mb-3">
                                                                            <dt class="col-sm-5">Valid Vehicle Papers
                                                                            </dt>
                                                                            <dd class="col-sm-7 mb-0">Registration
                                                                                Papers</dd>
                                                                        </dl>

                                                                        <dl class="row mb-3">
                                                                            <dt class="col-sm-5">Additional Features
                                                                            </dt>
                                                                            <dd class="col-sm-7 mb-0">None</dd>
                                                                        </dl>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 ">
                                                    <h6 class="type-bold mb-4">Contact Agent</h6>
                                                    <form>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control"
                                                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                                                placeholder="Full Name">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control"
                                                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                                                placeholder="Contact Number">
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea class="form-control"
                                                                id="exampleFormControlTextarea1"
                                                                rows="3">I am interested in the 2015 Toyota Corolla LE</textarea>
                                                        </div>
                                                        <button type="submit"
                                                            class="btn btn-primary btn-sm btn-block">Submit</button>
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="row pt-4">
                                                    <h6 class="type-bold mb-4">Photo Gallery</h6>
                                                    <div class="gallery-carousel owl-carousel owl-theme" data-items="3"
                                                        data-margin="15" data-items-mobile-portrait="1"
                                                        data-autoplay="true" data-autoplay-timeout="2000"
                                                        data-loop="true">
                                                        <div class="item" style="">
                                                            <img src="{{asset('img/cars/feat-3.jpg')}}" alt="">
                                                        </div>
                                                        <div class="item" style="">
                                                            <img src="{{asset('img/cars/feat-3.jpg')}}" alt="">
                                                        </div>
                                                        <div class="item" style="">
                                                            <img src="{{asset('img/cars/feat-3.jpg')}}" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row pt-5">
                                                <div class="col-md-12">
                                                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link active" id="pills-home-tab"
                                                                data-toggle="pill" href="#pills-home" role="tab"
                                                                aria-controls="pills-home" aria-selected="true">Rates &
                                                                Reviews (5)</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="pills-profile-tab"
                                                                data-toggle="pill" href="#pills-profile" role="tab"
                                                                aria-controls="pills-profile"
                                                                aria-selected="false">Recommendations (2)</a>
                                                        </li>
                                                    </ul>

                                                    <div class="tab-content" id="pills-tabContent">
                                                        <div class="tab-pane fade show active" id="pills-home"
                                                            role="tabpanel" aria-labelledby="pills-home-tab">
                                                            @forelse ($listing->reviews as $review)
                                                            <div class="row mt-2">
                                                                <div class="col-md-2 text-center">
                                                                    <img src="{{asset('img/gravatar.png')}}"
                                                                        class="image-round">
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>

                                                                    <p><span class="type-bold color-dark">Uzo H.
                                                                            said</span> "Lorem ipsum dolor sit amet,
                                                                        consectetur adipiscing elit. Nam at tortor
                                                                        posuere, finibus tortor sed, accumsan magna. Nam
                                                                        magna tortor, iaculis ut odio eu, bibendum
                                                                        dignissim tellus. Proin fermentum, nibh
                                                                        condimentum laoreet tincidunt, magna tellus
                                                                        rhoncus mauris, sit amet scelerisque nisi nulla
                                                                        ut metus." </p>
                                                                </div>
                                                            </div>
                                                            @empty
                                                            <p>There are no reviews yet. Be the first to leave a review.
                                                            </p>
                                                            @endforelse
                                                        </div>

                                                        <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                                            aria-labelledby="pills-profile-tab">
                                                            <div class="col-md-12 text-center pt-3">
                                                                <img src="{{asset('img/gravatar.png')}}"
                                                                    class="image-round" data-toggle="tooltip"
                                                                    data-placement="bottom" title="Uzo H. (1)">
                                                                <img src="{{asset('img/gravatar-default.png')}}"
                                                                    class="image-round" data-toggle="tooltip"
                                                                    data-placement="bottom" title="Akeem F. (3)">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row plr-40 bg-secondary-1">

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-share-square mr-1"></i> Share
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-star-half-alt mr-1"></i> Rate & Review
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-comments mr-1"></i> Contact Agent
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-thumbs-up mr-1"></i> Recommend
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-bookmark mr-1"></i> Save
                                            </a>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END DETAILS CARDS -->

                    <!-- Articles -->
                    <div class="container">
                        <div class="row mb-4">
                            <div class="col-6 col-md-6 col-lg-6 col-xl-6">
                                <h class="type-bold">You might also like...</h>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
                                <a href="#">
                                    <div class="featured card bordered-1">
                                        <img src="{{asset('img/cars/feat-1.jpg')}}" class="card-img-top"
                                            alt="Featured 1">
                                        <div class="card-body">
                                            <p class="card-title type-bold mb-1">Lexus Rx325</p>
                                            <p class="price type-medium mb-2">N 1,450,000.00</p>

                                            <div class="listing-address mb-2">
                                                <i class="fas fa-map-marker-alt"></i>
                                                <span>Lekki, Lagos</span>
                                            </div>

                                            <div class="d-flex justify-content-between">
                                                <div>
                                                    <small class="type-medium">Condition:</small>
                                                    <small>Used</small>
                                                </div>
                                                <div>
                                                    <small class="type-medium">Color:</small>
                                                    <small>Black</small>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div>
                                                    <small class="type-medium">Year:</small>
                                                    <small>2015</small>
                                                </div>
                                                <div>
                                                    <small class="type-medium">Odo:</small>
                                                    <small>89343 kms</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
                                <a href="#">
                                    <div class="featured card bordered-1">
                                        <img src="{{asset('img/cars/feat-2.jpg')}}" class="card-img-top"
                                            alt="Featured 1">
                                        <div class="card-body">
                                            <p class="card-title type-bold mb-1">2011 Toyota Silver Camry</p>
                                            <p class="price type-medium mb-2">N 3,180,000.00</p>

                                            <div class="listing-address mb-2">
                                                <i class="fas fa-map-marker-alt"></i>
                                                <span>Lekki, Lagos</span>
                                            </div>

                                            <div class="d-flex justify-content-between">
                                                <div>
                                                    <small class="type-medium">Condition:</small>
                                                    <small>Used</small>
                                                </div>
                                                <div>
                                                    <small class="type-medium">Color:</small>
                                                    <small>Black</small>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div>
                                                    <small class="type-medium">Year:</small>
                                                    <small>2015</small>
                                                </div>
                                                <div>
                                                    <small class="type-medium">Odo:</small>
                                                    <small>89343 kms</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">

                                <a href="#">
                                    <div class="featured card bordered-1">
                                        <img src="{{asset('img/cars/feat-3.jpg')}}" class="card-img-top"
                                            alt="Featured 1">
                                        <div class="card-body">
                                            <p class="card-title type-bold mb-1">2007 Silver Toyota Corolla</p>
                                            <p class="price type-medium mb-2">N 2,280,000.00</p>

                                            <div class="listing-address mb-2">
                                                <i class="fas fa-map-marker-alt"></i>
                                                <span>36, Oyewole Road, Lagos</span>
                                            </div>

                                            <div class="d-flex justify-content-between">
                                                <div>
                                                    <small class="type-medium">Condition:</small>
                                                    <small>Used</small>
                                                </div>
                                                <div>
                                                    <small class="type-medium">Color:</small>
                                                    <small>Beige</small>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div>
                                                    <small class="type-medium">Year:</small>
                                                    <small>2015</small>
                                                </div>
                                                <div>
                                                    <small class="type-medium">Odo:</small>
                                                    <small>89343 kms</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- end Articles section -->

                    <div class="container mb-2 mt-2">
                        <img src="{{asset('img/businesses/ad-5.jpg')}}" class="image-inherit">
                    </div>

                </div>

                <!-- biz results right sidebar -->
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div class="bordered card">
                                <div class="card-body">
                                    <h6 class="color-primary-1 type-bold">Featured Businesses</h6>

                                    <ul class="arrow mt-3">
                                        <li><a href="" class="color-dark">Body Retreat Spa</a></li>
                                        <li><a href="" class="color-dark">The Nigeria Incentive-Based Risk Sharing
                                                System For Agriculture</a></li>
                                        <li><a href="" class="color-dark">Forex Time Nigeria (Lagos)</a></li>
                                        <li><a href="" class="color-dark">Union Bank Nigeria (Headquarter)</a></li>
                                        <li><a href="" class="color-dark">Sigma Pensions Limited (Abuja)</a></li>
                                        <li><a href="" class="color-dark">Platform Branding Company</a></li>
                                        <li><a href="" class="color-dark">MGS Boriatop</a></li>
                                        <li><a href="" class="color-dark">Xpressteachers.Com</a></li>
                                        <li><a href="" class="color-dark">Rack Centre</a></li>
                                        <li><a href="" class="color-dark">Isura Jewelry</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="container">
                            <img src="{{asset('img/businesses/ad-1.jpg')}}" class="image-inherit mb-3">
                            <img src="{{asset('img/businesses/ad-2.jpg')}}" class="image-inherit mb-3">
                            <img src="{{asset('img/businesses/ad-3.jpg')}}" class="image-inherit mb-3">
                            <img src="{{asset('img/businesses/ad-4.jpg')}}" class="image-inherit mb-3">
                        </div>
                    </div>

                </div>
                <!-- end biz results right sidebar -->
            </div>
        </div>
    </section>
    <!-- end Articles section -->

    <!-- FOOTER CTA -->
    @include('includes.footer-cta')
    <!-- END FOOTER CTA -->

    <!-- FOOTER -->
    @include('includes.footer')
    <!-- FOOTER -->
</div>
@endsection