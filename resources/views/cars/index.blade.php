@extends('layouts.app')

@section('title', 'Cars')

@section('styles')
<link href="{{ asset('css/theme-cars.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')

<div class="nav-container">
  <div>
    @include('includes.top-menu')
    @include('includes.cars-nav')
  </div>
</div>

<div class="main-container">
  <!-- HERO BANNER -->
  <section class="imagebg height-40">
    <div class="background-image-holder">
      <img src="{{asset('img/cars/hero-banner-1.jpg')}}" alt="CN Business" />
    </div>
    <div class="container pos-vertical-center">
      <div class="row align-items-center">
        <div class="col-md-12 col-lg-9 col-xl-9">
          <form class="search-bar mb-2" action="">
            <div class="row">
              <div class="col-5 col-md-3 mb-2 mb-md-0 mb-lg-0 mb-xl-0">
                <div class="input-select">
                  <select class="form-control form-control-lg cars-search-dropdown" name="condition">
                    <option value="newUsed">New & Used</option>
                    <option value="brandNew">Brand New</option>
                  </select>
                </div>
              </div>
              <div class="col-7 col-md-4 mb-2 mb-md-0 mb-lg-0 mb-xl-0">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">Search</span>
                  </div>
                  <input name="search" type="text" class="form-control" placeholder="Make or Model">
                </div>
              </div>
              <div class="col-9 col-md-4 pr-xs-0">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">In</span>
                  </div>
                  <input name="in" type="text" class="form-control" placeholder="Lagos, Nigeria">
                </div>
              </div>
              <div class="col-3 col-md-1 pl-xs-0">
                <button type="submit" class="btn btn-primary h-100"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </form>

          <div class="popular-searches">
            <span class="color-primary">
              <small class="type-bold">Popular:</small>
            </span>
            <span>
              <small class="color-white">Home Repair, Beauty Salon, Delivery Services, Electrician</small>
            </span>
          </div>
        </div>
        <div class="col-lg-3 col-xl-3 hidden-xs hidden-sm">
          <p class="mb-0"><small>Advertisements</small></p>
          <div class="medrec-carousel owl-carousel owl-theme" data-items="1" data-items-mobile-portrait="1"
            data-autoplay="true" data-autoplay-timeout="5000" data-loop="true">
            <div class="item">
              <img src="{{asset('img/cars/advert-4.jpg')}}" width="240" class="text-center mb-3">
            </div>
            <div class="item">
              <img src="{{asset('img/cars/advert-2.jpg')}}" width="240" class="text-center mb-3">
            </div>
            <div class="item">
              <img src="{{asset('img/cars/advert-3.jpg')}}" width="240" class="text-center mb-3">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END HERO BANNER -->

  <!-- CATEGORIES -->
  <section class="pb-0">
    <div class="container">

      <div class="row">
        <div class="col-12">
          <ul class="nav nav-tabs browse">
            <li><span class="type-bold">Browse by</span></li>
            <li><a data-toggle="tab" href="#make">Make</a></li>
            <li><a data-toggle="tab" href="#body-style" class="active">Body Style</a></li>
            <li><a data-toggle="tab" href="#price">Price</a></li>

          </ul>

          <div class="tab-content">
            <div id="make" class="tab-pane fade in active show">
              <div class="category-carousel owl-carousel owl-theme" data-items="8" data-margin="20"
                data-items-mobile-portrait="3" data-autoplay="true" data-loop="true" data-autoplay-timeout="3000">
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-1.jpg')}}">

                    <p class="cat-name"><small class="type-bold">MPV</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-2.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Convertible</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-3.jpg')}}">

                    <p class="cat-name"><small class="type-bold">SUV</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-4.jpg')}}">
                    <p class="cat-name"><small class="type-bold">Estate</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-5.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Coupe</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-6.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Hatchback</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-7.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Truck</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-8.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Van</small></p>
                  </a>
                </div>

              </div><!-- end carousel -->
            </div><!-- end tab-pane -->


            <div id="body-style" class="tab-pane fade">
              <div class="category-carousel owl-carousel owl-theme" data-items="8" data-margin="20"
                data-items-mobile-portrait="3" data-autoplay="true" data-loop="true" data-autoplay-timeout="3000">
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-1.jpg')}}">

                    <p class="cat-name"><small class="type-bold">MPV</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-2.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Convertible</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-3.jpg')}}">

                    <p class="cat-name"><small class="type-bold">SUV</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-4.jpg')}}">
                    <p class="cat-name"><small class="type-bold">Estate</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-5.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Coupe</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-6.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Hatchback</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-7.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Truck</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-8.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Van</small></p>
                  </a>
                </div>

              </div><!-- end carousel -->
            </div><!-- end tab-pane -->

            <div id="price" class="tab-pane fade">
              <div class="category-carousel owl-carousel owl-theme" data-items="8" data-margin="20"
                data-items-mobile-portrait="3" data-autoplay="true" data-loop="true" data-autoplay-timeout="3000">
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-1.jpg')}}">

                    <p class="cat-name"><small class="type-bold">MPV</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-2.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Convertible</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-3.jpg')}}">

                    <p class="cat-name"><small class="type-bold">SUV</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-4.jpg')}}">
                    <p class="cat-name"><small class="type-bold">Estate</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-5.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Coupe</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-6.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Hatchback</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-7.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Truck</small></p>
                  </a>
                </div>
                <div class="item text-center">
                  <a href="#">
                    <img src="{{asset('img/cars/cat-8.jpg')}}">

                    <p class="cat-name"><small class="type-bold">Van</small></p>
                  </a>
                </div>

              </div><!-- end carousel -->
            </div><!-- end tab-pane -->

          </div>
        </div>
      </div>

    </div><!-- end container -->
  </section>
  <!-- END CATEGORIES -->

  <!-- FEAT CARS -->
  <section class="featured-biz pb-0">
    <div class="container">

      <div class="row">
        <div class="col-md-9 col-lg-9 col-xl-9">
          <div class="row justify-content-end">
            <div class="col-7 col-md-9">
              <h5 class="type-bold mb-4">Featured Cars Listings</h5>
            </div>
            <div class="col-5 col-md-3 text-right">
              <a href="">View All Cars <span class="color-dark ml-2"><i class="fas fa-angle-right"></i></span></a>
            </div>
          </div>
          <div class="row">

            @forelse ($listings as $listing)
            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
              <a href="{{ route('cars.show', $listing->id) }}">
                <div class="featured card">
                  <img src="{{asset('img/cars/feat-1.jpg')}}" class="card-img-top" alt="Featured 1">
                  <div class="card-body">
                    <p class="card-title type-bold mb-1">{{ ucfirst($listing->name) }}</p>
                    <p class="price type-medium mb-2">N {{ number_format($listing->price, 2) }}</p>

                    <div class="listing-address mb-2">
                      <i class="fas fa-map-marker-alt"></i>
                      @if (is_null($listing->business))
                      <span>Coming soon</span>
                      @else
                      <span>{{$listing->business->businessLocation[0]->address_landmark}}</span>
                      @endif
                    </div>

                    <div class="d-flex justify-content-between">
                      <div>
                        <small class="type-medium">Condition:</small>
                        <small>Used</small>
                      </div>
                      <div>
                        <small class="type-medium">Color:</small>
                        <small>Black</small>
                      </div>
                    </div>
                    <div class="d-flex justify-content-between">
                      <div>
                        <small class="type-medium">Year:</small>
                        <small>2015</small>
                      </div>
                      <div>
                        <small class="type-medium">Odo:</small>
                        <small>89343 kms</small>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>

            @empty
            <p>There are currently no cars.</p>
            @endforelse
          </div>
        </div>

        <div class="col-md-3 col-lg-3 col-xl-3 text-center">
          <p class="mb-4"><small>Advertisements</small></p>
          <div class="hidden-xs hidden-sm">
            <img src="{{asset('img/cars/advert-1.jpg')}}" width="240" class="text-center mb-3">
            <img src="{{asset('img/cars/advert-2.jpg')}}" width="240" class="text-center mb-3">
            <img src="{{asset('img/cars/advert-3.jpg')}}" width="240" class="text-center mb-3">
          </div>
          <div class="hidden-lg hidden-md">
            <div class="medrec-carousel owl-carousel owl-theme" data-items-mobile-portrait="1" data-autoplay="true"
              data-autoplay-timeout="5000" data-loop="true">
              <div class="item">
                <img src="{{asset('img/cars/advert-1.jpg')}}" width="240" class="text-center mb-3">
              </div>
              <div class="item">
                <img src="{{asset('img/cars/advert-2.jpg')}}" width="240" class="text-center mb-3">
              </div>
              <div class="item">
                <img src="{{asset('img/cars/advert-3.jpg')}}" width="240" class="text-center mb-3">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END of FEATURED CARS -->

  <section>
    <div class="container">
      <!-- CTAs -->
      <div class="row">
        <!-- Get a quote -->
        <div class="col-md-7 col-lg-7 col-xl-7 mb-3 mb-lg-0 mb-xl-0">
          <div class="card home-card-ctas ">
            <div class="row no-gutters">


              <div class="col-md-4 col-lg-4 col-xl-4">
                <div class="imagebg h-100 second">
                  <div class="background-image-holder border-tl-radius border-bl-radius">
                    <img src="{{asset('img/cars/home-1.jpg')}}" alt="">
                  </div>
                </div>
              </div><!-- end col -->

              <div class="col-md-8 col-lg-8 col-xl-8">
                <div class="card-body border-tr-radius border-br-radius bg-dark">
                  <h3 class="card-title">Car news and expert <span class="next-line">reviews </span></h3>
                  <p>Connect Nigeria cars brings you car news, expert reviews in the automotive industries, car
                    maintenance tips and buying advice from our experts.</p>
                  <a href="#" class="btn btn-orange">Read Car News</a>
                </div>
              </div><!-- end col -->
            </div><!-- end row -->
          </div><!-- end card -->
        </div><!-- end quote cta -->

        <!-- Confirm Businesses -->
        <div class="col-md-5 col-lg-5 col-xl-5">
          <div class="card home-card-ctas h-100 ">
            <div class="imagebg image-dark">
              <div class="background-image-holder border-radius-5">
                <img src="{{asset('img/cars/home-2.jpg')}}" class="card-img">
              </div>
              <div class="card-img-overlay card-body">
                <div class="row">
                  <div class="col-11 col-lg-7">
                    <h3 class="card-title">Looking for car repair services</h3>
                    <p class="card-text">Whether you need a quick oil change or a complete exhaust system overhaul, our
                      experts are ready to help you.</p>
                    <a href="#" class="btn btn-primary-1">Browse Services</a>
                  </div>
                </div>
              </div>
            </div>

          </div><!-- end card -->
        </div><!-- end confirm cta -->
      </div><!-- end row -->
    </div>
  </section>

  <!-- Articles -->
  <section class="bg-secondary home-news">
    <div class="container">
      <div class="row mb-4">
        <div class="col-6 col-md-6 col-lg-6 col-xl-6">
          <h5 class="type-bold">In the news</h5>
        </div>
        <div class="col-6 col-md-6 col-lg-6 col-xl-6 text-right">
          <a href="">Browse Articles <span class="color-dark ml-2"><i class="fas fa-angle-right"></i></span></a>
        </div>
      </div>
      <div class="home-news-carousel owl-carousel owl-theme" data-items="4" data-margin="15"
        data-items-mobile-portrait="1" data-items-tablet-portrait="2" data-loop="true" data-autoplay="true"
        data-autoplay-timeout="3000">

        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/cars/article-1.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Alex Obigdu</span></small>
                </p>
                <p class="card-title">2021 Toyota Supra Review: More, and Less, Power Changes</p>
              </div>
            </div>
          </a>
        </div>
        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/cars/article-2.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">19 Jul 2020 | <span class="meta-cat">Uzo Ajawi</span></small>
                </p>
                <p class="card-title">2020 Mitsubishi Outlander Sport Review: More Misses Than Hits</p>
              </div>
            </div>
          </a>
        </div>
        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/cars/article-3.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Martin Andrews</span></small>
                </p>
                <p class="card-title">Tesla chooses Austin site for its $1.1B Cybertruck factory</p>
              </div>
            </div>
          </a>
        </div>
        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/cars/article-4.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">18 Jul 2020 | <span class="meta-cat">Kelly Abudhi</span></small>
                </p>
                <p class="card-title">German prosecutors search offices in Fiat Chrysler, Iveco probe</p>
              </div>
            </div>
          </a>
        </div>

      </div>
    </div>
  </section>
  <!-- end Articles section -->

  <!-- FOOTER CTA -->
  @include('includes.footer-cta')
  <!-- END FOOTER CTA -->

  <!-- FOOTER -->
  @include('includes.footer')
  <!-- FOOTER -->
</div>
@endsection