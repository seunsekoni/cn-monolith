@extends('layouts.app')

@section('title', 'Cars Listings')

@section('styles')
<link href="{{ asset('css/theme-cars.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')

<div class="nav-container">
    <div>
        @include('includes.top-menu')
        @include('includes.cars-nav')
    </div>
</div>

<div class="main-container">

    <!-- Left Sidebar -->
    <section class="bg-secondary">
        <div class="container">
            <div class="row">

                <div class="col-12 col-md-2">
                    <div class="hidden-xs hidden-sm">
                        <p class="type-bold">Related Categories</p>
                        <a href="#" class="btn-sm btn-light d-inline-block mb-2">
                            Banks & Credits Union
                        </a>
                        <a href="#" class="btn-sm btn-light d-inline-block mb-2">
                            Financial Services
                        </a>
                        <a href="#" class="btn-sm btn-light d-inline-block mb-2">
                            Mortgage Brokers
                        </a>
                        <a href="#" class="btn-sm btn-light d-inline-block mb-2">
                            Financial Advicing
                        </a>
                        <a href="#" class="d-block mb-4">
                            View all
                        </a>

                        <hr>

                        <p class="type-bold mt-4">Distance</p>

                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1"
                                value="option1" checked>
                            <!-- <span class="checkmark" for="exampleRadios1"></span> -->
                            <label class="form-check-label" for="exampleRadios1">
                                Within 1 km
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2"
                                value="option1">
                            <!-- <span class="checkmark"></span> -->
                            <label class="form-check-label" for="exampleRadios2">
                                Within 10 km
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3"
                                value="option1">
                            <!-- <span class="checkmark"></span> -->
                            <label class="form-check-label" for="exampleRadios3">
                                Within 25 km
                            </label>
                        </div>
                        <div class="form-check mb-4">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios4"
                                value="option1">
                            <!-- <span class="checkmark"></span> -->
                            <label class="form-check-label" for="exampleRadios4">
                                Within 50 km
                            </label>
                        </div>

                        <hr>

                        <p class="type-bold mt-4">Location</p>

                        <div class="form-group mb-2">
                            <select class="form-control input-field" id="exampleFormControlSelect1">
                                <option>All States</option>
                                <option>2</option>
                            </select>
                        </div>

                        <div class="form-group mb-2">
                            <select class="form-control input-field" id="exampleFormControlSelect1">
                                <option>All LGA</option>
                                <option>2</option>
                            </select>
                        </div>

                        <div class="form-group mb-2">
                            <select class="form-control input-field" id="exampleFormControlSelect1">
                                <option>All Cities</option>
                                <option>2</option>
                            </select>
                        </div>

                    </div>


                </div>


                <div class="col-md-7">
                    <h6 class="color-gray mb-3"> There are <span class="color-dark type-bold">62</span> listings found
                        for <span class="color-dark type-bold">'Toyota Corolla'</span>. You can browse by category
                        below.</h6>


                    <div class="container mb-lg-4 mb-md-4 mb-5">
                        <img src="{{asset('img/businesses/logos-ad.jpg')}}" class="image-inherit">
                    </div>

                    <div class="visible-xs visible-sm">
                        <button class="callout filter-collapse" type="button" data-toggle="collapse"
                            data-target="#filter-search" aria-expanded="false" aria-controls="filter-search"><i
                                class="fas fa-filter"></i> Filter Search</button>
                    </div>
                    <div class="collapse" id="filter-search">
                        <div class="card card-body">
                            <p class="type-bold">Related Categories</p>
                            <a href="#" class="btn-sm btn-light d-inline-block mb-2">
                                Banks & Credits Union
                            </a>
                            <a href="#" class="btn-sm btn-light d-inline-block mb-2">
                                Financial Services
                            </a>
                            <a href="#" class="btn-sm btn-light d-inline-block mb-2">
                                Mortgage Brokers
                            </a>
                            <a href="#" class="btn-sm btn-light d-inline-block mb-2">
                                Financial Advicing
                            </a>
                            <a href="#" class="d-block mb-4">
                                View all
                            </a>

                            <hr>

                            <p class="type-bold mt-4">Distance</p>

                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1"
                                    value="option1" checked>
                                <!-- <span class="checkmark" for="exampleRadios1"></span> -->
                                <label class="form-check-label" for="exampleRadios1">
                                    Within 1 km
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2"
                                    value="option1">
                                <!-- <span class="checkmark"></span> -->
                                <label class="form-check-label" for="exampleRadios2">
                                    Within 10 km
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3"
                                    value="option1">
                                <!-- <span class="checkmark"></span> -->
                                <label class="form-check-label" for="exampleRadios3">
                                    Within 25 km
                                </label>
                            </div>
                            <div class="form-check mb-4">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios4"
                                    value="option1">
                                <!-- <span class="checkmark"></span> -->
                                <label class="form-check-label" for="exampleRadios4">
                                    Within 50 km
                                </label>
                            </div>

                            <hr>

                            <p class="type-bold mt-4">Location</p>

                            <div class="form-group mb-2">
                                <select class="form-control input-field" id="exampleFormControlSelect1">
                                    <option>All States</option>
                                    <option>2</option>
                                </select>
                            </div>

                            <div class="form-group mb-2">
                                <select class="form-control input-field" id="exampleFormControlSelect1">
                                    <option>All LGA</option>
                                    <option>2</option>
                                </select>
                            </div>

                            <div class="form-group mb-2">
                                <select class="form-control input-field" id="exampleFormControlSelect1">
                                    <option>All Cities</option>
                                    <option>2</option>
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="row align-items-center justify-content-end mt-lg-0 mt-md-0 mt-3">
                        <div class="col-6">
                            <small class="type-bold">64 Toyota Corolla in Lagos, Nigeria</small>
                        </div>
                        <div class="col-6 text-right">
                            <div class="sortby">
                                <small class="color-gray">Sort by</small>
                                <select class="no-border-select input-field" id="exampleFormControlSelect1">
                                    <option>Recommended</option>
                                    <option>2</option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="float-right d-inline-block">

                    </div>

                    <div class="row  mt-3">
                        <div class="col-md-12 mb-3">
                            <div class="bordered card results-card-horizontal">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-5 mb-lg-0 mb-md-0 mb-2">
                                            <img src="{{asset('img/cars/feat-3.jpg')}}" class="image-inherit mb-0">

                                        </div>
                                        <div class="col-md-7">
                                            <h5 class="type-bold title mb-2">
                                                <span class="align-middle">2015 Toyota Corolla LE</span> </h5>
                                            <div class="d-flex ">
                                                <p class="price type-medium mb-2 mr-2">N 1,450,000.00</p>
                                                <span><small>Lekki, Lagos</small></span>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-6">
                                                    <div>
                                                        <small class="type-medium">Condition:</small>
                                                        <small>Used</small>
                                                    </div>
                                                    <div>
                                                        <small class="type-medium">Year:</small>
                                                        <small>2015</small>
                                                    </div>

                                                </div>
                                                <div class="col-6">
                                                    <div>
                                                        <small class="type-medium">Color:</small>
                                                        <small>Black</small>
                                                    </div>
                                                    <div>
                                                        <small class="type-medium">Odo:</small>
                                                        <small>89343 kms</small>
                                                    </div>
                                                </div>
                                            </div>

                                            <a href="#"
                                                class="btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2">
                                                <i class="fas fa-car mr-2"></i> View Car
                                            </a>

                                            <a href="#"
                                                class="btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2">
                                                <i class="fas fa-comments mr-2"></i> Contact Owner
                                            </a>
                                            <a href="#"
                                                class="btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2">
                                                <i class="fas fa-save mr-2"></i> Save
                                            </a>


                                        </div>



                                    </div>
                                </div>
                            </div>

                            <div class="bordered card results-card-horizontal">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-5 mb-lg-0 mb-md-0 mb-2">
                                            <img src="{{asset('img/cars/feat-2.jpg')}}" class="image-inherit mb-0">

                                        </div>
                                        <div class="col-md-7">
                                            <h5 class="type-bold title mb-2">
                                                <span class="align-middle">2011 Toyota Silver Camry</span> </h5>
                                            <div class="d-flex ">
                                                <p class="price type-medium mb-2 mr-2">N 3,180,000.00</p>
                                                <span><small>Lekki, Lagos</small></span>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-6">
                                                    <div>
                                                        <small class="type-medium">Condition:</small>
                                                        <small>Used</small>
                                                    </div>
                                                    <div>
                                                        <small class="type-medium">Year:</small>
                                                        <small>2015</small>
                                                    </div>

                                                </div>
                                                <div class="col-6">
                                                    <div>
                                                        <small class="type-medium">Color:</small>
                                                        <small>Black</small>
                                                    </div>
                                                    <div>
                                                        <small class="type-medium">Odo:</small>
                                                        <small>89343 kms</small>
                                                    </div>
                                                </div>
                                            </div>

                                            <a href="#"
                                                class="btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2">
                                                <i class="fas fa-car mr-2"></i> View Car
                                            </a>

                                            <a href="#"
                                                class="btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2">
                                                <i class="fas fa-comments mr-2"></i> Contact Owner
                                            </a>
                                            <a href="#"
                                                class="btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2">
                                                <i class="fas fa-save mr-2"></i> Save
                                            </a>


                                        </div>



                                    </div>
                                </div>
                            </div>

                            <div class="bordered card results-card-horizontal">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-5 mb-lg-0 mb-md-0 mb-2">
                                            <img src="{{asset('img/cars/feat-4.jpg')}}" class="image-inherit mb-0">

                                        </div>
                                        <div class="col-md-7">
                                            <h5 class="type-bold title mb-2">
                                                <span class="align-middle">Mercedes Benz S550 (S-Class)</span> </h5>
                                            <div class="d-flex ">
                                                <p class="price type-medium mb-2 mr-2">N 6,550,000.00</p>
                                                <span><small>Lekki, Lagos</small></span>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-6">
                                                    <div>
                                                        <small class="type-medium">Condition:</small>
                                                        <small>Used</small>
                                                    </div>
                                                    <div>
                                                        <small class="type-medium">Year:</small>
                                                        <small>2015</small>
                                                    </div>

                                                </div>
                                                <div class="col-6">
                                                    <div>
                                                        <small class="type-medium">Color:</small>
                                                        <small>Black</small>
                                                    </div>
                                                    <div>
                                                        <small class="type-medium">Odo:</small>
                                                        <small>89343 kms</small>
                                                    </div>
                                                </div>
                                            </div>

                                            <a href="#"
                                                class="btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2">
                                                <i class="fas fa-car mr-2"></i> View Car
                                            </a>

                                            <a href="#"
                                                class="btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2">
                                                <i class="fas fa-comments mr-2"></i> Contact Owner
                                            </a>
                                            <a href="#"
                                                class="btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2">
                                                <i class="fas fa-save mr-2"></i> Save
                                            </a>


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="bordered card results-card-horizontal">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-5 mb-lg-0 mb-md-0 mb-2">
                                            <img src="{{asset('img/cars/feat-5.jpg')}}" class="image-inherit mb-0">

                                        </div>
                                        <div class="col-md-7">
                                            <h5 class="type-bold title mb-2">
                                                <span class="align-middle">Honda CR-V</span> </h5>
                                            <div class="d-flex ">
                                                <p class="price type-medium mb-2 mr-2">N 2,300,000.00</p>
                                                <span><small>Lekki, Lagos</small></span>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-6">
                                                    <div>
                                                        <small class="type-medium">Condition:</small>
                                                        <small>Used</small>
                                                    </div>
                                                    <div>
                                                        <small class="type-medium">Year:</small>
                                                        <small>2015</small>
                                                    </div>

                                                </div>
                                                <div class="col-6">
                                                    <div>
                                                        <small class="type-medium">Color:</small>
                                                        <small>Black</small>
                                                    </div>
                                                    <div>
                                                        <small class="type-medium">Odo:</small>
                                                        <small>89343 kms</small>
                                                    </div>
                                                </div>
                                            </div>

                                            <a href="#"
                                                class="btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2">
                                                <i class="fas fa-car mr-2"></i> View Car
                                            </a>

                                            <a href="#"
                                                class="btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2">
                                                <i class="fas fa-comments mr-2"></i> Contact Owner
                                            </a>
                                            <a href="#"
                                                class="btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2">
                                                <i class="fas fa-save mr-2"></i> Save
                                            </a>


                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>


                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div class="bordered card">
                                <div class="card-body">
                                    <h6 class="color-primary-1 type-bold">Featured Businesses</h6>

                                    <ul class="arrow mt-3">
                                        <li><a href="" class="color-dark">Body Retreat Spa</a></li>
                                        <li><a href="" class="color-dark">The Nigeria Incentive-Based Risk Sharing
                                                System For Agriculture</a></li>
                                        <li><a href="" class="color-dark">Forex Time Nigeria (Lagos)</a></li>
                                        <li><a href="" class="color-dark">Union Bank Nigeria (Headquarter)</a></li>
                                        <li><a href="" class="color-dark">Sigma Pensions Limited (Abuja)</a></li>
                                        <li><a href="" class="color-dark">Platform Branding Company</a></li>
                                        <li><a href="" class="color-dark">MGS Boriatop</a></li>
                                        <li><a href="" class="color-dark">Xpressteachers.Com</a></li>
                                        <li><a href="" class="color-dark">Rack Centre</a></li>
                                        <li><a href="" class="color-dark">Isura Jewelry</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="container">
                            <img src="{{asset('img/businesses/ad-1.jpg')}}" class="image-inherit mb-3">
                            <img src="{{asset('img/businesses/ad-2.jpg')}}" class="image-inherit mb-3">
                            <img src="{{asset('img/businesses/ad-3.jpg')}}" class="image-inherit mb-3">
                            <img src="{{asset('img/businesses/ad-4.jpg')}}" class="image-inherit mb-3">
                        </div>
                    </div>

                </div>


            </div>

        </div>
    </section>
    <!-- end Articles section -->

    <!-- FOOTER CTA -->
    @include('includes.footer-cta')
    <!-- END FOOTER CTA -->

    <!-- FOOTER -->
    @include('includes.footer')
    <!-- FOOTER -->
</div>
@endsection