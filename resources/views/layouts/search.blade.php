<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }} | @yield('title')</title>
        <meta name="description" content="@yield('meta_description', config('app.name'))">

        <!-- Scripts -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all">
        <link href="{{ asset('vendor/fontawesome/css/all.min.css') }}" rel="stylesheet">
        <link href="{{asset('css/owl.carousel.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/theme-global.css')}}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">

        @stack('styles')
    </head>

    <body>
        <!-- navbar -->
        <div class="nav-container">
            <div>
                @include('includes.top-menu')
                <nav class="navbar navbar-expand-lg sticky-top" id="main-menu">
                    <div class="container">
                        <div class="row align-items-center w-100 flex-1">
                            <div class="col-12 col-lg-2 col-xl-2 d-flex justify-content-between">
                                <a class="navbar-brand" href="{{ route('site.index') }}">
                                    <img src="{{ asset('img/cn-logo-dark.png') }}" class="logo logo-cn" alt="CN Logo">
                                </a>
                                <button class="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                    <i class="fas fa-bars"></i>
                                </button>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-6 p-0 pr-lg-10 pl-lg-10">
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav">
                                        <li class="nav-item active">
                                            <a class="nav-link" href="{{ route('businesses.index') }}">
                                                Browse Businesses
                                                <span class="sr-only">(current)</span>
                                            </a>
                                        </li>
                                        @include('components.top-bar-menu')
                                        <a href="#"
                                            class="nav-mobile-btn bg-primary-1 mt-4 text-center hidden-lg hidden-md">Sign
                                            In</a>
                                        <a href="#"
                                            class="nav-mobile-btn bg-primary text-center hidden-lg hidden-md">List
                                            Business</a>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4 text-right text-center-xs hidden-xs hidden-sm">
                                <ul class="navbar-nav justify-content-end">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#">Sign In</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link btn btn-sm btn-primary" href="#">List Business</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </nav>
            </div>
        </div>

        <main class="main-container">
            @yield('content')
        </main>

        <!-- footer -->
        @include('includes.footer')

        @stack('before-scripts')
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('js/owl.carousel.js') }}"></script>
        <script src="{{ asset('js/flickity.min.js') }}" defer></script>
        <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
        @stack('after-scripts')
    </body>

</html>
