<div class="collapse" id="reply{{ $reply->parent_id ?? $reply->id}}">
    <input type="hidden" class="parent_id" name="parent_id" value="{{ $reply->id }}">
    <div class="comment-replies">
        <div class="comment-reply pl-4">
            <div class="d-flex justify-content-between align-items-center">
                <div class="user d-flex flex-row align-items-center"> 
                    <img src="{{asset('img/placeholders/avatar.jpg')}}" width="30" class="user-img rounded-circle mr-2"> 
                    <span>
                        <small class="font-weight-bold text-primary commenter">{{ $reply->user->first_name ?? '' }}</small> - 
                        <small class="font-weight-bold">{{ $reply->body }}</small>
                    </span> 
                </div> 
                <small>{{ $reply->created_at->diffForHumans() }}</small>
            </div>
            <div class="action d-flex justify-content-between mt-2 align-items-center">
                <div class="reply tail px-4">
                    <small>Reply</small>
                </div>
            </div>
            @each('listings.includes.replies', $reply->replies, 'reply')
        </div>
    </div>
</div>