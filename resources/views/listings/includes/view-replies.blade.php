
@if($reply->replies->count())
<div class="view-replies text-primary font-weight-bold px-4 py-3" data-toggle="collapse" data-target="#reply{{$reply->id }}" aria-expanded="false" aria-controls="reply{{ $reply->id }}">
    <i class="fa fa-caret-down"></i> View {{ \Illuminate\Support\Str::of('reply')->plural($reply->replies->count()) }} ({{ $reply->replies->count() }})
</div>
@endif
<div class="collapse" id="reply{{ $reply->id }}">
    <!-- This hidden input feeds the reply form with the parent id of its comment. Tread carefully -->
    <input type="hidden" class="parent_id" name="parent_id" value="{{ $reply->parent_id }}">
    <div class="comment-replies">
        <div class="comment-reply pl-4">
            <div class="d-flex justify-content-between align-items-center">
                <div class="user d-flex flex-row align-items-center"> 
                    <img src="{{asset('img/placeholders/avatar.jpg')}}" width="30" class="user-img rounded-circle mr-2"> 
                    <span>
                        <small class="font-weight-bold text-primary commenter">{{ $reply->user->first_name ?? '' }}</small> - 
                        <small class="font-weight-bold">{{ $reply->body }}</small>
                    </span> 
                </div> 
                <small>{{ $reply->created_at->diffForHumans() }}</small>
            </div>
            <div class="action d-flex justify-content-between mt-2 align-items-center">
                <div class="reply tail px-4">
                    <small>Reply</small>
                </div>
            </div>
            @if ($replies = $reply->scopeApproved($reply->replies))
                @each('listings.includes.replies', $replies, 'reply')
            @endif
        </div>
    </div>
</div>