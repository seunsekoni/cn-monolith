@extends('layouts.app')

@section('title', "{$listing->name} Businesses Listing ")

@section('styles')
    <link href="{{ asset('css/theme-home.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="nav-container">
        <div>
            @include('includes.nav-two')
        </div>
    </div>

    <div class="main-container">
        <!-- Left Sidebar -->
        <section class="bg-secondary">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <x-tabuna-breadcrumbs
                                    class="breadcrumb-item"
                                    active="active"
                                />
                            </ol>
                        </nav>

                        <!-- LISTING DETAILS CARDS -->
                        <div class="row mt-3">
                            <div class="col-md-12 mb-3">
                                <div class="bordered card">
                                    <div class="card-body p0" style="overflow: hidden">
                                        <div class="row p40">
                                            <div class="col-md-8">
                                                <h4 class="type-bold mb-3">{{ $listing->name }}</h4>
                                                @if($listing->business->featured)
                                                    <span class="badge bg-orange h6-size color-white type-normal">FEATURED</span>
                                                @endif
                                                <div class="listing-indicators mb-2">
                                                    <div class="listing-rate d-inline-block">
                                                        @for ($i = 0; $i < 5; $i++)
                                                            <i 
                                                                class="{{ getAverageRating($listing) <= $i ? 'far fa-star' : 'fas fa-star' }} font-small"
                                                            >  
                                                            </i>
                                                        @endfor
                                                        <span>{{ $listing->business->reviews()->count() ?? '' }}</span>
                                                    </div>

                                                    <div class="listing-like d-inline-block ml-3">
                                                        <i class="fas fa-thumbs-up color-lime"></i>
                                                        <span>{{ $listing->business->likes }}</span>
                                                    </div>
                                                    <div class="listing-connected d-inline-block ml-3">
                                                        <i class="fas fa-link color-lime"></i>
                                                        <span>{{ $listing->business->bookmarks }}</span>
                                                    </div>
                                                </div>

                                                <p class="color-gray mb-3">{{ $listing->category->name }}</p>

                                                <div class="row mb-3">
                                                    @isset($listing->business->businessLocations()->first()->street_address)
                                                    <div class="col-md-12 mb-2">
                                                        <div class="listing-address">
                                                            <i class="fas fa-map-marker-alt color-lime mr-2"></i>
                                                            <span class="font-small">
															{{ $listing->business->businessLocations()->first()->street_address ?? '' }}
														</span>
                                                        </div>
                                                    </div>
                                                    @endisset
                                                    @isset($listing->business->businessContact->phone)
                                                    <div class="col-md-12 mb-2">
                                                        <div class="listing-address">
                                                            <i class="fas fa-phone-alt color-lime mr-2"></i>
                                                            <span class="font-small">
															{{$listing->business->businessContact->phone ?? ''}}
														</span>
                                                        </div>
                                                    </div>
                                                    @endisset
                                                    @isset($listing->business->businessContact->email)
                                                    <div class="col-md-12 mb-2">
                                                        <div class="listing-address">
                                                            <i class="fas fa-envelope color-lime mr-2"></i>
                                                            <span class="font-small">
															<a href="mailto:{{$listing->business->businessContact->email ?? ''}}">
																{{$listing->business->businessContact->email ?? ''}}
															</a>
														</span>
                                                        </div>
                                                    </div>
                                                    @endisset
                                                    @isset($listing->business->businessLocations()->first()->website_url)
                                                    <div class="col-md-12 mb-2">
                                                        <div class="listing-address">
                                                            <i class="fas fa-mouse-pointer color-lime mr-3"></i>
                                                            <span class="font-small">
															<a href="{{ $listing->business->businessLocations()->first()->website_url ?? '' }}"
                                                               target="_blank">
																{{ $listing->business->businessLocations()->first()->website_url ?? '' }}
															</a>
														</span>
                                                        </div>
                                                    </div>
                                                    @endisset
                                                </div>
                                                <!-- <a href="#" class="btn btn-cta btn-gray d-inline-block mb-0 type-uppercase">
                                                    Confirm this business
                                                </a>

                                                <a href="#" class="btn btn-cta btn-gray d-inline-block mb-0 type-uppercase">
                                                    Suggest an edit
                                                </a> -->
                                            </div>
                                            <div class="col-md-4">
                                                <img src="{{ $listing->featured_image }}" class="image-inherit mb-0">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row plr-40 bg-secondary-1">
                                                <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                    <i class="fas fa-share-alt mr-1"></i> Share
                                                </a>

                                                <a href="#"
                                                   class="btn btn-cta btn-light d-inline-block mb-0 mr-2 showReviews"
                                                   onclick="setDefaultStar()">
                                                    <i class="fas fa-star-half-alt mr-1"></i> Rate & Review
                                                </a>

                                                <!-- <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                    <i class="fas fa-comments mr-1"></i> Request Quote
                                                </a> -->

                                                <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                    <i class="fas fa-thumbs-up mr-1"></i> Like
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-12 p-0">
                                            <div class="row p40">
                                                <div class="col-md-4">
                                                    <div class="card hours">
                                                        @if($listing->business->business_hours)
                                                            <div class="card-header type-bold">
                                                                Business Hours
                                                            </div>
                                                            <ul class="list-group list-group-flush">
                                                                @foreach(collect(json_decode($listing->business->business_hours)) as $business_hour)
                                                                    <li class="list-group-item bg-secondary-1 font-small">
                                                                        <dl class="row mb-0">
                                                                            <dt class="col-sm-5 ">{{substr($business_hour->day,0,3)}}</dt>
                                                                            <dd class="col-sm-7 mb-0">{{$business_hour->open}}
                                                                                - {{$business_hour->close}}</dd>
                                                                        </dl>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <iframe
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.546441382114!2d3.385522114494949!3d6.452222595332086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b8b0fb5756d71%3A0x7c6a0510ac25ea80!2sStallion%20Plaza!5e0!3m2!1sen!2sph!4v1597047213715!5m2!1sen!2sph"
                                                        height="260"
                                                        frameborder="0"
                                                        style="border:1px solid #bfc3c9; width: 100%"
                                                        allowfullscreen=""
                                                        aria-hidden="false"
                                                        tabindex="0"
                                                    ></iframe>
                                                </div>

                                                <div class="col-12 row pt-4">
                                                    <div class="col-12">
                                                        <h5 class="type-bold mb-2">Description</h5>
                                                        <div class="text-justify" style="line-height: 1.8em;">
                                                            {{ $listing->description }}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 p-4">
                                                    @if($listing->galleries()->count())
                                                        <div class="row pt-4">
                                                            <h6 class="type-bold mb-4">Photo Gallery</h6>
                                                            <div
                                                                class="gallery-carousel owl-carousel owl-theme"
                                                                data-items="3"
                                                                data-margin="15"
                                                                data-items-mobile-portrait="1"
                                                                data-autoplay="true"
                                                                data-autoplay-timeout="2000"
                                                                data-loop="true"
                                                            >
                                                                @foreach($listing->galleries as $gallery)
                                                                    @if($gallery->type == "media")
                                                                        @foreach($gallery->getMedia(constant("\App\Enums\MediaCollection::PHOTOS")) as $photo)
                                                                            <div class="item">
                                                                                <img src="{{ $photo->getFullUrl() }}"
                                                                                     alt="">
                                                                            </div>
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>

                                                <!-- Reviews and Rating modal -->
                                                <div class="modal fade center reviewModal" id="ratingModal"
                                                     tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog modal-md" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <div class="top-strip">
                                                                    <div class="close-review-modal">
                                                                        <i class="fa fa-times"></i>
                                                                    </div>
                                                                    <!-- Review form -->
                                                                    <div class="reviewHeading">Drop a review</div>
                                                                    <form method="post" onsubmit="submitReviews(event)"
                                                                          id="reviewForm">
                                                                        @csrf
                                                                        <input type="hidden" id="businessReview">
                                                                        <textarea name="comment" id="reviewText"
                                                                                  required cols="30" rows="10"
                                                                                  placeholder="Enter your review"></textarea>
                                                                        <div class='rating-widget'>
                                                                            <!-- Rating Stars Box -->

                                                                            <!-- This input has the percentage of stars -->
                                                                            <input type="hidden"
                                                                                   name="starRatingPercentage"
                                                                                   id="starRatingPercentage">
                                                                            <div class='rating-stars text-center'>
                                                                                <ul id='stars'>
                                                                                    <li class='star' title='Poor'
                                                                                        data-value='1'>
                                                                                        <i class='fa fa-star fa-fw'></i>
                                                                                    </li>
                                                                                    <li class='star' title='Fair'
                                                                                        data-value='2'>
                                                                                        <i class='fa fa-star fa-fw'></i>
                                                                                    </li>
                                                                                    <li class='star' title='Good'
                                                                                        data-value='3'>
                                                                                        <i class='fa fa-star fa-fw'></i>
                                                                                    </li>
                                                                                    <li class='star' title='Excellent'
                                                                                        data-value='4'>
                                                                                        <i class='fa fa-star fa-fw'></i>
                                                                                    </li>
                                                                                    <li class='star' title='WOW!!!'
                                                                                        data-value='5'>
                                                                                        <i class='fa fa-star fa-fw'></i>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>

                                                                            <div class='success-box'>
                                                                                <i class="fas fa-check"></i>
                                                                                <div class='text-message'></div>
                                                                                <div class='clearfix'></div>
                                                                            </div>
                                                                        </div>
                                                                        <button type="submit" id="reviewSubmit">Submit
                                                                            your Review
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                            <!-- <div class="review-modal">
																<div class="reviewHeading">Customers' Reviews</div>


																	<div class="review-card">
																		<div class="meta">
																			<img class="photo" src="{{asset('img/businesses/ad-1.jpg')}}"></img>
																		</div>
																		<div class="description">
																			<h1>Great Product!!</h1>
																			<span class="fa fa-star checked"></span>
																			<span class="fa fa-star checked"></span>
																			<span class="fa fa-star checked"></span>
																			<span class="fa fa-star"></span>
																			<span class="fa fa-star"></span>
																			<br>
																			<h2>Adittya Dey</h2>
																			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis!</p>
																		</div>
																	</div>

																	<br>
																	<div class="review-card">
																		<div class="meta">
																			<img class="photo" src="{{asset('img/businesses/ad-1.jpg')}}"></img>
																		</div>
																		<div class="description">
																			<h1>Great Product!!</h1>
																			<span class="fa fa-star checked"></span>
																			<span class="fa fa-star checked"></span>
																			<span class="fa fa-star checked"></span>
																			<span class="fa fa-star"></span>
																			<span class="fa fa-star"></span>
																			<br>
																			<h2>Adittya Dey</h2>
																			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis!</p>
																		</div>
																	</div>

																	<br>

																	<div class="review-card">
																		<div class="meta">
																			<img class="photo" src="{{asset('img/businesses/ad-1.jpg')}}"></img>
																		</div>
																		<div class="description">
																			<h1>Sleek design.</h1>
																			<span class="fa fa-star checked"></span>
																			<span class="fa fa-star checked"></span>
																			<span class="fa fa-star checked"></span>
																			<span class="fa fa-star"></span>
																			<span class="fa fa-star"></span>
																			<br>
																			<h2>Adittya Dey</h2>
																			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis!</p>

																		</div>
																	</div>
																</div>
															<div class="bottom-strip"></div> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row pt-5 px-3">
                                                    <div class="col-md-12">
                                                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                            <li class="nav-item">
                                                                <a
                                                                    class="nav-link active"
                                                                    id="pills-home-tab"
                                                                    data-toggle="pill"
                                                                    href="#pills-home"
                                                                    role="tab"
                                                                    aria-controls="pills-home"
                                                                    aria-selected="true"
                                                                >
                                                                    Reviews ({{ $approved_reviews->count() }})
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a
                                                                    class="nav-link"
                                                                    id="pills-contact-tab"
                                                                    data-toggle="pill"
                                                                    href="#pills-comments"
                                                                    role="tab"
                                                                    aria-controls="pills-comments"
                                                                    aria-selected="false"
                                                                >
                                                                    Comments ({{ $approved_comments->count() }})
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content" id="pills-tabContent">
                                                            <div
                                                                class="tab-pane fade show active"
                                                                id="pills-home"
                                                                role="tabpanel"
                                                                aria-labelledby="pills-home-tab"
                                                            ><!-- Fed by AJAX --></div>

                                                            <div class="tab-pane fade" id="pills-profile"
                                                                 role="tabpanel"
                                                                 aria-labelledby="pills-profile-tab">
                                                                <div class="col-md-12 text-center pt-3">
                                                                    @foreach($approved_reviews as $review)
                                                                        <img
                                                                            src="{{asset('img/placeholders/avatar.jpg')}}"
                                                                            class="image-round" data-toggle="tooltip"
                                                                            data-placement="bottom"
                                                                            title="{{ $review->user->first_name ?? ''  }}"
                                                                        >
                                                                    @endforeach
                                                                </div>
                                                            </div>

                                                            <div class="tab-pane fade" id="pills-contact"
                                                                 role="tabpanel"
                                                                 aria-labelledby="pills-contact-tab">
                                                                <div class="col-md-12 text-center pt-3">
                                                                    @foreach($approved_reviews as $review)
                                                                        <img
                                                                            src="{{ asset('img/gravatar.png') }}"
                                                                            class="image-round"
                                                                            data-toggle="tooltip"
                                                                            data-placement="bottom"
                                                                            title=""
                                                                        >
                                                                    @endforeach
                                                                </div>
                                                            </div>

                                                            <div class="tab-pane fade" id="pills-comments"
                                                                 role="tabpanel"
                                                                 aria-labelledby="pills-comment-tab">
                                                                <div class="comment-add my-3 row">
                                                                    <form action="#" method="post" id="addComment">
                                                                        <div class="col-12">
                                                                            <input type="text" name="" id="comment">
                                                                        </div>
                                                                        <div class="col-12 mt-2">
                                                                            <button type="submit"
                                                                                    class="btn btn-primary">Add Comment
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                                <div class="comment mb-5">
                                                                    <div class="row d-flex justify-content-center">
                                                                        <div class="col-12" id="commentDiv">
                                                                            <!-- Comments are fetched with AJAX -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- END COMMENT SECTION -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <div class="row mb-4">
                                <div class="col-6 col-md-6 col-lg-6 col-xl-6">
                                    <h5 class="type-bold">You might also like...</h5>
                                </div>
                            </div>
                            <div class="row">
                                @forelse($related_listings as $listing)
                                    <x-frontend.listing-card :listing="$listing"/>
                                @empty
                                    <p>There are currently no related listings available.</p>
                                @endforelse
                            </div>
                        </div>

                        <div class="container row pb-2 pt-2">
                            <img src="{{asset('img/businesses/ad-5.jpg')}}" class="image-inherit">
                        </div>
                    </div>

                    <div class="col-md-3">
                        @include('includes.featured-businesses')

                        <div class="row">
                            <div class="container">
                                <img src="{{asset('img/businesses/ad-1.jpg')}}" class="image-inherit mb-3">
                                <img src="{{asset('img/businesses/ad-2.jpg')}}" class="image-inherit mb-3">
                                <img src="{{asset('img/businesses/ad-3.jpg')}}" class="image-inherit mb-3">
                                <img src="{{asset('img/businesses/ad-4.jpg')}}" class="image-inherit mb-3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @include('includes.footer-cta', ['category' => $category ?? ''])

        @include('includes.footer')
    </div>
@endsection
@once
    @push('scripts')
        @include('listings.includes.listing-scripts')
    @endpush
@endonce
