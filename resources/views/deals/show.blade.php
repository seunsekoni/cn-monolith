@extends('layouts.app')

@section('title', 'Deals Detail')

@section('content')
<div class="nav-container">
    <div>
        @include('includes.top-menu')
        @include('includes.deals-nav')
    </div>
</div>

<div class="main-container">
    <section class="bg-secondary">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Deals</a></li>
                            <li class="breadcrumb-item"><a href="#">Training and Development</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Big Deal Driving School</li>
                        </ol>
                    </nav>

                    <div class="row  mt-3">
                        <div class="col-md-12 mb-3">
                            <div class="bordered card">
                                <div class="card-body p0">
                                    <div class="row p40">
                                        <div class="col-md-7">
                                            <h4 class="type-bold mb-2">Big Deal Driving School
                                                <!-- <span class="badge bg-orange h6-size color-white type-normal">CONFIRMED</span> -->
                                            </h4>
                                            <h6 class="color-orange type-medium mb-3 mr-2">N 10,000.00 <span
                                                    class="small color-dark">(N30,000) <strong>-67%</strong></span></h6>

                                            <div class="listing-indicators mb-4">
                                                <div class="listing-rate d-inline-block">
                                                    <i class="far fa-star color-orange"></i>
                                                    <i class="far fa-star color-orange"></i>
                                                    <i class="far fa-star color-orange"></i>
                                                    <i class="far fa-star color-orange"></i>
                                                    <i class="far fa-star color-orange"></i>
                                                    <span>10</span>
                                                </div>

                                                <div class="listing-like d-inline-block ml-3">
                                                    <i class="fas fa-heart color-orange"></i>
                                                    <span>5</span>
                                                </div>
                                                <div class="listing-recommend d-inline-block ml-3">
                                                    <i class="fas fa-thumbs-up color-orange"></i>
                                                    <span>2</span>
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <div class="col-md-12 mb-2 pl-3">
                                                    <div class="listing-address">
                                                        <i class="fas fa-map-marker-alt color-orange mr-2"></i>
                                                        <span class="font-small">Wuse Zone 6, Abuja, Federal Capital
                                                            Territory</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mb-2 pl-3">
                                                    <div class="listing-address">
                                                        <i class="far fa-calendar color-orange mr-2"></i>
                                                        <span>Aug 4, 2020 - Aug 8, 2020 (3 days left)</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mb-2 pl-3">
                                                    <div class="listing-address mb-2">
                                                        <i class="fas fa-tag color-orange mr-2"></i>
                                                        <span>Training and Development</span>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- <a href="#" class="btn btn-cta btn-gray d-inline-block mb-0 type-uppercase">
                        Confirm this business
                    </a> -->

                                            <a href="#" class="btn btn-cta btn-gray d-inline-block mb-0 type-uppercase">
                                                Suggest an edit
                                            </a>


                                        </div>


                                        <div class="col-md-5">
                                            <img src="{{asset('img/deals/feat-2.jpg')}}" class="image-inherit mb-0">
                                            <!-- <a href="#" class="btn btn-open mb-0">
                        FOR SALE
                    </a> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row plr-40 bg-secondary-1">

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-share-square mr-1"></i> Share
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-star-half-alt mr-1"></i> Rate & Review
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-comments mr-1"></i> Contact Agent
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-heart mr-1"></i> Like
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-thumbs-up mr-1"></i> Recommend
                                            </a>

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row p40">

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="card hours">
                                                        <div class="card-header type-bold">
                                                            Seller Information
                                                        </div>
                                                        <ul class="list-group list-group-flush">
                                                            <li class="list-group-item bg-secondary-1 font-small">
                                                                <dl class="row mb-0">
                                                                    <dt class="col-sm-5 ">Name</dt>
                                                                    <dd class="col-sm-7 mb-0">Jimmy Momoh</dd>
                                                                </dl>
                                                            </li>
                                                            <li class="list-group-item bg-secondary-1 font-small">
                                                                <dl class="row mb-0">
                                                                    <dt class="col-sm-5 ">Website</dt>
                                                                    <dd class="col-sm-7 mb-0">www.bigdeals.com</dd>
                                                                </dl>
                                                            </li>
                                                            <li class="list-group-item bg-secondary-1 font-small">
                                                                <dl class="row mb-0">
                                                                    <dt class="col-sm-5" style="padding-right: 10px;">
                                                                        Contact No.</dt>
                                                                    <dd class="col-sm-7 mb-0">09050480580, 09050480580
                                                                    </dd>
                                                                </dl>
                                                            </li>
                                                            <li class="list-group-item bg-secondary-1 font-small">
                                                                <dl class="row mb-0">
                                                                    <dt class="col-sm-5 ">Email</dt>
                                                                    <dd class="col-sm-7 mb-0">jimiteck@gmail.com</dd>
                                                                </dl>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="col-md-7">
                                                    <iframe
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.546441382114!2d3.385522114494949!3d6.452222595332086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b8b0fb5756d71%3A0x7c6a0510ac25ea80!2sStallion%20Plaza!5e0!3m2!1sen!2sph!4v1597047213715!5m2!1sen!2sph"
                                                        width="450" height="230" frameborder="0"
                                                        style="border:1px solid #bfc3c9;" allowfullscreen=""
                                                        aria-hidden="false" tabindex="0"></iframe>
                                                </div>
                                            </div>

                                            <div class="row pt-4">


                                                <div class="col-md-8">
                                                    <h6 class="type-bold mb-4">Description</h6>

                                                    <p><span class="type-medium color-dark">Date Listed:</span>
                                                        September 09, 2019 <i>(about 12 months ago)</i></p>

                                                    <p class="mb-3">Big Deal Driving School is a foremost driving
                                                        academy in Abuja with multiple years of experience. our primary
                                                        focused is tailored towards breeding confident and defensive
                                                        road users in Nigeria.</p>

                                                    <hr>

                                                    <div class="row ">
                                                        <div class="col-6 pl-3 mt-3">
                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Category</dt>
                                                                <dd class="col-sm-7 mb-0">Training & Development</dd>
                                                            </dl>

                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Brand</dt>
                                                                <dd class="col-sm-7 mb-0">Unbranded</dd>
                                                            </dl>

                                                        </div>
                                                        <div class="col-6 mt-3">
                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Deal Period</dt>
                                                                <dd class="col-sm-7 mb-0">Sep 09, 2019 to<br>Sep 10,
                                                                    2020</dd>
                                                            </dl>

                                                            <dl class="row mb-3">
                                                                <dt class="col-sm-5">Views</dt>
                                                                <dd class="col-sm-7 mb-0">539 views</dd>
                                                            </dl>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="col-md-4 ">
                                                    <h6 class="type-bold mb-4">Contact Seller</h6>
                                                    <form>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control"
                                                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                                                placeholder="Full Name">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control"
                                                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                                                placeholder="Contact Number">
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea class="form-control"
                                                                id="exampleFormControlTextarea1"
                                                                rows="3">I am interested in Big Deal Driving School</textarea>
                                                        </div>
                                                        <button type="submit"
                                                            class="btn btn-primary btn-sm btn-block">Submit</button>
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="row pt-4">
                                                    <h6 class="type-bold mb-4">Photo Gallery</h6>
                                                    <div class="gallery-carousel owl-carousel owl-theme" data-items="3"
                                                        data-margin="15" data-items-mobile-portrait="1"
                                                        data-autoplay="true" data-autoplay-timeout="2000"
                                                        data-loop="true">
                                                        <div class="item" style="">
                                                            <img src="img/deals/feat-2.jpg" alt="">
                                                        </div>
                                                        <div class="item" style="">
                                                            <img src="img/deals/feat-2.jpg" alt="">
                                                        </div>
                                                        <div class="item" style="">
                                                            <img src="img/deals/feat-2.jpg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row pt-5">
                                                <div class="col-md-12">
                                                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link active" id="pills-home-tab"
                                                                data-toggle="pill" href="#pills-home" role="tab"
                                                                aria-controls="pills-home" aria-selected="true">Rates &
                                                                Reviews (5)</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="pills-profile-tab"
                                                                data-toggle="pill" href="#pills-profile" role="tab"
                                                                aria-controls="pills-profile"
                                                                aria-selected="false">Recommendations (2)</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="pills-contact-tab"
                                                                data-toggle="pill" href="#pills-contact" role="tab"
                                                                aria-controls="pills-contact"
                                                                aria-selected="false">Likes (8)</a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content" id="pills-tabContent">
                                                        <div class="tab-pane fade show active" id="pills-home"
                                                            role="tabpanel" aria-labelledby="pills-home-tab">
                                                            <div class="row mt-4">
                                                                <div class="col-md-2 text-center">
                                                                    <img src="{{asset('img/gravatar.png')}}"
                                                                        class="image-round">
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>

                                                                    <p><span class="type-bold color-dark">Uzo H.
                                                                            said</span> "Lorem ipsum dolor sit amet,
                                                                        consectetur adipiscing elit. Nam at tortor
                                                                        posuere, finibus tortor sed, accumsan magna. Nam
                                                                        magna tortor, iaculis ut odio eu, bibendum
                                                                        dignissim tellus. Proin fermentum, nibh
                                                                        condimentum laoreet tincidunt, magna tellus
                                                                        rhoncus mauris, sit amet scelerisque nisi nulla
                                                                        ut metus." </p>
                                                                </div>
                                                            </div>

                                                            <div class="row mt-2">
                                                                <div class="col-md-2 text-center">
                                                                    <img src="{{asset('img/gravatar.png')}}"
                                                                        class="image-round">
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>

                                                                    <p><span class="type-bold color-dark">Uzo H.
                                                                            said</span> "Lorem ipsum dolor sit amet,
                                                                        consectetur adipiscing elit. Nam at tortor
                                                                        posuere, finibus tortor sed, accumsan magna. Nam
                                                                        magna tortor, iaculis ut odio eu, bibendum
                                                                        dignissim tellus. Proin fermentum, nibh
                                                                        condimentum laoreet tincidunt, magna tellus
                                                                        rhoncus mauris, sit amet scelerisque nisi nulla
                                                                        ut metus." </p>
                                                                </div>
                                                            </div>

                                                            <div class="row mt-2">
                                                                <div class="col-md-2 text-center">
                                                                    <img src="{{asset('img/gravatar-default.png')}}"
                                                                        class="image-round">
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>
                                                                    <i class="fas fa-star color-yellow font-small"></i>

                                                                    <p><span class="type-bold color-dark">Akeem F.
                                                                            said</span> "Lorem ipsum dolor sit amet,
                                                                        consectetur adipiscing elit. Nam at tortor
                                                                        posuere, finibus tortor sed, accumsan magna. Nam
                                                                        magna tortor, iaculis ut odio eu, bibendum
                                                                        dignissim tellus. Proin fermentum, nibh
                                                                        condimentum laoreet tincidunt, magna tellus
                                                                        rhoncus mauris, sit amet scelerisque nisi nulla
                                                                        ut metus." </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                                            aria-labelledby="pills-profile-tab">
                                                            <div class="col-md-12 text-center pt-3">
                                                                <img src="{{asset('img/gravatar.png')}}"
                                                                    class="image-round" data-toggle="tooltip"
                                                                    data-placement="bottom" title="Uzo H. (1)">
                                                                <img src="{{asset('img/gravatar-default.png')}}"
                                                                    class="image-round" data-toggle="tooltip"
                                                                    data-placement="bottom" title="Akeem F. (3)">
                                                            </div>
                                                        </div>

                                                        <div class="tab-pane fade" id="pills-contact" role="tabpanel"
                                                            aria-labelledby="pills-contact-tab">
                                                            <div class="col-md-12 text-center pt-3">
                                                                <img src="{{asset('img/gravatar.png')}}"
                                                                    class="image-round" data-toggle="tooltip"
                                                                    data-placement="bottom" title="Uzo H.">
                                                                <img src="{{asset('img/gravatar-default.png')}}"
                                                                    class="image-round" data-toggle="tooltip"
                                                                    data-placement="bottom" title="Akeem F.">
                                                                <img src="{{asset('img/gravatar.png')}}"
                                                                    class="image-round" data-toggle="tooltip"
                                                                    data-placement="bottom" title="Uzo H.">
                                                                <img src="{{asset('img/gravatar.png')}}"
                                                                    class="image-round" data-toggle="tooltip"
                                                                    data-placement="bottom" title="Uzo H.">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row plr-40 bg-secondary-1">

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-share-square mr-1"></i> Share
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-star-half-alt mr-1"></i> Rate & Review
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-comments mr-1"></i> Contact Agent
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-heart mr-1"></i> Like
                                            </a>

                                            <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                <i class="fas fa-thumbs-up mr-1"></i> Recommend
                                            </a>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row mb-4">
                            <div class="col-6 col-md-6 col-lg-6 col-xl-6">
                                <h class="type-bold">You might also like...</h>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
                                <a href="#">
                                    <div class="featured card bordered-1">
                                        <img src="{{asset('img/deals/feat-4.jpg')}}" class="card-img-top"
                                            alt="Featured 1">
                                        <div class="card-body">
                                            <p class="card-title type-bold mb-1">Massage Therapy Course with e-Careers
                                            </p>
                                            <p class="color-orange type-medium mb-2">N 6,999 <span class="small">(N
                                                    12,000) <strong>-26%</strong></span></p>

                                            <div class="listing-address mb-2">
                                                <i class="far fa-calendar color-orange"></i>
                                                <span>Aug 4, 2020 - Aug 8, 2020 (3 days left)</span>
                                            </div>

                                            <div class="d-flex justify-content-between">
                                                <div class="d-inline-block small mt-1">
                                                    <i class="fas fa-star color-orange"></i>
                                                    <i class="fas fa-star color-orange"></i>
                                                    <i class="fas fa-star color-orange"></i>
                                                    <i class="far fa-star color-orange"></i>
                                                    <i class="far fa-star color-orange"></i>
                                                    <span>10</span>
                                                </div>
                                                <div>
                                                    <small>Training & Education</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
                                <a href="#">
                                    <div class="featured card bordered-1">
                                        <img src="{{asset('img/deals/feat-5.jpg')}}" class="card-img-top"
                                            alt="Featured 1">
                                        <div class="card-body">
                                            <p class="card-title type-bold mb-1">Half Day Spa Package for One or Two at
                                                Mangwanani Spa</p>
                                            <p class="color-orange type-medium mb-2">N 10,000 <span class="small">(N
                                                    30,000) <strong>-67%</strong></span></p>

                                            <div class="listing-address mb-2">
                                                <i class="far fa-calendar color-orange"></i>
                                                <span>Aug 4, 2020 - Aug 8, 2020 (3 days left)</span>
                                            </div>

                                            <div class="d-flex justify-content-between">
                                                <div class="d-inline-block small mt-1">
                                                    <i class="fas fa-star color-orange"></i>
                                                    <i class="fas fa-star color-orange"></i>
                                                    <i class="fas fa-star color-orange"></i>
                                                    <i class="fas fa-star color-orange"></i>
                                                    <i class="far fa-star color-orange"></i>
                                                    <span>35</span>
                                                </div>
                                                <div>
                                                    <small>Beauty And Spa</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
                                <a href="#">
                                    <div class="featured card bordered-1">
                                        <img src="{{asset('img/deals/feat-6.jpg')}}" class="card-img-top"
                                            alt="Featured 1">
                                        <div class="card-body">
                                            <p class="card-title type-bold mb-1">Choice of a Main Meal for up to Six at
                                                Plantation Café</p>
                                            <p class="color-orange type-medium mb-2">N 6,999 <span class="small">(N
                                                    12,000) <strong>-26%</strong></span></p>

                                            <div class="listing-address mb-2">
                                                <i class="far fa-calendar color-orange"></i>
                                                <span>Aug 4, 2020 - Aug 8, 2020 (3 days left)</span>
                                            </div>

                                            <div class="d-flex justify-content-between">
                                                <div class="d-inline-block small mt-1">
                                                    <i class="fas fa-star color-orange"></i>
                                                    <i class="fas fa-star color-orange"></i>
                                                    <i class="fas fa-star color-orange"></i>
                                                    <i class="far fa-star color-orange"></i>
                                                    <i class="far fa-star color-orange"></i>
                                                    <span>15</span>
                                                </div>
                                                <div>
                                                    <small>Food & Beverage</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="container mb-2 mt-2">
                        <img src="{{asset('img/businesses/ad-5.jpg')}}" class="image-inherit">
                    </div>

                </div>

                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div class="bordered card">
                                <div class="card-body">
                                    <h6 class="color-primary-1 type-bold">Featured Deals</h6>

                                    <ul class="arrow mt-3">
                                        <li><a href="" class="color-dark">Best MLM Software for Network Marketing in
                                            </a></li>
                                        <li><a href="" class="color-dark">Total Cure For Staphylococcus Infection</a>
                                        </li>
                                        <li><a href="" class="color-dark">Cleanwash laundry and dry-cleaning </a></li>
                                        <li><a href="" class="color-dark">Total Cure For Ovarian Cyst</a></li>
                                        <li><a href="" class="color-dark">Effective Supplement For Menstrual Cramps</a>
                                        </li>
                                        <li><a href="" class="color-dark">Best Website Design Company in Nigeria</a>
                                        </li>
                                        <li><a href="" class="color-dark">Ruzu Herbal Bitters</a></li>
                                        <li><a href="" class="color-dark">IT/POS Software, Analysis Template</a></li>
                                        <li><a href="" class="color-dark">Six Sigma Yellow Belt Certification</a></li>
                                        <li><a href="" class="color-dark">FOREVER C9 NUTRITION CLEANSING PACK</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="container">
                            <img src="{{asset('img/deals/advert-1.jpg')}}" class="image-inherit mb-3">
                            <img src="{{asset('img/deals/advert-2.jpg')}}" class="image-inherit mb-3">
                            <img src="{{asset('img/deals/advert-3.jpg')}}" class="image-inherit mb-3">
                            <img src="{{asset('img/deals/advert-4.jpg')}}" class="image-inherit mb-3">
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </section>


    <section class="p-0">
        <div class="row no-gutters">
            <div class="col-md-6 col-lg-6 col-xl-6">
                <div class="imagebg height-50 image-dark">
                    <div class="background-image-holder">
                        <img src="{{asset('img/cars/footer-banners-1.jpg')}}">
                    </div>
                    <div class="container pos-vertical-center h-100">
                        <div class="cta-banner-footer">
                            <div class="row w-100 mb-2">
                                <div class="col-md-10 col-lg-10 col-xl-10">
                                    <h2 class="title type-bold mb-2">Reach your target market faster </h2>
                                    <p>with just <strong>N10,000</strong></p>
                                </div>
                            </div>
                            <div class="d-flex mb-3">
                                <div class="cta-stats mr-3">
                                    <img src="{{asset('img/businesses/window-pointer.png')}}">
                                    <div>
                                        <p class="type-medium mb-0">250,000+</p>
                                        <small>Daily Visitors</small>
                                    </div>
                                </div>
                                <div class="cta-stats">
                                    <img src="{{asset('img/businesses/briefcase.png')}}">
                                    <div>
                                        <p class="type-medium mb-0">500,000+</p>
                                        <small>Business Listings</small>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <a href="#" class="btn btn-lg btn-primary-1 mr-4">
                                    Advertise with us
                                </a>
                                <div class="">
                                    <p class="mb-0">Call <span class="type-medium">0700-800-5000</span></p>
                                    <small>grow@connectnigeria.com</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-6">
                <div class="imagebg height-50 image-dark">
                    <div class="background-image-holder">
                        <img src="{{asset('img/deals/footer-banners-2.jpg')}}">
                    </div>
                    <div class="container pos-vertical-center h-100">
                        <div class="cta-banner-footer-2 d-flex">
                            <div class="row align-items-center w-100 mb-2">
                                <div class="col-md-8 col-lg-8 col-xl-8">
                                    <h2 class="title type-bold mb-4">Be the first to know what’s happening</h2>
                                    <p class="mb-4">Sign up to our newsletter get interesting news and updates delivered
                                        right to your inbox</p>
                                    <a href="#" class="btn btn-lg btn-primary-1 mr-4">
                                        Subscribe Now
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('includes.footer')
</div>
@endsection