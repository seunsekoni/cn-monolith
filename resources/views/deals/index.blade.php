@extends('layouts.app')

@section('title', 'Deals')

@section('content')
<div class="nav-container">
  <div>
    @include('includes.top-menu')
    @include('includes.deals-nav')
  </div>
</div>

<div class="main-container">
  <section class="imagebg height-40">
    <div class="background-image-holder">
      <img src="{{asset('img/deals/hero-banner-1.jpg')}}" alt="CN Business" />
    </div>
    <div class="container pos-vertical-center">
      <div class="row align-items-center">
        <div class="col-md-12 col-lg-9 col-xl-9">
          <form class="search-bar mb-2">
            <div class="row">
              <div class="col-9 col-md-6 mb-2 mb-md-0 mb-lg-0 mb-xl-0">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">Search</span>
                  </div>
                  <input type="text" class="form-control" placeholder="deals, coupons or stores">
                </div>
              </div>
              <div class="col-9 col-md-4 pr-xs-0">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">In</span>
                  </div>
                  <input type="text" class="form-control" placeholder="Lagos, Nigeria">
                </div>
              </div>
              <div class="col-3 col-md-1 pl-xs-0">
                <button type="submit" class="btn btn-primary h-100"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </form>

          <div class="popular-searches">
            <span class="color-primary">
              <small class="type-bold">Popular:</small>
            </span>
            <span>
              <small class="color-white">iPhone 11 Pro, Macbook Air, Samsung Galaxy Note 10, Apple Watch </small>
            </span>
          </div>
        </div>
        <div class="col-lg-3 col-xl-3 hidden-xs hidden-sm">
          <p class="mb-0"><small>Advertisements</small></p>
          <div class="medrec-carousel owl-carousel owl-theme" data-items="1" data-items-mobile-portrait="1"
            data-autoplay="true" data-autoplay-timeout="5000" data-loop="true">
            <div class="item">
              <img src="{{asset('img/deals/advert-1.jpg')}}" width="240" class="text-center mb-3">
            </div>
            <div class="item">
              <img src="{{asset('img/deals/advert-2.jpg')}}" width="240" class="text-center mb-3">
            </div>
            <div class="item">
              <img src="{{asset('img/deals/advert-3.jpg')}}" width="240" class="text-center mb-3">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- categories -->
  <section class="pb-0">
    <div class="container">

      <div class="row">
        <div class="col-12">
          <div class="category-carousel owl-carousel owl-theme" data-items="8" data-margin="20"
            data-items-mobile-portrait="3" data-autoplay="true" data-loop="true" data-autoplay-timeout="3000">
            <div class="item text-center">
              <a href="#">
                <img src="{{asset('img/deals/cat-1.jpg')}}">

                <p class="cat-name"><small class="type-bold">Grocery</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <img src="{{asset('img/deals/cat-2.jpg')}}">

                <p class="cat-name"><small class="type-bold">Kitchen</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <img src="{{asset('img/deals/cat-3.jpg')}}">

                <p class="cat-name"><small class="type-bold">Electronics</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <img src="{{asset('img/deals/cat-4.jpg')}}">
                <p class="cat-name"><small class="type-bold">Computers</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <img src="{{asset('img/deals/cat-5.jpg')}}">

                <p class="cat-name"><small class="type-bold">Fashion</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <img src="{{asset('img/deals/cat-6.jpg')}}">
                <p class="cat-name"><small class="type-bold">Beauty & Spas</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <img src="{{asset('img/deals/cat-7.jpg')}}">
                <p class="cat-name"><small class="type-bold">Toys & Games</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <img src="{{asset('img/deals/cat-8.jpg')}}">
                <p class="cat-name"><small class="type-bold">Sports & Outdoors</small></p>
              </a>
            </div>

          </div><!-- end carousel -->
        </div>
      </div>
    </div>
  </section>

  <!-- featured -->
  <section class="featured-biz">
    <div class="container">

      <div class="row">
        <div class="col-md-9 col-lg-9 col-xl-9">
          <div class="row justify-content-end">
            <div class="col-7 col-md-9">
              <h5 class="type-bold mb-4">Latest Deals</h5>
            </div>
            <div class="col-5 col-md-3 text-right">
            </div>
          </div>
          <div class="row equal-container">

            @forelse ($listings as $listing)
            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
              <a href="#">
                <div class="featured card">
                  <img src="{{ asset('img/deals/feat-1.jpg') }}" class="card-img-top" alt="Featured 1">
                  <div class="card-body">
                    <div class="equal">
                      <p class="card-title type-bold mb-1">Choice of Any 2-Course Meal with a Beverage for up to Six</p>
                    </div>
                    <p class="color-orange type-medium mb-2">N 6,999 <span class="small">(N 12,000)
                        <strong>-26%</strong></span></p>

                    <div class="listing-address mb-2">
                      <i class="far fa-calendar color-orange"></i>
                      <span>Aug 4, 2020 - Aug 8, 2020 (3 days left)</span>
                    </div>

                    <div class="d-flex justify-content-between">
                      <div class="d-inline-block small mt-1">
                        <i class="fas fa-star color-orange"></i>
                        <i class="fas fa-star color-orange"></i>
                        <i class="fas fa-star color-orange"></i>
                        <i class="far fa-star color-orange"></i>
                        <i class="far fa-star color-orange"></i>
                        <span>10</span>
                      </div>
                      <div>
                        <small>Food & Beverage</small>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>

            @empty
            <p>There are currently no deals.</p>
            @endforelse
          </div>
        </div>

        <div class="col-md-3 col-lg-3 col-xl-3 text-center">
          <p class="mb-4"><small>Advertisements</small></p>
          <div class="hidden-xs hidden-sm">
            <img src="{{asset('img/deals/advert-1.jpg')}}" width="240" class="text-center mb-3">
            <img src="{{asset('img/deals/advert-2.jpg')}}" width="240" class="text-center mb-3">
            <img src="{{asset('img/deals/advert-3.jpg')}}" width="240" class="text-center mb-3">
          </div>
          <div class="hidden-lg hidden-md">
            <div class="medrec-carousel owl-carousel owl-theme" data-items-mobile-portrait="1" data-autoplay="true"
              data-autoplay-timeout="5000" data-loop="true">
              <div class="item">
                <img src="{{asset('img/deals/advert-1.jpg')}}" width="240" class="text-center mb-3">
              </div>
              <div class="item">
                <img src="{{asset('img/deals/advert-2.jpg')}}" width="240" class="text-center mb-3">
              </div>
              <div class="item">
                <img src="{{asset('img/deals/advert-3.jpg')}}" width="240" class="text-center mb-3">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- cta -->

  <section>
    <div class="container">
      <!-- CTAs -->
      <div class="row">
        <!-- Get a quote -->
        <div class="col-md-7 col-lg-7 col-xl-7 mb-3 mb-lg-0 mb-xl-0">
          <div class="card home-card-ctas ">
            <div class="row no-gutters">


              <div class="col-md-4 col-lg-4 col-xl-4">
                <div class="imagebg h-100 second">
                  <div class="background-image-holder border-tl-radius border-bl-radius">
                    <img src="{{asset('img/deals/home-1.jpg')}}" alt="">
                  </div>
                </div>
              </div><!-- end col -->

              <div class="col-md-8 col-lg-8 col-xl-8">
                <div class="card-body border-tr-radius border-br-radius bg-maroon">
                  <h6 class="type-bold color-orange">LOVE LOCAL. SHOP LOCAL.</h6>
                  <h1 class="card-title type-bold color-white mb-0">SHOP THE BEST FOR LESS</h1>
                  <p class="color-white">Up to 50% off your favorite brands and products.</p>
                  <a href="#" class="btn btn-orange color-white">Shop Now</a>
                </div>
              </div><!-- end col -->
            </div><!-- end row -->
          </div><!-- end card -->
        </div><!-- end quote cta -->

        <!-- Confirm Businesses -->
        <div class="col-md-5 col-lg-5 col-xl-5">
          <div class="card home-card-ctas h-100 ">
            <div class="imagebg image-dark d-flex h-100">
              <img src="{{asset('img/deals/home-2.jpg')}}" class="card-img">
            </div>

          </div><!-- end card -->
        </div><!-- end confirm cta -->
      </div><!-- end row -->
    </div>
  </section>

  <!-- Articles -->
  <section class="bg-secondary home-news">
    <div class="container">
      <div class="row mb-4">
        <div class="col-6 col-md-6 col-lg-6 col-xl-6">
          <h5 class="type-bold">In the news</h5>
        </div>
        <div class="col-6 col-md-6 col-lg-6 col-xl-6 text-right">
          <a href="">Browse Articles <span class="color-dark ml-2"><i class="fas fa-angle-right"></i></span></a>
        </div>
      </div>
      <div class="home-news-carousel owl-carousel owl-theme" data-items="4" data-margin="15"
        data-items-mobile-portrait="1" data-items-tablet-portrait="2" data-loop="true" data-autoplay="true"
        data-autoplay-timeout="3000">

        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/deals/article-1.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Alex Obigdu</span></small>
                </p>
                <p class="card-title">Ultimate Guide to at Home Deals</p>
              </div>
            </div>
          </a>
        </div>
        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/deals/article-2.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Uzo Ajawi</span></small>
                </p>
                <p class="card-title">Father’s Day Gift Guide 2020</p>
              </div>
            </div>
          </a>
        </div>
        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/deals/article-3.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Martin Andrews</span></small>
                </p>
                <p class="card-title">Cut The Chase And Grow Your Business</p>
              </div>
            </div>
          </a>
        </div>
        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/deals/article-4.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Kelly Abudhi</span></small>
                </p>
                <p class="card-title">Save More, Make More At The ConnectNigeria Marketplace</p>
              </div>
            </div>
          </a>
        </div>

      </div>
    </div>
  </section>
  <!-- end Articles section -->

  <section class="p-0">
    <div class="row no-gutters">
      <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="imagebg height-50 image-dark">
          <div class="background-image-holder">
            <img src="img/cars/footer-banners-1.jpg">
          </div>
          <div class="container pos-vertical-center h-100">
            <div class="cta-banner-footer">
              <div class="row w-100 mb-2">
                <div class="col-md-10 col-lg-10 col-xl-10">
                  <h2 class="title type-bold mb-2">Reach your target market faster </h2>
                  <p>with just <strong>N10,000</strong></p>
                </div>
              </div>
              <div class="d-flex mb-3">
                <div class="cta-stats mr-3">
                  <img src="img/businesses/window-pointer.png')}}">
                  <div>
                    <p class="type-medium mb-0">250,000+</p>
                    <small>Daily Visitors</small>
                  </div>
                </div>
                <div class="cta-stats">
                  <img src="img/businesses/briefcase.png')}}">
                  <div>
                    <p class="type-medium mb-0">500,000+</p>
                    <small>Business Listings</small>
                  </div>
                </div>
              </div>
              <div class="d-flex">
                <a href="#" class="btn btn-lg btn-primary-1 mr-4">
                  Advertise with us
                </a>
                <div class="">
                  <p class="mb-0">Call <span class="type-medium">0700-800-5000</span></p>
                  <small>grow@connectnigeria.com</small>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="imagebg height-50 image-dark">
          <div class="background-image-holder">
            <img src="img/deals/footer-banners-2.jpg">
          </div>
          <div class="container pos-vertical-center h-100">
            <div class="cta-banner-footer-2 d-flex">
              <div class="row align-items-center w-100 mb-2">
                <div class="col-md-8 col-lg-8 col-xl-8">
                  <h2 class="title type-bold mb-4">Be the first to know what’s happening</h2>
                  <p class="mb-4">Sign up to our newsletter get interesting news and updates delivered right to your inbox</p>
                  <a href="#" class="btn btn-lg btn-primary-1 mr-4">
                    Subscribe Now
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('includes.footer')
</div>
@endsection