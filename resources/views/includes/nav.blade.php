<div class="nav-container navbar-absolute">
    <nav class="navbar navbar-expand-lg" id="main-menu">
        <div class="container">
            <div class="row align-items-center w-100 flex-1">
                <div class="col-12 d-flex justify-content-between hidden-lg hidden-md">
                    <a class="navbar-brand hidden-lg hidden-md" href="{{ route('site.index') }}">
                        <img src="{{ asset('img/cn-logo-light-2.png') }}" class="logo">
                    </a>
                    <button
                        class="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <i class="fas fa-bars"></i>
                    </button>
                </div>
                <div class="col-12 col-lg-8 col-xl-8 p-0 pr-lg-10 pl-lg-10">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="{{ route('site.index') }}">
                                    Browse ConnectNigeria
                                </a>
                            </li>
                            @include('components.top-bar-menu')
                            <a href="{{ route('login') }}"
                               class="nav-mobile-btn bg-primary-1 mt-4 text-center hidden-lg hidden-md">Sign In</a>
                            <a href="{{ route('user.business.listings.create') }}" class="nav-mobile-btn bg-primary text-center hidden-lg hidden-md">List Now</a>

                        </ul>
                    </div>
                </div>
                @auth
                    <div class="col-lg-4 col-xl-4 text-right text-center-xs hidden-xs hidden-sm">
                        <ul class="navbar-nav justify-content-end">
                            <li class="nav-item active">
                                <a class="nav-link" href="{{ route('user.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a
                                    class="nav-link btn btn-sm btn-primary"
                                    href="{{ route('logout') }}"
                                    onclick="
                                        event.preventDefault();
                                        document.getElementById('logout-form').submit();
                                    "
                                >
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                @else
                    <div class="col-lg-4 col-xl-4 text-right text-center-xs hidden-xs hidden-sm">
                        <ul class="navbar-nav justify-content-end">
                            <li class="nav-item active">
                                <a class="nav-link" href="{{ route('login') }}">Sign In</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn btn-sm btn-primary" href="{{ route('user.business.listings.create') }}">List Now</a>
                            </li>
                        </ul>
                    </div>
                @endauth
            </div>
        </div>
    </nav>
</div>
