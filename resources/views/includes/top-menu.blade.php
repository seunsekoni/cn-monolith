<nav id="top-menu" class="navbar navbar-light bg-light">
    <div class="container">
        <div class="row w-100">
            <div class="col-5 col-md-6 col-lg-5 col-xl-5">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="{{ route('site.index') }}" class="type-uppercase">Back to home</a>
                    </li>
                </ul>
            </div>
            <div class="col-7 col-md-6 col-lg-7 col-xl-7">
                {{-- <ul class="navbar-nav justify-content-end">
                    <li class="nav-item hidden-xs hidden-sm">
                        <label class="type-uppercase">Browse CN:</label>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('businesses.index') }}" exact class="type-uppercase">
                            <i class="far fa-building"></i><span class="item">Businesses</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('cars.index') }}" exact class="type-uppercase">
                            <i class="fas fa-car-side"></i> <span class="item">Cars</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('deals.index') }}"  href="#" class="type-uppercase">
                            <i class="fas fa-tag"></i> <span class="item">Deals</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('events.index') }}" class="type-uppercase">
                            <i class="far fa-calendar-alt"></i> <span class="item">Events</span>
                        </a>
                    </li>
                    <li  class="nav-item">
                        <a href="{{ route('realestate.index') }}" class="type-uppercase">
                            <i class="fas fa-home"></i> <span class="item">Real Estate</span>
                        </a>
                    </li>
                    <li  class="nav-item">
                        <a href="{{ route('jobs.index') }}" class="type-uppercase">
                            <i class="fas fa-briefcase"></i> <span class="item">Jobs</span>
                        </a>
                    </li>
                </ul> --}}
            </div>
        </div>
    </div>
</nav>