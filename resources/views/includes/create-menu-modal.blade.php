<div class="modal fade" id="createMenuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('admin.menus.store')}}" id="create-menu">
                    @csrf
                    <input type="hidden" name="model_id" id="modelId">
                    @if ($is_child_menu && isset($menu))
                        <input type="hidden" name="parent_id" value="{{$menu->id}}">
                    @endif
                    <div class="form-group">
                        <label for="title">Title<span class="text-danger sup">*</span></label>
                        <input type="text" name="title" id="title"
                               value="{{old('title')}}" class="form form-control">
                        <small class="form-text text-muted">This field represents the title of the menu.</small>
                    </div>
                        <div
                            class="form-group justify-content-center custom-control custom-checkbox">
                            <input type="checkbox" name="is_model" id="is_page" value={{true}}
                                class="custom-control-input" {{ old('is_model') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="is_page">
                                Is this a page?
                            </label>
                            <small class="form-text text-muted">Do you want to create this menu for a page or for an external link</small>
                        </div>
                        <div class="form-group">
                            <div id="link-div">
                                <label for="link">Link<span class="text-danger sup">*</span></label>
                                <input type="url" name="link" id="link"
                                       value="{{old('link')}}" class="form form-control">
                                <small class="form-text text-muted">This field captures the link that is to be
                                    redirect with</small>
                            </div>
                            <div id="model-div">
                                <label for="model">Model<span class="text-danger sup">*</span></label>
                                <select class="form form-control" name="model" id="model">
                                    <option selected disabled="true">Select Model</option>
                                    <option value="{{\App\Enums\MenuModels::CATEGORY}}">Category</option>
                                    <option value="{{\App\Enums\MenuModels::PAGE}}">Page</option>
                                </select>
                                <small class="form-text text-muted">select the model the wish to attach this menu
                                    with</small>
                            </div>
                        </div>
                    <div>
                        <div class="form-group" id="page">
                            <label for="page">Pages<span class="text-danger sup">*</span></label>
                            <select name="page" id="page" class="form form-control">
                                <option selected disabled="true">Select Page</option>
                                @foreach($pages as $page)
                                    <option value="{{$page->id}}">{{$page->title}}</option>
                                @endforeach
                            </select>
                            <small class="form-text text-muted">select the page you want to attach to this menu</small>
                        </div>

                        <div class="form-group" id="category">
                            <label for="category">Categories<span class="text-danger sup">*</span></label>
                            <select name="category" id="category" class="form form-control">
                                <option selected disabled="true">Select Category</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                            <small class="form-text text-muted">select the page you want to attach to this menu</small>
                        </div>
                    </div>
                        <div class="form-group">
                            <label for="">Position On Page<span class="text-danger sup">*</span></label><br>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="position" id="top" class="form-check-input" value="top">
                                    <label class="form-check-label" for="top">Top</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="position" id="bottom" class="form-check-input" value="bottom">
                                    <label class="form-check-label" for="bottom">Bottom</label>
                                </div>

                            <small class="form-text text-muted">choose menu position</small>

                        </div>
                        <div class="form-group">
                            <label for="target">Target<span class="text-danger sup">*</span></label>
                            <select name="target" id="target" class="form form-control">
                                <option selected disabled="true">Select Target</option>
                                <option value="_blank">Open in a new tab</option>
                                <option value="_self">Open in the current tab</option>
                                <option value="_top">Open in another window</option>
                                <option value="_parent">Open in the current frame</option>
                            </select>
                            <small class="form-text text-muted">define the link behavior</small>
                        </div>
                    <div
                        class="form-group custom-control custom-checkbox">
                        <input type="checkbox" name="status" id="status"
                               class="custom-control-input" {{ old('status') ? 'checked' : '' }}>
                        <label class="custom-control-label" for="status">
                            Status
                        </label>
                        <small class="form-text text-muted">define menu status</small>

                    </div>
                    <div class="form-group">
                        <label class="" for="order">Order</label>
                        <input type="number" name="order" id="order"
                               class="form form-control">

                        <small class="form-text text-muted">define menu order</small>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" form="create-menu">Create Menu</button>
            </div>
        </div>
    </div>
</div>
@once
    @push('scripts')
        <script>
            $(document).ready(() => {
                manageIsPageToggle();
                manageModelToggle()
            })

            let modelId = $('#modelId')


            const manageIsPageToggle = () => {
                const is_page = $("#is_page");
                $("#model-div").hide();

                is_page.change(() => {
                    const is_page_selected = is_page.is(":checked");
                    if (is_page_selected) {
                        $("#model-div").show();
                        $("#link-div").hide();
                    } else {
                        $("#model-div").hide();
                        $("#link-div").show();
                    }
                });
            }

            const manageModelToggle = () => {
                const category = $("#category");
                const page = $("#page");
                category.hide();
                page.hide();
                $("#model").change((el) => {
                    switch (el.target.value) {
                        case "category":
                            category.show();
                            modelId.val();
                            category.change(() => {
                                let selected = category.find('option:selected').val()
                                modelId.val(selected)
                            })
                            page.hide();
                            break;
                        case "page":
                            category.hide();
                            page.show();
                            page.change(() => {
                                let selected = page.find('option:selected').val()
                                modelId.val(selected)
                            })
                            break;
                        default:
                            category.hide();
                            page.hide();
                    }
                })
            }
        </script>
    @endpush
@endonce
