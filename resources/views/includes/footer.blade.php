<footer>
    <div class="container">
        <div class="row">
            <div class="col-6 col-md-6 col-lg-3 col-xl-3">
                <div>
                    <img src="{{asset('img/cn-logo-dark.png')}}" class="logo">
                </div>
                <p><small class="type-print">26A, Awori Road, Off Corporation Drive, Dolphin Estate Ikoyi, Lagos Nigeria</small></p>
                <p class="mb-0"><small class="type-print">P: 0809 800 5000</small></p>
                <p><small class="type-print">E: businesses@connectnigeria.com</small></p>
            </div>
            <div class="col-6 col-md-6 col-lg-9 col-xl-9">
                <div class="row">
                    @foreach($footerMenuList as $footer)
                    <div class="col-6 col-md-6 col-lg-3 col-xl-{{ floor(12/$footerMenuList->count()) }}">
                        <ul>
                            <li>
                                <span class="color-primary-1 type-medium">{{ $footer->title }}</span>
                            </li>
                            @foreach($footer->children as $menu)
                            <li>
                                @if($menu->model == \App\Enums\MenuModels::CATEGORY)
                                <a href="{{ route('categories.index', ['category' => $menu->category]) }}" target="{{ $menu->target }}" class="type-bold">{{ $menu->title }}</a>
                                @elseif($menu->model == \App\Enums\MenuModels::PAGE)
                                <a href="{{ route('pages.show', ['page' => $menu->model_id]) }}" target="{{ $menu->target }}" class="type-bold">{{ $menu->title }}</a>
                                @else
                                <a href="{{ $menu->link }}" target="{{ $menu->target }}" class="type-bold">{{ $menu->title }}</a>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-left text-center-xs mb-2 mb-lg-0 mb-xl-0">
                <small>Copyright {{ now()->year }}. All Rights Reserved by ConnectNigeria.com</small>
            </div>
            <div class="col-md-5 text-center mb-2 mb-lg-0 mb-xl-0">
                <span><small class="type-uppercase color-orange type-bold">We Are Nigerians</small></span>
                <span><small class="type-uppercase color-primary type-bold">Who love Nigeria</small></span>
            </div>
            <div class="col-md-3 text-center-xs text-right mb-2 mb-lg-0 mb-xl-0">
                <ul class="list-horizontal social">
                    <li>
                        <a target="_blank" href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjI9t7Bz8fvAhVOyxoKHVtMCdwQFjAMegQICBAD&url=https%3A%2F%2Fwww.facebook.com%2FConnectNigeria%2F&usg=AOvVaw0DM2UI1maeQzq-JR2asXXq" class="social-facebook">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a target="_blank" href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjI9t7Bz8fvAhVOyxoKHVtMCdwQ6F56BAgFEAI&url=https%3A%2F%2Ftwitter.com%2FConnectNigeria%3Fref_src%3Dtwsrc%255Egoogle%257Ctwcamp%255Eserp%257Ctwgr%255Eauthor&usg=AOvVaw1x9ksHUL3QnOwSF3Jz0Ha2" class="social-twitter">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a target="_blank" href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjI9t7Bz8fvAhVOyxoKHVtMCdwQFjAPegQIChAD&url=https%3A%2F%2Fwww.instagram.com%2Fconnectnigeria%2F%3Fhl%3Den&usg=AOvVaw0mU4GyKC7gQPsKADngiMOV" class="social-instagram">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#" class="social-whatsapp">
                            <i class="fab fa-whatsapp"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>