
<div class="row">
    <div class="col-md-12 mb-3">
        <div class="bordered card">
            <div class="card-body">
                <h6 class="color-primary-1 type-bold">Featured Businesses</h6>

                <ul class="arrow mt-3">
                    @forelse($featuredBusinesses as $business)
                        <li>
                            <a href="{{ route('businesses.show', ['business' => $business] ) }}" class="color-dark">
                                {{ $business->name }}
                            </a>
                        </li>
                    @empty
                        <li>There are currently no featured businesses</li>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
</div>