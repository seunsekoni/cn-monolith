<div class="form-group search-bar">
    <input
        type="text"
        class="form-control"
        name="query"
        placeholder="products, services or business"
        value="{{ request()->get('query') }}"
    >
</div>

<hr>

<div class="form-group search-bar">
    <p class="font-weight-bold">Filter by Location</p>

    <input
        type="text"
        class="form-control mb-1"
        name="street"
        id="street"
        placeholder="Allen Avenue"
        value="{{ request()->get('street') }}"
    >

    <input
        type="text"
        class="form-control"
        name="city"
        id="city"
        placeholder="Lagos, Nigeria"
        value="{{ request()->get('city') }}"
    >
</div>

<div class="form-group search-bar">
    <p class="font-weight-bold">Filter by Distance</p>

    @foreach (\App\Enums\Distance::getInstances() as $distances => $body)
        <div class="form-check">
            <input
                class="form-check-input"
                type="radio"
                name="distance"
                id="kmdistance{{ $body->value }}"
                value="{{ $body->value }}"
                {{ ($distance == $body->value) ? 'checked' : '' }}
            >
            <label class="form-check-label" for="kmdistance{{ $body->value }}">
                Within {{ $body->description }}
            </label>
        </div>
    @endforeach
</div>

<hr>

<div class="form-group search-bar">
    <div class="">
        <p class="font-weight-bold">Filter by Category</p>

        <select
            name="category"
            id="search_category"
            class="select2 form-control"
        ></select>
    </div>
</div>

<div class="form-group search-bar">
    <button type="submit" class="btn btn-primary">
        <i class="fas fa-filter mr-2"></i>
        Filter
    </button>
</div>

@push('after-scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.geocode_api_key') }}&libraries=places"></script>

    <script>
        $(document).ready(function () {
            $('#search_category.select2').select2({
                placeholder: 'Search a category',
                allowClear: true,
                ajax: {
                    url: "{{ route('api.categories.search') }}",
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    processResults: function (response) {
                        return {
                            results: response.data.categories.map(category => {
                                return {
                                    id: category.name,
                                    text: category.name
                                }
                            })
                        };
                    }
                },
                templateSelection: function (data) {
                    return data.text;
                }
            });

            const autocomplete = new google.maps.places.Autocomplete(document.querySelector("#street"), {
                componentRestrictions: { country: "ng" },
                fields: ["address_components", "geometry"],
                types: ["address"],
            });
            autocomplete.addListener("place_changed", fillInAddress);

            function fillInAddress() {
                // Get the place details from the autocomplete object.
                const place = autocomplete.getPlace();
                let street = "";
                let city = "";

                // Get each component of the address from the place details,
                // and then fill-in the corresponding field on the form.
                // place.address_components are google.maps.GeocoderAddressComponent objects
                // which are documented at http://goo.gle/3l5i5Mr
                for (const component of place.address_components) {
                    const componentType = component.types[0];

                    switch (componentType) {
                        case "route":
                            street = component.long_name;
                            break;

                        case "locality":
                            city = `${component.long_name}, `;
                            break;

                        case "country":
                            city += component.long_name;
                            break;
                    }
                }

                $('#street').val(street);
                $('#city').val(city);
            }
        });

        let category = "{{ request()->get('category') }}";
        if (category) {
            getCategory(category);
        }

        function getCategory(name) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: `{{ route('api.categories.search') }}/?id=${name}`,
                type: 'get',
                success: function (response) {
                    let category = response.data?.category

                    var option = new Option(category?.name, category?.name, true, true);
                    $('#search_category.select2').append(option);

                    // manually trigger the `select2:select` event
                    $('#search_category.select2').trigger({
                        type: 'select2:select',
                        params: {
                            data: category
                        }
                    });
                },
                error: function (error) {
                    let response = JSON.parse(error.responseText)
                    alert(response.message)
                }
            });
        }
    </script>
@endpush