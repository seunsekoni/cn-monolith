<div class="modal fade" id="requestQuoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Request Quote</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" id="requestQuoteForm">
                    <div style="display: none" class="alert alert-danger" id="errorMesg"></div>
                    <div style="display: none" class="alert alert-success" id="successMesg"></div>
                    <div class="form-group">
                        <label for="first_name">First Name<span class="text-danger sup">*</span></label>
                        <input type="text" name="first_name" class="form-control" id="first_name">
                        <small class="form-text text-muted">kindly input your first name.</small>
                    </div>

                    <div class="form-group">
                        <label for="last_name">Last Name<span class="text-danger sup">*</span></label>
                        <input type="text" name="last_name" class="form-control" id="last_name">
                        <small class="form-text text-muted">kindly input your last name.</small>
                    </div>

                    <div class="form-group">
                        <label for="userEmail">Email<span class="text-danger sup">*</span></label>
                        <input type="text" name="email" class="form-control" id="email">
                        <small class="form-text text-muted">kindly input your email.</small>
                    </div>

                    <div class="form-group">
                        <label for="phone">Phone Number<span class="text-danger sup">*</span></label>
                        <input type="text" name="phone" class="form-control" id="phone">
                        <small class="form-text text-muted">kindly input your phone number.</small>
                    </div>

                    <div class="form-group">
                        <label for="listingInput">Listing<span class="text-danger sup">*</span></label>
                        <select class="form form-control" name="listing_id" id="listing_id">

                        </select>
                        <small class="form-text text-muted">Select the listing that you will associate this Quote
                            Request for.
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="quote-desc">Quote Description</label>
                        <textarea class="form-control" name="quote-desc" id="quote-desc" cols="30"
                                  rows="10"></textarea>
                    </div>
                    <small class="form-text text-muted">Add note for Business Owner</small>
                </form>
            </div>
            <div class="modal-footer justify-content-lg-start">
                <button type="submit" id="submitRequestQuoteForm" form="requestQuoteForm" class="btn btn-primary">Submit
                    Request
                </button>
            </div>
        </div>
    </div>
</div>
@once
    @push('after-scripts')
        <script type="text/javascript">
            $("#requestQuoteForm").submit((e) => {
                e.preventDefault()

                let requestQuoteFormData = new FormData();
                requestQuoteFormData.append('first_name', $("#first_name").val());
                requestQuoteFormData.append('last_name', $("#last_name").val());
                requestQuoteFormData.append('phone', $("#phone").val());
                requestQuoteFormData.append('email', $("#email").val());
                requestQuoteFormData.append('quote-desc', $("#quote-desc").val());
                requestQuoteFormData.append('listing_id', $("#listing_id").val());

                const url = "{{route('request_quote', ['listing' => ':listing_id'])}}";
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url.replace(':listing_id', $("#listing_id").val()),
                    processData: false,
                    contentType: false,
                    type: "POST",
                    data: requestQuoteFormData,
                    success: ({message}) => {
                        $("#successMesg").empty();
                        $("#successMesg").show();
                        $("#errorMesg").hide();


                         $("#successMesg").append(`<p style="color: inherit">${message}</p>`)

                        setTimeout(function(){
                            $("#requestQuoteModal").modal("hide");
                        }, 5000);
                    },
                    error: ({responseText}) => {
                        $("#errorMesg").empty();
                        const errors = JSON.parse(responseText).errors;

                        const errorMesg = Object.entries(errors).map((error) => {
                            return (error)[1].length && `<p style="color: inherit">${error[0]}:${Object.values(error)[1]}<p>`
                        })

                        $("#successMesg").hide();
                        $("#errorMesg").show();

                         $("#errorMesg").append(errorMesg)

                    }
                })
            })
        </script>
    @endpush
@endonce
