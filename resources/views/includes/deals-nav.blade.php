<nav class="navbar navbar-expand-lg sticky-top" id="main-menu">

  <div class="container">
    <div class="row align-items-center w-100 flex-1">
      <div class="col-12 col-lg-3 col-xl-3 d-flex justify-content-between">
        <a class="navbar-brand" href="#">
          <img src="{{ asset('img/deals/deals-cn-logo.png') }}" class="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <i class="fas fa-bars"></i>
        </button>
      </div>
      <div class="col-12 col-lg-6 col-xl-6 p-0 pr-lg-10 pl-lg-10">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="#">Browse Deals <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Blogs</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Advertise</a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                More on CN
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Top100</a>
                <a class="dropdown-item" href="#">CNTV</a>
                <a class="dropdown-item" href="#">Articles</a>
                <a class="dropdown-item" href="#">Writer's Conference</a>
                <a class="dropdown-item" href="#">Business Mixer</a>
                <a class="dropdown-item" href="#">CN Bizfair</a>
                <a class="dropdown-item" href="#">Club Connect</a>
              </div>
            </li>
            <a href="{{ route('login') }}" class="nav-mobile-btn bg-primary-1 mt-4 text-center hidden-lg hidden-md">
              Sign In
            </a>
            <a href="#" class="nav-mobile-btn bg-primary text-center hidden-lg hidden-md">
              List a Deal
            </a>
          </ul>

        </div>
      </div>
      <div class="col-lg-3 col-xl-3 text-right text-center-xs hidden-xs hidden-sm">
        <ul class="navbar-nav justify-content-end">
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('login') }}">Sign In</a>
          </li>
          <li class="nav-item">
            <a class="nav-link btn btn-sm btn-primary" href="#">List a Deal</a>
          </li>
        </ul>
      </div>
    </div>
  </div>

</nav>