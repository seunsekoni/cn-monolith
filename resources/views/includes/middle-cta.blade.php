<?php
    // Fed by the MiddleCallToActionCompser class
    $call_to_actions = $middle_cta->where('category_id', $category ?? null);
?>

<div class="container">
    <!-- CTAs -->
    <div class="row">
    <!-- instantiate counter to be zero -->
    @php
        $counter = 0;
    @endphp
    @foreach($call_to_actions as $action)
    <!-- select only actions that has position to be middle -->
        @if($action->position == "middle")
        <!-- increment counter to accept only two items -->
        @php
        $counter++;
        @endphp
        <!-- select only two items -->
        @if($action == $call_to_actions->first())
            <div class="col-md-7 col-lg-7 col-xl-7 mb-3 mb-lg-0 mb-xl-0">
                <div class="card home-card-ctas ">
                    <div class="row no-gutters">

                    <div class="col-md-8 col-lg-8 col-xl-8">
                        <div class="card-body border-tl-radius border-bl-radius bg-primary">
                        <h3 class="card-title">{{ $action->title }}</h3>
                        <p>{{ $action->description }}</p>
                        <a href="{{ $action->url }}" class="btn btn-{{ $action->color }}">{{ $action->button_text }}</a>
                        </div>
                    </div><!-- end col -->

                    <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="imagebg h-100 second">
                        <div class="background-image-holder border-tr-radius border-br-radius">
                            <img src="{{$action->backdrop}}" alt="">
                        </div>
                        </div>
                    </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end card -->
            </div><!-- end quote cta -->
        @else
            <div class="col-md-5 col-lg-5 col-xl-5">
            <div class="card home-card-ctas h-100 ">
                <div class="imagebg image-light">
                <div class="background-image-holder border-radius-5">
                    <img src="{{ $action->image }}" class="card-img">
                </div>
                <div class="card-img-overlay card-body">
                    <div class="row">
                    <div class="col-8 col-lg-7">
                        <h3 class="card-title">{{ $action->title }}</h3>
                        <p class="card-text">{{ $action->description }}</p>
                        <a href="{{ $action->url }}" class="btn btn-{{ $action->color }}">{{ $action->button_text }}</a>
                    </div>
                    </div>
                </div>
                </div>

            </div><!-- end card -->
            </div><!-- end confirm cta -->
        @endif
        @if($counter > 2)
            @break
            @endif
        @endif
    @endforeach
    </div><!-- end row -->
</div>
