<section class="pb-0">
	<div class="container">
	  <div class="row">
		<div class="col-12">
		  <div
			class="category-carousel owl-carousel owl-theme"
			data-items="10"
			data-margin="10"
			data-items-mobile-portrait="3"
			data-loop="true"
			data-autoplay="true"
			data-autoplay-hover-pause="true"
			data-autoplay-timeout="3000"
		  >
			@foreach($categoryIconList as $category)
				<div class="item text-center">
					<a href="{{ route('categories.index', ['category' => $category]) }}" class="text-dark">
						<img src="{{ $category->icon }}" alt="icon" class="category-slider">
						<span class="type-bold">{{ $category->name }}</span>
					</a>
				</div>
			@endforeach
		  </div>
		</div>
	  </div>
	</div>
</section>
