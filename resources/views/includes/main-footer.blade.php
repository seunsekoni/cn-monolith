<footer class="home-footer">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <hr class="hr-light hidden-xs hidden-sm">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 text-left text-center-xs mb-2 mb-lg-0 mb-xl-0">
        <small class="color-white">Copyright 2020. All Rights Reserved by ConnectNigeria.com</small>
      </div>
      <div class="col-md-5 text-center mb-2 mb-lg-0 mb-xl-0">
        <span><small class="type-uppercase color-orange type-bold">We Are Nigerians</small></span>
        <span><small class="type-uppercase color-primary type-bold">Who love Nigeria</small></span>
      </div>
      <div class="col-md-3 text-center-xs text-right mb-2 mb-lg-0 mb-xl-0">
        <ul class="list-horizontal social">
          <li>
            <a target="_blank" href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjI9t7Bz8fvAhVOyxoKHVtMCdwQFjAMegQICBAD&url=https%3A%2F%2Fwww.facebook.com%2FConnectNigeria%2F&usg=AOvVaw0DM2UI1maeQzq-JR2asXXq" class="social-facebook">
              <i class="fab fa-facebook-f"></i>
            </a>
            <a target="_blank" href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjI9t7Bz8fvAhVOyxoKHVtMCdwQ6F56BAgFEAI&url=https%3A%2F%2Ftwitter.com%2FConnectNigeria%3Fref_src%3Dtwsrc%255Egoogle%257Ctwcamp%255Eserp%257Ctwgr%255Eauthor&usg=AOvVaw1x9ksHUL3QnOwSF3Jz0Ha2" class="social-twitter">
              <i class="fab fa-twitter"></i>
            </a>
            <a target="_blank" href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjI9t7Bz8fvAhVOyxoKHVtMCdwQFjAPegQIChAD&url=https%3A%2F%2Fwww.instagram.com%2Fconnectnigeria%2F%3Fhl%3Den&usg=AOvVaw0mU4GyKC7gQPsKADngiMOV" class="social-instagram">
              <i class="fab fa-instagram"></i>
            </a>
            <a href="#" class="social-whatsapp">
              <i class="fab fa-whatsapp"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>