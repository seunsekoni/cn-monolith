<nav id="top-menu-inner" class="navbar navbar-light bg-light">
    <div class="container">
        <div class="row w-100">
            <div class="col-5 visible-xs visible-sm">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="type-uppercase">CN Home</a>
                    </li>
                </ul>
            </div>

            <div class="col-5 col-md-6 col-lg-5 col-xl-5 hidden-sm hidden-xs">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="type-uppercase">CN Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="type-uppercase">Browse Businesses</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="type-uppercase">Advertise</a>
                    </li>
                    
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="innerTopDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        More on CN
                        </a>
                        <div class="dropdown-menu" aria-labelledby="innerTopDropdown">
                            <a class="dropdown-item" href="#">Top100</a>
                            <a class="dropdown-item" href="#">CNTV</a>
                            <a class="dropdown-item" href="#">Articles</a>
                            <a class="dropdown-item" href="#">Writer's Conference</a>
                            <a class="dropdown-item" href="#">Business Mixer</a>
                            <a class="dropdown-item" href="#">CN Bizfair</a>
                            <a class="dropdown-item" href="#">Club Connect</a>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="col-7 col-md-6 col-lg-7 col-xl-7">
                <ul class="navbar-nav justify-content-end">
                    <li class="nav-item hidden-xs hidden-sm">
                        <label class="type-uppercase">Browse CN:</label>
                    </li>
                    <li class="nav-item">
                    <a href="http://businesses.connectnigeria.test" exact class="type-uppercase">
                            <i class="far fa-building"></i><span class="item">Businesses</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="http://cars.connectnigeria.test" exact class="type-uppercase"><i class="fas fa-car-side"></i> <span class="item">Cars</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="http://deals.connectnigeria.test"  href="#" class="type-uppercase"><i class="fas fa-tag"></i> <span class="item">Deals</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="http://events.connectnigeria.test" class="type-uppercase"><i class="far fa-calendar-alt"></i> <span class="item">Events</span></a>
                    </li>
                    <li  class="nav-item">
                        <a href="http://realestate.connectnigeria.test" class="type-uppercase"><i class="fas fa-home"></i> <span class="item">Real Estate</span></a>
                    </li>
                    <li  class="nav-item">
                        <a href="http://jobs.connectnigeria.test" class="type-uppercase"><i class="fas fa-briefcase"></i> <span class="item">Jobs</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

{{-- <nav id="top-menu" class="navbar navbar-light bg-light">
    <div class="container">
        <div class="row w-100">
            <div class="col-5 col-md-6 col-lg-5 col-xl-5">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="http://connectnigeria.test" class="type-uppercase">Back to home</a>
                    </li>
                </ul>
            </div>
            <div class="col-7 col-md-6 col-lg-7 col-xl-7">
                <ul class="navbar-nav justify-content-end">
                    <li class="nav-item hidden-xs hidden-sm">
                        <label class="type-uppercase">Browse CN:</label>
                    </li>
                    <li class="nav-item">
                    <a href="http://businesses.connectnigeria.test" exact class="type-uppercase">
                            <i class="far fa-building"></i><span class="item">Businesses</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="http://cars.connectnigeria.test" exact class="type-uppercase"><i class="fas fa-car-side"></i> <span class="item">Cars</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="http://deals.connectnigeria.test"  href="#" class="type-uppercase"><i class="fas fa-tag"></i> <span class="item">Deals</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="http://events.connectnigeria.test" class="type-uppercase"><i class="far fa-calendar-alt"></i> <span class="item">Events</span></a>
                    </li>
                    <li  class="nav-item">
                        <a href="http://realestate.connectnigeria.test" class="type-uppercase"><i class="fas fa-home"></i> <span class="item">Real Estate</span></a>
                    </li>
                    <li  class="nav-item">
                        <a href="http://jobs.connectnigeria.test" class="type-uppercase"><i class="fas fa-briefcase"></i> <span class="item">Jobs</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav> --}}