<?php
  $call_to_actions = $bottom_cta->where('category_id', $category ?? null)
?>

<section class="p-0">
  <div class="row no-gutters">
    @foreach($call_to_actions as $bottomAction)
      @if($bottomAction->position == "bottom")
        <div class="col-md-6 col-lg-6 col-xl-6">
          <div class="imagebg height-50 image-light">
            <div class="background-image-holder">
              <img src="{{ $bottomAction->backdrop }}">
            </div>
            <div class="container pos-vertical-center h-100">
              <div class="cta-banner-footer-2 d-flex">
                <div class="row align-items-center w-100 mb-2">
                  <div class="col-md-8 col-lg-8 col-xl-8">
                    <h2 class="title type-bold mb-4">{{ $bottomAction->title }}</h2>
                    <p class="mb-4">{{ $bottomAction->description }}</p>
                    <a href="{{ $bottomAction->url }}" target="_blank" class="btn btn-lg btn-{{ $bottomAction->color }}-1 mr-4">
                      {{ $bottomAction->button_text }}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endif
    @endforeach
  </div>
</section>
