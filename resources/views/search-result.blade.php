@extends('layouts.search')

@section('title', 'Search Results')

@section('content')
    @php
        $totalResults = $data->total();
    @endphp
    @include('includes.request-quote-modal')
    <section class="bg-secondary">
        <div class="container">
            <form action="{{ route('search') }}" id="filterForm" method="GET">
                <div class="row">
                    @if (!(new \Jenssegers\Agent\Agent())->isMobile())
                        <div class="col-md-3 hidden-xs hidden-sm">
                            <div class="search-side-filter">
                                @include('includes.search-params')
                            </div>
                        </div>
                    @endif

                    <div class="col-md-7">
                        {{-- For smaller screens --}}
                        @if ((new \Jenssegers\Agent\Agent())->isMobile())
                            <div class="visible-xs visible-sm mb-4">
                                <button
                                    class="callout filter-collapse"
                                    type="button"
                                    data-toggle="collapse"

                                    data-target="#filter-search"
                                    aria-expanded="false"
                                    aria-controls="filter-search"
                                >
                                    <i class="fas fa-filter"></i>
                                    Filter Search
                                </button>
                                <div class="collapse" id="filter-search">
                                    @include('includes.search-params')
                                </div>
                            </div>
                        @endif

                        {{-- Main search results --}}
                        <div class="row mb-3">
                            <div class="col-12 col-md-8 text-justify">
                                <small class="type-bold">
                                    All results ({{ $totalResults }})

                                    @if($keyword)
                                        for <span class="text-primary">'{{ $keyword }}'</span>
                                    @endif

                                    @if($location)
                                        at <span class="text-primary">'{{ $location }}'</span>
                                    @endif

                                    @if($distance)
                                        within <span class="text-primary">'{{ \App\Enums\Distance::getDescription($distance) }}'</span>
                                    @endif

                                    found in
                                    <span class="text-primary">'{{ pluralize($type) }}'</span>
                                </small>
                                <div class="sortby">
                                    <small class="color-gray">Sort by</small>
                                    <select
                                        class="no-border-select input-field"
                                        name="sortby"
                                        onchange="document.getElementById('filterForm').submit();"
                                    >
                                        <option
                                            value="relevance"
                                            {{ $sortby == 'relevance' ? 'selected' : ''}}
                                        >
                                            Relevance
                                        </option>
                                        <option
                                            value="recommended"
                                            {{ $sortby == 'recommended' ? 'selected' : ''}}
                                        >
                                            Recommended
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <input type="hidden" name="type" value="{{ $type }}" id="pageType">

                                <ul class="nav nav-pills" id="modelTabs" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a
                                            class="nav-link {{ $type != 'listing' ? 'active' : '' }}"
                                            aria-current="page"
                                            href="#"
                                            onclick="event.preventDefault(); toggleModel('business');"
                                        >
                                            Businesses
                                        </a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a
                                            class="nav-link {{ $type == 'listing' ? 'active' : '' }}"
                                            aria-current="page"
                                            href="#"
                                            onclick="event.preventDefault(); toggleModel('listing');"
                                        >
                                            Listings
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        @foreach($data as $result)
                            <div class="row mb-3">
                                <div class="col-md-12">
                                    <div class="bordered card results-card-horizontal">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <img
                                                        src="{{ $result->featured_image ?? $result->logo }}"
                                                        class="image-inherit mb-2"
                                                        alt="image"
                                                    >
                                                    <a
                                                        href="{{
                                                            route(pluralize($type) . '.show', [
                                                                singularize($type) => $result->slug
                                                            ])
                                                        }}"
                                                        class="btn btn-open mb-4 mb-lg-0"
                                                    >
                                                        OPEN
                                                    </a>
                                                </div>
                                                <div class="col-md-8">
                                                    <h5 class="type-bold title mb-2">
                                                        <span class="align-middle">
                                                            <a
                                                                class="text-decoration-none text-dark"
                                                                href="{{
                                                                    route(pluralize($type) . '.show', [
                                                                        singularize($type) => $result->slug
                                                                    ])
                                                                }}"
                                                            >
                                                                {{ $result->name }}
                                                            </a>
                                                        </span>
                                                        @if ($result->featured)
                                                            <span
                                                                class="badge bg-orange h6-size color-white align-middle type-normal">
                                                                FEATURED
                                                            </span>
                                                        @endif
                                                    </h5>
                                                    <div class="listing-indicators mb-2">
                                                        <div class="listing-rate d-inline-block">
                                                            @for ($i = 0; $i < 5; $i++)
                                                                <i
                                                                    class="{{ getAverageRating($result) <= $i ? 'far fa-star' : 'fas fa-star' }} font-small"
                                                                >
                                                                </i>
                                                            @endfor
                                                            <span>{{ $result->reviews()->count() }}</span>
                                                        </div>

                                                        <div class="listing-recommend">
                                                            <i class="fas fa-thumbs-up color-lime"></i>
                                                            <span>{{ $result->likes }}</span>
                                                        </div>
                                                        <div class="listing-connected">
                                                            <i class="fas fa-link color-lime"></i>
                                                            <span>{{ $result->shares }}</span>
                                                        </div>
                                                    </div>

                                                    <p class="mb-2">
                                                        <small class="color-gray">
                                                            {{ $result->businessType->name ?? $result->category->name }}
                                                        </small>
                                                    </p>

                                                    @if ($primaryLocation = ($result->primaryLocation ?? optional($result->business)->primaryLocation))
                                                        <div class="row mb-lg-4 mb-md-4 mb-3">
                                                            <div class="col-md-12 mb-2 mb-lg-0 mb-xl-0">
                                                                <div class="listing-address">
                                                                    <i class="fas fa-map-marker-alt color-lime mr-2"></i>
                                                                    <span class="font-small">
                                                                        {{ $location ?: $primaryLocation->full_address }}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif

                                                    @if ($contact = ($result->businessContact ?? optional($result->business)->businessContact))
                                                        <div>
                                                            <a
                                                                href="mailto:{{ $contact->email }}"
                                                                class="
                                                                btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2
                                                                {{ !$contact->email ? 'd-none' : '' }}
                                                                    "
                                                            >
                                                                <i class="far fa-envelope"></i> Email Us
                                                            </a>

                                                            <a
                                                                href="tel:{{ $contact->phone }}"
                                                                target="__blank"
                                                                class="
                                                                btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2
                                                                {{ !$contact->phone ? 'd-none' : '' }}
                                                                    "
                                                            >
                                                                <i class="fas fa-mobile"></i> Call Us
                                                            </a>
                                                            <button
                                                                type="button" data-toggle="modal"
                                                                data-target="#requestQuoteModal"
                                                                onclick="setBusinessOrListingId(this)"
                                                                class="
                                                                btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2
                                                                {{ !$contact->email ? 'd-none' : '' }}
                                                                    "
                                                                data-resource="{{$result}}"
                                                            >
                                                                <i class="far fa-envelope"></i> Request Quote
                                                            </button>

                                                            @if ($primaryLocation)
                                                                <a
                                                                    href="{{ $primaryLocation->website_url }}"
                                                                    target="__blank"
                                                                    class="
                                                                    btn btn-cta btn-light d-inline-block mb-lg-0 mb-xl-0 mb-2
                                                                    {{ $primaryLocation->website_url ? 'd-none' : '' }}
                                                                        "
                                                                >
                                                                    <i class="fas fa-mouse-pointer"></i> Visit Website
                                                                </a>
                                                            @endif
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        {{ $data->appends(request()->all())->links() }}
                    </div>

                    <div class="col-md-2">
                        @include('includes/featured-businesses')

                        <div class="row">
                            <div class="container">
                                <img src="img/businesses/ad-1.jpg" class="image-inherit mb-3">
                                <img src="img/businesses/ad-2.jpg" class="image-inherit mb-3">
                                <img src="img/businesses/ad-3.jpg" class="image-inherit mb-3">
                                <img src="img/businesses/ad-4.jpg" class="image-inherit mb-3">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

@push('after-scripts')
    <script type="text/javascript">
        $(window).on("scroll", function () {
            $(".search-side-filter").css({"position": "sticky", "top": "140px"})
        })

        function toggleModel(model) {
            let pageType = "{{ $type }}";

            if (pageType == model) {
                return;
            }

            $('#pageType').val(model);
            $('#filterForm').trigger('submit');
        }

        let listingId;
        /**
         * this is intended to save the id of business||listing
         * that has been selected
         * @param element
         */
        const setBusinessOrListingId = (element) => {
            const resource = JSON.parse(element.getAttribute('data-resource'));

            const businessId = resource.business_id ?? resource.id;
            listingId = resource.business_id ? resource.id : null;

            queryListingDetails(businessId);
        }

        /**
         * make a network request to fetch all listing
         * associated to a business
         * @param businessId
         */
        const queryListingDetails = (businessId) => {
            const url = "{{route('businesses_listings', ['business' => ":business_id"])}}";

            $.ajax({
                url: url.replace(':business_id', businessId),
                type: "get",
                success: ({data}) => {

                    // this is intended to enable or disable the submit peradventure the selected
                    // business has no listings under it. since the request cannot be processed
                    if (!data.length) {
                        $("#submitRequestQuoteForm").attr("disabled", true)
                    }else {
                        $("#submitRequestQuoteForm").attr("disabled", false)
                    }

                    // attach fetched listings to the select option
                    appendListingsToForm(data)
                }
            })
        }

        /**
         * append the listings to the select input
         * @param listings
         */
        const appendListingsToForm = (listings) => {
            $("#listing_id").empty();

            const selectOptions = listings.map((listing) => {
                return `<option ${listing.id === listingId ? "selected" : null} value=${listing.id}>${listing.name}</option>`
            })

            $("#listing_id").append(selectOptions)
        }
    </script>
@endpush
