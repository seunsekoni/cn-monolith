<div class="alert alert-{{ $type }}">
    <div class="font-weight-bold">{!! $message !!}</div>

    {!! $slot !!}
</div>
