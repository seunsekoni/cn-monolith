<div class="container-fluid">

    <!-- 404 Error Text -->
    <div class="text-center">
        <div style="font-size: 2rem !important" class="error mx-auto" data-text="{{$title}}">{{$title}}</div>
        <p class="lead text-gray-800 mb-5">{{$description}}</p>
        <p class="text-gray-500 mb-0">Coming Soon</p>
        <a href="{{route('admin.dashboard')}}">&larr; Back to Dashboard</a>
    </div>

</div>
