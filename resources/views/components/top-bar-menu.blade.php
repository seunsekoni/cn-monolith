@foreach($menus->take(5) as $menu)
    @if($menu->getNoOfChildrenAttribute() > 0)
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ $menu->title }}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                @foreach($menu->children as $child)
                    @if($child->model == \App\Enums\MenuModels::CATEGORY)
                        <a href="{{ route('categories.index', ['category' => $child->category]) }}"
                           target="{{ $child->target }}"
                           class="dropdown-item">{{ $child->title }}</a>
                    @elseif($child->model == \App\Enums\MenuModels::PAGE)
                        <a href="{{-- route('pages.index', ['page' => $child]) --}}"
                           target="{{ $child->target }}"
                           class="dropdown-item">{{ $child->title }}</a>
                    @else
                        <a href="{{ $child->link }}" target="{{ $child->target }}"
                           class="dropdown-item">{{ $child->title }}</a>
                    @endif
                @endforeach
            </div>
        </li>
    @else
        <li class="nav-item">
            @if($menu->model == \App\Enums\MenuModels::CATEGORY)
                <a href="{{ route('categories.index', ['category' => $menu->category]) }}"
                   target="{{ $menu->target }}" class="nav-link">{{ $menu->title }}</a>
            @elseif($menu->model == \App\Enums\MenuModels::PAGE)
                <a href="{{-- route('pages.index', ['page' => $menu]) --}}"
                   target="{{ $menu->target }}" class="nav-link">{{ $menu->title }}</a>
            @else
                <a href="{{ $menu->link }}" target="{{ $menu->target }}"
                   class="nav-link">{{ $menu->title }}</a>
            @endif
        </li>
    @endif
@endforeach
