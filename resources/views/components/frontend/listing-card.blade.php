<div class="col-md-4 col-lg-4 col-xl-4 mb-3">
    <a href="{{ route('listings.show', ['listing' => $listing]) }}">
        <div class="featured card">
            <div class="listing-card-img d-flex justify-content-center align-items-center">
                <img src="{{ $listing->featured_image }}" class="card-img-top" alt="{{ $listing->name }}">
            </div>
            <div class="card-body">
                <div class="equal">
                    <p class="card-title type-bold mb-0">{{ ucfirst($listing->name) }}</p>
                </div>
                <p class="card-text meta-posted">
                    <small class="meta-muted">
                        {{ $listing->category->name ?? '' }}
                    </small>
                </p>
                <div class="listing-indicators">
                    <div class="listing-rate">
                        @for ($i = 0; $i < 5; $i++)
                            <i
                                class="{{ getAverageRating($listing) <= $i ? 'far fa-star' : 'fas fa-star' }} font-small"
                            >
                            </i>
                        @endfor
                        <span>{{ $listing->reviews()->count() }}</span>
                    </div>
                    <div class="listing-recommend">
                        <i class="fas fa-thumbs-up"></i>
                        <span>{{ $listing->likes }}</span>
                    </div>
                    <div class="listing-connected">
                        <i class="fas fa-link"></i>
                        <span>{{ $listing->shares }}</span>
                    </div>
                </div>

                @if ($primaryLocation = optional($listing->business)->primaryLocation)
                    <div class="listing-address">
                        <i class="fas fa-map-marker-alt"></i>
                        <span>{{ $primaryLocation->full_address }}</span>
                    </div>
                @endif
            </div>
        </div>
    </a>
</div>
