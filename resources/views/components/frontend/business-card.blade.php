<div class="col-md-4 col-lg-4 col-xl-4 mb-3">
    <a href="{{ route('businesses.show', ['business' => $business]) }}">
        <div class="featured card">
            <div class="listing-card-img d-flex justify-content-center align-items-center">
                <img src="{{ $business->logo }}" class="card-img-top"
                alt="{{ $business->name }}">
            </div>
            <div class="card-body">
                <div class="equal">
                    <p class="card-title type-bold mb-0">{{ ucfirst($business->name) }}</p>
                </div>
                <p class="card-text meta-posted">
                    <small class="meta-muted">{{ $business->businessType->name ?? '' }}</small>
                </p>
                <div class="listing-indicators">
                    <div class="listing-rate">
                        @for ($i = 0; $i < 5; $i++)
                            <i
                                class="{{ getAverageRating($business) <= $i ? 'far fa-star' : 'fas fa-star' }} font-small"
                            >
                            </i>
                        @endfor
                        <span>{{ $business->reviews()->count() }}</span>
                    </div>
                    <div class="listing-recommend">
                        <i class="fas fa-thumbs-up"></i>
                        <span>{{ $business->likes }}</span>
                    </div>
                    <div class="listing-connected">
                        <i class="fas fa-link"></i>
                        <span>{{ $business->shares }}</span>
                    </div>
                </div>

                @if ($primaryLocation = $business->primaryLocation)
                    <div class="listing-address">
                        <i class="fas fa-map-marker-alt"></i>
                        <span>{{ $primaryLocation->full_address }}</span>
                    </div>
                @endif
            </div>
        </div>
    </a>
</div>