<div
    class="modal fade"
    id="deleteModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="deleteModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                Deleting this <span id="model-name"></span>
                (<span class="text-danger" id="deleteComponentName"></span>)
                <span class="font-weight-bold" id="deleteComponentNamePermanently"></span>
                might affect other related records.
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a
                    class="btn btn-danger"
                    href="#"
                    onclick="
                        event.preventDefault();
                        document.getElementById('delete-form').submit();
                    "
                >
                    Yes! Delete it
                </a>
                <form id="delete-form" action="#" method="POST">
                    @method('DELETE')
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>

@once
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#deleteModal').on('show.bs.modal', function (e) {
                    let name = $(e.relatedTarget).data('name');
                    let route = $(e.relatedTarget).data('route');
                    let modelName = $(e.relatedTarget).data('model');

                    $('#deleteComponentName').html(name);
                    $('#deleteComponentNamePermanently').html(route.includes('force') ? ' permanently' : '');
                    $('#model-name').html(modelName);
                    $('#delete-form').attr('action', route);
                });
                $('#deleteModal').on('hide.bs.modal', function (e) {
                    $('#delete-form').trigger('reset');
                });
            });
        </script>
    @endpush
@endonce
