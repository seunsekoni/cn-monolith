<div
    class="modal fade"
    id="restoreModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="restoreModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                Restore this <span id="restore-model-name"></span>
                (<span class="text-danger" id="restoreComponentName"></span>).
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a
                    class="btn btn-success"
                    href="#"
                    onclick="
                        event.preventDefault();
                        document.getElementById('restore-form').submit();
                    "
                >
                    Yes! Restore it
                </a>
                <form id="restore-form" action="#" method="POST">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>

@once
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#restoreModal').on('show.bs.modal', function (e) {
                    let name = $(e.relatedTarget).data('name');
                    let route = $(e.relatedTarget).data('route');
                    let modelName = $(e.relatedTarget).data('model');

                    $('#restoreComponentName').html(name);
                    $('#restore-model-name').html(modelName);
                    $('#restore-form').attr('action', route);
                });
            });
        </script>
    @endpush
@endonce
