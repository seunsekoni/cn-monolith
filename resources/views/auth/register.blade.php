@extends('user.layouts.auth')

@section('title', 'Create an Account')

@section('auth-content')
    <div class="col-lg-6">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create New Account</h1>
            </div>

            @if($errors->any())
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
            @endif

            <form
                class="user"
                id="admin-user-form"
                method="post"
                action="{{ route('register') }}"
            >
                @csrf
                <div class="form-group">
                    <input
                        type="text"
                        class="form-control form-control-user @error('first_name') is-invalid @enderror"
                        id="inputFirstName"
                        placeholder="First Name"
                        name="first_name"
                        value="{{ old('first_name') }}"
                    >
                </div>
                <div class="form-group">
                    <input
                        type="text"
                        class="form-control form-control-user @error('last_name') is-invalid @enderror"
                        id="inputLastName"
                        placeholder="Last Name"
                        name="last_name"
                        value="{{ old('last_name') }}"
                    >
                </div>
                <div class="form-group">
                    <input
                        type="tel"
                        class="form-control form-control-user @error('phone') is-invalid @enderror"
                        id="inputPhone"
                        placeholder="Phone Number"
                        name="phone"
                        value="{{ old('phone') }}"
                    >
                </div>
                <div class="form-group">
                    <input
                        type="email"
                        class="form-control form-control-user @error('email') is-invalid @enderror"
                        id="exampleInputEmail"
                        aria-describedby="emailHelp"
                        placeholder="Email Address..."
                        name="email"
                        value="{{ old('email') }}"
                        required
                    >
                </div>
                <div class="form-group">
                    <input
                        type="password"
                        class="form-control form-control-user"
                        id="exampleInputPassword"
                        placeholder="Password"
                        name="password"
                        required
                    >
                </div>
                <div class="form-group">
                    <input
                        type="password"
                        class="form-control form-control-user"
                        id="exampleInputPassword2"
                        placeholder="Confirm Password"
                        name="password_confirmation"
                        required
                    >
                </div>
                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Register
                </button>
            </form>
            <hr>
            <div class="text-center">
                <a class="small forgot-pwd" href="{{ route('login') }}">Sign In</a>
            </div>
        </div>
    </div>
@endsection
