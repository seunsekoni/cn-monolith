@extends('user.layouts.auth')

@section('title', 'Sign In')

@section('auth-content')
    <div class="col-lg-6">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
            </div>

            @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form
                class="user"
                id="admin-user-form"
                method="post"
                action="{{ route('login') }}"
            >
                @csrf
                <div class="form-group">
                    <input
                        type="email"
                        class="form-control form-control-user @error('email') is-invalid @enderror"
                        id="exampleInputEmail"
                        aria-describedby="emailHelp"
                        placeholder="Enter Email Address..."
                        name="email"
                        value="{{ old('email') }}"
                        required
                    >
                </div>
                <div class="form-group">
                    <input
                        type="password"
                        class="form-control form-control-user"
                        id="exampleInputPassword"
                        placeholder="Password"
                        name="password"
                        required
                    >
                </div>
                <div class="form-group">
                    <div class="custom-control custom-checkbox small">
                        <input
                            type="checkbox"
                            class="custom-control-input"
                            id="customCheck"
                            name="remember"
                            {{ old('remember') ? 'checked' : '' }}
                        >
                        <label class="custom-control-label" for="customCheck">
                            Remember Me
                        </label>
                    </div>
                </div>
                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Login
                </button>
            </form>
            <hr>
            <div class="text-center">
                <a class="small forgot-pwd" href="{{ route('register') }}">Create an Account</a>
            </div>
            <div class="text-center">
                <a class="small forgot-pwd" href="{{ route('password.request') }}">Forgot Password?</a>
            </div>
        </div>
    </div>
@endsection
