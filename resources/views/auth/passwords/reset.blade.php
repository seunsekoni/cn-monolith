@extends('admin.layouts.auth')

@section('title', 'Reset Password')

@section('auth-content')
    <div class="col-lg-6">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Password Reset!</h1>
            </div>

            @if($errors->any())
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
            @endif

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form
                class="user"
                id="admin-user-form"
                method="post"
                action="{{ route('password.update') }}"
            >
                @csrf
                <input type="hidden" name="token" value="{{ request()->route('token') }}">
                <div class="form-group">
                    <input
                        type="email"
                        class="form-control form-control-user @error('email') is-invalid @enderror"
                        id="exampleInputEmail"
                        aria-describedby="emailHelp"
                        placeholder="Email Address..."
                        name="email"
                        value="{{ request()->input('email') }}"
                        required
                    >
                </div>
                <div class="form-group">
                    <input
                        type="password"
                        class="form-control form-control-user"
                        id="exampleInputPassword"
                        placeholder="Password"
                        name="password"
                        required
                    >
                </div>
                <div class="form-group">
                    <input
                        type="password"
                        class="form-control form-control-user"
                        id="exampleInputPassword2"
                        placeholder="Confirm Password"
                        name="password_confirmation"
                        required
                    >
                </div>
                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Reset Password
                </button>
            </form>
        </div>
    </div>
@endsection
