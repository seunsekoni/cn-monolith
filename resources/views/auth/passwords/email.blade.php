@extends('user.layouts.auth')

@section('title', 'Forgot Password')

@section('auth-content')
    <div class="col-lg-6">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Reset Password!</h1>
            </div>

            @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form
                class="user"
                id="user-form"
                method="post"
                action="{{ route('password.email') }}"
            >
                @csrf
                <div class="form-group">
                    <input
                        type="email"
                        class="form-control form-control-user @error('email') is-invalid @enderror"
                        id="exampleInputEmail"
                        aria-describedby="emailHelp"
                        placeholder="Enter Email Address..."
                        name="email"
                        value="{{ old('email') }}"
                        required
                    >
                </div>

                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Request Reset Link
                </button>
            </form>
            <hr>
            <div class="text-center">
                <a class="small forgot-pwd" href="{{ route('login') }}">Login</a>
            </div>
        </div>
    </div>
@endsection