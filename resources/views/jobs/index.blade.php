@extends('layouts.app')

@section('title', 'Jobs')

@section('content')
<div class="nav-container">
  <div>
    @include('includes.top-menu')
    @include('includes.jobs-nav')
  </div>
</div>

<div class="main-container">
  <section class="imagebg height-40">
    <div class="background-image-holder">
      <img src="{{asset('img/jobs/hero-banner-1.jpg')}}" alt="CN Business" />
    </div>
    <div class="container pos-vertical-center">
      <div class="row align-items-center">
        <div class="col-md-12 col-lg-9 col-xl-9">
          <form class="search-bar mb-2">
            <div class="row">
              <div class="col-5 col-md-2 mb-2 mb-md-0 mb-lg-0 mb-xl-0">
                <div class="input-select">
                  <select class="form-control form-control-lg cars-search-dropdown">
                    <option>Companies</option>
                    <option>Jobs</option>
                  </select>
                </div>
              </div>
              <div class="col-9 col-md-4 mb-2 mb-md-0 mb-lg-0 mb-xl-0">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">Search</span>
                  </div>
                  <input type="text" class="form-control" placeholder="keywords, title or company">
                </div>
              </div>
              <div class="col-9 col-md-4 pr-xs-0">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">In</span>
                  </div>
                  <input type="text" class="form-control" placeholder="Lagos, Nigeria">
                </div>
              </div>
              <div class="col-3 col-md-1 pl-xs-0">
                <button type="submit" class="btn btn-primary h-100"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </form>

          <div class="popular-searches">
            <span class="color-primary">
              <small class="type-bold">Popular:</small>
            </span>
            <span>
              <small class="color-white">Web Developer in Lagos, Civil Engineer in Abuja, Nurse Hiring </small>
            </span>
          </div>
        </div>
        <div class="col-lg-3 col-xl-3 hidden-xs hidden-sm">
          <p class="mb-0"><small>Advertisements</small></p>
          <div class="medrec-carousel owl-carousel owl-theme" data-items="1" data-items-mobile-portrait="1"
            data-autoplay="true" data-autoplay-timeout="5000" data-loop="true">
            <div class="item">
              <img src="{{asset('img/jobs/ads-1.jpg')}}" width="240" class="text-center mb-3">
            </div>
            <div class="item">
              <img src="{{asset('img/jobs/ads-2.jpg')}}" width="240" class="text-center mb-3">
            </div>
            <div class="item">
              <img src="{{asset('img/jobs/ads-3.jpg')}}" width="240" class="text-center mb-3">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- categories -->
  <section class="pb-0">
    <div class="container">

      <ul class="nav nav-tabs browse">
        <li><span class="type-bold">Browse by</span></li>
        <li><a data-toggle="tab" href="#functions">Functions</a></li>
        <li><a data-toggle="tab" href="#industries" class="active">Industries</a></li>
        <li><a data-toggle="tab" href="#location">Location</a></li>
      </ul>


      <div class="tab-content">
        <object src="{{asset('svg/svg_cats.svg')}}" style="display: none;"></object>
        <div id="functions" class="tab-pane fade in active show">
          <div class="category-carousel owl-carousel owl-theme" data-items="8" data-margin="10"
            data-items-mobile-portrait="3" data-autoplay="true" data-loop="true" data-autoplay-timeout="3000">
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#agriculture')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Agriculture</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#aviation')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Aviation</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#financial-services')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Financial Services</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#construction')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Construction</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#consultation')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Consultancy</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#education')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Education</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#government')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Government</small></p>
              </a>
            </div>

            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#healthcare')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Healthcare</small></p>
              </a>
            </div>

          </div><!-- end carousel -->
        </div><!-- end tab-pane -->


        <div id="industries" class="tab-pane fade">
          <div class="category-carousel owl-carousel owl-theme" data-items="8" data-margin="10"
            data-items-mobile-portrait="3" data-autoplay="true" data-loop="true" data-autoplay-timeout="3000">
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#agriculture')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Agriculture</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#aviation')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Aviation</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#financial-services')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Financial Services</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#construction')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Construction</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#consultation')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Consultancy</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#education')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Education</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#government')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Government</small></p>
              </a>
            </div>

            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#healthcare')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Healthcare</small></p>
              </a>
            </div>

          </div><!-- end carousel -->
        </div><!-- end tab-pane -->

        <div id="location" class="tab-pane fade">
          <div class="category-carousel owl-carousel owl-theme" data-items="8" data-margin="10"
            data-items-mobile-portrait="3" data-autoplay="true" data-loop="true" data-autoplay-timeout="3000">
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#agriculture')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Agriculture</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#aviation')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Aviation</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#financial-services')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Financial Services</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#construction')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Construction</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#consultation')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Consultancy</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#education')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Education</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#government')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Government</small></p>
              </a>
            </div>

            <div class="item text-center">
              <a href="#">
                <svg viewBox="0 0 100 100" class="icon mb-3">
                  <use xlink:href="{{asset('svg/svg_cats.svg#healthcare')}}"></use>
                </svg>

                <p class="cat-name"><small class="type-bold">Healthcare</small></p>
              </a>
            </div>

          </div><!-- end carousel -->
        </div><!-- end tab-pane -->

      </div>
    </div>
  </section>

  <!-- featured -->
  <section class="featured-biz pb-0">
    <div class="container">

      <div class="row">
        <div class="col-md-9 col-lg-9 col-xl-9">
          <div class="row justify-content-end">
            <div class="col-7 col-md-9">
              <h5 class="type-bold mb-4">Featured Job Listings</h5>
            </div>
            <div class="col-5 col-md-3 text-right">
              <a href="">View Job Listings <span class="color-dark ml-2"><i class="fas fa-angle-right"></i></span></a>
            </div>
          </div>
          <div class="row">

            @forelse ($listings as $listing)

            <div class="col-md-6 col-lg-6 col-xl-6 mb-3">
              <a href="#">
                <div class="featured-2 card">

                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-9 pr-0 order-2 order-md-1">

                        <div class="mb-3">
                          <h6 class="card-title type-bold d-inline-block mb-0 mr-2">Elasticsearch Developer</h6>
                          <span class="badge bg-primary color-white">Full Time</span>
                        </div>


                        <div class="listing-indicators-jobs">

                          <div class="listing-company">
                            <i class="fas fa-user-tie color-primary"></i>
                            <span class="type-bold">Nextdaysite</span>
                          </div>
                        </div>
                        <div class="listing-indicators-jobs">

                          <div class="listing-company">
                            <i class="fas fa-briefcase color-primary"></i>
                            <span class="">IT Computers - Software</span>
                          </div>
                          <div class="listing-date">
                            <i class="far fa-calendar color-primary"></i>
                            <span class="">Dec 01, 2020</span>
                          </div>
                        </div>
                        <div class="listing-indicators-jobs">

                          <div class="listing-address">
                            <i class="fas fa-map-marker-alt"></i>
                            <span>Dolphin Estate, Lagos</span>
                          </div>

                        </div>
                      </div>
                      <div class="col-md-3 order-1 order-md-2">
                        <img src="{{asset('img/jobs/job-1.jpg')}}" width="80" class="mb-3 mb-lg-0 mb-md-0">
                      </div>
                    </div>
                    <p class="mb-0">
                      <small>The main role is to Install and manage Elasticsearch unto a Linux-base server, monitor
                        elastic data, create powerful search API's and algorithms...</small>
                    </p>
                  </div>
                </div>
              </a>
            </div>

            @empty

            <p>There are currently no jobs.</p>

            @endforelse

          </div> <!-- end row -->
        </div>

        <div class="col-md-3 col-lg-3 col-xl-3 text-center">
          <p class="mb-4"><small>Advertisements</small></p>
          <div class="hidden-xs hidden-sm">
            <img src="{{asset('img/jobs/ads-2.jpg')}}" width="240" class="text-center mb-3">
            <img src="{{asset('img/jobs/ads-3.jpg')}}" width="240" class="text-center mb-3">
          </div>
          <div class="hidden-lg hidden-md">
            <div class="medrec-carousel owl-carousel owl-theme" data-items-mobile-portrait="1" data-autoplay="true"
              data-autoplay-timeout="5000" data-loop="true">
              <div class="item">
                <img src="{{asset('img/jobs/ads-2.jpg')}}" width="240" class="text-center mb-3">
              </div>
              <div class="item">
                <img src="{{asset('img/jobs/ads-3.jpg')}}" width="240" class="text-center mb-3">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- cta -->

  <section>
    <div class="container">
      <!-- CTAs -->
      <div class="row">
        <!-- Get a quote -->
        <div class="col-md-7 col-lg-7 col-xl-7 mb-3 mb-lg-0 mb-xl-0">
          <div class="card home-card-ctas ">
            <div class="row no-gutters">


              <div class="col-md-4 col-lg-4 col-xl-4">
                <div class="imagebg h-100 second">
                  <div class="background-image-holder border-tl-radius border-bl-radius">
                    <img src="{{asset('img/jobs/home-1.jpg')}}" alt="">
                  </div>
                </div>
              </div><!-- end col -->

              <div class="col-md-8 col-lg-8 col-xl-8">
                <div class="card-body border-tr-radius border-br-radius bg-orange">
                  <h3 class="card-title color-white">Job resources and<span class="next-line color-white">career news
                    </span></h3>
                  <p class="color-white">Connect Nigeria Jobs brings you career news, resources you need or information
                    you should know to land that job, even personal finance tips and consumer news.</p>
                  <a href="#" class="btn btn-black">Read Job Resources</a>
                </div>
              </div><!-- end col -->
            </div><!-- end row -->
          </div><!-- end card -->
        </div><!-- end quote cta -->

        <!-- Confirm Businesses -->
        <div class="col-md-5 col-lg-5 col-xl-5">
          <div class="card home-card-ctas h-100 ">
            <div class="imagebg image-dark">
              <div class="background-image-holder border-radius-5">
                <img src="{{asset('img/jobs/home-2.jpg')}}" class="card-img">
              </div>
              <div class="card-img-overlay card-body">
                <div class="row">
                  <div class="col-11 col-lg-7">
                    <h3 class="card-title">Looking for right manpower?</h3>
                    <p class="card-text">Need the right human resource for your company or a project? Just post a job
                      and find the right talents. </p>
                    <a href="#" class="btn btn-primary-1">Browse Services</a>
                  </div>
                </div>
              </div>
            </div>

          </div><!-- end card -->
        </div><!-- end confirm cta -->
      </div><!-- end row -->
    </div>
  </section>
  <!-- Articles -->
  <section class="bg-secondary home-news">
    <div class="container">
      <div class="row mb-4">
        <div class="col-6 col-md-6 col-lg-6 col-xl-6">
          <h5 class="type-bold">In the news</h5>
        </div>
        <div class="col-6 col-md-6 col-lg-6 col-xl-6 text-right">
          <a href="">Browse Articles <span class="color-dark ml-2"><i class="fas fa-angle-right"></i></span></a>
        </div>
      </div>
      <div class="home-news-carousel owl-carousel owl-theme" data-items="4" data-margin="15"
        data-items-mobile-portrait="1" data-items-tablet-portrait="2" data-loop="true" data-autoplay="true"
        data-autoplay-timeout="3000">

        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/jobs/article-1.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Alex Obigdu</span></small>
                </p>
                <p class="card-title">Career Of The Week - Chinyere Anarado, Facilities Manager</p>
              </div>
            </div>
          </a>
        </div>
        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/jobs/article-2.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Uzo Ajawi</span></small>
                </p>
                <p class="card-title">Career Of The Week - Ore Sonola, Maid Whisperer</p>
              </div>
            </div>
          </a>
        </div>
        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/jobs/article-3.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Martin Andrews</span></small>
                </p>
                <p class="card-title">Tête-À-Tête About Career Changes In Nigeria</p>
              </div>
            </div>
          </a>
        </div>
        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/jobs/article-4.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Kelly Abudhi</span></small>
                </p>
                <p class="card-title">Kickstart Your Coding Career Powered by Microsoft</p>
              </div>
            </div>
          </a>
        </div>

      </div>
    </div>
  </section>
  <!-- end Articles section -->

  <section class="p-0">
    <div class="row no-gutters">
      <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="imagebg height-50 image-dark">
          <div class="background-image-holder">
            <img src="{{asset('img/cars/footer-banners-1.jpg')}}">
          </div>
          <div class="container pos-vertical-center h-100">
            <div class="cta-banner-footer">
              <div class="row w-100 mb-2">
                <div class="col-md-10 col-lg-10 col-xl-10">
                  <h2 class="title type-bold mb-2">Reach your target market faster </h2>
                  <p>with just <strong>N10,000</strong></p>
                </div>
              </div>
              <div class="d-flex mb-3">
                <div class="cta-stats mr-3">
                  <img src="{{asset('img/businesses/window-pointer.png')}}">
                  <div>
                    <p class="type-medium mb-0">250,000+</p>
                    <small>Daily Visitors</small>
                  </div>
                </div>
                <div class="cta-stats">
                  <img src="{{asset('img/businesses/briefcase.png')}}">
                  <div>
                    <p class="type-medium mb-0">500,000+</p>
                    <small>Business Listings</small>
                  </div>
                </div>
              </div>
              <div class="d-flex">
                <a href="#" class="btn btn-lg btn-primary-1 mr-4">
                  Advertise with us
                </a>
                <div class="">
                  <p class="mb-0">Call <span class="type-medium">0700-800-5000</span></p>
                  <small>grow@connectnigeria.com</small>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="imagebg height-50 image-dark">
          <div class="background-image-holder">
            <img src="{{asset('img/jobs/footer-banners-2.jpg')}}">
          </div>
          <div class="container pos-vertical-center h-100">
            <div class="cta-banner-footer-2 d-flex">
              <div class="row align-items-center w-100 mb-2">
                <div class="col-md-8 col-lg-8 col-xl-8">
                  <h2 class="title type-bold mb-4">Be the first to know what’s happening</h2>
                  <p class="mb-4">Sign up to our newsletter get interesting news and updates delivered right to your
                    inbox</p>
                  <a href="#" class="btn btn-lg btn-primary-1 mr-4">
                    Subscribe Now
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('includes.footer')
</div>
@endsection