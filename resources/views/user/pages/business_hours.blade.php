@extends('user.layouts.dashboard')

@section('title', 'Business Hours')

@section('main-content')
    <div class="card">
        <div class="card-header">
            Business Hour
        </div>
        <div class="card-body d-flex  flex-column">
            <div class="alert alert-success" id="business_hours_alert" role="alert" style="display:none">
            </div>
            <div class="mx-auto col-6">
                    @csrf
                    @foreach($business_hours as $key => $business_hour)
                        <div class="form-group">
                            <label class="font-weight-bold"
                                   for="{{$business_hour->day}}">{{$business_hour->day}}</label>
                            <input type="hidden" name="day" id="{{"day$key"}}" value="{{$business_hour->day}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <input class="form-control form" type="time"
                                           name="open" id="{{"open$key"}}" value="{{$business_hour->open}}">
                                    <small class="form-text text-muted">Open</small>
                                </div>
                                <div class="col-md-6">
                                    <input class="form-control form" type="time"
                                           name="close" id="{{"close$key"}}" value="{{$business_hour->close}}">
                                    <small class="form-text text-muted">Close</small>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <button class="btn btn-primary btn-user btn-block" onclick="handleSubmitBusinessHours()">
                        Submit
                    </button>
            </div>
        </div>
    </div>
@endsection
@once
    @push('scripts')
        <script>
            const collectBusinessHourFormVal = () => {
                const businessHours = [];
                for (let i = 0; i < 7; i++) {
                    const temp_obj = {
                        day: document.getElementById(`day${i}`).value,
                        open: document.getElementById(`open${i}`).value,
                        close: document.getElementById(`close${i}`).value
                    }
                    businessHours.push(temp_obj);
                }
                return JSON.stringify(businessHours);
            }

            const handleSubmitBusinessHours = () => {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('user.business-hours.store')}}",
                    type: "POST",
                    dataType: "json",
                    data: {business_hours: collectBusinessHourFormVal()},
                    success: (date) => {
                        $("#business_hours_alert").text(date.message).show()
                        console.log('saved successfully')
                    }
                })
            }
        </script>
    @endpush
@endonce
