@extends('user.layouts.dashboard')

@section('title', 'Dashboard')

@section('main-content')
    @php
        $user_business = Auth::user()->business;
    @endphp

    <div>
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <p class="">Manage your personal &amp; business data</p>

        @if (session('success'))
            <x-alert type="success" :message="session('success')"/>
        @endif

        @if(!$user_business)
            <div class="">
                <div class="alert alert-info font-weight-bold" role="alert">
                    <div class="d-flex justify-content-between">
                        <div>
                            <p class="m-0">Need a business to engage your customers on the platform?</p>
                            <small>A business allows you to register all your products and services</small>
                        </div>
                        <div>
                            <a
                                href="{{ route('user.business.create') }}"
                                class="btn btn-lg btn-primary"
                            >
                                Create Now
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @else
            @if ($user_business->verified)
                <div class="row">
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xl font-weight-bold text-primary text-uppercase mb-1">
                                            Total Comments
                                        </div>
                                        <div class="h1 mb-0 font-weight-bold text-gray-800">{{auth()->user()->comments->count()}}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-building fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xl font-weight-bold text-success text-uppercase mb-1">
                                            Total Reviews
                                        </div>
                                        <div class="h1 mb-0 font-weight-bold text-gray-800">
                                            {{$user_business->reviews->count()}}
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-address-card fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Pending Requests Card Example -->
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xl font-weight-bold text-warning text-uppercase mb-1">
                                            Open support ticket
                                        </div>
                                        <div class="h1 mb-0 font-weight-bold text-gray-800">
                                            {{ auth()->user()->supportTickets()->count() }}
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-briefcase fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="alert alert-info font-weight-bold" role="alert">
                    Your business is currently under verification!
                </div>
            @endif
        @endif
    </div>
@endsection
