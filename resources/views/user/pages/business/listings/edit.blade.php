@extends('user.layouts.dashboard')

@section('title', "Update {$listing->name} Listing")

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/dropify/dropify.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">

        <style type="text/css">
            .dropify-wrapper .dropify-message p {
                font-size: 15px;
            }
        </style>
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Edit Listing</h1>
        <a href="{{ route('user.business.listings.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Update this listing.</p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">
                Editing <span class="text-danger">{{ $listing->name }}</span> listing
            </h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif
            <form
                id="businessListingForm"
                action="{{ route('user.business.listings.update', ['listing' => $listing->id ]) }}"
                class="user"
                method="POST"
                enctype="multipart/form-data"
            >
                @method('PUT')
                @csrf

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input
                                type="file"
                                class="dropify featured_image"
                                accept="{{ $types = implode(', ', App\Enums\MediaType::IMAGES) }}"
                                name="featured_image"
                                data-default-file="{{ $listing->featured_image }}"
                            >
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="category_id">
                                Category
                                <span class="text-danger sup">*</span>
                            </label>
                            <select
                                id="category_id"
                                name="category_id"
                                class="select2 form-control form-control-user @error('category_id') is-invalid @enderror"
                                required
                            ></select>
                            <small class="form-text text-muted">The category the listing belongs to.</small>
                        </div>

                        <div class="d-none border-left pl-2" id="category-properties-container">
                            <label class="small text-muted">
                                Provide all required properties below
                                <span class="text-danger sup">*</span>
                            </label>
                            <div id="category-properties-content"></div>
                        </div>

                        <div class="form-group">
                            <label for="name">
                                Product/Service name
                                <span class="text-danger sup">*</span>
                            </label>
                            <small class="maxlengthWarningText d-none text-warning">We recommend that you keep this at {{ config('cnsettings.listing_name_limit') }} characters</small>
                            <input
                                type="text"
                                id="name"
                                name="name"
                                class="form-control form-control-user @error('name') is-invalid @enderror"
                                value="{{ old('name') ?? $listing->name }}"
                                required
                            >
                            <small class="form-text text-muted">Name of the product or service</small>
                        </div>

                        <div class="form-group">
                            <label for="description">
                                Description
                                <span class="text-danger sup">*</span>
                            </label>
                            <textarea
                                rows="4"
                                id="description"
                                name="description"
                                class="form-control @error('description') is-invalid @enderror"
                                required
                            >{{ old('description') ?? $listing->description }}</textarea>
                            <small class="form-text text-muted">
                                Describe the new listing as explanatory as possible.
                            </small>
                        </div>

                        <div class="form-group">
                            <label for="price">
                                Price
                            </label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input
                                    type="number"
                                    id="price"
                                    name="price"
                                    pattern="[0-9]+([\.,][0-9]+)?"
                                    step="0.1"
                                    class="form-control form-control-user @error('price') is-invalid @enderror"
                                    value="{{ old('price') ?? $listing->price }}"
                                >
                                <small class="form-text text-muted">Price for the listing (optional)</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="listingTags">Update tags</label>
                            <select
                                id="listingTags"
                                name="tags[]"
                                class="select2 form-control form-control-user @error('tags') is-invalid @enderror"
                                multiple
                            ></select>
                            <small class="form-text text-muted">
                                Add tags (keywords) to enhance the search
                            </small>
                        </div>

                        <div class="jumbotron">
                            <h5 class="font-weight-bold">
                                <div class="custom-control custom-checkbox">
                                    <input
                                        type="checkbox"
                                        name="discount_enabled"
                                        class="custom-control-input"
                                        id="discountEnabled"
                                        onchange="activateDiscount()"
                                        {{
                                            (old('discount_enabled') ?? $listing->discount_enabled)
                                                ? 'checked'
                                                : ''
                                        }}
                                    >
                                    <label class="custom-control-label" for="discountEnabled">
                                        Activate Special Discount
                                        <small>(optional)</small>
                                    </label>
                                </div>
                            </h5>
                            <p class="lead small text-muted">Create a special listing to show in the deals section</p>

                            <div class="form-group">
                                <label for="discount_type_id">Discount Type</label>
                                <select
                                    id="discount_type_id"
                                    name="discount_type_id"
                                    class="form-control form-control-user @error('discount_type_id') is-invalid @enderror"
                                    disabled
                                >
                                    <option value="" disabled>Select a discount type</option>
                                    @foreach ($discountTypes as $discountType)
                                        <option
                                            value="{{ $discountType->id }}"
                                            {{
                                                (
                                                    old('discount_type_id') == $discountType->id
                                                    || $listing->discount_type_id == $discountType->id
                                                ) ? 'selected' : ''
                                            }}
                                        >
                                            {{ $discountType->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="discount_value">
                                    Discount value
                                </label>
                                <input
                                    type="number"
                                    id="discount_value"
                                    name="discount_value"
                                    class="form-control form-control-user @error('discount_value') is-invalid @enderror"
                                    value="{{ old('discount_value') ?? $listing->discount_value }}"
                                    disabled
                                >
                            </div>

                            <div class="form-group">
                                <label for="discount_start">
                                    Discount Start Date
                                </label>
                                <input
                                    type="datetime-local"
                                    id="discount_start"
                                    name="discount_start"
                                    class="form-control form-control-user @error('discount_start') is-invalid @enderror"
                                    value="{{ old('discount_start') ?? $listing->discount_start }}"
                                    disabled
                                >
                            </div>

                            <div class="form-group">
                                <label for="discount_end">
                                    Discount End Date
                                </label>
                                <input
                                    type="datetime-local"
                                    id="discount_end"
                                    name="discount_end"
                                    class="form-control form-control-user @error('discount_end') is-invalid @enderror"
                                    value="{{ old('discount_end') ?? $listing->discount_end }}"
                                    disabled
                                >
                            </div>
                        </div>
                    </div>
                </div>

                <button id="formButton" class="btn btn-success btn-user btn-block" type="submit">
                    Update Listing
                </button>
            </form>
        </div>
    </div>
@endsection

@once
    @push('scripts')
        <script src="{{ asset('vendor/tagator/tagator.js') }}"></script>
        <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
        <script src="{{ asset('vendor/dropify/dropify.js') }}"></script>
        <script>
            $(document).ready(function () {

                $(document).ready(function () {
                //  Validate length of name
                $('#name').on('input', function() {
                    validateMaxLength()
                })

                function validateMaxLength() {
                    const maxAllowedNameLength = "{{ config('cnsettings.listing_name_limit') }}"
                    let nameValue = $('#name').val()

                    if (nameValue.length > maxAllowedNameLength) {
                        $('#name').addClass('border border-warning')
                        $('.maxlengthWarningText').removeClass('d-none')
                    } else {
                        $('#name').removeClass('border border-warning')
                        $('.maxlengthWarningText').addClass('d-none')
                    }
                }

                activateDiscount();

                $('#category_id').change(function () {
                    showProperties($(this).find('option:selected').data('properties'));
                })
            });

            $('.dropify.featured_image').dropify({
                messages: {
                    'default': 'Drag and drop the listing featured image here',
                }
            });

            $('#category_id.select2').select2({
                placeholder: 'Select a category',
                ajax: {
                    url: "{{ route('api.categories.search') }}",
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    processResults: function (response) {
                        return {
                            results: response.data.categories.map(category => {
                                return {
                                    id: category.id,
                                    text: category.name,
                                    properties: JSON.stringify(category.properties)
                                }
                            })
                        };
                    }
                },
                templateSelection: function (data) {
                    $(data.element).attr('data-properties', data.properties);
                    return data.text;
                }
            });

            let categoryId = ("{{ old('category_id') }}" || "{{ $listing->category_id}}");
            if (categoryId) {
                getCategory(categoryId);
            }

            // Tags
            $('#listingTags.select2').select2({
                tags: true,
                tokenSeparators: [','],
                placeholder: 'Select the tags',
                createTag: function (params) {
                    console.log(params)
                    var term = $.trim(params.term);

                    if (term === '') {
                        return null;
                    }

                    return {
                        id: term.ucwords(),
                        text: term.ucwords(),
                        newTag: true // add additional parameters
                    }
                },
                ajax: {
                    url: "{{ route('api.tags.search') }}",
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    processResults: function (response) {
                        return {
                            results: response.data.tags.map(tag => {
                                return {
                                    id: tag.name,
                                    text: tag.name
                                }
                            })
                        };
                    }
                }
            });

            let tags = @json((old('tags') ?: $listing->tags), JSON_PRETTY_PRINT);
            if (tags) {
                getTags(tags)
            }

            function activateDiscount() {
                let discountEnabled = $('#discountEnabled').prop('checked');

                if (discountEnabled) {
                    $('#discount_type_id').attr('disabled', false);
                    $('#discount_value').attr('disabled', false);
                    $('#discount_start').attr('disabled', false);
                    $('#discount_end').attr('disabled', false);
                } else {
                    $('#discount_type_id').attr('disabled', true);
                    $('#discount_value').attr('disabled', true);
                    $('#discount_start').attr('disabled', true);
                    $('#discount_end').attr('disabled', true);
                }
            }

            function showProperties(properties) {
                let selectedListing = @json($listing);

                let categoryPropertiesContainer = $('#category-properties-container');
                let categoryPropertiesContent = $('#category-properties-content');
                categoryPropertiesContainer.addClass('d-none');
                categoryPropertiesContent.empty();

                if (!properties.length) {
                    return;
                }

                properties.forEach(property => {
                    let propertyValues = null;
                    let propValue = selectedListing.properties.filter(
                        prop => prop.name.toLowerCase() == property.name.toLowerCase()
                    )[0];

                    if (!property.values.length) {
                        propertyValues = `
                            <input
                                type="text"
                                name="properties[${property.name.toLowerCase()}]"
                                class="form-control form-control-user"
                                placeholder="..."
                                value="${propValue?.pivot?.category_property_value}"
                                required
                            >
                        `;
                    } else {
                        let values = '';
                        property.values.forEach(value => {
                            values += `
                                <option
                                    value="${value.id}"
                                    ${propValue?.pivot?.category_property_value == value.id ? 'selected' : ''}
                                >
                                    ${value.name}
                                </option>
                            `;
                        });

                        propertyValues = `
                            <select
                                name="properties[${property.name.toLowerCase()}]"
                                class="form-control form-control-user"
                                required
                            >
                                <option value="">Select a ${property.name.toLowerCase()}</option>
                                ${values}
                            </select>
                        `;
                    }

                    categoryPropertiesContent.append(`
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">${property.name}</div>
                                </div>
                                ${propertyValues}
                            </div>
                            <small class="form-text text-muted">${property.hint}</small>
                        </div>
                    `);
                });

                categoryPropertiesContainer.removeClass('d-none');
            }

            function getCategory(id) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: `{{ route('api.categories.search') }}/?id=${id}`,
                    type: 'get',
                    success: function (response) {
                        let category = response.data.category
                        let properties = JSON.stringify(category.properties)

                        // create the option and append to Select2
                        var option = `<option value='${category.id}' data-properties='${properties}'>${category.name}</option>`;
                        $('#category_id.select2').append(option).trigger('change');

                        // manually trigger the `select2:select` event
                        $('#category_id.select2').trigger({
                            type: 'select2:select',
                            params: {
                                data: category
                            }
                        });
                    },
                    error: function (error) {
                        let response = JSON.parse(error.responseText)
                        alert(response.message)
                    }
                });
            }

            function getTags(tags) {
                tags.forEach(tag => {
                    var newOption = new Option(tag, tag, true, true);
                    $('#listingTags.select2').append(newOption).trigger('change');
                });
            }
        </script>
    @endpush
@endonce
