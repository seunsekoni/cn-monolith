@extends('user.layouts.dashboard')

@section('title', 'Manage your Business')
@section('description', truncate($business->profile, 30))

@section('main-content')
    <h1 class="h3 text-gray-800">Business Data</h1>
    <p class="">All extra relations for your business</p>

    <div class="card shadow mb-4">
        <div class="card-body">
            <ul class="nav nav-tabs nav-justified">
                <li class="nav-item">
                    <a
                        class="nav-link {{
                            request()->routeIs('user.business.index') ? 'active' : ''
                        }}"
                        href="{{ route('user.business.index', ['business' => $business->id]) }}"
                    >
                        Basic Info
                    </a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link {{
                             request()->routeIs('user.business.locations.index') ? 'active' : ''
                        }}"
                        href="{{ route('user.business.locations.index') }}"
                    >
                        Locations
                    </a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link {{
                            request()->routeIs('user.business.contact.index') ? 'active' : ''
                        }}"
                        href="{{ route('user.business.contact.index') }}"
                    >
                        Contact
                    </a>
                </li>

            </ul>

            <div class="mt-4">
                @yield('business-relation')
            </div>
        </div>
    </div>
@endsection
