@extends('user.pages.business.layouts.relations')

@include('user.pages.business.includes.gallery-modal')

@section('business-relation')
    <div class="row">
        <div class="col-md-3">
            <figure class="figure mb-2">
                <img src="{{ $business->logo }}" alt="logo" class="figure-img img-fluid img-responsive rounded">
                <figcaption class="figure-caption">
                    {{ $business->name }} {{ constant('App\\Enums\\MediaCollection::LOGO') }}
                </figcaption>
            </figure>
            <a
                href="{{ route('user.business.edit') }}"
                class="btn btn-success btn-block"
            >
                Edit Business
            </a>
            <button
                data-id="{{ $business->id }}"
                data-name="{{ $business->name }}"
                data-route="{{ route('user.business.gallery.index') }}"
                data-toggle="modal"
                data-target="#galleryModal"
                class="btn btn-primary btn-block"
            >
                <i class="fa fa-photo-video"></i>
                Open Gallery
            </button>
        </div>
        <div class="col-md-9">
            <div class="mb-4">
                <h6 class="font-weight-bold">Type</h6>
                {{ optional($business->businessType)->name ?? 'N/A' }}
            </div>
            <div class="mb-4">
                <h6 class="font-weight-bold">Name</h6>
                {{ $business->name }}
            </div>
            <div class="mb-4">
                <h6 class="font-weight-bold">Profile</h6>
                <span class="text-justify">{{ $business->profile }}</span>
            </div>
            <div class="mb-4">
                <h6 class="font-weight-bold">Tags</h6>
                @forelse ($business->tags as $tag)
                    <span class="badge badge-primary mr-2">
                        {{ $tag->name }}
                    </span>
                @empty
                    No tags found!
                @endforelse
            </div>
        </div>
    </div>
@endsection
