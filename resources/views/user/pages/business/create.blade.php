@extends('user.layouts.dashboard')

@section('title', 'Create Business')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/dropify/dropify.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">

        <style type="text/css">
            .dropify-wrapper .dropify-message p {
                font-size: 15px;
            }
        </style>
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Create Business</h1>
        <a href="{{ route('user.dashboard') }}" class="btn btn-sm btn-danger" sr-only="Go back">
            <i class="fas fa-times"></i>
            Cancel Registration
        </a>
    </div>
    <p class="">A business allows you to register all your products and services</p>

    <div class="card shadow mb-4">
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form
                action="{{ route('user.business.store') }}"
                method="POST"
                enctype="multipart/form-data"
            >
                @csrf

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input
                                type="file"
                                class="dropify logo"
                                accept="{{ $types = implode(', ', App\Enums\MediaType::IMAGES) }}"
                                name="logo"
                            >
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="businessType">
                                Type of the business
                                <span class="text-danger sup">*</span>
                            </label>
                            <select
                                id="business_type_id"
                                name="business_type_id"
                                class="select2 form-control form-control-user @error('business_type_id') is-invalid @enderror"
                                required
                            ></select>
                            <small class="form-text text-muted">
                                What form of business ownership is the business?
                            </small>
                        </div>

                        <div class="form-group">
                            <label for="name">
                                Business name
                                <span class="text-danger sup">*</span>
                            </label>
                            <small class="maxlengthWarningText d-none text-warning">We recommend that you keep this at {{ config('cnsettings.listing_name_limit') }} characters</small>
                            <input
                                type="text"
                                id="name"
                                name="name"
                                class="form-control form-control-user @error('name') is-invalid @enderror"
                                placeholder="Name"
                                value="{{ old('name') }}"
                                required
                            >
                            <small class="form-text text-muted">Name of the business</small>
                        </div>

                        <div class="form-group">
                            <label for="profile">
                                Description
                                <span class="text-danger sup">*</span>
                            </label>
                            <textarea
                                id="profile"
                                name="profile"
                                class="form-control form-control-user rounded @error('profile') is-invalid @enderror"
                                placeholder="Description"
                                rows="5"
                                required
                            >{{ old('profile') }}</textarea>
                            <small class="form-text text-muted">Describe the business</small>
                        </div>

                        <div class="form-group">
                            <label for="businessTags">Add business tags</label>
                            <select
                                id="businessTags"
                                name="tags[]"
                                class="select2 form-control form-control-user @error('tags') is-invalid @enderror"
                                multiple
                            ></select>
                            <small class="form-text text-muted">
                                Add tags (keywords) to enhance search
                            </small>
                        </div>
                    </div>

                    <button class="btn btn-primary btn-user btn-block" type="submit">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@once
    @push('scripts')
        <script src="{{ asset('vendor/dropify/dropify.js') }}"></script>
        <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
        <script>
            //  Validate length of name
            $('#name').on('input', function() {
                validateMaxLength()
            })

            function validateMaxLength() {
                const maxAllowedNameLength = "{{ config('cnsettings.listing_name_limit') }}"
                let nameValue = $('#name').val()

                if (nameValue.length > maxAllowedNameLength) {
                    $('#name').addClass('border border-warning')
                    $('.maxlengthWarningText').removeClass('d-none')
                } else {
                    $('#name').removeClass('border border-warning')
                    $('.maxlengthWarningText').addClass('d-none')
                }
            }

            $(document).ready(() => {
                $('.dropify.logo').dropify({
                    messages: {
                        'default': 'Drag and drop the business logo here',
                    }
                });

                $('#business_type_id.select2').select2({
                    placeholder: 'Select a business type',
                    ajax: {
                        url: "{{ route('api.business-types') }}",
                        dataType: 'json',
                        delay: 250,
                        cache: true,
                        processResults: function (response) {
                            return {
                                results: response.data.business_types.map(businessType => {
                                    return {
                                        id: businessType.id,
                                        text: businessType.name
                                    }
                                })
                            };
                        }
                    }
                });

                let businessTypeId = "{{ old('business_type_id') }}";
                if (businessTypeId) {
                    getBusinessType(businessTypeId);
                }

                function getBusinessType(id) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: `{{ route('api.business-types.search') }}/?id=${id}`,
                        type: 'get',
                        success: function (response) {
                            let business_type = response.data.business_type

                            // create the option and append to Select2
                            var option = new Option(business_type.name, business_type.id, true, true);
                            $('#business_type_id.select2').append(option).trigger('change');

                            // manually trigger the `select2:select` event
                            $('#business_type_id.select2').trigger({
                                type: 'select2:select',
                                params: {
                                    data: business_type
                                }
                            });
                        },
                        error: function (error) {
                            let response = JSON.parse(error.responseText)
                            alert(response.message)
                        }
                    });
                }

                // Tags
                $('#businessTags.select2').select2({
                    tags: true,
                    tokenSeparators: [','],
                    placeholder: 'Select the tags',
                    createTag: function (params) {
                        var term = $.trim(params.term);

                        if (term === '') {
                            return null;
                        }

                        return {
                            id: term,
                            text: term,
                            newTag: true // add additional parameters
                        }
                    },
                    ajax: {
                        url: "{{ route('api.tags.search') }}",
                        dataType: 'json',
                        delay: 250,
                        cache: true,
                        processResults: function (response) {
                            return {
                                results: response.data.tags.map(tag => {
                                    return {
                                        id: tag.name,
                                        text: tag.name
                                    }
                                })
                            };
                        }
                    }
                });

                let tags = @json(old('tags'), JSON_PRETTY_PRINT);
                if (tags) {
                    getTags(tags)
                }

                function getTags(tags) {
                    tags.forEach(tag => {
                        var newOption = new Option(tag, tag, true, true);
                        $('#businessTags.select2').append(newOption).trigger('change');
                    });
                }
            })
        </script>
    @endpush
@endonce
