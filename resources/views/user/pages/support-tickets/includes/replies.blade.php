<div class="accordion mb-3" id="accordionComment{{ $reply->id }}">
    <div class="card">
        <div class="card-header" id="heading{{ $reply->id }}">
            <h2 class="mb-0">
                <button class="
                        btn btn-default btn-sm btn-block text-left
                        d-flex justify-content-between align-items-center
                        shadow-none
                    " type="button" data-toggle="collapse" data-target="#collapse{{$reply->id}}" aria-expanded="true" aria-controls="collapse{{$reply->id}}">
                    <div>
                        <i class="fa fa-angle-double-down mr-3"></i>
                        <span>{{ $reply->updated_at->diffForHumans() }}</span>
                    </div>
                    <span class="text-info font-weight-bold">
                        @if ($repliesCount = $reply->replies->count())
                        {{ $repliesCount }} more
                        {{ \Illuminate\Support\Str::of('reply')->plural($repliesCount) }}
                        @endif
                    </span>
                </button>
            </h2>
        </div>
        <div id="collapse{{ $reply->id }}" class="collapse {{ $reply->replies()->count() ? '' : 'show' }}" aria-labelledby="heading{{ $reply->id }}" data-parent="#accordionComment{{ $reply->id }}">
            <div class="card-body">
                <div class="media">
                    @if(class_basename(get_class($reply->supportable)) == 'Admin')
                    <img src="{{ $reply->admin->avatar ?? '' }}" class="mr-3 img-fluid rounded-circle" alt="avatar" width="33px">
                    <div class="media-body">
                        <h6 class="mt-0 font-weight-bold border-bottom">
                            {{ 'Admin' }}
                            <p class="small text-muted m-0"></p>
                        </h6>

                        <p class="mb-0">{{ $reply->message }}</p>
                        <form class="user mt-4" action="{{ route('user.support-tickets.store') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="hidden" name="parent" value="{{ $reply->id }}">
                                        <input type="hidden" name="subject" value="{{ $reply->subject }}">
                                        <input id="message" name="message" class="form-control form-control-user rounded @error('message') is-invalid @enderror" placeholder="Type your message here..." rows="5" required>{{ old('message') }}
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-user " type="submit">
                                Reply
                            </button>
                        </form>

                    </div>
                    @else
                    <img src="{{ $reply->user->avatar ?? '' }}" class="mr-3 img-fluid rounded-circle" alt="avatar" width="33px">
                    <div class="media-body">
                        <h6 class="mt-0 font-weight-bold border-bottom">
                            {{ ucwords($reply->user->first_name) }}
                            <p class="small text-muted m-0">{{ $reply->user->email ?? '' }}</p>
                        </h6>

                        <p class="mb-0">{{ $reply->message }}</p>
                        <div class="media">
                            <div class="media-body">
                                <form class="user mt-4" action="{{ route('user.support-tickets.store') }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="hidden" name="parent" value="{{ $reply->id }}">
                                                <input type="hidden" name="subject" value="{{ $reply->subject }}">
                                                <input id="message" name="message" class="form-control form-control-user rounded @error('message') is-invalid @enderror" placeholder="Type your message here..." rows="5" required>{{ old('message') }}
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-user " type="submit">
                                        Reply
                                    </button>
                                </form>
                            </div>
                        </div </div>
                        @endif
                    </div>

                    @if ($replies = $reply->replies)
                    <div class="mt-2">
                        @each('user.pages.support-tickets.includes.replies', $replies, 'reply')
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>