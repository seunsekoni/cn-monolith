@extends('user.layouts.dashboard')

@section('title', 'New Ticket')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/dropify/dropify.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">

        <style type="text/css">
            .dropify-wrapper .dropify-message p {
                font-size: 15px;
            }
        </style>
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Create New Ticket</h1>
        <a href="{{ route('user.support-tickets.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Create a new support ticket.</p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Create</h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form
                class="user"
                action="{{ route('user.support-tickets.store') }}"
                method="post"
                enctype="multipart/form-data"
            >
                @csrf

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">
                                Subject
                                <span class="text-danger sup">*</span>
                            </label>
                            <input
                                type="text"
                                id="subject"
                                name="subject"
                                class="form-control form-control-user @error('subject') is-invalid @enderror"
                                placeholder="Subject of the ticket"
                                value="{{ old('subject') }}"
                                required
                            >
                            <small class="form-text text-muted">Give the ticket a subject (title)</small>
                        </div>

                        <div class="form-group">
                            <label for="profile">
                                Message
                                <span class="text-danger sup">*</span>
                            </label>
                            <textarea
                                id="message"
                                name="message"
                                class="form-control form-control-user rounded @error('message') is-invalid @enderror"
                                placeholder="Type your message here..."
                                rows="5"
                                required
                            >{{ old('message') }}</textarea>
                            <small class="form-text text-muted">Give detailed information regarding the ticket</small>
                        </div>
                    </div>

                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Submit
                </button>
            </form>
        </div>
    </div>
@endsection
