@extends('user.layouts.dashboard')

@section('title', 'Update review')

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Edit review</h1>
        <a href="{{ route('user.reviews.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Update this review.</p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">
                Editing the <span class="text-danger">{{ $review->comment }}</span> review
            </h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form
                id="reviewForm"
                action="{{ route('user.reviews.update', ['review' => $review->id]) }}"
                class="user"
                method="POST"
            >
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label for="description">
                        Review
                        <span class="text-danger sup">*</span>
                    </label>
                    <textarea
                        rows="4"
                        id="body"
                        name="body"
                        class="form-control @error('body') is-invalid @enderror"
                        required
                    >{{ old('body') ?? $review->comment }}</textarea>
                </div>

                <button id="formButton" class="btn btn-success btn-user btn-block" type="submit">
                    Update Review
                </button>
            </form>
        </div>
    </div>
@endsection
