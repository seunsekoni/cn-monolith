<div class="accordion mb-3" id="accordionReview{{ $reply->id }}">
    <div class="card">
        <div class="card-header" id="heading{{ $reply->id }}">
            <h2 class="mb-0">
                <button
                    class="
                        btn btn-default btn-sm btn-block text-left
                        d-flex justify-content-between align-items-center
                        shadow-none
                    "
                    type="button"
                    data-toggle="collapse"
                    data-target="#collapse{{$reply->id}}"
                    aria-expanded="true"
                    aria-controls="collapse{{$reply->id}}"
                >
                    <div>
                        <i class="fa fa-angle-double-down mr-3"></i>
                        <span>{{ $reply->updated_at->diffForHumans() }}</span>
                    </div>
                    <span class="text-info font-weight-bold">
                        @if ($repliesCount = $reply->replies->count())
                            {{ $repliesCount }} more
                            {{ \Illuminate\Support\Str::of('reply')->plural($repliesCount) }}
                        @endif
                    </span>
                </button>
            </h2>
        </div>
        <div
            id="collapse{{ $reply->id }}"
            class="collapse {{ $reply->replies()->count() ? '' : 'show' }}"
            aria-labelledby="heading{{ $reply->id }}"
            data-parent="#accordionReview{{ $reply->id }}"
        >
            <div class="card-body">
                <div class="media">
                    <img
                        src="{{ $reply->user->avatar }}"
                        class="mr-3 img-fluid rounded-circle"
                        alt="avatar"
                        width="33px"
                    >
                    <div class="media-body">
                        <h6 class="mt-0 font-weight-bold border-bottom">
                            {{ ucwords($reply->user->full_name) }}
                            <p class="small text-muted m-0">{{ $reply->user->email }}</p>
                        </h6>

                        <p class="mb-0">{{ $reply->comment }}</p>

                        <div class="mt-1">
                            <div class="btn-group">
                                <a
                                    href="#"
                                    class="badge badge-{{ $reply->approved ? 'danger' : 'success' }} mr-2"
                                    onclick="
                                        event.preventDefault();
                                        document.getElementById('update-comment{{$reply->id}}-form').submit();
                                    "
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="currently {{ $reply->approved ? 'approved' : 'unapproved'}}"
                                >
                                    <i class="fa fa-{{ $reply->approved ? 'thumbs-down' : 'thumbs-up' }}"></i>
                                    {{ $reply->approved ? 'unapprove' : 'approve' }}
                                </a>
                                <a
                                    href="{{
                                        route('admin.reviews.edit', [
                                            'review' => $reply->id,
                                        ])
                                    }}"
                                    class="badge badge-primary mr-2"
                                >
                                    <i class="fa fa-edit"></i>
                                    Edit
                                </a>
                                <a
                                    href="#"
                                    data-id="{{ $reply->id }}"
                                    data-name="{{ $reply->comment }}"
                                    data-model="review"
                                    data-route="{{
                                        route('admin.reviews.destroy', [
                                            'review' => $reply->id,
                                        ])
                                    }}"
                                    data-toggle="modal"
                                    data-target="#deleteModal"
                                    class="badge badge-danger"
                                >
                                    <i class="fa fa-trash"></i>
                                    Delete
                                </a>
                            </div>
                            <form
                                id="update-comment{{$reply->id}}-form"
                                action="{{ route('admin.reviews.update', [
                                    'review' => $reply->id,
                                    'approved' => !$reply->approved
                                ]) }}"
                                method='POST'
                            >
                                <input type='hidden' name='_method' value='PUT'>
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>

                @if ($replies = $reply->replies)
                    <div class="mt-2">
                        @each('admin.pages.reviews.includes.comments', $replies, 'reply')
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
