@extends('user.layouts.dashboard')

@section('title', 'Reviews')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Reviews</h1>
    </div>
    <p class="">List of all your reviews.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')"/>
    @endif

    <div class="row row-cols-1 row-cols-md-3">
        <div class="col mb-3">
            <div class="card bg-primary text-white">
                <div class="card-body d-flex justify-content-between">
                    <div class="">
                        <h5 class="font-weight-bold">Total Reviews</h5>
                        <p class="small lead font-italic">Total number of all reviews</p>
                    </div>
                    <h1 class="font-weight-bold text-right">{{ $reviews->count() }}</h1>
                </div>
            </div>
        </div>
        <div class="col mb-3">
            <div class="card bg-primary text-white">
                <div class="card-body d-flex justify-content-between">
                    <div class="">
                        <h5 class="font-weight-bold">Total Approved</h5>
                        <p class="small lead font-italic">Visible to the public</p>
                    </div>
                    <h1 class="font-weight-bold text-right">{{ $reviews->where('approved', true)->count() }}</h1>
                </div>
            </div>
        </div>
        <div class="col mb-3">
            <div class="card bg-primary text-white">
                <div class="card-body d-flex justify-content-between">
                    <div class="">
                        <h5 class="font-weight-bold">Total Unapproved</h5>
                        <p class="small lead font-italic">Yet to be reviewed</p>
                    </div>
                    <h1 class="font-weight-bold text-right">{{ $reviews->where('approved', false)->count() }}</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th width="7%">S\N</th>
                        <th>Review</th>
                        <th>Status</th>
                        <th>Last Update</th>
                        <th width="25%">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($reviews as $review)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                <div class='d-flex justify-content-between'>
                                    <a href="{{ route('user.reviews.show', ['review' => $review->id]) }}">{{ truncate($review->comment)  }}</a>
                                    <div>
                                        <span
                                            class="badge badge-{{ (new \ReflectionClass($review->reviewable_type))->getShortName() == 'BusinessListing' ? 'success' : 'danger' }}">
                                        {{ (new \ReflectionClass($review->reviewable_type))->getShortName() == 'BusinessListing' ? 'Listing' : 'Business' }}
                                        </span>
                                    </div>
                                </div>
                            </td>

                            @if($review->approved)
                                <td>Approved</td>
                            @else
                                <td>Unapproved</td>
                            @endif
                            <td>{{ $review->updated_at->diffForHumans() }}</td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <a
                                        href="{{ route('user.reviews.show', ['review' => $review->id]) }}"
                                        class="btn btn-sm btn-info mr-2"
                                    >
                                        <i class="fa fa-eye"></i>
                                        Show
                                    </a>
                                    @if(!$review->approved)
                                        <a
                                            href="{{ route('user.reviews.edit', ['review' => $review->id]) }}"
                                            class="btn btn-sm btn-primary mr-2"
                                        >
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        <a
                                            data-toggle="modal"
                                            href="#deleteReview{{ $review->id }}"
                                            class="btn btn-sm btn-danger mr-2"
                                        >
                                            <i class="fa fa-trash"></i>
                                            Delete
                                        </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <div id="deleteReview{{ $review->id }}" class="modal fade">
                            <div class="modal-dialog modal-confirm">
                                <div class="modal-content">
                                    <div class="modal-header flex-column">
                                        <div class="icon-box">
                                            <i class="fas fa-times"></i>
                                        </div>
                                        <h4 class="modal-title w-100">Are you sure?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Do you really want to delete this review? - <span class="font-weight-bold">"{{ $review->comment }}"</span>
                                        </p>
                                        <p>This process cannot be undone.</p>
                                    </div>
                                    <form method="post"
                                          action="{{ route('user.reviews.destroy', ['review' => $review->id]) }}">
                                        @method('DELETE')
                                        @csrf
                                        <div class="modal-footer justify-content-center">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                Cancel
                                            </button>
                                            <button type="submit" class="btn btn-danger"><a>Delete</a></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation/>

@once
    @push('scripts')
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('#dataTable').DataTable();
            });
        </script>
    @endpush
@endonce
