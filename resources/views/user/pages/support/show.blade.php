@extends('layouts.user.dashboard')

@section('title', 'Read Ticket')

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Read Ticket</h1>
        <a href="{{ route('admin.reviews.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class=""></p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-danger">
                {{ $support->message }}
            </h6>
        </div>
        <div class="card-body">
            <div class="alert alert-info">
                There are {{ $supportCount = $support->replies()->count() }}
                {{ \Illuminate\Support\Str::of('reply')->plural($supportCount) }}
                to this ticket.
            </div>

            @each('user.support-tickets.includes.replies', $support->replies, 'reply')
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation />
