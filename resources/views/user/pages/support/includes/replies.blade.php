<div class="accordion mb-3" id="accordionComment{{ $reply->id }}">
    <div class="card">
        <div class="card-header" id="heading{{ $reply->id }}">
            <h2 class="mb-0">
                <button
                    class="
                        btn btn-default btn-sm btn-block text-left
                        d-flex justify-content-between align-items-center
                        shadow-none
                    "
                    type="button"
                    data-toggle="collapse"
                    data-target="#collapse{{$reply->id}}"
                    aria-expanded="true"
                    aria-controls="collapse{{$reply->id}}"
                >
                    <div>
                        <i class="fa fa-angle-double-down mr-3"></i>
                        <span>{{ $reply->updated_at->diffForHumans() }}</span>
                    </div>
                    <span class="text-info font-weight-bold">
                        @if ($repliesCount = $reply->replies->count())
                            {{ $repliesCount }} more
                            {{ \Illuminate\Support\Str::of('reply')->plural($repliesCount) }}
                        @endif
                    </span>
                </button>
            </h2>
        </div>
        <div
            id="collapse{{ $reply->id }}"
            class="collapse {{ $reply->replies()->count() ? '' : 'show' }}"
            aria-labelledby="heading{{ $reply->id }}"
            data-parent="#accordionComment{{ $reply->id }}"
        >
            <div class="card-body">
                <div class="media">
                    @if(class_basename(get_class($reply->supportable)) == 'Admin')
                        <img
                            src="{{ $reply->admin->avatar ?? '' }}"
                            class="mr-3 img-fluid rounded-circle"
                            alt="avatar"
                            width="33px"
                        >
                        <div class="media-body">
                            <h6 class="mt-0 font-weight-bold border-bottom">
                                {{ 'Admin' }}
                                <p class="small text-muted m-0"></p>
                            </h6>

                            <p class="mb-0">{{ $reply->message }}</p>
                        </div>
                    @else
                        <img
                            src="{{ $reply->user->avatar ?? '' }}"
                            class="mr-3 img-fluid rounded-circle"
                            alt="avatar"
                            width="33px"
                        >
                        <div class="media-body">
                            <h6 class="mt-0 font-weight-bold border-bottom">
                                {{ ucwords($reply->user->full_name ?? '') }}
                                <p class="small text-muted m-0">{{ $reply->user->email ?? '' }}</p>
                            </h6>

                            <p class="mb-0">{{ $reply->message }}</p>
                        </div>
                    @endif
                </div>

                @if ($replies = $reply->replies)
                    <div class="mt-2">
                        @each('user.support.includes.replies', $replies, 'reply')
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
