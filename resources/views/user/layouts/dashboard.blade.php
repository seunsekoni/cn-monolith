@extends('user.layouts.app')

@section('content')
    <div id="wrapper">
        @include('user.includes.sidebar')

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                @include('user.includes.topbar')

                <!-- Begin Page Content -->
                <div class="container-fluid vh-100">
                    @yield('main-content')
                </div>
            </div>
        </div>
    </div>
@endsection
