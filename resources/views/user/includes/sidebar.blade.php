<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar">
    <a href="{{ route('site.index') }}" class="sidebar-brand d-flex align-items-center justify-content-center">
        <img src="{{asset('img/admin/CN-Logo-White.png')}}" alt="Logo" class="cn-admin-logo"/>
    </a>

    <hr class="sidebar-divider">

    <li class="nav-item {{ request()->routeIs('user.dashboard') ? 'active' : '' }}">
        <a href="{{ route('user.dashboard') }}" class="nav-link">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    @if (optional(auth()->user()->business)->verified)
        <hr class="sidebar-divider">

        <div class="sidebar-heading">
            Main
        </div>

        <li
            class="nav-item {{
                request()->routeIs('user.business.*') ? 'active' : ''
            }}"
        >
            <a
                class="nav-link collapsed"
                href="#"
                data-toggle="collapse"
                data-target="#businessCollapse"
                aria-expanded="true"
                aria-controls="manageBusiness"
            >
                <i class="fas fa-fw fa-laptop-house"></i>
                <span>Manage Business</span>
            </a>
            <div
                id="businessCollapse"
                class="collapse {{
                    (request()->routeIs('user.business.*') || request()->routeIs('user.business-hours.index')) ? 'show' : ''
                }}"
                aria-labelledby="manageBusiness"
                data-parent="#accordionSidebar"
            >
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Manage Your Business</h6>
                    <a
                        class="collapse-item {{ request()->routeIs('user.business.index') ? 'active' : '' }}"
                        href="{{ route('user.business.index') }}"
                    >
                        View Details
                    </a>
                    <a
                        class="collapse-item {{
                            (
                                request()->routeIs('user.business.edit')
                                || request()->routeIs('user.business.contact.index')
                                || request()->routeIs('user.business.locations.index')
                            )
                            ? 'active' : ''
                        }}"
                        href="{{ route('user.business.edit') }}"
                    >
                        Update Data
                    </a>
                    <a
                        class="collapse-item"
                        href="#"
                    >
                        Reviews
                    </a>
                    <a
                        class="collapse-item"
                        href="#"
                    >
                        Comments
                    </a>
                    <a
                        class="collapse-item {{request()->routeIs('user.business-hours.*') ? 'active' : ''}}"
                        href="{{route('user.business-hours.index')}}"
                    >
                        Business Hours
                    </a>
                </div>
            </div>
        </li>

        <li
            class="nav-item {{
                request()->routeIs('user.business.listings.*') ? 'active' : ''
            }}"
        >
            <a
                class="nav-link collapsed"
                href="#"
                data-toggle="collapse"
                data-target="#listingsCollapse"
                aria-expanded="true"
                aria-controls="collapseTwo"
            >
                <i class="fas fa-fw fa-list"></i>
                <span>Listings</span>
            </a>
            <div
                id="listingsCollapse"
                class="collapse {{
                    request()->routeIs('user.business.listings.*') ? 'show' : ''
                }}"
                aria-labelledby="manageListings"
                data-parent="#accordionSidebar"
            >
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Manage Listings</h6>
                    <a
                        class="collapse-item {{ request()->routeIs('user.business.listings.create') ? 'active' : '' }}"
                        href="{{ route('user.business.listings.create') }}"
                    >
                        Add New
                    </a>
                    <a
                        class="collapse-item {{
                            request()->routeIs('user.business.listings.index') ? 'active' : ''
                        }}"
                        href="{{ route('user.business.listings.index') }}"
                    >
                        View All
                    </a>
                </div>

            </div>
        </li>
    @endif

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Personal
    </div>

    <li class="nav-item {{ request()->routeIs('user.reviews.*') ? 'active' : '' }}">
        <a href="{{ route('user.reviews.index') }}" class="nav-link">
            <i class="fas fa-fw fa-comments"></i>
            <span>My Reviews</span>
        </a>
    </li>

    <li class="nav-item {{ request()->routeIs('user.comments.*') ? 'active' : '' }}">
        <a href="{{ route('user.comments.index') }}" class="nav-link">
            <i class="fas fa-fw fa-comment-dots"></i>
            <span>My Comments</span>
        </a>
    </li>

    <li
        class="nav-item {{
            request()->routeIs('user.support-tickets.*') ? 'active' : ''
        }}"
    >
        <a
            class="nav-link collapsed"
            href="#"
            data-toggle="collapse"
            data-target="#supportTicketCollapse"
            aria-expanded="true"
            aria-controls="collapseTwo"
        >
            <i class="fas fa-fw fa-headset"></i>
            <span>Support</span>
        </a>
        <div
            id="supportTicketCollapse"
            class="collapse {{
                request()->routeIs('user.support-tickets.*') ? 'show' : ''
            }}"
            aria-labelledby="manageSupportTickets"
            data-parent="#accordionSidebar"
        >
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Manage Support Tickets</h6>
                <a
                    class="collapse-item {{ request()->routeIs('user.support-tickets.create') ? 'active' : '' }}"
                    href="{{ route('user.support-tickets.create') }}"
                >
                    Create New Ticket
                </a>
                <a
                    class="collapse-item {{
                        request()->routeIs('user.support-tickets.index') ? 'active' : ''
                    }}"
                    href="{{ route('user.support-tickets.index') }}"
                >
                    View All
                </a>
            </div>
        </div>
    </li>


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline w-100 sidebarToggleBg">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
