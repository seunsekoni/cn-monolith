@extends('layouts.app')

@section('title', 'Home')

@section('styles')
  <link href="{{ asset('css/theme-home.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
@include('includes.nav')
<div class="main-container">
    <section class="imagebg home height-100" data-overlay="7">
        <div
            class="background-image-holder"
            style="background-image: url({{ mapMonthBackground('home') }})"
        ></div>
        <div class="container pos-vertical-center">
            <div class="row align-items-center mb-4 home-search">
                <div class="col-12">
                    <div class="d-flex justify-content-center">
                        <div class="hidden-xs hidden-sm">
                            <img src="{{ asset('img/cn-logo-light-2.png') }}" class="img-logo mr-4">
                        </div>
                        <form action="{{ route('search') }}" method="GET" class="w-100">
                            <div class="input-group search-bar mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon3">Search</span>
                                </div>
                                <input type="text" class="form-control" name="query" placeholder="products, services or business" aria-label="Recipient's username" aria-describedby="button-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit" id="button-addon2">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-4 col-lg-4 col-xl-4 mb-4 mb-lg-0 mb-md-0">
                    <div class="card bg-dark mb-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="imagebg">
                                        <a href="#">
                                            <div
                                                class="background-image-holder"
                                                style="background-image: url({{ asset('img/home-1.jpg') }})">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <a href="#" class="card-title type-bold mb-1">
                                        {{ truncate('Attend Africa’s Largest Virtual Business Fair', 25) }}
                                    </a>
                                    <p class="mb-2 text-paragraph">
                                        <small class="card-text">
                                            {{ truncate('August 27-28, 2020. Refer a friend and win a laptop.', 50) }}
                                        </small>
                                    </p>
                                    <a href="#" class="type-uppercase mb-0">
                                        <small class="color-primary type-medium">Learn More</small>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4 col-lg-4 col-xl-4 mb-4 mb-lg-0 mb-md-0">
                    <div class="card bg-dark mb-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="imagebg">
                                        <a href="#">
                                            <div
                                                class="background-image-holder"
                                                style="background-image: url({{ asset('img/home-2.jpg') }})">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <a href="#" class="card-title type-bold mb-1">
                                        {{ truncate('Establish the Right Corporate Branding', 25) }}
                                    </a>
                                    <p class="mb-2 text-paragraph">
                                        <small class="card-text">
                                            {{ truncate('Make your business stand out with Platform Branding', 50) }}
                                        </small>
                                    </p>
                                    <a href="#" class="type-uppercase mb-0">
                                        <small class="color-primary type-medium">Learn More</small>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4 col-lg-4 col-xl-4 mb-4 mb-lg-0 mb-md-0">
                    <div class="card bg-dark mb-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="imagebg">
                                        <a href="#">
                                            <div
                                                class="background-image-holder"
                                                style="background-image: url({{ asset('img/home-3.jpg') }})">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <a href="#" class="card-title type-bold mb-1">
                                        {{ truncate('Advertise in CN & watch your business grow', 25) }}
                                    </a>
                                    <p class="mb-2 text-paragraph">
                                        <small class="card-text">
                                            {{ truncate('Advertse with CN and watch your business grow', 50) }}
                                        </small>
                                    </p>
                                    <a href="#" class="type-uppercase mb-0">
                                        <small class="color-primary type-medium">Learn More</small>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if($count = $categories->count())
                <div class="category-slider mt-3 p-2 w-100">
                    <div class="{{ $count > 5 ? 'browse-list' : '' }} category-content">
                        @foreach($categories as $category)
                            <div class="slide">
                                <a
                                    class="d-flex align-items-center"
                                    href="{{ route('categories.index', ['category' => $category]) }}"
                                    title="{{ $category->name }}"
                                >
                                    {{-- <img class="img-fluid mr-2" src="{{ $category->white_icon }}" alt="icon" style="width: 25px;"> --}}
                                    <span class="slick-category-name">{{ truncate($category->name, 17) }}</span>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </section>

    @include('includes.main-footer')
</div>
@endsection
