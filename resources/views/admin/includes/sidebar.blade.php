<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar">
    <a href="{{ route('admin.dashboard') }}" class="sidebar-brand d-flex align-items-center justify-content-center">
        <img src="{{asset('img/admin/CN-Logo-White.png')}}" alt="CN Admin" class="cn-admin-logo"/>
    </a>

    <hr class="sidebar-divider">

    <li class="nav-item {{ request()->routeIs('admin.dashboard') ? 'active' : '' }}">
        <a href="{{ route('admin.dashboard') }}" class="nav-link">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Main
    </div>
    <li
        class="nav-item {{
            (
                request()->routeIs('admin.lookups.categories.*')
                || request()->routeIs('admin.lookups.business-types.*')
                || request()->routeIs('admin.lookups.kyc-requirements.*')
            ) ? 'active' : ''
        }}"
    >
        <a
            class="nav-link collapsed"
            href="#"
            data-toggle="collapse"
            data-target="#componentCollapse"
            aria-expanded="true"
            aria-controls="collapseTwo"
        >
            <i class="fas fa-fw fa-cog"></i>
            <span>Components</span>
        </a>
        <div
            id="componentCollapse"
            class="collapse {{
                (
                    request()->routeIs('admin.lookups.categories.*')
                    || request()->routeIs('admin.lookups.business-types.*')
                    || request()->routeIs('admin.lookups.kyc-requirements.*')
                ) ? 'show' : ''
            }}"
            aria-labelledby="component"
            data-parent="#accordionSidebar"
        >
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">System Settings</h6>
                <a
                    class="collapse-item {{ request()->routeIs('admin.lookups.categories.*') ? 'active' : '' }}"
                    href="{{ route('admin.lookups.categories.index') }}"
                >
                    Categories
                </a>
                <a
                    class="collapse-item {{ request()->routeIs('admin.lookups.business-types.*') ? 'active' : '' }}"
                    href="{{ route('admin.lookups.business-types.index') }}"
                >
                    Business Types
                </a>
                <a
                    class="collapse-item {{ request()->routeIs('admin.lookups.kyc-requirements.*') ? 'active' : '' }}"
                    href="{{ route('admin.lookups.kyc-requirements.index') }}"
                >
                    KYC Requirements
                </a>
            </div>
        </div>
    </li>
    <li
        class="nav-item {{
            request()->routeIs('admin.businesses.*') ? 'active' : ''
        }}"
    >
        <a
            class="nav-link collapsed"
            href="#"
            data-toggle="collapse"
            data-target="#businessCollapse"
            aria-expanded="true"
            aria-controls="collapseTwo"
        >
            <i class="fas fa-fw fa-briefcase"></i>
            <span>Businesses</span>
        </a>
        <div
            id="businessCollapse"
            class="collapse {{
                request()->routeIs('admin.businesses.*') ? 'show' : ''
            }}"
            aria-labelledby="manageBusinesses"
            data-parent="#accordionSidebar"
        >
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Manage Businesses</h6>
                <a
                    class="collapse-item {{ request()->routeIs('admin.businesses.create') ? 'active' : '' }}"
                    href="{{ route('admin.businesses.create') }}"
                >
                    Add New
                </a>
                <a
                    class="collapse-item {{
                        request()->routeIs('admin.businesses.index') ? 'active' : ''
                    }}"
                    href="{{ route('admin.businesses.index') }}"
                >
                    View All
                </a>
            </div>
        </div>
    </li>

    <li
        class="nav-item {{
            request()->routeIs('admin.listings.*') ? 'active' : ''
        }}"
    >
        <a
            class="nav-link collapsed"
            href="#"
            data-toggle="collapse"
            data-target="#listingsCollapse"
            aria-expanded="true"
            aria-controls="collapseTwo"
        >
            <i class="fas fa-fw fa-list"></i>
            <span>Listings</span>
        </a>
        <div
            id="listingsCollapse"
            class="collapse {{
                request()->routeIs('admin.listings.*') ? 'show' : ''
            }}"
            aria-labelledby="manageListings"
            data-parent="#accordionSidebar"
        >
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Manage Listings</h6>
                <a
                    class="collapse-item {{ request()->routeIs('admin.listings.create') ? 'active' : '' }}"
                    href="{{ route('admin.listings.create') }}"
                >
                    Add New
                </a>
                <a
                    class="collapse-item {{
                        request()->routeIs('admin.listings.index') ? 'active' : ''
                    }}"
                    href="{{ route('admin.listings.index') }}"
                >
                    View All
                </a>
            </div>
        </div>
    </li>

    <li class="nav-item {{ request()->routeIs('admin.admin-user.*')? 'active':''}}">
        <a
            class="nav-link collapsed"
            href="#"
            data-toggle="collapse"
            data-target="#userCollapse"
            aria-expanded="true"
            aria-controls="collapseTwo"
        >
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span>
        </a>
        <div id="userCollapse"
             class="collapse {{ request()->routeIs('admin.admin-user.*')? 'show':''}}"
             aria-labelledby="manageUsers" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Manage Users</h6>
                <a class="collapse-item {{request()->routeIs('admin.admin-user.create')? 'active':''}}"
                   href="{{route('admin.admin-user.create')}}">Add New</a>
                <a class="collapse-item {{request()->routeIs('admin.admin-user.index')? 'active':''}}"
                   href="{{route('admin.admin-user.index')}}">View All</a>
            </div>
        </div>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Resources
    </div>

    <li
        class="nav-item {{
            (
                request()->routeIs('admin.lookups.regions.*')
                || request()->routeIs('admin.lookups.countries.*')
                || request()->routeIs('admin.lookups.states.*')
                || request()->routeIs('admin.lookups.cities.*')
            ) ? 'active' : ''
        }}"
    >
        <a
            class="nav-link collapsed"
            href="#"
            data-toggle="collapse"
            data-target="#lookupCollapse"
            aria-expanded="true"
            aria-controls="collapseTwo"
        >
            <i class="fas fa-fw fa-map"></i>
            <span>Locations</span>
        </a>
        <div
            id="lookupCollapse"
            class="collapse {{
                (
                    request()->routeIs('admin.lookups.regions.*')
                    || request()->routeIs('admin.lookups.countries.*')
                    || request()->routeIs('admin.lookups.states.*')
                    || request()->routeIs('admin.lookups.cities.*')
                ) ? 'show' : ''
            }}"
            aria-labelledby="locations"
            data-parent="#accordionSidebar"
        >
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Config Data</h6>
                <a
                    class="collapse-item {{ request()->routeIs('admin.lookups.regions.*') ? 'active' : '' }}"
                    href="{{ route('admin.lookups.regions.index') }}"
                >
                    Regions
                </a>
                <a
                    class="collapse-item {{ request()->routeIs('admin.lookups.countries.*') ? 'active' : '' }}"
                    href="{{ route('admin.lookups.countries.index') }}"
                >
                    Countries
                </a>
                <a
                    class="collapse-item {{ request()->routeIs('admin.lookups.states.*') ? 'active' : '' }}"
                    href="{{ route('admin.lookups.states.index') }}"
                >
                    States
                </a>
                <a
                    class="collapse-item {{ request()->routeIs('admin.lookups.cities.*') ? 'active' : '' }}"
                    href="{{ route('admin.lookups.cities.index') }}"
                >
                    Cities
                </a>
            </div>
        </div>
    </li>

    <li
        class="nav-item {{
        (
            request()->routeIs('admin.lookups.payment-types.*')
            || request()->routeIs('admin.lookups.payment-gateways.*')
        ) ? 'active' : ''
    }}"
    >
        <a
            class="nav-link collapsed"
            href="#"
            data-toggle="collapse"
            data-target="#paymentCollapse"
            aria-expanded="true"
            aria-controls="collapseTwo"
        >
            <i class="fas fa-fw fa-credit-card"></i>
            <span>Payment</span>
        </a>
        <div
            id="paymentCollapse"
            class="collapse {{
                (
                    request()->routeIs('admin.lookups.payment-types.*')
                    || request()->routeIs('admin.lookups.payment-gateways.*')
                ) ? 'show' : ''
            }}"
            aria-labelledby="payment"
            data-parent="#accordionSidebar"
        >
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Payment Settings</h6>
                <a
                    class="collapse-item {{ request()->routeIs('admin.lookups.payment-types.*') ? 'active' : '' }}"
                    href="{{ route('admin.lookups.payment-types.index') }}"
                >
                    Types
                </a>
                <a
                    class="collapse-item {{ request()->routeIs('admin.lookups.payment-gateways.*') ? 'active' : '' }}"
                    href="{{ route('admin.lookups.payment-gateways.index') }}"
                >
                    Gateways
                </a>
            </div>
        </div>
    </li>


    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Management
    </div>

    <li class="nav-item {{ request()->routeIs('admin.pages.index') ? 'active':''}}">
        <a
            class="nav-link collapsed"
            href="#"
            data-toggle="collapse"
            data-target="#frontendManagementCollapse"
            aria-expanded="true"
            aria-controls="collapseTwo"
        >
            <i class="fas fa-fw fa-desktop"></i>
            <span>Frontend</span>
        </a>
        <div id="frontendManagementCollapse"
             class="collapse {{
                (
                    request()->routeIs('admin.pages.*')
                    || request()->routeIs('admin.menus.*')
                    || request()->routeIs('admin.call-to-actions.*')
                ) ? 'show':''
            }}"
             aria-labelledby="lookupConfig"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Website</h6>
                <a
                    class="collapse-item {{request()->routeIs('admin.pages.*')? 'active':''}}"
                    href="{{ route('admin.pages.index') }}"
                >
                    Pages
                </a>
                <a
                    class="collapse-item {{request()->routeIs('admin.menus.*')? 'active':''}}"
                    href="{{ route('admin.menus.index') }}"
                >
                    Menus
                </a>
                <a
                    class="collapse-item {{request()->routeIs('admin.call-to-actions.*')? 'active':''}}"
                    href="{{ route('admin.call-to-actions.index') }}"
                >
                    Call To Action
                </a>
            </div>
        </div>
    </li>

    <li
        class="nav-item {{
            (
                request()->routeIs('admin.reviews.*')
                || request()->routeIs('admin.comments.*')
                || request()->routeIs('admin.support-tickets.*')
                || request()->routeIs('admin.application_issues')
                || request()->routeIs('admin.documentation')
            ) ? 'active' : ''
        }}"
    >
        <a
            class="nav-link collapsed"
            href="#"
            data-toggle="collapse"
            data-target="#otherDataCollapse"
            aria-expanded="true"
            aria-controls="collapseTwo"
        >
            <i class="fas fa-fw fa-question"></i>
            <span>Support</span>
        </a>
        <div
            id="otherDataCollapse"
            class="collapse {{
                (
                    request()->routeIs('admin.reviews.*')
                    || request()->routeIs('admin.comments.*')
                    || request()->routeIs('admin.support-tickets.*')
                    || request()->routeIs('admin.application_issues')
                    || request()->routeIs('admin.documentation')
                ) ? 'show' : ''
            }}"
            aria-labelledby="otherData"
            data-parent="#accordionSidebar"
        >
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Other Data</h6>
                <a
                    class="collapse-item {{ request()->routeIs('admin.comments.*') ? 'active' : '' }}"
                    href="{{ route('admin.comments.index') }}"
                >
                    Comments
                </a>
                <a
                    class="collapse-item {{ request()->routeIs('admin.reviews.*') ? 'active' : '' }}"
                    href="{{ route('admin.reviews.index') }}"
                >
                    Reviews
                </a>
                <a
                    class="collapse-item {{ request()->routeIs('admin.support-tickets.*') ? 'active' : '' }}"
                    href="{{ route('admin.support-tickets.index') }}"
                >
                    Support Tickets
                </a>
                <a class="collapse-item {{request()->routeIs('admin.application_issues')? 'active':''}}"
                   href="{{route('admin.application_issues')}}">Application Issue</a>
                <a class="collapse-item {{request()->routeIs('admin.documentation')? 'active':''}}"
                   href="{{route('admin.documentation')}}">Documentation</a>

            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    @can('view audit')
    <li class="nav-item {{ request()->routeIs('admin.audit-trails.*') ? 'active' : '' }}">
        <a href="{{ route('admin.audit-trails.index') }}" class="nav-link">
            <i class="fas fa fa-history"></i>
            <span>Audit Trails</span>
        </a>
    </li>
    @endcan

    <hr class="sidebar-divider">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline w-100 sidebarToggleBg">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
