@extends('admin.layouts.auth')

@section('title', 'Admin Request Password Reset')

@section('auth-content')
    <div class="col-lg-6">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Request Password Reset</h1>
            </div>

            @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            @if (session('status'))
                <x-alert type="success" :message="session('status')" />
            @endif

            <form
                class="user"
                method="post"
                action="{{ route('admin.password.email') }}"
            >
                @csrf
                <div class="form-group">
                    <input
                        type="email"
                        class="form-control form-control-user @error('email') is-invalid @enderror"
                        id="exampleInputEmail"
                        aria-describedby="emailHelp"
                        placeholder="Enter Email Address..."
                        name="email"
                        value="{{ old('email') }}"
                        required
                    >
                </div>
                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Reset Password
                </button>
            </form>
            <hr>
            <div class="text-center">
                <a class="small" href="{{ route('admin.login') }}">Sign In</a>
            </div>
        </div>
    </div>
@endsection
