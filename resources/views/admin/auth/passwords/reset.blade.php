@extends('admin.layouts.auth')

@section('title', 'Admin Reset Password')

@section('auth-content')
    <div class="col-lg-6">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Reset Password!</h1>
            </div>

            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form class="user" method="POST" action="{{ route('admin.password.update') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group">
                    <input
                        type="email"
                        class="form-control form-control-user @error('email') is-invalid @enderror"
                        placeholder="Enter Email Address..."
                        name="email"
                        value="{{ $email }}"
                        required
                    >
                </div>

                <div class="form-group">
                    <input
                        type="password"
                        class="form-control form-control-user @error('password') is-invalid @enderror"
                        placeholder="Password"
                        name="password"
                        required
                    >
                </div>

                <div class="form-group">
                    <input
                        type="password"
                        class="form-control form-control-user @error('password') is-invalid @enderror"
                        placeholder="Confirm Password"
                        name="password_confirmation"
                        required
                    >
                </div>

                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Reset Password
                </button>
            </form>
        </div>
    </div>
@endsection
