@extends('admin.layouts.dashboard')

@section('title', "Update Profile")

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Update Profile</h1>
    </div>
    <p class="">Update profile.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">
                Update <span class="text-danger"></span> profile
            </h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form class="user" action="{{ route('admin.profile.update') }}" method="post">
                @method('PUT')
                @csrf

                <div class="form-group">
                    <input
                        type="text"
                        name="first_name"
                        class="form-control form-control-user @error('first_name') is-invalid @enderror"
                        placeholder="Name"
                        value="{{ old('first_name') ?: $admin->first_name }}"
                        required
                    >
                    <small class="form-text text-muted">First Name.</small>
                </div>

                <div class="form-group">
                    <input
                        type="text"
                        name="last_name"
                        class="form-control form-control-user @error('last_name') is-invalid @enderror"
                        placeholder="Last Name"
                        value="{{ old('last_name') ?: $admin->last_name }}"
                        required
                    >
                    <small class="form-text text-muted">Last Name.</small>
                </div>

                <div class="form-group">
                    <input
                        type="text"
                        name="phone"
                        class="form-control form-control-user @error('phone') is-invalid @enderror"
                        placeholder="Phone Number"
                        value="{{ old('phone') ?: $admin->phone }}"
                        required
                    >
                    <small class="form-text text-muted">Phone Number.</small>
                </div>

                <div class="form-group">
                    <input
                        type="text"
                        name="email"
                        class="form-control form-control-user @error('email') is-invalid @enderror"
                        placeholder="Email"
                        value="{{ old('email') ?: $admin->email }}"
                        required
                    >
                    <small class="form-text text-muted">Email.</small>
                </div>

                <button class="btn btn-primary btn-block btn-user" type="submit">
                    Update
                </button>
            </form>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">
                Update <span class="text-danger"></span> password
            </h6>
        </div>
        <div class="card-body">
            <form class="user" action="{{ route('admin.profile.update.password') }}" method="post">
                @method('PUT')
                @csrf

                <div class="form-group">
                    <input
                        type="password"
                        name="old_password"
                        class="form-control form-control-user @error('old_password') is-invalid @enderror"
                        placeholder="Old Password"
                        required
                    >
                    <small class="form-text text-muted">Old Password.</small>
                </div>

                <div class="form-group">
                    <input
                        type="password"
                        name="password"
                        class="form-control form-control-user @error('password') is-invalid @enderror"
                        placeholder="Password"
                        required
                    >
                    <small class="form-text text-muted">New Password.</small>
                </div>

                <div class="form-group">
                    <input
                        type="password"
                        name="password_confirmation"
                        class="form-control form-control-user @error('password_confirmation') is-invalid @enderror"
                        placeholder="Confirm Password"
                        required
                    >
                    <small class="form-text text-muted">Password Confirmation.</small>
                </div>

                <button class="btn btn-primary btn-block btn-user" type="submit">
                    Update Password
                </button>
            </form>
        </div>
    </div>
@endsection

