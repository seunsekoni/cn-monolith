@extends('admin.layouts.dashboard')

@section('title', 'View Page')

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Pages</h1>
        <a href="{{ route('admin.pages.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">View a page.</p>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{$page->title}}</h6>
        </div>
        <div class="card-body">

          {!! $page->body !!}
        </div>
    </div>
@endsection
