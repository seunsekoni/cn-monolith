@extends('admin.layouts.dashboard')

@section('title', 'Create Pages')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
        <style>
            .ck-editor__editable_inline {
                min-height: 400px;
            }
        </style>
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Create Pages</h1>
        <a href="{{ route('admin.pages.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Create a new page.</p>
    @if (session('success'))
        <x-alert type="success" :message="session('success')"/>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Create</h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form
                method="post"
                action="{{ route('admin.pages.store') }}"
                class="user"
                enctype="multipart/form-data"
            >
                @csrf

                <div class="text-center">
                    <div class="form-group custom-control custom-checkbox">
                        <input type="checkbox" name="status" id="status" class="custom-control-input" {{ old('status') ? 'checked' : '' }}>
                        <label class="custom-control-label" for="status">
                            Toggle Public Visibility
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="title">Title<span class="text-danger sup">*</span></label>
                    <input
                        type="text"
                        name="title"
                        id="title"
                        class="form-control form-control-user @error('title') is-invalid @enderror"
                        placeholder="Title"
                        value="{{ old('title') }}"
                        required
                    >
                    <small class="form-text text-muted">this field is where the title of the page is imputed.</small>
                </div>

                <div class="form-group">
                    <label for="description">Description<span class="text-danger sup">*</span></label>
                    <input
                        type="text"
                        name="description"
                        id="description"
                        class="form-control form-control-user @error('description') is-invalid @enderror"
                        placeholder="Description"
                        value="{{ old('description') }}"
                        required
                    >
                    <small class="form-text text-muted">this field is where the description of the page is imputed.</small>
                </div>

                <div class="form-group">
                    <label for="keywords">Add Keywords</label>
                    <select
                        id="keywords"
                        name="keywords[]"
                        class="select2 form-control form-control-user @error('keywords') is-invalid @enderror"
                        multiple
                    ></select>
                    <small class="form-text text-muted">
                        Add keywords to improve SEO (separated by <code>comma</code>)
                    </small>
                </div>

                <div class="form-group">
                    <label for="body">Body<span class="text-danger sup">*</span></label>
                    <textarea name="body" id="body">{{ old('body') }}</textarea>
                </div>

                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Submit
                </button>
            </form>
        </div>
    </div>
@endsection
@once
    @push('scripts')
        <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
        <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
        <script>
            ClassicEditor
                .create(document.querySelector('#body'))
                .then(editor => {
                })
                .catch(error => {
                    console.error(error);
                });

            // Tags
            $('#keywords.select2').select2({
                tags: true,
                tokenSeparators: [','],
                placeholder: 'Add keywords',
            });

            let keywords = @json(old('keywords'), JSON_PRETTY_PRINT);
            if (keywords) {
                getKeywords(keywords)
            }

            function getKeywords(keywords) {
                keywords.forEach(keyword => {
                    var newOption = new Option(keyword, keyword, true, true);
                    $('#keywords.select2').append(newOption).trigger('change');
                });
            }
        </script>
    @endpush
@endonce
