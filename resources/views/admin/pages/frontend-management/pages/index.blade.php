@extends('admin.layouts.dashboard')

@section('title', 'Pages')
@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    @endpush
@endonce
@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Pages</h1>
        <a href="{{ route('admin.pages.create') }}" class="btn btn-primary">Create A New Page</a>
    </div>
    <p class="">List of Pages available in the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')"/>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Pages</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="callToActionDataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th width="7%">S\N</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Keywords</th>
                        <th>Description</th>
                        <th width="25%">Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation />
<!-- Restore Modal -->
<x-restore-confirmation />
@once
    @push('scripts')
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#callToActionDataTable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.pages.index') }}",
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'title'
                        },
                        {
                            data: 'status',
                            searchable: false,
                            render: function (data) {
                                return `
                                    <i
                                        class="
                                            fa fa-${data ? 'check' : 'minus'}
                                            text-${data ? 'success' : 'danger'}
                                            font-weight-bold
                                        "
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="${data ? 'verified' : 'unverified'}"
                                    ></i>
                                `;
                            }
                        },
                        {
                            data: 'keywords',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'description',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: null,
                            orderable: false,
                            searchable: false,
                            render: function (data) {
                                return `
                                    <div class="d-flex align-items-center">
                                        <a
                                            href="${data.show_page_url}"
                                            class="btn btn-sm btn-primary mr-3"
                                        >
                                            <i class="fa fa-eye"></i>
                                            Show
                                        </a>
                                        <a
                                            href="${data.edit_page_url}"
                                            class="btn btn-sm btn-success mr-3"
                                        >
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        ${
                                            data.deleted_at != null
                                                ? `
                                                    <button
                                                        data-id="${data.id}"
                                                        data-name="${data.title}"
                                                        data-model="page"
                                                        data-route="${data.restore_page_url}"
                                                        data-toggle="modal"
                                                        data-target="#restoreModal"
                                                        class="btn btn-sm btn-warning mr-2"
                                                    >
                                                        <i class="fa fa-trash-restore"></i>
                                                        Restore
                                                    </button>
                                                `
                                                : 
                                                `
                                                    <a
                                                        href="#"
                                                        data-id="${data.id}"
                                                        data-name="${data.title}"
                                                        data-model="page"
                                                        data-route="${data.delete_page_url}"
                                                        data-toggle="modal"
                                                        data-target="#deleteModal"
                                                        class="btn btn-sm btn-danger mr-2"
                                                    >
                                                        <i class="fa fa-trash"></i>
                                                        Delete
                                                    </a>
                                                `
                                        }
                                    </div>
                                `;
                            }
                        }
                    ]
                });
            });
        </script>
    @endpush
@endonce
