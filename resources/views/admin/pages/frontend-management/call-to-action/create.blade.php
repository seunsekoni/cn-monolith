@extends('admin.layouts.dashboard')

@section('title', 'Create Call To Action')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/dropify/dropify.css') }}">

        <style type="text/css">
            .dropify-wrapper .dropify-message p {
                font-size: 15px;
            }

        </style>
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Create New Call To Action</h1>
        <a href="{{ route('admin.call-to-actions.index') }}" class="btn btn-sm btn-circle btn-primary"
            sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Create a new call to action for the application.</p>

    @if(session('success'))
        <x-alert type="success" :message="session('success')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Create</h6>
        </div>
        <div class="card-body">
            @if($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif
            <form
                method="post"
                action="{{ route('admin.call-to-actions.store') }}"
                class="user"
                enctype="multipart/form-data"
            >
                @csrf

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="file" class="dropify" name="backdrop">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select
                                id="category_id"
                                name="category_id"
                                class="select2 form-control form-control-user @error('category') is-invalid @enderror"
                            >
                                <option value="">Select a category</option>
                                @foreach($categories as $category)
                                    <option {{ old("category_id") == $category->id ? "selected":"" }} value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                            <small class="form-text text-muted">Specify the category it falls under</small>
                        </div>

                        <div class="form-group">
                            <input
                                type="text"
                                name="title"
                                class="form-control form-control-user @error('title') is-invalid @enderror"
                                placeholder="Title"
                                value="{{ old('title') }}"
                                required
                            >
                            <small class="form-text text-muted">Give it a title</small>
                        </div>

                        <div class="form-group">
                            <textarea
                                placeholder="Description"
                                class="form-control @error('description') is-invalid @enderror"
                                name="description"
                                cols="30"
                                rows="4"
                            >{{ old('description') }}</textarea>
                            <small class="form-text text-muted">Briefly describe the purpose of the call to action</small>
                        </div>

                        <div class="form-group">
                            <input
                                type="url"
                                name="url"
                                placeholder="url"
                                class="form-control form-control-user @error('title') is-invalid @enderror"
                                value="{{ old('url') }}"
                                required
                            >
                            <small class="form-text text-muted">Specify the url link for the call to action</small>
                        </div>

                        <div class="form-group">
                            <input
                                type="text"
                                name="button_text"
                                placeholder="Button text"
                                class="form-control form-control-user @error('button_text') is-invalid @enderror"
                                value="{{ old('button_text') }}"
                                
                            >
                            <small class="form-text text-muted">Specify the text that should appear on the call to action button.</small>
                        </div>

                        <div class="form-group">
                            <select
                                name="color"
                                class="form-control form-control-user @error('color') is-invalid @enderror"
                                required
                            >
                                <option value="">Select Button color</option>
                                @foreach($colors as $key => $color)
                                    <option  value="{{ $key }}">{{ $color }}</option>
                                @endforeach
                            </select>
                            <small class="form-text text-muted">Specify color of the button</small>
                        </div>

                        <div class="form-group">
                            <select
                                name="position"
                                class="form-control form-control-user @error('position') is-invalid @enderror"
                                required
                            >
                                <option value="">Select Position</option>
                                @foreach($button_positions as $key => $position)
                                    <option value="{{ $key }}">{{ $position }}</option>
                                @endforeach                        
                            </select>
                            <small class="form-text text-muted">Specify the position of the page the call to action should appear</small>
                        </div>
                    </div>
                </div>

                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Submit
                </button>
            </form>
        </div>
    </div>
@endsection
@once
    @push('scripts')
        <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
        <script src="{{ asset('vendor/dropify/dropify.js') }}"></script>
        <script>
            $('.dropify').dropify({
                messages: {
                    'default': 'Drag and drop the backdrop for the call to action here',
                    'replace': 'Drag and drop or click to replace',
                    'remove':  'Remove',
                    'error':   'Ooops, something wrong happened.'
                }
            });
        </script>
        <script>
            $('#category_id.select2').select2({
                placeholder: 'Select a category',
                ajax: {
                    url: "{{ route('api.categories.search') }}",
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    processResults: function (response) {
                        return {
                            results: response.data.categories.map(category => {
                                return {
                                    id: category.id,
                                    text: category.name,
                                    properties: JSON.stringify(category.properties)
                                }
                            })
                        };
                    }
                },
            });
        </script>
    @endpush
@endonce
