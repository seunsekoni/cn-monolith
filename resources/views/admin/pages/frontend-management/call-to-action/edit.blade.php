@extends('admin.layouts.dashboard')

@section('title', 'Edit Call To Action')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/dropify/dropify.css') }}">

        <style type="text/css">
            .dropify-wrapper .dropify-message p {
                font-size: 15px;
            }
        </style>
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Edit Call To Action</h1>
        <a href="{{ route('admin.call-to-actions.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edit
                <span class="text-danger">{{ $call_to_action->title }}</span> Call To Action
            </h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif
            <form
                method="post"
                action="{{route('admin.call-to-actions.update', $call_to_action->id)}}"
                class="user"
                enctype="multipart/form-data"
            >
                @csrf
                @method("PUT")

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input
                                type="file"
                                class="dropify"
                                data-default-file="{{ $call_to_action->backdrop }}"
                                data-show-remove="false"
                                name="backdrop"
                            >
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select
                                name="category_id"
                                id="category_id"
                                class="select2 form-control form-control-user @error('category') is-invalid @enderror"
                                required
                            >
                                    <option value="">Select a category</option>
                                    @foreach ($categories as $category)
                                    <option
                                        value="{{$category->id}}"
                                        {{$category->id == optional($call_to_action->category)->id ? 'selected' : null}}
                                    >
                                        {{$category->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <input
                                type="text"
                                name="title"
                                class="form-control form-control-user @error('title') is-invalid @enderror"
                                placeholder="Title"
                                value="{{ $call_to_action->title }}"
                                required
                            >
                        </div>

                        <div class="form-group">
                            <textarea
                                placeholder="Description"
                                class="form-control @error('description') is-invalid @enderror"
                                name="description"
                                cols="30"
                                rows="4"
                            >{{$call_to_action->description}}</textarea>
                        </div>

                        <div class="form-group">
                            <input
                                type="url"
                                name="url"
                                placeholder="url"
                                class="form-control form-control-user @error('title') is-invalid @enderror"
                                value="{{$call_to_action->url}}"
                                required
                            >
                        </div>

                        <div class="form-group">
                            <input
                                type="text"
                                name="button_text"
                                placeholder="button text"
                                class="form-control form-control-user @error('button_text') is-invalid @enderror"
                                value="{{$call_to_action->button_text}}"
                                required
                            >
                        </div>

                        <div class="form-group">
                            <select
                                name="color"
                                class="form-control form-control-user @error('color') is-invalid @enderror"
                                required
                            >
                                <option value="">Select Button color</option>
                                @foreach($colors as $key => $color)
                                    <option
                                        {{ $call_to_action->color == $key ? 'selected' : '' }}
                                        value="{{ $key }}"
                                    >
                                        {{ $color }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <select
                                name="position"
                                class="form-control form-control-user @error('position') is-invalid @enderror"
                                required
                            >
                                <option value="">Select Position</option>
                                @foreach($button_positions as $key => $position)
                                    <option
                                        {{ $call_to_action->position == $key ? 'selected' : '' }}
                                        value="{{ $key }}"
                                    >
                                        {{ $position }}
                                    </option>
                                @endforeach  
                            </select>
                        </div>
                    </div>
                </div>

                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Update
                </button>
            </form>
        </div>
    </div>
@endsection
@once
    @push('scripts')
        <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
        <script src="{{ asset('vendor/dropify/dropify.js') }}"></script>
        <script>
            $('.dropify').dropify({
                messages: {
                    'default': 'Drag and drop the backdrop for the call to action here',
                    'replace': 'Drag and drop or click to replace',
                    'remove':  'Remove',
                    'error':   'Ooops, something wrong happened.'
                }
            });

            $('#category_id.select2').select2({
                placeholder: 'Select a category',
                ajax: {
                    url: "{{ route('api.categories.search') }}",
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    processResults: function (response) {
                        return {
                            results: response.data.categories.map(category => {
                                return {
                                    id: category.id,
                                    text: category.name,
                                    properties: JSON.stringify(category.properties)
                                }
                            })
                        };
                    }
                },
            });

            let categoryId = ("{{ old('category_id') }}" || "{{ optional($call_to_action->category)->id}}");
            if (categoryId) {
                getCategory(categoryId);
            }

            function getCategory(id) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: `{{ route('api.categories.search') }}/?id=${id}`,
                    type: 'get',
                    success: function (response) {
                        let category = response.data.category
                        let properties = JSON.stringify(category.properties)

                        // create the option and append to Select2
                        var option = `<option value='${category.id}' data-properties='${properties}'>${category.name}</option>`;
                        $('#category_id.select2').append(option).trigger('change');

                        // manually trigger the `select2:select` event
                        $('#category_id.select2').trigger({
                            type: 'select2:select',
                            params: {
                                data: category
                            }
                        });
                    },
                    error: function (error) {
                        let response = JSON.parse(error.responseText)
                        alert(response.message)
                    }
                });
            }
        </script>
    @endpush
@endonce