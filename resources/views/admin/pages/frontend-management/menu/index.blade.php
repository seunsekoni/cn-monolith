@extends('admin.layouts.dashboard')

@section('title', 'Menus')
@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    @endpush
@endonce
@section('main-content')
    @include('includes.create-menu-modal')

    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Menus</h1>
        <button data-target="#createMenuModal" data-toggle="modal" class="btn btn-primary">Create A New Menu</button>
    </div>
    <p class="">List of Menus available in the application.</p>


    @if (isset($menu))
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.menus.index')}}">Menu</a></li>
                @if (isset($menu) && $menu->parent)
                    <li class="breadcrumb-item active" aria-current="page"><a
                            href="{{route('admin.menus.show', $menu->parent->id)}}">{{$menu->parent->title}}</a></li>
                @endif
                <li class="breadcrumb-item active" aria-current="page">{{$menu->title}}</li>

            </ol>
        </nav>
    @else
        <div class="alert alert-primary" role="alert">
            Disclaimer!!! Please try to avoid too many nested menus so as to maintain a decent display on the
            website
        </div>
    @endif
    @if (session('status'))
        <x-alert type="success" :message="session('status')"/>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Pages</h6>
        </div>

        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif
            <div class="table-responsive">
                <table class="table table-bordered" id="callToActionDataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th width="7%">S\N</th>
                        <th>Title</th>
                        <th>Link</th>
                        <th>Is Page</th>
                        <th>Target</th>
                        <th>No Of Sub Menus</th>
                        <th width="25%">Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation/>
<!-- Restore Modal -->
<x-restore-confirmation />
@once
    @push('scripts')
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('#callToActionDataTable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ $is_child_menu && isset($menu) ? route('admin.menus.show', $menu->id) : route('admin.menus.index')  }}",
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'title'
                        },
                        {
                            data: 'link',
                        },
                        {
                            data: 'is_model',
                            searchable: false,
                            render: function (data) {
                                return `
                                    <i
                                        class="
                                            fa fa-${data ? 'check' : 'minus'}
                                            text-${data ? 'success' : 'danger'}
                                            font-weight-bold
                                        "
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="${data ? 'verified' : 'unverified'}"
                                    ></i>
                                `;
                            }
                        },
                        {
                            data: 'target',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'no_of_children',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: null,
                            orderable: false,
                            searchable: false,
                            render: function (data) {
                                return `
                                    <div class="d-flex align-items-center">
                                        <a
                                            href="${data.show_menu_url}"
                                            class="btn btn-sm btn-primary mr-3"
                                        >
                                            <i class="fa fa-eye"></i>
                                            Show
                                        </a>
                                        <a
                                            href="${data.edit_menu_url}"
                                            class="btn btn-sm btn-success mr-3"
                                        >
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        ${
                                            data.deleted_at != null
                                                ? `
                                                    <button
                                                        data-id="${data.id}"
                                                        data-name="${data.title}"
                                                        data-model="menu"
                                                        data-route="${data.restore_menu_url}"
                                                        data-toggle="modal"
                                                        data-target="#restoreModal"
                                                        class="btn btn-sm btn-warning mr-2"
                                                    >
                                                        <i class="fa fa-trash-restore"></i>
                                                        Restore
                                                    </button>
                                                `
                                                : 
                                                `
                                                    <a
                                                        href="#"
                                                        data-id="${data.id}"
                                                        data-name="${data.title}"
                                                        data-model="menu"
                                                        data-route="${data.delete_menu_url}"
                                                        data-toggle="modal"
                                                        data-target="#deleteModal"
                                                        class="btn btn-sm btn-danger mr-2"
                                                    >
                                                        <i class="fa fa-trash"></i>
                                                        Delete
                                                    </a>
                                                `
                                        }
                                    </div>
                                `;
                            }
                        }
                    ]
                });
            });
        </script>
    @endpush
@endonce
