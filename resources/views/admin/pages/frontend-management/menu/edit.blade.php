@extends('admin.layouts.dashboard')

@section('title', 'Edit Menu')
@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Edit Menu</h1>
        <a href="{{ route('admin.menus.update', $menu->id) }}" class="btn btn-sm btn-circle btn-primary"
           sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Edit a menu for the application.</p>
    @if (session('success'))
        <x-alert type="success" :message="session('success')"/>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edit</h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form method="POST" action="{{route('admin.menus.update', $menu->id)}}" id="create-menu">
                @csrf
                @method('put')
                <input type="hidden" name="model_id" id="modelId">
                <div class="form-group">
                    <label for="title">Title<span class="text-danger sup">*</span></label>
                    <input type="text" name="title" id="title"
                           value="{{$menu->title}}" class="form form-control">
                    <small class="form-text text-muted">This field represents the title of the menu.</small>
                </div>
                <div
                    class="form-group justify-content-center custom-control custom-checkbox">
                    <input type="checkbox" name="is_model" id="is_page" value={{true}}
                        class="custom-control-input" {{ $menu->is_model ? 'checked' : '' }}>
                    <label class="custom-control-label" for="is_page">
                        Is this a page?
                    </label>
                    <small class="form-text text-muted">Do you want to create this menu for a page or for an external
                        link</small>
                </div>
                <div class="form-group">
                    <div id="link-div">
                        <label for="link">Link<span class="text-danger sup">*</span></label>
                        <input type="text" name="link" id="link"
                               value="{{$menu->link}}" class="form form-control">
                        <small class="form-text text-muted">This field captures the link that is to be
                            redirect with</small>
                    </div>
                    <div id="model-div">
                        <label for="model">Model<span class="text-danger sup">*</span></label>
                        <select class="form form-control" name="model" id="model">
                            @foreach($menuModels as $key => $menuModel)
                                <option
                                    {{ $key === $menu->model ? 'selected' : '' }} value="{{ $key }}">{{ $menuModel }}</option>
                            @endforeach

                        </select>
                        <small class="form-text text-muted">select the model the wish to attach this menu
                            with</small>
                    </div>
                </div>
                <div>
                    <div class="form-group" id="page">
                        <label for="page">Pages<span class="text-danger sup">*</span></label>
                        <select name="page" id="page" class="form form-control">
                            @foreach($pages as $page)
                                <option {{ $menu->model_id === $page->id ? 'selected' : '' }}
                                        value="{{$page->id}}">{{$page->title}}</option>
                            @endforeach
                        </select>
                        <small class="form-text text-muted">select the page you want to attach to this menu</small>
                    </div>

                    <div class="form-group" id="category">
                        <label for="category">Categories<span class="text-danger sup">*</span></label>
                        <select name="category" id="category" class="form form-control">
                            @foreach($categories as $category)
                                <option {{ $menu->model_id === $category->id ? 'selected' : '' }}
                                        value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        <small class="form-text text-muted">select the page you want to attach to this menu</small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Position On Page<span class="text-danger sup">*</span></label><br>
                    <div class="form-check form-check-inline">
                        <input type="radio" name="position" id="top" class="form-check-input" value="top"
                            {{ $menu->top_visible ? 'checked' : '' }}>
                        <label class="form-check-label" for="top">Top</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input type="radio" name="position" id="bottom" class="form-check-input" value="bottom"
                            {{ $menu->bottom_visible ? 'checked' : '' }}>
                        <label class="form-check-label" for="bottom">Bottom</label>
                    </div>
                    <small class="form-text text-muted">choose menu position</small>

                </div>
                <div class="form-group">
                    <label for="target">Target<span class="text-danger sup">*</span></label>
                    <select name="target" id="target" class="form form-control">
                        <option {{ $menu->target == "_blank" ? 'selected' : '' }} value="_blank">
                            Open in a new tab
                        </option>
                        <option {{ $menu->target == "_self" ? 'selected' : '' }} value="_self">
                            Open in the current tab
                        </option>
                        <option {{ $menu->target == "_top" ? 'selected' : '' }} value="_top">
                            Open in another window
                        </option>
                        <option {{ $menu->target == "_parent" ? 'selected' : '' }} value="_parent">
                            Open in the current frame
                        </option>
                    </select>
                    <small class="form-text text-muted">define the link behavior</small>
                </div>
                <div
                    class="form-group custom-control custom-checkbox">
                    <input type="checkbox" name="status" id="status"
                           class="custom-control-input" {{ $menu->active ? 'checked' : '' }}>
                    <label class="custom-control-label" for="status">
                        Status
                    </label>
                    <small class="form-text text-muted">define menu status</small>

                </div>

                <div class="form-group">
                    <label class="" for="order">Order</label>
                    <input type="number" name="order" id="order"
                           class="form form-control" value="{{old('order')}}">

                    <small class="form-text text-muted">define menu order</small>
                </div>
                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Submit
                </button>

            </form>
        </div>
    </div>
@endsection
@once
    @push('scripts')
        <script>
            $(document).ready(() => {
                manageIsPageToggle();
                manageModelToggle()
            })

            let modelId = $('#modelId')

            const manageIsPageToggle = () => {
                const is_page = $("#is_page");
                const is_page_selected = is_page.is(":checked");

                const model_div = $("#model-div");
                const link_div = $("#link-div");

                model_div.hide();
                is_page_selected ? (model_div.show(),
                    link_div.hide()) : (model_div.hide(),
                    link_div.show());

                is_page.change(() => {
                    const is_page_selected = is_page.is(":checked");
                    if (is_page_selected) {
                        model_div.show();
                        link_div.hide();
                    } else {
                        model_div.hide();
                        link_div.show();
                    }
                });
            }

            const manageModelToggle = () => {
                const category = $("#category");
                const page = $("#page");
                category.hide();
                page.hide();
                $("#model").change((el) => {
                    switch (el.target.value) {
                        case "category":
                            category.show();
                            modelId.val();
                            category.change(() => {
                                let selected = category.find('option:selected').val()
                                modelId.val(selected)
                            })
                            page.hide();
                            break;
                        case "page":
                            category.hide();
                            page.show();
                            page.show();
                            page.change(() => {
                                let selected = page.find('option:selected').val()
                                modelId.val(selected)
                            })
                            break;
                        default:
                            category.hide();
                            page.hide();
                    }
                })
            }
        </script>
    @endpush
@endonce
