@extends('admin.layouts.dashboard')

@section('main-content')
    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-download fa-sm text-white-50"></i> Generate Report
        </a>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-4 col-md-4 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xl font-weight-bold text-info text-uppercase mb-1">
                                Total Businesses
                            </div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">
                                {{ number_format($business['total_business'], 0, '.', ',') }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-industry fa-2x text-gray-400"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-md-4 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xl font-weight-bold text-success text-uppercase mb-1">
                                Confirmed Businesses
                            </div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">
                                {{ number_format($business['confirmed_business'], 0, '.', ',') }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-industry fa-2x text-gray-600"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-md-4 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xl font-weight-bold text-warning text-uppercase mb-1">
                                Unconfirmed Businesses
                            </div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">
                                {{ number_format($business['unconfirmed_business'], 0, '.', ',') }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-industry fa-2x text-gray-200"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-4 col-md-4 mb-4">
            <div class="card border-bottom-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xl font-weight-bold text-info text-uppercase mb-1">
                                Total Listings
                            </div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">
                                {{ number_format($listing['total_listing'], 0, '.', ',') }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-concierge-bell fa-2x text-gray-400"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-md-4 mb-4">
            <div class="card border-bottom-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xl font-weight-bold text-success text-uppercase mb-1">
                                Confirmed Listings
                            </div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">
                                {{ number_format($listing['confirmed_listing'], 0, '.', ',') }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-concierge-bell fa-2x text-gray-600"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-md-4 mb-4">
            <div class="card border-bottom-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xl font-weight-bold text-warning text-uppercase mb-1">
                                Unconfirmed Listings
                            </div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">
                                {{ number_format($listing['unconfirmed_listing'], 0, '.', ',') }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-concierge-bell fa-2x text-gray-200"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">
        <!-- Area Chart -->
        <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Daily Confirmed Businesses</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                           data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                             aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Dropdown Header:</div>
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="dailyConfirmedBusiness"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <!-- Pie Chart -->
        <div class="col-xl-4 col-lg-5">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Confirmed Businesses</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                           data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                             aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Dropdown Header:</div>
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-pie pt-4 pb-2">
                        <canvas id="myPieChart"></canvas>
                    </div>
                    <div class="mt-4 text-center small">
                        <span class="mr-2">
                            <i class="fas fa-circle" style="color:#4d73df"></i> Confirmed
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle text-success"></i> Unconfirmed
                        </span>
                        <span class="mr-2">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">
        <!-- Content Column -->
        <div class="col-lg-6 mb-4">
            <!-- Project Card Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Projects</h6>
                </div>
                <div class="card-body">
{{--                    <h4 class="small font-weight-bold">Total businesses without website <span--}}
{{--                            class="float-right">{{ $without["without_website"]." Businesses (".$business["total_business"] < 1 ?0:--}}
{{--                                                        number_format($without["without_website"]/$business["total_business"] * 100, 2) ."%" }}</span>--}}
{{--                    </h4>--}}
{{--                    <div class="progress mb-4">--}}
{{--                        <div class="progress-bar bg-danger" role="progressbar"--}}
{{--                             style="width: {{ $business["total_business"] < 1 ?0:($without["without_website"]/$business["total_business"] * 100) }}%"--}}
{{--                             aria-valuenow="{{ $business["total_business"] < 1 ?0:($without["without_website"]/$business["total_business"] * 100) }}"--}}
{{--                             aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                    </div>--}}
                    <h4 class="small font-weight-bold">Total businesses without email <span
                            class="float-right">{{ $without["without_email"]." Businesses (".$business["total_business"] < 1 ?0:
                                                        number_format($without["without_email"]/$business["total_business"] * 100, 2) ."%" }}</span>
                    </h4>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-warning" role="progressbar"
                             style="width: {{ $business["total_business"] < 1 ?0:($without["without_email"]/$business["total_business"] * 100) }}%"
                             aria-valuenow="{{ $business["total_business"] < 1 ?0:($without["without_email"]/$business["total_business"] * 100) }}"
                             aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <h4 class="small font-weight-bold">Total businesses without telephone <span
                            class="float-right">{{ $without["without_phone"]." Businesses (".$business["total_business"] < 1 ?0:
                                                        number_format($without["without_phone"]/$business["total_business"] * 100, 2) ."%" }}</span>
                    </h4>
                    <div class="progress mb-4">
                        <div class="progress-bar" role="progressbar"
                             style="width: {{ $business["total_business"] < 1 ?0:($without["without_phone"]/$business["total_business"] * 100) }}%"
                             aria-valuenow="{{ $business["total_business"] < 1 ?0:($without["without_phone"]/$business["total_business"] * 100) }}"
                             aria-valuemin="0" aria-valuemax="100"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row col-lg-6 mb-4">
            <div class="col-lg-6 mb-4">
                <div class="card bg-primary text-white shadow">
                    <div class="card-body">
                        Categories
                        <div class="text-white-50">{{presentCategories()->count()}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-4">
                <div class="card bg-success text-white shadow">
                    <div class="card-body">
                        Business Types
                        <div class="text-white-50">{{presentBusinessType()->count()}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-4">
                <div class="card bg-info text-white shadow">
                    <div class="card-body">
                        Countries
                        <div class="text-white-50">{{presentCountries()->count()}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-4">
                <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        States
                        <div class="text-white-50">{{presentStates()->count()}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-4">
                <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                        Cities
                        <div class="text-white-50">{{presentCities()->count()}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-4">
                <div class="card bg-secondary text-white shadow">
                    <div class="card-body">
                        Comments
                        <div class="text-white-50">{{presentComments()->count()}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-4">
                <div class="card bg-light text-black shadow">
                    <div class="card-body">
                        Reviews
                        <div class="text-black-50">{{presentReviews()->count()}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-4">
                <div class="card bg-dark text-white shadow">
                    <div class="card-body">
                        Pages
                        <div class="text-white-50">{{presentPages()->count()}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@once
    @push('scripts')
        <script>
            let chart = @json($chart);
            let business = @json($business);

        </script>
        <script src="{{ asset('js/admin/chart/Chart.js') }}"></script>
        <script
            src="{{ asset('js/admin/custom-chart/daily-confirmed-business.chart.js') }}">
        </script>
        <script src="{{ asset('js/admin/custom-chart/confirmed-business.pie.js') }}">
        </script>
    @endpush
@endonce
