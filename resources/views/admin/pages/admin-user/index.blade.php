@extends('admin.layouts.dashboard')

@section('title', 'Admin User')
@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    @endpush
@endonce
@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Admin User</h1>
        <a href="{{route('admin.admin-user.create')}}" class="btn btn-primary">Create A New Admin Account</a>
    </div>
    <p class="">List of All Admin in the application.</p>

    @if (session('success'))
        <x-alert type="success" :message="session('success')"/>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Admins</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="AdminUserDataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th width="7%">S\N</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th width="25%">Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
<!-- Delete Modal -->
<x-delete-confirmation />
<!-- Restore Modal -->
<x-restore-confirmation />

@once
    @push('scripts')
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#AdminUserDataTable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.admin-user.index') }}",
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'first_name'
                        },
                        {
                            data: 'last_name'
                        },
                        {
                            data: 'email'
                        },

                        {
                            data: null,
                            orderable: false,
                            searchable: false,
                            render: function (data) {
                                return `
                                    <div class="d-flex align-items-center">

                                        <a
                                            href="${data.edit_admin_url}"
                                            class="btn btn-sm btn-success mr-3"
                                        >
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        ${
                                    data.deleted_at != null
                                        ? `
                                                    <button
                                                        data-id="${data.id}"
                                                        data-name="${data.first_name+" "+data.last_name}"
                                                        data-model="page"
                                                        data-route="${data.restore_admin_url}"
                                                        data-toggle="modal"
                                                        data-target="#restoreModal"
                                                        class="btn btn-sm btn-warning mr-2"
                                                    >
                                                        <i class="fa fa-trash-restore"></i>
                                                        Restore
                                                    </button>
                                                `
                                        :
                                        `
                                                    <a
                                                        href="#"
                                                        data-id="${data.id}"
                                                        data-name="${data.first_name+" "+data.last_name}"
                                                        data-model="page"
                                                        data-route="${data.delete_admin_url}"
                                                        data-toggle="modal"
                                                        data-target="#deleteModal"
                                                        class="btn btn-sm btn-danger mr-2"
                                                    >
                                                        <i class="fa fa-trash"></i>
                                                        Delete
                                                    </a>
                                                `
                                }
                                    </div>
                                `;
                            }
                        }
                    ]
                });
            });
        </script>
    @endpush
@endonce
