@extends('admin.layouts.dashboard')

@section('title', 'Create Admin User')

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Create Admin User</h1>
        <a href="{{ route('admin.admin-user.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Create a new Admin.</p>
    @if (session('success'))
        <x-alert type="success" :message="session('success')"/>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Create</h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif
            <form method="post"
                  action="{{ route('admin.admin-user.store') }}"
                  class="user"
                  enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="first_name">First Name<span class="text-danger sup">*</span></label>
                    <input type="text" name="first_name" id="first_name" value="{{old('first_name')}}"
                           class="form-control form-control-user @error('first_name') is-invalid @enderror">
                </div>

                <div class="form-group">
                    <label for="last_name">Last Name<span class="text-danger sup">*</span></label>
                    <input type="text" name="last_name" id="last_name" value="{{old('last_name')}}"
                           class="form-control form-control-user @error('last_name') is-invalid @enderror">
                </div>

                <div class="form-group">
                    <label for="email">Email<span class="text-danger sup">*</span></label>
                    <input type="email" name="email" id="email" value="{{old('email')}}"
                           class="form-control form-control-user @error('email') is-invalid @enderror">
                </div>

                <div class="form-group">
                    <label for="phone">Telephone<span class="text-danger sup">*</span></label>
                    <input type="tel" name="phone" id="phone" value="{{old('phone')}}"
                           class="form-control form-control-user @error('phone') is-invalid @enderror">
                </div>

                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Submit
                </button>
            </form>
        </div>
    </div>
@endsection
