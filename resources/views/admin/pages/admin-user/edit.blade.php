@extends('admin.layouts.dashboard')

@section('title', 'Edit Admin User')

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Edit Admin User</h1>
        <a href="{{ route('admin.admin-user.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Edit a new Admin.</p>
    @if (session('success'))
        <x-alert type="success" :message="session('success')"/>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edit</h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif
            <form method="post"
                  action="{{ route('admin.admin-user.update', $admin_user->id) }}"
                  class="user"
                  enctype="multipart/form-data">

                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="first_name">First Name<span class="text-danger sup">*</span></label>
                    <input type="text" name="first_name" id="first_name" value="{{$admin_user->first_name}}"
                           class="form-control form-control-user @error('first_name') is-invalid @enderror">
                </div>

                <div class="form-group">
                    <label for="last_name">Last Name<span class="text-danger sup">*</span></label>
                    <input type="text" name="last_name" id="last_name" value="{{$admin_user->last_name}}"
                           class="form-control form-control-user @error('last_name') is-invalid @enderror">
                </div>

                <div class="form-group">
                    <label for="email">Email<span class="text-danger sup">*</span></label>
                    <input type="email" name="email" id="email" value="{{$admin_user->email}}"
                           class="form-control form-control-user @error('email') is-invalid @enderror">
                </div>

                <div class="form-group">
                    <label for="phone">Telephone<span class="text-danger sup">*</span></label>
                    <input type="tel" name="phone" id="phone" value="{{$admin_user->phone}}"
                           class="form-control form-control-user @error('phone') is-invalid @enderror">
                </div>

                <button class="btn btn-primary btn-user btn-block" type="submit">
                    Update
                </button>
            </form>
        </div>
    </div>
@endsection
