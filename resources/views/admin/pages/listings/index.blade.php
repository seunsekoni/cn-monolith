@extends('admin.layouts.dashboard')

@section('title', 'Listings')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Listings</h1>
        <a href="{{ route('admin.listings.create') }}" class="btn btn-primary">Create New Listing</a>
    </div>
    <p class="">List of all the business listings in the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-flex justify-content-between align-items-center">
                <h6 class="m-0 font-weight-bold text-primary">Listings</h6>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="7%">S\N</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Business</th>
                            <th>Category</th>
                            <th></th>
                            <th width="25%">Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation />
<!-- Restore Modal -->
<x-restore-confirmation />

@include('admin.pages.businesses.relations.includes.listing-gallery-modal')

@once
    @push('scripts')
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#dataTable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.listings.index') }}",
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'name'
                        },
                        {
                            data: 'description',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'business.name',
                        },
                        {
                            data: 'category.name',
                        },
                        {
                            data: 'featured',
                            searchable: false,
                            render: function (data) {
                                return `
                                    <i
                                        class="fa${data ? 's' : 'r'} fa-star"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="${data ? 'featured' : 'unfeatured'}"
                                    ></i>
                                `;
                            }
                        },
                        {
                            data: null,
                            orderable: false,
                            searchable: false,
                            render: function (data) {
                                return `
                                    <div class="d-flex align-items-center">
                                        <button
                                            data-id="${data.id}"
                                            data-name="${data.name}"
                                            data-route="${data.listing_gallery_url}"
                                            data-toggle="modal"
                                            data-target="#galleryModal"
                                            class="btn btn-sm btn-primary mr-2"
                                        >
                                            <i class="fa fa-photo-video"></i>
                                            Gallery
                                        </button>
                                        <a
                                            href="${data.edit_listing_url}"
                                            class="btn btn-sm btn-success mr-2"
                                        >
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        ${
                                            data.deleted_at != null
                                                ? `
                                                    <button
                                                        data-id="${data.id}"
                                                        data-name="${data.name}"
                                                        data-model="listing"
                                                        data-route="${data.restore_listing_url}"
                                                        data-toggle="modal"
                                                        data-target="#restoreModal"
                                                        class="btn btn-sm btn-warning mr-2"
                                                    >
                                                        <i class="fa fa-trash-restore"></i>
                                                        Restore
                                                    </button>
                                                `
                                                :
                                                `
                                                    <a
                                                        href="#"
                                                        data-id="${data.id}"
                                                        data-name="${data.name}"
                                                        data-model="listing"
                                                        data-route="${data.delete_listing_url}"
                                                        data-toggle="modal"
                                                        data-target="#deleteModal"
                                                        class="btn btn-sm btn-danger mr-2"
                                                    >
                                                        <i class="fa fa-trash"></i>
                                                        Delete
                                                    </a>
                                                `
                                        }
                                    </div>
                                `;
                            }
                        }
                    ]
                });
            });
        </script>
    @endpush
@endonce
