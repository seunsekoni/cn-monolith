@extends('admin.layouts.dashboard')

@section('title', "Update {$discount_type->name} Discount Type")

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Edit Discount Type</h1>
        <a href="{{ route('admin.lookups.discount-types.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Update the discount type for the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">
                Edit <span class="text-danger">{{ $discount_type->name }}</span> dimension
            </h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif


            <form class="user" action="{{ route('admin.lookups.discount-types.update', ['discount_type' => $discount_type]) }}" method="post">
                @method('PUT')
                @csrf

                <div class="row">
                    <div class="col-xl-6">
                        <div class="form-group">
                            <input
                                type="text"
                                name="name"
                                class="form-control form-control-user @error('name') is-invalid @enderror"
                                placeholder="Name"
                                value="{{ $discount_type->name }}"
                                required
                            >
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="form-group">
                            <input
                                type="text"
                                name="regex_format"
                                class="form-control form-control-user @error('name') is-invalid @enderror"
                                placeholder="Regex Format"
                                value="{{ $discount_type->regex_format }}"
                            >
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input
                                    type="checkbox"
                                    name="status"
                                    class="custom-control-input"
                                    id="statusCheck"
                                    {{ (old('status') ?? $discount_type->status) ? 'checked' : '' }}
                                >
                                <label class="custom-control-label" for="statusCheck">
                                    Active Status
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success btn-user" type="submit">
                    Update
                </button>
            </form>
        </div>
    </div>
@endsection
