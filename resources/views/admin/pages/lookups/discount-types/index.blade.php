@extends('admin.layouts.dashboard')

@section('title', 'Discount Types')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Discount Types</h1>
        <a href="{{ route('admin.lookups.discount-types.create') }}" class="btn btn-primary">Create New Discount Type</a>
    </div>
    <p class="">List of Discount types configured in the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Discount Types</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="8%">S\N</th>
                            <th>Name</th>
                            <th>Regex</th>
                            <th>Status</th>
                            <th width="30%">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($discounts as $discount)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $discount->name }}</td>
                                <td>{{ $discount->regex_format }}</td>
                                <td>
                                    <span class="badge {{ $discount->status ? 'badge-primary' : 'badge-warning' }}">
                                        {{ $discount->status ? 'active' : 'inactive' }}
                                    </span>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <a
                                            href="{{ route('admin.lookups.discount-types.edit', ['discount_type' => $discount]) }}"
                                            class="btn btn-sm btn-success mr-3"
                                        >
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        <a
                                            href="#"
                                            data-id="{{ $discount->id }}"
                                            data-name="{{ $discount->name }}"
                                            data-model="Discount Type"
                                            data-route="{{ route('admin.lookups.discount-types.destroy', ['discount_type' => $discount->id]) }}"
                                            data-toggle="modal"
                                            data-target="#deleteModal"
                                            class="btn btn-sm btn-danger mr-3"
                                        >
                                            <i class="fa fa-trash"></i>
                                            Delete
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <b>Nothing found!</b>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation />

@once
    @push('scripts')
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#dataTable').DataTable();
            });
        </script>
    @endpush
@endonce
