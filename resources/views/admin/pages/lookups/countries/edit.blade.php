@extends('admin.layouts.dashboard')

@section('title', "Update {$country->name} County")

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
    @endpush
@endonce
@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Edit Country</h1>
        <a href="{{ route('admin.lookups.countries.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Update the country for the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">
                Edit the <span class="text-danger">{{ $country->name }}</span> country
            </h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form class="user" action="{{ route('admin.lookups.countries.update', ['country' => $country->id]) }}" method="POST">
                @method('PUT')
                @csrf

                <div class="form-group">
                    <select name="region_id" id="region_id" class="select2 form-control form-control-user @error('region_id') is-invalid @enderror">
                    </select>
                    <small class="form-text text-muted">Region where the country is located.</small>
                </div>

                <div class="form-group">
                    <input
                        type="text"
                        name="name"
                        class="form-control form-control-user @error('name') is-invalid @enderror"
                        placeholder="Name"
                        value="{{ old('name') ?: $country->name }}"
                        required
                    >
                    <small class="form-text text-muted">Name of the country.</small>
                </div>

                <div class="form-group">
                    <input
                        type="text"
                        name="code"
                        class="form-control form-control-user @error('code') is-invalid @enderror"
                        placeholder="Country Code"
                        value="{{ old('code') ?: $country->code }}"
                        required
                    >
                    <small class="form-text text-muted">The country code e.g "NG" for "Nigeria"</small>
                </div>

                <div class="form-group">
                    <input
                        type="text"
                        name="currency"
                        class="form-control form-control-user @error('currency') is-invalid @enderror"
                        placeholder="Currency"
                        value="{{ old('currency') ?: $country->currency }}"
                        required
                    >
                    <small class="form-text text-muted">The currency e.g "NGN" for "Nigeria"</small>
                </div>

                <div class="form-group">
                    <input
                        type="text"
                        name="phone_code"
                        class="form-control form-control-user @error('phone_code') is-invalid @enderror"
                        placeholder="Phone Code"
                        value="{{ old('phone_code') ?: $country->phone_code }}"
                        required
                    >
                    <small class="form-text text-muted">Country dialing code e.g 234</small>
                </div>

                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            name="status"
                            class="custom-control-input"
                            id="statusCheck"
                            {{ (old('status') ?: $country->status) ? 'checked' : '' }}
                        >
                        <label class="custom-control-label" for="statusCheck">
                            Active Status
                        </label>
                    </div>
                </div>

                <button class="btn btn-primary btn-block btn-user" type="submit">
                    Save
                </button>
            </form>
        </div>
    </div>
@endsection
@once
    @push('scripts')
        <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
        <script>
            // Fetch Regions
            $('#region_id.select2').select2({
                placeholder: 'Select a Region',
                ajax: {
                    url: "{{ route('api.regions.search') }}",
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    processResults: function (response) {
                        return {
                            results: response.data.regions.map(region => {
                                return {
                                    id: region.id,
                                    text: region.name
                                }
                            })
                        };
                    }
                }
            });

            function getRegion(id) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: `{{ route('api.regions.search') }}/?id=${id}`,
                    type: 'get',
                    success: function (response) {
                        let region = response.data.region

                        // create the option and append to Select2
                        var option = new Option(region.name, region.id, true, true);
                        $('#region_id.select2').append(option).trigger('change');

                        // manually trigger the `select2:select` event
                        $('#region_id.select2').trigger({
                            type: 'select2:select',
                            params: {
                                data: region
                            }
                        });
                    },
                    error: function (error) {
                        let response = JSON.parse(error.responseText)
                        alert(response.message)
                    }
                });
            }

            let regionId = ("{{ old('region_id') }}" || "{{ $country->region->id ?? ''  }}")
            if (regionId) {
                getRegion(regionId);
            }
        </script>
    @endpush
@endonce
