@extends('admin.layouts.dashboard')

@section('title', 'New State')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
    @endpush
@endonce
@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Create New State</h1>
        <a href="{{ route('admin.lookups.states.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Register a new state for the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Create</h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form class="user" action="{{ route('admin.lookups.states.store') }}" method="post">
                @csrf

                <div class="form-group">
                    <select
                        name="country_id"
                        id="country_id"
                        class="select2 form-control form-control-user @error('country_id') is-invalid @enderror"
                        required
                    ></select>
                    <small class="form-text text-muted">Country where the state is located.</small>
                </div>

                <div class="form-group">
                    <input
                        type="text"
                        name="name"
                        class="form-control form-control-user @error('name') is-invalid @enderror"
                        placeholder="Name"
                        value="{{ old('name') }}"
                        required
                    >
                    <small class="form-text text-muted">The state's name.</small>
                </div>

                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            name="status"
                            class="custom-control-input"
                            id="statusCheck"
                            {{ old('status') ? 'checked' : '' }}
                        >
                        <label class="custom-control-label" for="statusCheck">
                            Active Status
                        </label>
                    </div>
                </div>

                <button class="btn btn-block btn-primary btn-user" type="submit">
                    Save
                </button>
            </form>
        </div>
    </div>
@endsection

@once
    @push('scripts')
        <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
        <script>
            $('#country_id.select2').select2({
                placeholder: 'Select a Country',
                ajax: {
                    url: "{{ route('api.countries.search') }}",
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    processResults: function (response) {
                        return {
                            results: response.data.countries.map(country => {
                                return {
                                    id: country.id,
                                    text: country.name
                                }
                            })
                        };
                    }
                }
            });

            let countryId = @json(old('country_id'), JSON_PRETTY_PRINT);
            if (countryId) {
                getCountry(countryId);
            }

            function getCountry(id) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: `{{ route('api.countries.search') }}/?id=${id}`,
                    type: 'get',
                    success: function (response) {
                        let country = response.data.country

                        // create the option and append to Select2
                        var option = new Option(country.name, country.id, true, true);
                        $('#country_id.select2').append(option).trigger('change');

                        // manually trigger the `select2:select` event
                        $('#country_id.select2').trigger({
                            type: 'select2:select',
                            params: {
                                data: country
                            }
                        });
                    },
                    error: function (error) {
                        let response = JSON.parse(error.responseText)
                        alert(response.message)
                    }
                });
            }
        </script>
    @endpush
@endonce
