@extends('admin.layouts.dashboard')

@section('title', "Category - {$category->name}")
@section('description', $category->description)

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Category Data</h1>
        <a
            href="{{ route('admin.lookups.categories.index') }}"
            class="btn btn-sm btn-circle btn-primary"
            sr-only="Go back"
        >
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">All extra relations for the category</p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">
                <span class="text-danger">{{ $category->name }}</span> as a category
            </h6>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs nav-justified">
                <li class="nav-item">
                    <a
                        class="nav-link {{
                            request()->routeIs('admin.lookups.categories.show') ? 'active' : ''
                        }}"
                        href="{{ route('admin.lookups.categories.show', ['category' => $category->id]) }}"
                    >
                        Basic Info
                    </a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link {{
                            request()->routeIs('admin.lookups.categories.properties.index') ? 'active' : ''
                        }}"
                        href="{{ route('admin.lookups.categories.properties.index', ['category' => $category->id]) }}"
                    >
                        Properties
                    </a>
                </li>
            </ul>

            <div class="mt-4">
                @yield('category-relation')
            </div>
        </div>
    </div>
@endsection
