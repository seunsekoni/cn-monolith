@extends('admin.pages.lookups.categories.layouts.relations')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    @endpush
@endonce

@section('category-relation')
    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    @if ($errors->any())
        <x-alert type="danger" message="Validation Errors Occurred!">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </x-alert>
    @endif

    <div class="">
        <x-alert type="info" message="Note:">
            <p class="mb-0">
                Ensure you set the values for properties that are not &ldquo;optional&rdquo; before you setting its status as public.
            </p>
        </x-alert>

        <button
            data-id="{{ $category->id }}"
            data-category="{{ $category->name }}"
            data-method="Create"
            data-route="{{
                route('admin.lookups.categories.properties.store', [
                    'category' => $category,
                ])
            }}"
            data-toggle="modal"
            data-target="#propertyModal"
            class="btn btn-primary btn-block"
        >
            Add New Property
        </button>
    </div>

    <div class="card my-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="5%">S\N</th>
                            <th>Name</th>
                            <th>Hint</th>
                            <th>Status</th>
                            <th>Optional</th>
                            <th>Filterable</th>
                            <th>Values</th>
                            <th width="32%">Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation />
<x-restore-confirmation />

<!-- Create property modal -->
@include('admin.pages.lookups.categories.includes.property-modal')
@include('admin.pages.lookups.categories.includes.property-values-modal')

@once
    @push('scripts')
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#dataTable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.lookups.categories.properties.index', ['category' => $category->id]) }}",
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'name'
                        },
                        {
                            data: 'hint',
                        },
                        {
                            data: 'status',
                            searchable: false,
                            orderable: false,
                            render: function (data) {
                                return `
                                    <span class="badge badge-${data ? 'primary' : 'warning'}">
                                        ${data ? 'active' : 'inactive'}
                                    </span>
                                `;
                            }
                        },
                        {
                            data: 'optional',
                            searchable: false,
                            orderable: false,
                            render: function (data) {
                                return `
                                <i
                                        class="
                                            fa fa-${data ? 'unlock' : 'lock'}
                                            text-${data ? 'success' : 'danger'}
                                            font-weight-bold
                                        "
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="${data ? 'yes' : 'no'}"
                                    ></i>
                                `;
                            }
                        },
                        {
                            data: 'filterable',
                            searchable: false,
                            orderable: false,
                            render: function (data) {
                                return `
                                <i
                                        class="
                                            fa fa-${data ? 'search' : 'eye-slash'}
                                            text-${data ? 'success' : 'danger'}
                                            font-weight-bold
                                        "
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="${data ? 'yes' : 'no'}"
                                    ></i>
                                `;
                            }
                        },
                        {
                            data: null,
                            orderable: false,
                            searchable: false,
                            render: function (data) {
                                return data.values_count;
                            }
                        },
                        {
                            data: null,
                            orderable: false,
                            searchable: false,
                            render: function (data) {
                                let property = JSON.stringify(data);
                                let _property = JSON.parse(property);

                                return `
                                    <div class="d-flex align-items-center">
                                        ${
                                            !_property.optional
                                                ? `
                                                    <button
                                                        data-id="${data.category.id}"
                                                        data-category="${data.category.name}"
                                                        data-route="${data.property_value_url}"
                                                        data-property='${property}'
                                                        data-toggle="modal"
                                                        data-target="#propertyValueModal"
                                                        class="btn btn-sm btn-primary mr-2"
                                                    >
                                                        <i class="fa fa-rocket"></i>
                                                        Manage Values
                                                    </button>
                                                `
                                                : ''
                                        }
                                        <button
                                            data-id="${data.category.id}"
                                            data-category="${data.category.name}"
                                            data-method="Update"
                                            data-route="${data.edit_property_url}"
                                            data-property='${property}'
                                            data-toggle="modal"
                                            data-target="#propertyModal"
                                            class="btn btn-sm btn-success mr-2"
                                        >
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </button>
                                        ${
                                            _property.deleted_at != null
                                                ? `
                                                    <button
                                                        data-id="${data.id}"
                                                        data-name="${data.name}"
                                                        data-model="category property"
                                                        data-route="${data.restore_property_url}"
                                                        data-toggle="modal"
                                                        data-target="#restoreModal"
                                                        class="btn btn-sm btn-warning mr-2"
                                                    >
                                                        <i class="fa fa-trash-restore"></i>
                                                        Restore
                                                    </button>
                                                `
                                                : ''
                                        }
                                        <button
                                            data-id="${data.id}"
                                            data-name="${data.name}"
                                            data-model="category property"
                                            data-route="${data.delete_property_url}${_property.deleted_at != null ? '?force=true' : ''}"
                                            data-toggle="modal"
                                            data-target="#deleteModal"
                                            class="btn btn-sm btn-danger"
                                        >
                                            <i class="fa fa-trash"></i>
                                            Delete ${_property.deleted_at != null ? 'permanently' : ''}
                                        </button>
                                    </div>
                                `;
                            }
                        }
                    ]
                });
            });
        </script>
    @endpush
@endonce
