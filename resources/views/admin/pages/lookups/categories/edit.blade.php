@extends('admin.layouts.dashboard')

@section('title', "Update {$category->name} Category")

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/dropify/dropify.css') }}">

        <style type="text/css">
            .dropify-wrapper .dropify-message p {
                font-size: 15px;
            }
        </style>
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Edit Categories</h1>
        <a href="{{ route('admin.lookups.categories.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Update the category for the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">
                Edit <span class="text-danger">{{ $category->name }}</span> category
            </h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif


            <form
                class="user"
                action="{{ route('admin.lookups.categories.update', ['category' => $category]) }}"
                method="post"
                enctype="multipart/form-data"
            >
                @method('PUT')
                @csrf

                <div class="row">
                    <div class="col-xl-12">
                        <div class="form-group">
                            <input
                                type="file"
                                class="dropify backdrop"
                                accept="{{ $types = implode(', ', \App\Enums\MediaType::IMAGES) }}"
                                name="backdrop"
                                data-height="100"
                                data-max-file-size="2M"
                                data-allowed-file-extensions="png jpeg jpg"
                                data-default-file="{{ $category->backdrop }}"
                            >
                        </div>
                    </div>
                    <div class="col-xl-2">
                        <div class="form-group">
                            <input
                                type="file"
                                class="dropify icon"
                                accept="{{ $types = implode(', ', \App\Enums\MediaType::ICONS) }}"
                                name="icon"
                                data-height="150"
                                data-max-file-size="2M"
                                data-default-file="{{ $category->icon }}"
                            >
                        </div>
                        <div class="form-group">
                            <input
                                type="file"
                                class="dropify white_icon"
                                accept="{{ $types = implode(', ', \App\Enums\MediaType::ICONS) }}"
                                name="white_icon"
                                data-height="150"
                                data-max-file-size="2M"
                                data-default-file="{{ $category->white_icon }}"
                            >
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input
                                    type="checkbox"
                                    name="status"
                                    class="custom-control-input"
                                    id="statusCheck"
                                    {{ (old('status') ?? $category->status) ? 'checked' : '' }}
                                >
                                <label class="custom-control-label" for="statusCheck">
                                    Active Status
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-10">
                        <div class="form-group">
                            <input
                                type="text"
                                name="name"
                                class="form-control form-control-user @error('name') is-invalid @enderror"
                                placeholder="Name"
                                value="{{ old('name') ?? $category->name }}"
                                required
                            >
                            <small class="form-text text-muted">Provide a good name to group listings.</small>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea
                                name="description"
                                id="description"
                                cols="30"
                                rows="10"
                                class="form-control @error('description') is-invalid @enderror"
                            >{{ old('description', $category->description) }}</textarea>
                            <small class="form-text text-muted">Describe the category in brief.</small>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success btn-user btn-block" type="submit">
                    Update
                </button>
            </form>
        </div>
    </div>
@endsection

@once
    @push('scripts')
        <script src="{{ asset('vendor/dropify/dropify.js') }}"></script>
        <script>
            $('.dropify.backdrop').dropify({
                messages: {
                    'default': 'Drag and drop the category backdrop here',
                }
            });
            $('.dropify.icon').dropify({
                messages: {
                    'default': 'Drag and drop the category icon here',
                }
            });
            $('.dropify.white_icon').dropify({
                messages: {
                    'default': 'Drag and drop the category white icon here',
                }
            });
        </script>
    @endpush
@endonce
