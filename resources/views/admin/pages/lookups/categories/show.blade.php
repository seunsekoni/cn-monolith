@extends('admin.pages.lookups.categories.layouts.relations')

@section('category-relation')
    <div class="row">
        <div class="col-md-3">
            <figure class="figure mb-2">
                <img src="{{ $category->icon }}" alt="logo" class="figure-img img-fluid rounded">
                <figcaption class="figure-caption">
                    {{ $category->name }} {{ constant('App\\Enums\\MediaCollection::ICON') }}
                </figcaption>
            </figure>
            <a
                href="{{ route('admin.lookups.categories.edit', ['category' => $category->id]) }}"
                class="btn btn-success btn-block"
            >
                Edit Category
            </a>
        </div>
        <div class="col-md-9">
            <div class="mb-4">
                <h6 class="font-weight-bold">Name</h6>
                {{ $category->name }}
            </div>
            <div class="mb-4">
                <h6 class="font-weight-bold">Profile</h6>
                <span class="text-justify">{{ $category->description }}</span>
            </div>
        </div>
    </div>
@endsection
