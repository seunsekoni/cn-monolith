<div
    class="modal fade"
    id="propertyValueModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="propertyValueModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary font-weight-bold" id="propertyValueModalLabel"></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if ($errors->any())
                    <x-alert type="danger" message="Validation Errors Occurred!">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </x-alert>
                @endif

                <form
                    action="#"
                    method="post"
                    class="user"
                    id="propertyValueForm"
                    onsubmit="event.preventDefault(); submitPropertyValueForm();"
                >
                    @csrf

                    <div class="form-group">
                        <label for="propertyValueName">
                            Name
                            <span class="text-danger sup">*</span>
                        </label>
                        <input
                            type="text"
                            name="name"
                            id="propertyValueName"
                            placeholder="Name"
                            value="{{ old('name') }}"
                            class="form-control form-control-user @error('name') is-invalid @enderror"
                            required
                        >
                        <small class="form-text text-muted">Give the property value a name</small>
                        <small id="nameHelpBlock" class="form-text text-danger"></small>
                    </div>

                    <button type="submit" class="btn btn-user btn-block btn-primary">
                        Save Property Value
                    </button>
                </form>

                <hr>

                <div class="d-flex justify-content-center">
                    <div id="loadingContainer">
                        <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
                    </div>

                    <div id="propertyValuesContainer" class="table-responsive d-none">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="10%">S\N</th>
                                    <th>Name</th>
                                    <th width="30%">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="propertyValuesList"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#propertyValueModal').on('show.bs.modal', function (e) {
                let route = $(e.relatedTarget).data('route');
                let property = $(e.relatedTarget).data('property');

                $('#propertyValueModalLabel').html(`Manage the values for the "${property.name}" category property`);
                $('#propertyValueForm').attr('action', route);

                fetchPropertyValues(route);
            });

            $('#propertyValueModal').on('hide.bs.modal', function (e) {
                $('#propertyValueForm').attr('action', '#');
                $('#propertyValueForm').trigger('reset');
            });
        });

        function fetchPropertyValues(route) {
            $('#loadingContainer').removeClass('d-none');
            $('#propertyValuesContainer').addClass('d-none');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: route,
                type: 'get',
                success: function (response) {
                    $('#loadingContainer').addClass('d-none');
                    $('#propertyValuesContainer').removeClass('d-none');

                    buildPropertyValuesList(response.data.propertyValues, route);
                },
                error: function (error) {
                    let response = JSON.parse(error.responseText)
                    alert(response.message)
                }
            });
        }

        function buildPropertyValuesList(values, route) {
            $('#propertyValuesList').html('');

            if (!values.length) {
                $('#propertyValuesList').append(`
                    <tr class="text-center">
                        <td colspan="3">No data found!</td>
                    </tr>
                `);
                return;
            }

            values.forEach(function(value, index) {
                $('#propertyValuesList').append(`
                    <tr>
                        <td>${index+1}</td>
                        <td>${value.name}</td>
                        <td>
                            <a
                                href="#"
                                class="btn btn-sm btn-danger"
                                onclick="event.preventDefault(); deletePropertyValue('${value.id}', '${route}');"
                            >
                                <i class="fa fa-trash"></i>
                                Delete
                            </a>
                        </td>
                    </tr>
                `);
            });
        }

        function submitPropertyValueForm() {
            let name = $('#propertyValueName');
            let helpBlock = $('#nameHelpBlock');
            let route = $('#propertyValueForm').attr('action');

            name.removeClass('is-invalid')
            helpBlock.html('')

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: route,
                type: 'post',
                data: {
                    name: name.val()
                },
                success: function (response) {
                    fetchPropertyValues(route);
                    $('#propertyValueForm').trigger('reset')
                },
                error: function (error) {
                    let response = JSON.parse(error.responseText)
                    name.addClass('is-invalid')
                    helpBlock.html(response.message)
                }
            });
        }

        function deletePropertyValue(id, route) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: `${route}/${id}`,
                type: 'delete',
                success: function (response) {
                    fetchPropertyValues(route);
                },
                error: function (error) {
                    let response = JSON.parse(error.responseText);
                    alert(response.message);
                }
            });
        }
    </script>
@endpush