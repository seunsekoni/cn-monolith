@push('styles')
    <style>
        .form-group {
            margin-bottom: 30px;
            border-bottom: 3px dashed #f5f5f5;
        }
    </style>
@endpush

<div
    class="modal fade"
    id="propertyModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="propertyModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary font-weight-bold" id="propertyModalLabel"></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if ($errors->any())
                    <x-alert type="danger" message="Validation Errors Occurred!">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </x-alert>
                @endif

                <form action="#" method="post" class="user" id="propertyForm">
                    @csrf
                    <input type="hidden" name="_method" value="POST" id="_propertyFormMethod">

                    <div class="form-group">
                        <label for="propertyName">
                            Name
                            <span class="text-danger sup">*</span>
                        </label>
                        <input
                            type="text"
                            name="name"
                            id="propertyName"
                            placeholder="Name"
                            value="{{ old('name') }}"
                            class="form-control form-control-user @error('name') is-invalid @enderror"
                            required
                        >
                        <small class="form-text text-muted">Give the property a name</small>
                    </div>
                    <div class="form-group">
                        <label for="propertyHint">Hint</label>
                        <input
                            type="text"
                            name="hint"
                            id="propertyHint"
                            placeholder="What is expected from the user creating a listing?"
                            class="form-control form-control-user @error('hint') is-invalid @enderror"
                            value="{{ old('hint') }}"
                        >
                        <small class="form-text text-muted">
                            Add a placeholder to better describe the property to the user creating/updating a listing.
                        </small>
                    </div>
                    <div class="form-group">
                        <label>Toggle Public Visibility</label>
                        <div class="custom-control custom-switch">
                            <input
                                type="checkbox"
                                name="status"
                                class="custom-control-input"
                                id="propertyStatusCheck"
                                {{ old('status') ? 'checked' : '' }}
                            >
                            <label class="custom-control-label" for="propertyStatusCheck">
                                Status
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Can user input or pick from available category property values?</label>
                        <div class="custom-control custom-switch">
                            <input
                                type="checkbox"
                                class="custom-control-input"
                                id="propertyOptionalCheck"
                                name="optional"
                                {{ old('optional') ? 'checked' : '' }}
                            >
                            <label class="custom-control-label" for="propertyOptionalCheck">
                                Optional
                            </label>
                        </div>
                        <small class="form-text text-muted">
                            Leave "off" if you will still set strict values for it later.
                        </small>
                    </div>
                    <div class="form-group">
                        <label>Should property appear on the webpage for the category to enable searching?</label>
                        <div class="custom-control custom-switch">
                            <input
                                type="checkbox"
                                class="custom-control-input"
                                id="propertyFilterableCheck"
                                name="filterable"
                                {{ old('filterable') ? 'checked' : '' }}
                            >
                            <label class="custom-control-label" for="propertyFilterableCheck">
                                Filterable
                            </label>
                        </div>
                        <small class="form-text text-muted">
                            PS, avoid making too many filterable properties so you do not distort the webpage
                        </small>
                    </div>

                    <button type="submit" class="btn btn-user btn-block btn-primary">
                        Save Property
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#propertyModal').on('show.bs.modal', function (e) {
                let route = $(e.relatedTarget).data('route');
                let category = $(e.relatedTarget).data('category');
                let method = $(e.relatedTarget).data('method');

                $('#propertyModalLabel').html(`${method} property for the ${category} category`);
                $('#propertyForm').attr('action', route);

                if (method?.toLowerCase().includes('update')) {
                    let property = $(e.relatedTarget).data('property');

                    $('#propertyName').val(property.name);
                    $('#propertyHint').val(property.hint);
                    $('#propertyStatusCheck').prop('checked', property.status);
                    $('#propertyOptionalCheck').prop('checked', property.optional);
                    $('#propertyFilterableCheck').prop('checked', property.filterable);
                    $('#_propertyFormMethod').val('PUT');
                }
            });

            $('#propertyModal').on('hide.bs.modal', function (e) {
                $('#propertyForm').attr('action', '#');
                $('#propertyForm').trigger('reset');
            });
        });
    </script>
@endpush