@extends('admin.layouts.dashboard')

@section('title', "Update {$business_type->name} Business Type")

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/dropify/dropify.css') }}">

        <style type="text/css">
            .dropify-wrapper .dropify-message p {
                font-size: 15px;
            }
        </style>
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Edit Business Type</h1>
        <a href="{{ route('admin.lookups.business-types.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Update the business type for the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">
                Edit <span class="text-danger">{{ $business_type->name }}</span> business type
            </h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif


            <form
                class="user"
                action="{{ route('admin.lookups.business-types.update', ['business_type' => $business_type]) }}"
                method="post"
                enctype="multipart/form-data"
            >
                @method('PUT')
                @csrf

                <div class="row">
                    <div class="col-xl-12">
                        <div class="form-group">
                            <input
                                type="file"
                                class="dropify backdrop"
                                accept="{{ $types = implode(', ', \App\Enums\MediaType::IMAGES) }}"
                                name="backdrop"
                                data-height="150"
                                data-max-file-size="2M"
                                data-default-file="{{ $business_type->backdrop }}"
                            >
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="form-group">
                            <input
                                type="file"
                                class="dropify icon"
                                accept="{{ $types = implode(', ', \App\Enums\MediaType::ICONS) }}"
                                name="icon"
                                data-height="250"
                                data-max-file-size="2M"
                                data-default-file="{{ $business_type->icon }}"
                            >
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input
                                    type="checkbox"
                                    name="status"
                                    class="custom-control-input"
                                    id="statusCheck"
                                    {{ (old('status') ?? $business_type->status) ? 'checked' : '' }}
                                >
                                <label class="custom-control-label" for="statusCheck">
                                    Active Status
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-9">
                        <div class="form-group">
                            <input
                                type="text"
                                name="name"
                                class="form-control form-control-user @error('name') is-invalid @enderror"
                                placeholder="Name"
                                value="{{ $business_type->name }}"
                                required
                            >
                            <small class="form-text text-muted">The type of this business' ownership.</small>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ old('description', $business_type->description) }}</textarea>
                            <small class="form-text text-muted mb-4">A nice description of this business type.</small>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success btn-user btn-block" type="submit">
                    Update
                </button>
            </form>
        </div>
    </div>
@endsection

@once
    @push('scripts')
        <script src="{{ asset('vendor/dropify/dropify.js') }}"></script>
        <script>
            $('.dropify.icon').dropify({
                messages: {
                    'default': 'Drag and drop the icon here or click',
                }
            });
            $('.dropify.backdrop').dropify({
                messages: {
                    'default': 'Drag and drop the backdrop here or click',
                }
            });
        </script>
    @endpush
@endonce
