@extends('admin.layouts.dashboard')

@section('title', 'New Region')

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Create New Region</h1>
        <a href="{{ route('admin.lookups.regions.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Register a new region for the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Create</h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form class="user" action="{{ route('admin.lookups.regions.store') }}" method="post">
                @csrf

                <div class="form-group">
                    <input
                        type="text"
                        name="name"
                        class="form-control form-control-user @error('name') is-invalid @enderror"
                        placeholder="Name"
                        value="{{ old('name') }}"
                        required
                    >

                    <small class="form-text text-muted">Region or continent name.</small>
                </div>

                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            name="status"
                            class="custom-control-input"
                            id="statusCheck"
                            {{ old('status') ? 'checked' : '' }}
                        >
                        <label class="custom-control-label" for="statusCheck">
                            Active Status
                        </label>
                    </div>
                </div>

                <button class="btn btn-block btn-primary btn-user" type="submit">
                    Save
                </button>
            </form>
        </div>
    </div>
@endsection
