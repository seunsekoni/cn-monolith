@extends('admin.layouts.dashboard')

@section('title', "Update {$region->name} Region")

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Edit Region</h1>
        <a href="{{ route('admin.lookups.regions.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Update the region for the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">
                Edit the <span class="text-danger">{{ $region->name }}</span> region
            </h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form class="user" action="{{ route('admin.lookups.regions.update', ['region' => $region->id]) }}" method="post">
                @method('PUT')
                @csrf

                <div class="form-group">
                    <input
                        type="text"
                        name="name"
                        class="form-control form-control-user @error('name') is-invalid @enderror"
                        placeholder="Name"
                        value="{{ old('name') ?? $region->name }}"
                        required
                    >
                    <small class="form-text text-muted">Region or continent name.</small>
                </div>

                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            name="status"
                            class="custom-control-input"
                            id="statusCheck"
                            {{ (old('status') ?? $region->status) ? 'checked' : '' }}
                        >
                        <label class="custom-control-label" for="statusCheck">
                            Active Status
                        </label>
                    </div>
                </div>

                <button class="btn btn-block btn-success btn-user" type="submit">
                    Update
                </button>
            </form>
        </div>
    </div>
@endsection
