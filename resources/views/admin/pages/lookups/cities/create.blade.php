@extends('admin.layouts.dashboard')

@section('title', 'New City')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
    @endpush
@endonce
@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Create New City</h1>
        <a href="{{ route('admin.lookups.cities.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Register a new city for the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Create</h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form class="user" action="{{ route('admin.lookups.cities.store') }}" method="post">
                @csrf

                <div class="form-group">
                    <select name="state_id" id="state_id" class="select2 form-control form-control-user @error('state_id') is-invalid @enderror">
                    </select>
                    <small class="form-text text-muted">State where the city is located.</small>
                </div>

                <div class="form-group">
                    <input
                        type="text"
                        name="name"
                        class="form-control form-control-user @error('name') is-invalid @enderror"
                        placeholder="Name"
                        value="{{ old('name') }}"
                        required
                    >
                    <small class="form-text text-muted">Name of the city.</small>
                </div>

                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            name="status"
                            class="custom-control-input"
                            id="statusCheck"
                            {{ old('status') ? 'checked' : '' }}
                        >
                        <label class="custom-control-label" for="statusCheck">
                            Active Status
                        </label>
                    </div>
                </div>

                <button class="btn btn-primary btn-block btn-user" type="submit">
                    Save
                </button>
            </form>
        </div>
    </div>
@endsection
@once
    @push('scripts')
        <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
        <script>
            $('#state_id.select2').select2({
                placeholder: 'Select a State',
                ajax: {
                    url: "{{ route('api.states.search') }}",
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    processResults: function (response) {
                        return {
                            results: response.data.states.map(state => {
                                return {
                                    id: state.id,
                                    text: `${state.name} - ${state.country}`
                                }
                            })
                        };
                    }
                }
            });

            let stateId = @json(old('state_id'), JSON_PRETTY_PRINT);
            if (stateId) {
                getState(stateId);
            }

            function getState(id) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: `{{ route('api.states.search') }}/?id=${id}`,
                    type: 'get',
                    success: function (response) {
                        let state = response.data.state

                        // create the option and append to Select2
                        var option = new Option(`${state.name} - ${state.country}`, state.id, true, true);
                        $('#state_id.select2').append(option).trigger('change');

                        // manually trigger the `select2:select` event
                        $('#state_id.select2').trigger({
                            type: 'select2:select',
                            params: {
                                data: state
                            }
                        });
                    },
                    error: function (error) {
                        let response = JSON.parse(error.responseText)
                        alert(response.message)
                    }
                });
            }
        </script>
    @endpush
@endonce

