@extends('admin.layouts.dashboard')

@section('title', 'Cities')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Cities</h1>
        <a href="{{ route('admin.lookups.cities.create') }}" class="btn btn-primary">Create New City</a>
    </div>
    <p class="">List of cities configured in the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Cities</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="8%">S\N</th>
                            <th>Country</th>
                            <th>State</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th width="30%">Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation />
<!-- Restore Modal -->
<x-restore-confirmation />

@once
    @push('scripts')
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#dataTable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.lookups.cities.index') }}",
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'state.country.name',
                            orderable: false
                        },
                        {
                            data: 'state.name',
                            orderable: false
                        },
                        {
                            data: 'name'
                        },
                        {
                            data: 'status',
                            searchable: false,
                            render: function (data) {
                                return `
                                    <span class="badge badge-${data ? 'primary' : 'warning'}">
                                        ${data ? 'active' : 'inactive'}
                                    </span>
                                `;
                            }
                        },
                        {
                            data: null,
                            orderable: false,
                            searchable: false,
                            render: function (data) {
                                return `
                                    <div class="d-flex align-items-center">
                                        <a
                                            href="${data.edit_city_url}"
                                            class="btn btn-sm btn-success mr-2"
                                        >
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        ${
                                            data.deleted_at != null
                                                ? `
                                                    <button
                                                        data-id="${data.id}"
                                                        data-name="${data.name}"
                                                        data-model="city"
                                                        data-route="${data.restore_city_url}"
                                                        data-toggle="modal"
                                                        data-target="#restoreModal"
                                                        class="btn btn-sm btn-warning mr-2"
                                                    >
                                                        <i class="fa fa-trash-restore"></i>
                                                        Restore
                                                    </button>
                                                `
                                                :
                                                `
                                                    <a
                                                        href="#"
                                                        data-id="${data.id}"
                                                        data-name="${data.name}"
                                                        data-model="city"
                                                        data-route="${data.delete_state_url}"
                                                        data-toggle="modal"
                                                        data-target="#deleteModal"
                                                        class="btn btn-sm btn-danger mr-2"
                                                    >
                                                        <i class="fa fa-trash"></i>
                                                        Delete
                                                    </a>
                                                `
                                        }
                                    </div>
                                `;
                            }
                        }
                    ]
                });
            });
        </script>
    @endpush
@endonce
