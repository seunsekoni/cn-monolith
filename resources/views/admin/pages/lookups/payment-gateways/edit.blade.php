@extends('admin.layouts.dashboard')

@section('title', "Update {$payment_gateway->name} Gateway")

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Edit Payment Gateway</h1>
        <a href="{{ route('admin.lookups.payment-gateways.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Update the payment gateway for the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">
                Edit <span class="text-danger">{{ $payment_gateway->name }}</span> payment gateway
            </h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif


            <form class="user" action="{{ route('admin.lookups.payment-gateways.update', ['payment_gateway' => $payment_gateway]) }}" method="post">
                @method('PUT')
                @csrf

                <div class="row">
                    <div class="col-xl-12">
                        <div class="form-group">
                            <input
                                type="text"
                                name="name"
                                class="form-control form-control-user @error('name') is-invalid @enderror"
                                placeholder="Name"
                                value="{{ $payment_gateway->name }}"
                                required
                            >
                            <small class="form-text text-muted">A name for the payment gateway.</small>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $payment_gateway->description }}</textarea>
                            <small class="form-text text-muted">An explanatory description of the payment gateway.</small>
                        </div>
                    </div>

                    <div class="col-xl-6">
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input
                                    type="checkbox"
                                    name="status"
                                    class="custom-control-input"
                                    id="statusCheck"
                                    {{ (old('status') ?? $payment_gateway->status) ? 'checked' : '' }}
                                >
                                <label class="custom-control-label" for="statusCheck">
                                    Active Status
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success btn-user btn-block" type="submit">
                    Update
                </button>
            </form>
        </div>
    </div>
@endsection
