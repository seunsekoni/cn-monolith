@extends('admin.layouts.dashboard')

@section('title', "Update {$kyc_requirement->name} KYC Requirement")

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Edit KYC Requirement</h1>
        <a href="{{ route('admin.lookups.kyc-requirements.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">Update the kyc requirement for the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">
                Edit <span class="text-danger">{{ $kyc_requirement->name }}</span> kyc requirement
            </h6>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif


            <form class="user" action="{{ route('admin.lookups.kyc-requirements.update', ['kyc_requirement' => $kyc_requirement]) }}" method="post">
                @method('PUT')
                @csrf

                <div class="row">
                    <div class="col-xl-12">
                        <div class="form-group">
                            <input
                                type="text"
                                name="name"
                                class="form-control form-control-user @error('name') is-invalid @enderror"
                                placeholder="Name"
                                value="{{ $kyc_requirement->name }}"
                                required
                            >
                            <small class="form-text text-muted">Title of this requirement. E.g.; Passport, CAC docs, etc.</small>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $kyc_requirement->description }}</textarea>
                            <small class="form-text text-muted">An explanatory description of this requirement.</small>
                        </div>
                    </div>

                    <div class="col-xl-6">
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input
                                    type="checkbox"
                                    name="status"
                                    class="custom-control-input"
                                    id="statusCheck"
                                    {{ (old('status') ?? $kyc_requirement->status) ? 'checked' : '' }}
                                >
                                <label class="custom-control-label" for="statusCheck">
                                    Active Status
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success btn-user btn-block" type="submit">
                    Update
                </button>
            </form>
        </div>
    </div>
@endsection
