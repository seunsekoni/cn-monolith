@extends('admin.layouts.dashboard')

@section('title', 'Reviews')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Reviews</h1>
    </div>
    <p class="">List of all the reviews in the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="row row-cols-1 row-cols-md-3">
        <div class="col mb-3">
            <div class="card bg-primary text-white">
                <div class="card-body d-flex justify-content-between">
                    <div class="">
                        <h5 class="font-weight-bold">Total Reviews</h5>
                        <p class="small lead font-italic">Total number of all reviews</p>
                    </div>
                    <h1 class="font-weight-bold text-right">{{ $reviews->count() }}</h1>
                </div>
            </div>
        </div>
        <div class="col mb-3">
            <div class="card bg-primary text-white">
                <div class="card-body d-flex justify-content-between">
                    <div class="">
                        <h5 class="font-weight-bold">Total Approved</h5>
                        <p class="small lead font-italic">Visible to the public</p>
                    </div>
                    <h1 class="font-weight-bold text-right">{{ $reviews->where('approved', true)->count() }}</h1>
                </div>
            </div>
        </div>
        <div class="col mb-3">
            <div class="card bg-primary text-white">
                <div class="card-body d-flex justify-content-between">
                    <div class="">
                        <h5 class="font-weight-bold">Total Unapproved</h5>
                        <p class="small lead font-italic">Yet to be reviewed</p>
                    </div>
                    <h1 class="font-weight-bold text-right">{{ $reviews->where('approved', false)->count() }}</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="7%">S\N</th>
                            <th>Relation</th>
                            <th>User</th>
                            <th>Comment</th>
                            <th>Rating</th>
                            <th>Status</th>
                            <th width="25%">Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation />

@once
    @push('scripts')
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#dataTable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.reviews.index') }}",
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'relation',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'user',
                            name: 'user.email',
                        },
                        {
                            data: 'comment'
                        },
                        {
                            data: 'rating',
                            searchable: false,
                        },
                        {
                            data: 'approved',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: null,
                            orderable: false,
                            searchable: false,
                            render: function (data) {
                                return `
                                    <div class="d-flex align-items-center">
                                        <a
                                            href="${data.show_review_url}"
                                            class="btn btn-sm btn-info mr-2"
                                        >
                                            <i class="fa fa-eye"></i>
                                            Show
                                        </a>
                                        <a
                                            href="${data.edit_review_url}"
                                            class="btn btn-sm btn-primary mr-2"
                                        >
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        <a
                                            href="#"
                                            data-id="${data.id}"
                                            data-name="${data.comment}"
                                            data-model="review"
                                            data-route="${data.delete_review_url}"
                                            data-toggle="modal"
                                            data-target="#deleteModal"
                                            class="btn btn-sm btn-danger mr-2"
                                        >
                                            <i class="fa fa-trash"></i>
                                            Delete
                                        </a>
                                    </div>
                                `;
                            }
                        }
                    ]
                });
            });
        </script>
    @endpush
@endonce
