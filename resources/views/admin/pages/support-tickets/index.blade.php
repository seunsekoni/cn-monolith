@extends('admin.layouts.dashboard')

@section('title', 'Support Ticket')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Support Ticket</h1>
        <a href="{{ route('admin.support-tickets.create') }}" class="btn btn-primary">Create New Ticket</a>
    </div>
    <p class="">List of support tickets.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Support Tickets</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="7%">S\N</th>
                            <th>Subject</th>
                            <th>Message</th>
                            <th>Status</th>
                            <th width="25%">Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation />

@once
    @push('scripts')
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#dataTable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.support-tickets.index') }}",
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'subject',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'message',
                        },

                        {
                            data: 'status',
                            searchable: false,
                            render: function (data) {
                                return `
                                    <span class="
                                            badge  ${data !== '{{ App\Enums\SupportTicketStatus::CLOSE }}' ? 'badge-primary' : 'badge-danger' }
                                        ">
                                        ${ data }
                                    </span>
                                `;
                            }
                        },
                        {
                            data: null,
                            orderable: false,
                            searchable: false,
                            render: function (data) {
                                return `
                                    <div class="d-flex align-items-center">
                                        <a
                                            href="${data.show_support_url}"
                                            class="btn btn-sm btn-primary mr-3"
                                        >
                                            <i class="fa fa-eye"></i>
                                            Show
                                        </a>
                                        <a
                                            href="#"
                                            data-id="${data.id}"
                                            data-name="${data.subject}"
                                            data-model="ticket"
                                            data-route="${data.delete_support_url}"
                                            data-toggle="modal"
                                            data-target="#deleteModal"
                                            class="btn btn-sm btn-danger mr-3"
                                        >
                                            <i class="fa fa-trash"></i>
                                            Delete
                                        </a>
                                    </div>
                                `;
                            }
                        }
                    ]
                });
            });
        </script>
    @endpush
@endonce
