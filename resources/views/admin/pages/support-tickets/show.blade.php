@extends('admin.layouts.dashboard')

@section('title', 'Read Ticket')

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Read Ticket</h1>
        <a href="{{ route('admin.support-tickets.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class=""></p>

    <div class="card shadow mb-4">
        
        <div class="accordion mb-3" id="accordionComment{{ $support->id }}">
            <div class="card">
                <div class="card-header" id="heading{{ $support->id }}">
                    <h2 class="mb-0">
                        <button
                            class="
                                btn btn-default btn-sm btn-block text-left
                                d-flex justify-content-between align-items-center
                                shadow-none
                            "
                            type="button"
                            data-toggle="collapse"
                            data-target="#collapse{{ $support->id }}"
                            aria-expanded="true"
                            aria-controls="collapse{{ $support->id }}"
                        >
                        <div class="text-right">
                            Reply
                        </div>
                        </button>
                    </h2>
                </div>
                <div
                    id="collapse{{ $support->id }}"
                    class="collapse {{ $support->id ? '' : 'show' }}"
                    aria-labelledby="heading{{ $support->id }}"
                    data-parent="#accordionComment{{ $support->id }}"
                >
                    <div class="card-body">
                        <div class="media">
                            <div class="media-body">
                                <form
                                    class="user"
                                    action="{{ route('admin.support-tickets.store') }}"
                                    method="post"
                                >
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="hidden" name="parent" value="{{ $support->id }}">
                                                <input type="hidden" name="subject" value="{{ $support->subject }}">
                                                <textarea
                                                    id="message"
                                                    name="message"
                                                    class="form-control form-control-user rounded @error('message') is-invalid @enderror"
                                                    placeholder="Type your message here..."
                                                    rows="5"
                                                    required>{{ old('message') }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-user btn-block" type="submit">
                                        Submit
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <h6 class="m-0 font-weight-bold text-danger">
                {{ $support->message }}
            </h6>
        </div>
        <div class="card-body">
            <div class="alert alert-info">
                There are {{ $supportCount = $support->replies()->count() }}
                {{ \Illuminate\Support\Str::of('reply')->plural($supportCount) }}
                to this ticket.
            </div>

            @each('admin.pages.support-tickets.includes.replies', $support->replies, 'reply')
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation />
