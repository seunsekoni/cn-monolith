@extends('admin.layouts.dashboard')

@section('title', 'Regions')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">User Logs</h1>
    </div>
    <p class="">Admin User Logs in the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">User Logs</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="8%">S\N</th>
                            <th>Full Name</th>
                            <th>Created Businesses</th>
                            <th>Updated Businesses</th>
                            <th>Created Listings</th>
                            <th>Updated Listings</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@once
    @push('scripts')
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#dataTable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: `{{ route('admin.audit-trails.index') }}`,
                    },
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'full_name'
                        },
                        {
                            data: 'created_business_count',
                        },
                        {
                            data: 'updated_business_count'
                        },
                        {
                            data: 'created_listing_count',
                        },
                        {
                            data: 'updated_listing_count'
                        },
                    ]
                });
            });
        </script>
    @endpush
@endonce
