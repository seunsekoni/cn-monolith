@extends('admin.layouts.dashboard')

@section('title', 'Businesses')

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    @endpush
@endonce

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Businesses</h1>
        <a href="{{ route('admin.businesses.create') }}" class="btn btn-primary">Create New Business</a>
    </div>
    <p class="">List of businesses configured in the application.</p>

    @if (session('status'))
        <x-alert type="success" :message="session('status')" />
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Businesses</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="7%">S\N</th>
                            <th>Name</th>
                            <th>Profile</th>
                            <th>Verified</th>
                            <th>Featured</th>
                            <th width="25%">Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation />
<!-- Restore Modal -->
<x-restore-confirmation />

@once
    @push('scripts')
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#dataTable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.businesses.index') }}",
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'name'
                        },
                        {
                            data: 'profile',
                            orderable: false,
                            searchable: false,
                        },
                        {
                            data: 'verified',
                            searchable: false,
                            render: function (data) {
                                return `
                                    <i
                                        class="
                                            fa fa-${data ? 'check' : 'minus'}
                                            text-${data ? 'success' : 'danger'}
                                            font-weight-bold
                                        "
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="${data ? 'verified' : 'unverified'}"
                                    ></i>
                                `;
                            }
                        },
                        {
                            data: 'featured',
                            searchable: false,
                            render: function (data) {
                                return `
                                    <i
                                        class="fa${data ? 's' : 'r'} fa-star"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="${data ? 'featured' : 'unfeatured'}"
                                    ></i>
                                `;
                            }
                        },
                        {
                            data: null,
                            orderable: false,
                            searchable: false,
                            render: function (data) {
                                return `
                                    <div class="d-flex align-items-center">
                                        <a
                                            href="${data.show_business_url}"
                                            class="btn btn-sm btn-primary mr-3"
                                        >
                                            <i class="fa fa-eye"></i>
                                            Show
                                        </a>
                                        <a
                                            href="${data.edit_business_url}"
                                            class="btn btn-sm btn-success mr-3"
                                        >
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </a>
                                        ${
                                            data.deleted_at != null
                                                ? `
                                                    <button
                                                        data-id="${data.id}"
                                                        data-name="${data.name}"
                                                        data-model="business"
                                                        data-route="${data.restore_business_url}"
                                                        data-toggle="modal"
                                                        data-target="#restoreModal"
                                                        class="btn btn-sm btn-warning mr-2"
                                                    >
                                                        <i class="fa fa-trash-restore"></i>
                                                        Restore
                                                    </button>
                                                `
                                                : 
                                                `
                                                    <a
                                                        href="#"
                                                        data-id="${data.id}"
                                                        data-name="${data.name}"
                                                        data-model="business"
                                                        data-route="${data.delete_business_url}"
                                                        data-toggle="modal"
                                                        data-target="#deleteModal"
                                                        class="btn btn-sm btn-danger mr-3"
                                                    >
                                                        <i class="fa fa-trash"></i>
                                                        Delete
                                                    </a>
                                                `
                                        }
                                    </div>
                                `;
                            }
                        }
                    ]
                });
            });
        </script>
    @endpush
@endonce
