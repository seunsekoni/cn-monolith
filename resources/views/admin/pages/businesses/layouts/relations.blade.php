@extends('admin.layouts.dashboard')

@section('title', "Businesses - {$business->name}")
@section('description', $business->profile)

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Business Data</h1>
        <a href="{{ route( 'admin.businesses.index') }}"
           class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class="">All extra relations for the business</p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">
                <span class="text-danger">{{ $business->name }}</span> as a business
            </h6>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs nav-justified">
                <li class="nav-item">
                    <a
                        class="nav-link {{
                            request()->routeIs('admin.businesses.show') ? 'active' : ''
                        }}"
                        href="{{ route('admin.businesses.show', ['business' => $business->id]) }}"
                    >
                        Basic Info
                    </a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link {{
                            request()->routeIs('admin.businesses.locations.index') ? 'active' : ''
                        }}"
                        href="{{ route('admin.businesses.locations.index', ['business' => $business->id]) }}"
                    >
                        Locations
                    </a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link {{
                            request()->routeIs('admin.businesses.contacts.index') ? 'active' : ''
                        }}"
                        href="{{ route( 'admin.businesses.contacts.index', ['business' => $business->id]) }}"
                    >
                        Contact
                    </a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link {{
                            request()->routeIs('admin.businesses.listings.index') ? 'active' : ''
                        }}"
                        href="{{  route('admin.businesses.listings.index', ['business' => $business->id]) }}"
                    >
                        Products/Services
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a
                        class="nav-link dropdown-toggle {{
                            (
                                request()->routeIs('admin.businesses.reviews.index')
                                || request()->routeIs('admin.businesses.comments.index')
                            ) ? 'active' : ''
                        }}"
                        data-toggle="dropdown"
                        href="#"
                        role="button"
                        aria-haspopup="true"
                        aria-expanded="false"
                    >
                        Others
                    </a>
                    <div class="dropdown-menu" style="width: 100%;">
                        <a
                            class="dropdown-item {{
                                request()->routeIs('admin.businesses.reviews.index') ? 'active' : ''
                            }}"
                            href="{{ route('admin.businesses.reviews.index', ['business' => $business->id]) }}"
                        >
                            Reviews
                        </a>
                        <a
                            class="dropdown-item {{
                                request()->routeIs('admin.businesses.comments.index') ? 'active' : ''
                            }}"
                            href="{{ route('admin.businesses.comments.index', ['business' => $business->id]) }}"
                        >
                            Comments
                        </a>
                    </div>
                </li>
            </ul>

            <div class="mt-4">
                @yield('business-relation')
            </div>
        </div>
    </div>
@endsection
