@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/dropzone/dropzone.min.css') }}">
    @endpush
@endonce

<div
    class="modal fade"
    id="galleryModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="galleryModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary font-weight-bold" id="galleryModalLabel">Gallery</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="font-italic">
                    You can upload photos that describe the business.
                    However, if you prefer to add a link, maybe a Youtube video, you can add a link.
                </p>

                <form action="#" class="dropzone" id="gallery-dropzone">
                    @csrf
                    <div class="fallback">
                        <input name="gallery[]" type="file" accept="image/*" multiple>
                    </div>
                </form>

                <hr />

                <form
                    id="galleryUrlForm"
                    action="#"
                    method="post"
                    class="user"
                    onsubmit="event.preventDefault(); submitGalleryUrlForm();"
                >
                    @csrf
                    <div class="form-group">
                        <div class="input-group">
                            <input
                                type="url"
                                id="galleryUrl"
                                name="url"
                                placeholder="Enter a video URL here"
                                class="form-control form-control-user"
                                required
                            >
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                        <small id="galleryUrlHelpBlock" class="form-text text-danger"></small>
                    </div>
                </form>

                <hr />

                <div class="d-flex justify-content-center">
                    <div id="loadingContainer">
                        <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
                    </div>

                    <div id="galleryContainer" class="table-responsive d-none">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="8%">S\N</th>
                                    <th>Type</th>
                                    <th>Content</th>
                                    <th width="30%">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="galleryList"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@once
    @push('scripts')
        <script src="{{ asset('vendor/dropzone/dropzone.min.js') }}"></script>
        <script>
            Dropzone.options.galleryDropzone = false;
            Dropzone.options.galleryDropzone = {
                url: '#',
                paramName: 'gallery',
                uploadMultiple: true,
                autoProcessQueue: true,
                acceptedFiles: 'image/*',
                maxFilesize: 5,
                addRemoveLinks: true,
                dictDefaultMessage: 'Drop photos for this business here to upload',
                init: function () {
                    this.on('complete', function (file) {
                        if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                            fetchGallery();
                            this.removeAllFiles(true);
                        }
                    });
                }
            };

            $(document).ready(function () {
                $('#galleryModal').on('show.bs.modal', function (e) {
                    let route = $(e.relatedTarget).data('route');
                    let name = $(e.relatedTarget).data('name');

                    $('#galleryModalLabel').html(`Gallery [${name}]`);
                    $('#gallery-dropzone').attr('action', route);
                    $('#galleryUrlForm').attr('action', route);

                    let galleryDropzone = Dropzone.forElement('#gallery-dropzone');
                    galleryDropzone.options.url = route;

                    fetchGallery();
                });
            });

            function fetchGallery() {
                $('#loadingContainer').removeClass('d-none');
                $('#galleryContainer').addClass('d-none');
                let route = $('#galleryUrlForm').attr('action');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: route,
                    type: 'get',
                    success: function (response) {
                        $('#loadingContainer').addClass('d-none');
                        $('#galleryContainer').removeClass('d-none');

                        buildGalleryList(response.data.gallery);
                    },
                    error: function (error) {
                        let response = JSON.parse(error.responseText)
                        alert(response.message)
                    }
                });
            }

            function buildGalleryList(galleries) {
                $('#galleryList').html('');

                galleries.forEach(function(gallery, index) {
                    let content = (gallery.type == 'media')
                        ? gallery.media.map(medium => medium.full_url)
                        : gallery.url

                    if (Array.isArray(content)) {
                        links = '';
                        content.forEach(function (link) {
                            links += `<img src="${link}" class="rounded-circle mr-2" width="40px" height="40px">`;
                        });
                        content = links;
                    }

                    $('#galleryList').append(`
                        <tr>
                            <td>${index+1}</td>
                            <td>${gallery.type}</td>
                            <td>${content}</td>
                            <td>
                                <a
                                    href="#"
                                    class="btn btn-sm btn-danger"
                                    onclick="event.preventDefault(); deleteGallery(${gallery.id});"
                                >
                                    <i class="fa fa-trash"></i>
                                    Delete
                                </a>
                            </td>
                        </tr>
                    `);
                });
            }

            function submitGalleryUrlForm() {
                let url = $('#galleryUrl');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('#galleryUrlForm').attr('action'),
                    type: 'post',
                    data: {
                        url: url.val()
                    },
                    success: function (response) {
                        fetchGallery();
                        $('#galleryUrlForm').trigger('reset')
                    },
                    error: function (error) {
                        let response = JSON.parse(error.responseText)
                        url.addClass('is-invalid')
                        $('#galleryUrlHelpBlock').html(response.message)
                    }
                });
            }

            function deleteGallery(galleryId) {
                let route = $('#galleryUrlForm').attr('action');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: `${route}/${galleryId}`,
                    type: 'delete',
                    success: function (response) {
                        fetchGallery();
                    },
                    error: function (error) {
                        let response = JSON.parse(error.responseText);
                        alert(response.message);
                    }
                });
            }
        </script>
    @endpush
@endonce
