@extends('admin.pages.businesses.layouts.relations')

@section('business-relation')
    @if (session('status'))
        <x-alert type="success" :message="session('status')"/>
    @endif

    @if ($errors->any())
        <x-alert type="danger" message="Validation Errors Occurred!">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </x-alert>
    @endif

    <form
        action="{{ route('admin.businesses.contacts.store', ['business' => $business->id]) }}"
        class="user"
        method="POST"
    >
        @csrf

        <div class="form-group">
            <label for="name">
                Representative name
            </label>
            <input
                type="text"
                id="name"
                name="name"
                class="form-control form-control-user @error('name') is-invalid @enderror"
                value="{{ old('name') ?: optional($business->businessContact)->name }}"
            >
        </div>

        <div class="form-group">
            <label for="phone">
                Representative phone
            </label>
            <input
                type="tel"
                id="phone"
                name="phone"
                class="form-control form-control-user @error('phone') is-invalid @enderror"
                value="{{ old('phone') ?: optional($business->businessContact)->phone }}"
            >
        </div>

        <div class="form-group">
            <label for="email">
                Representative email
            </label>
            <input
                type="text"
                id="email"
                name="email"
                class="form-control form-control-user @error('email') is-invalid @enderror"
                value="{{ old('email') ?: optional($business->businessContact)->email }}"
            >
        </div>

        <div class="form-group">
            <label for="facebook">
                Facebook
            </label>
            <input
                type="text"
                id="facebook"
                name="facebook"
                class="form-control form-control-user @error('facebook') is-invalid @enderror"
                value="{{ old('facebook') ?: optional($business->businessContact)->facebook }}"
            >
        </div>

        <div class="form-group">
            <label for="twitter">
                Twitter
            </label>
            <input
                type="text"
                id="twitter"
                name="twitter"
                class="form-control form-control-user @error('twitter') is-invalid @enderror"
                value="{{ old('twitter') ?: optional($business->businessContact)->twitter }}"
            >
        </div>

        <div class="form-group">
            <label for="instagram">
                Instagram
            </label>
            <input
                type="text"
                id="instagram"
                name="instagram"
                class="form-control form-control-user @error('instagram') is-invalid @enderror"
                value="{{ old('instagram') ?: optional($business->businessContact)->instagram }}"
            >
        </div>

        <div class="form-group">
            <label for="yookos">
                Yookos
            </label>
            <input
                type="text"
                id="yookos"
                name="yookos"
                class="form-control form-control-user @error('yookos') is-invalid @enderror"
                value="{{ old('yookos') ?: optional($business->businessContact)->yookos }}"
            >
        </div>

        <div class="form-group">
            <label for="linkedin">
                LinkedIn
            </label>
            <input
                type="text"
                id="linkedin"
                name="linkedin"
                class="form-control form-control-user @error('linkedin') is-invalid @enderror"
                value="{{ old('linkedin') ?: optional($business->businessContact)->linkedin }}"
            >
        </div>

        <div class="form-group">
            <label for="tiktok">
                Tiktok
            </label>
            <input
                type="text"
                id="tiktok"
                name="tiktok"
                class="form-control form-control-user @error('tiktok') is-invalid @enderror"
                value="{{ old('tiktok') ?: optional($business->businessContact)->tiktok }}"
            >
        </div>

        <button class="btn btn-primary btn-user btn-block" type="submit">
            Save Contact
        </button>
    </form>
@endsection
