@extends('admin.pages.businesses.layouts.relations')

@section('business-relation')
    @if (session('status'))
        <x-alert type="success" :message="session('status')"/>
    @endif

    {{ $listings->links() }}

    <div class="{{ $listings->count() ? 'row row-cols-1 row-cols-md-3' : '' }}">
        @forelse ($listings as $listing)
            <div class="col mb-4">
                <div class="card h-100 shadow">
                    <img
                        src="{{ $listing->featured_image }}"
                        class="card-img-top"
                        alt="{{ $listing->name }} image"
                    >
                    <div class="card-body">
                        <h5 class="card-title font-weight-bold">{{ $listing->name }}</h5>
                        <p class="card-text small">{{ truncate($listing->description, 100) }}</p>
                        <p class="card-text small">
                            <small class="text-muted">
                                Last updated {{ $listing->updated_at->diffForHumans() }}
                            </small>
                        </p>
                    </div>
                    <div class="card-footer">
                        <div class="btn-group btn-block" role="group" aria-label="Listing options">
                            <button
                                data-id="{{ $listing->id }}"
                                data-name="{{ $listing->name }}"
                                data-route="{{
                                    route( 'admin.listings.galleries.store' , [
                                        'listing' => $listing->id
                                    ])
                                }}"
                                data-toggle="modal"
                                data-target="#galleryModal"
                                class="btn btn-sm btn-primary"
                            >
                                Gallery
                            </button>
                            <a
                                href="{{ route('admin.listings.edit', [
                                    'listing' => $listing->id
                                ]) }}"
                                class="btn btn-sm btn-success"
                            >
                                Edit
                            </a>
                            <a
                                href="#"
                                data-id="{{ $listing->id }}"
                                data-name="{{ $listing->name }}"
                                data-model="listing"
                                data-route="{{
                                    route('admin.businesses.listings.destroy' , [
                                        'business' => $business->id,
                                        'listing' => $listing->id
                                    ])
                                }}"
                                data-toggle="modal"
                                data-target="#deleteModal"
                                class="btn btn-sm btn-danger"
                            >
                                Delete
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="alert alert-info">
                No listings found!
            </div>
        @endforelse
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation/>

@includeWhen($listings->count(), 'admin.pages.businesses.relations.includes.listing-gallery-modal')
