@extends('admin.pages.businesses.layouts.relations')

@includeWhen(
    $business->businessLocations()->count(),
    'admin.pages.businesses.relations.includes.location-gallery-modal'
)


@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
    @endpush
@endonce

@section('business-relation')
    @if (session('status'))
        <x-alert type="success" :message="session('status')"/>
    @endif

    <div class="row">
        <div class="col-md-4 border-right">
            <ul class="list-group">
                @forelse ($business->businessLocations->sortByDesc('updated_at') as $location)
                    <li class="list-group-item">
                        <div class="row small">
                            <div class="col-12 col-md-8">
                                <div class="mb-1">
                                    <a
                                        href="#"
                                        id="businessLocationId_{{ $location->id }}"
                                        data-route="{{
                                            route('admin.businesses.locations.update', [
                                                'business' => $business->id,
                                                'location' => $location->id
                                            ])
                                        }}"
                                        onclick="
                                            event.preventDefault();
                                            updateLocation(this.id);
                                        "
                                        class="text-decoration-none"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="Update Location"
                                    >
                                        <span>{{ $loop->iteration }}. &nbsp;</span>
                                        <span>{{ truncate($location->full_address) }}</span>
                                    </a>
                                </div>
                                @if(!$location->deleted_at)
                                    @if ($business->primary_location_id == $location->id)
                                        <span class="badge badge-success">current primary address</span>
                                    @else
                                        <form
                                            action="{{ route('admin.businesses.primary-location', ['business' => $business]) }}"
                                            method="post"
                                            class="m-0"
                                        >
                                            @csrf
                                            <input type="hidden" name="location" value="{{ $location->id }}">

                                            <button type="submit" class="badge badge-light border-0">
                                                Set as primary location
                                            </button>
                                        </form>
                                    @endif
                                @endif
                            </div>
                            <div class="col-12 col-md-4 text-md-right">
                                @if($location->deleted_at !==  null)
                                    <button
                                        data-id="{{ $location->id }}"
                                        data-name="{{ truncate($location->full_address, 40) }}"
                                        data-model="location"
                                        data-route="{{
                                            route( 'admin.businesses.locations.restore', [
                                                'business' => $business->id,
                                                'location' => $location->id
                                            ])
                                        }}"
                                        data-toggle="modal"
                                        data-target="#restoreModal"
                                        class="badge badge-warning border-0 mb-1"
                                    >
                                        <i class="fa fa-trash-restore"></i>
                                        Restore
                                    </button>
                                @else
                                    <button
                                        data-id="{{ $location->id }}"
                                        data-name="{{ truncate($location->full_address, 40) }}"
                                        data-model="location"
                                        data-route="{{
                                            route( 'admin.businesses.locations.destroy', [
                                                'business' => $business->id,
                                                'location' => $location->id
                                            ])
                                        }}"
                                        data-toggle="modal"
                                        data-target="#deleteModal"
                                        class="badge badge-danger border-0 mb-1"
                                    >
                                        <i class="fa fa-trash"></i>
                                        Delete
                                    </button>
                                @endif
                                <br>
                                <button
                                    data-id="{{ $location->id }}"
                                    data-name="{{ truncate($location->full_address, 40) }}"
                                    data-route="{{
                                        route( 'admin.locations.galleries.store', [
                                            'location' => $location->id
                                        ])
                                    }}"
                                    data-toggle="modal"
                                    data-target="#galleryModal"
                                    class="badge badge-primary border-0"
                                >
                                    <i class="fa fa-photo-video"></i>
                                    Gallery
                                </button>
                            </div>
                        </div>
                    </li>
                @empty
                    Existing locations will appear here!
                @endforelse
            </ul>
        </div>
        <div class="col-md-8">
            @if ($errors->any())
                <x-alert type="danger" message="Validation Errors Occurred!">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-alert>
            @endif

            <form
                id="businessLocationForm"
                action="{{ route( 'admin.businesses.locations.store' , ['business' => $business->id]) }}"
                class="user"
                method="POST"
            >
                @csrf
                <input type="hidden" name="_method" value="POST" id="_businessLocationFormMethod">

                <div class="d-flex justify-content-between align-items-center border-bottom mb-4">
                    <h5 class="font-weight-bold" id="formTitle">New Entry</h5>
                    <button
                        id="resetButton"
                        type="reset"
                        class="btn btn-sm btn-warning rounded-pill"
                        onclick="resetForm();"
                    >
                        Reset
                    </button>
                </div>

                <div class="form-group text-right">
                    <div class="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            name="status"
                            class="custom-control-input"
                            id="statusCheck"
                            {{ old('status') ? 'checked' : '' }}
                        >
                        <label class="custom-control-label" for="statusCheck">
                            Set visibility to public
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="city_id">
                                City
                                <span class="text-danger sup">*</span>
                            </label>
                            <select
                                type="text"
                                id="city_id"
                                name="city_id"
                                class="select2 form-control form-control-user @error('city_id') is-invalid @enderror"
                                required
                            ></select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="street_address">
                                Street Address
                                <span class="text-danger sup">*</span>
                            </label>
                            <input
                                type="text"
                                id="street_address"
                                name="street_address"
                                class="form-control form-control-user @error('street_address') is-invalid @enderror"
                                value="{{ old('street_address') }}"
                                required
                            >
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="address_landmark">
                                Address Landmark
                                <span class="text-danger sup">*</span>
                            </label>
                            <input
                                type="text"
                                id="address_landmark"
                                name="address_landmark"
                                class="form-control form-control-user @error('address_landmark') is-invalid @enderror"
                                value="{{ old('address_landmark') }}"
                                required
                            >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="website_url">
                                Website URL
                            </label>
                            <input
                                type="url"
                                id="website_url"
                                name="website_url"
                                class="form-control form-control-user @error('website_url') is-invalid @enderror"
                                value="{{ old('website_url') }}"
                            >
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Phone Number(s)</label>
                            <div class="input-group">
                                <input
                                    type="tel"
                                    id="phone_1"
                                    name="phone_1"
                                    class="form-control form-control-user @error('phone_1') is-invalid @enderror"
                                    value="{{ old('phone_1') }}"
                                    placeholder="Primary number"
                                >
                                <input
                                    type="tel"
                                    id="phone_2"
                                    name="phone_2"
                                    class="form-control form-control-user @error('phone_2') is-invalid @enderror"
                                    value="{{ old('phone_2') }}"
                                    placeholder="Secondary number"
                                >
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <div class="custom-control custom-checkbox">
                                            <input
                                                type="checkbox"
                                                name="enable_sms"
                                                class="custom-control-input"
                                                id="enableSmsCheck"
                                                {{ old('enable_sms') ? 'checked' : '' }}
                                            >
                                            <label class="custom-control-label" for="enableSmsCheck">
                                                Enable SMS
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <input
                                    type="tel"
                                    id="sms_number"
                                    name="sms_number"
                                    placeholder="SMS Phone Number"
                                    class="form-control form-control-user @error('sms_number') is-invalid @enderror"
                                    value="{{ old('sms_number') }}"
                                >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <div class="custom-control custom-checkbox">
                                            <input
                                                type="checkbox"
                                                name="enable_feedback"
                                                class="custom-control-input"
                                                id="enableFeedbackCheck"
                                                {{ old('enable_feedback') ? 'checked' : '' }}
                                            >
                                            <label class="custom-control-label" for="enableFeedbackCheck">
                                                Enable Feedback
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <input
                                    type="email"
                                    id="feedback_email"
                                    name="feedback_email"
                                    placeholder="Feedback Email Address"
                                    class="form-control form-control-user @error('feedback_email') is-invalid @enderror"
                                    value="{{ old('feedback_email') }}"
                                >
                            </div>
                        </div>
                    </div>
                </div>

                <button id="formButton" class="btn btn-primary btn-user btn-block" type="submit">
                    Save Location
                </button>
            </form>
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation/>
<!-- Delete Modal -->
<x-restore-confirmation/>

@once
    @push('scripts')
        <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
        <script>
            function updateLocation(locationDocumentId) {
                // Get the configured route
                let route = $(`#${locationDocumentId}`).data('route');

                // Decrypt the ID for the locationDocumentId
                let locationId = locationDocumentId.split('_')[1];

                // Get the real location data
                let allLocations = JSON.parse('{!! $business->businessLocations !!}');
                let location = allLocations.filter(location => location.id == locationId)[0];

                // Fill the form and other properties for updating the location
                $('#formTitle').html('Update Location').addClass('text-success');
                $('#businessLocationForm').attr('action', route);
                $('#_businessLocationFormMethod').val('PUT');
                getCity(location.city_id);
                $('#street_address').val(location.street_address);
                $('#address_landmark').val(location.address_landmark);
                $('#phone_1').val(location.phone_1);
                $('#phone_2').val(location.phone_2);
                $('#website_url').val(location.website_url);
                $('#geolocation').val(location.geolocation);
                $('#enableSmsCheck').prop('checked', location.enable_sms);
                $('#sms_number').val(location.sms_number);
                $('#enableFeedbackCheck').prop('checked', location.enable_feedback);
                $('#feedback_email').val(location.feedback_email);
                $('#statusCheck').prop('checked', location.status);
                $('#formButton').html('Update Location').addClass('btn-success');
            }

            function resetForm() {
                $('#formTitle').html('New Entry').removeClass('text-success');
                $('#businessLocationForm').attr('action', `{{
                    route('admin.businesses.locations.store', ['business' => $business->id])
                }}`);
                $('#_businessLocationFormMethod').val('POST');
                $('#formButton').html('Save Location').removeClass('btn-success');
            }

            $('#city_id.select2').select2({
                placeholder: 'Select a City',
                ajax: {
                    url: "{{ route('api.cities.search') }}",
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    processResults: function (response) {
                        return {
                            results: response.data.cities.map(city => {
                                return {
                                    id: city.id,
                                    text: city.name,
                                }
                            })
                        };
                    }
                },
            });

            let cityId = @json(old('city_id'));
            if (cityId) {
                getCity(cityId);
            }

            function getCity(id) {
                $('#city_id').val(id);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: `{{ route('api.cities.search') }}/?id=${id}`,
                    type: 'get',
                    success: function (response) {
                        let city = response.data.city

                        // create the option and append to Select2
                        var option = new Option(city.name, city.id, true, true);
                        $('#city_id.select2').append(option).trigger('change');

                        // manually trigger the `select2:select` event
                        $('#city_id.select2').trigger({
                            type: 'select2:select',
                            params: {
                                data: city
                            }
                        });
                    },
                    error: function (error) {
                        let response = JSON.parse(error.responseText)
                        alert(response.message)
                    }
                });
            }
        </script>
    @endpush
@endonce
