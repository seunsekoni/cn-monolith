@extends('admin.layouts.dashboard')

@section('title', 'Read Comment')

@section('main-content')
    <div class="d-flex justify-content-between align-items-center">
        <h1 class="h3 text-gray-800">Read Comment</h1>
        <a href="{{ route('admin.comments.index') }}" class="btn btn-sm btn-circle btn-primary" sr-only="Go back">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>
    <p class=""></p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-danger">
                {{ $comment->body }}
            </h6>
        </div>
        <div class="card-body">
            <div class="alert alert-info">
                There are {{ $repliesCount = $comment->replies()->count() }}
                {{ \Illuminate\Support\Str::of('reply')->plural($repliesCount) }}
                to this comment.
            </div>

            @each('admin.pages.comments.includes.replies', $comment->replies, 'reply')
        </div>
    </div>
@endsection

<!-- Delete Modal -->
<x-delete-confirmation />
