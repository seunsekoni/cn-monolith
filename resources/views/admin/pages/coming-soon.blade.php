@extends('admin.layouts.dashboard')

@section('main-content')
    <div class="wrapper">
        <x-unknown-page :title="$title" :description="$description"/>
    </div>
@endsection
