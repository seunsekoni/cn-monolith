@extends('admin.layouts.app')

@section('body-backgroud-class', 'bg-gradient-secondary')

@section('content')
    <div class="container">
        <div class="row d-flex justify-content-center align-items-center vh-100">
            <div class="col-xl-10 col-lg-12 col-md-9">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div
                                class="col-lg-6 d-none d-lg-block"
                                style="background: url('{{ mapMonthBackground('auth') }}'); background-position: center; background-size: cover;"
                            ></div>

                            @yield('auth-content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
