@extends('admin.layouts.app')

@section('content')
    <div id="wrapper">
        @include('admin.includes.sidebar')

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                @include('admin.includes.topbar')

                <!-- Begin Page Content -->
                <div class="container-fluid vh-100">
                    @yield('main-content')
                </div>
            </div>
        </div>
    </div>
@endsection
