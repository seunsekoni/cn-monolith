@extends('layouts.app')

@section('title', 'Page')

@section('styles')
<link href="{{ asset('css/theme-home.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
	<div class="nav-container">
		<div>
			@include('includes.nav-two')
		</div>
	</div>

	<div class="main-container">
        <div class="container py-5">
            <div class="pages-section">
                <div class="page-title">{{ ucwords($page->title) }}</div>
                <div class="page-subtitle">ConnectNigeria.com</div>
                <div class="page-body">{!! $page->body !!}</div>
            </div>
        </div>
        
        <!-- Adding shadow to the nav bar to make it distinct -->
        <style>
            nav{
                box-shadow: 0 0 10px 1px #00000018;
            }
        </style>

		<section>
			@include('includes.middle-cta', ['category' => $category ?? ''])
		</section>
		<!-- FOOTER CTA -->
		@include('includes.footer-cta', ['category' => $category->id ?? ''])
		<!-- END FOOTER CTA -->

		<!-- FOOTER -->
		@include('includes.footer')
		<!-- FOOTER -->
	</div>
@endsection
