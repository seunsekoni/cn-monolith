@extends('layouts.app')

@section('title', 'Real Estate & Property in Nigeria for Sale and Rent')
@section('description', 'Find property in Nigeria for sale, rent and lease on ConnectNigeria Real Estate.  Best deals on houses, plots of land, flats, event centres, offices, shops and commercial properties to buy or rent at affordable prices.')
@section('keywords', 'Property for sales in Nigeria, cheap house for sale, buy and sell properties , plots of land for sales, commercial property for sales. houses for rent in Nigeria , flat for rent, properties for rent in Nigeria')

@section('styles')
<link href="{{ asset('css/theme-global.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')

<div class="nav-container">
  <div>
    @include('includes.top-menu')
    @include('includes.realestate-nav')
  </div>
</div>

<div class="main-container">
  <section class="imagebg height-40">
    <div class="background-image-holder">
        <img class="img-fluid h-100" src="{{asset('img/real-estate/hero-banner-1.jpg')}}" alt="CN Business"/>
    </div>
    <div class="container pos-vertical-center">
      <div class="row align-items-center">
        <div class="col-md-12 col-lg-9 col-xl-9">
          <div class="mb-4">
            <h1 class="h3 mb-1 type-medium color-white">Find Property For Sale and Rent in Nigeria</h1>
          </div>

          <!-- search -->
          <form class="search-bar mb-2">
            <div class="row">
              <div class="col-5 col-md-2 mb-2 mb-md-0 mb-lg-0 mb-xl-0">
                <div class="input-select">
                  <select class="form-control form-control-lg cars-search-dropdown">
                  <option>For Sale</option>
                  <option>For Rent</option>
                </select>
                </div>
              </div>
              <div class="col-5 col-md-2 mb-2 mb-md-0 mb-lg-0 mb-xl-0">
                <div class="input-select">
                  <select class="form-control form-control-lg cars-search-dropdown">
                  <option>All Types</option>
                  <option>Bungalow</option>
                  <option>Apartment</option>
                  <option>Duplex</option>
                </select>
                </div>
              </div>
              <div class="col-5 col-md-2 mb-2 mb-md-0 mb-lg-0 mb-xl-0">
                <div class="input-select">
                  <select class="form-control form-control-lg cars-search-dropdown">
                  <option>Beds</option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                </select>
                </div>
              </div>
              <div class="col-5 col-md-2 mb-2 mb-md-0 mb-lg-0 mb-xl-0">
                <div class="input-select">
                  <select class="form-control form-control-lg cars-search-dropdown">
                  <option>Max Price</option>
                  <option>Under 100,000</option>
                  <option>Under 250,000</option>
                  <option>Under 500,000</option>
                  <option>Under 1,000,000</option>
                </select>
                </div>
              </div>
              <div class="col-9 col-md-3 pr-xs-0">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">In</span>
                  </div>
                  <input type="text" class="form-control" placeholder="Lagos, Nigeria">
                </div>
              </div>
              <div class="col-3 col-md-1 pl-xs-0">
                <button type="submit" class="btn btn-primary h-100"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </form>

          <div class="popular-searches">
            <span class="color-primary">
              <small class="type-bold">Popular:</small>
            </span>
            <span>
              <small class="color-white">Bungalow in Lagos, Apartment in Abuja, 2 Bedroom House in Lekki </small>
            </span>
          </div>
        </div>
        <div class="col-lg-3 col-xl-3 hidden-xs hidden-sm">
          <p class="mb-0"><small>Advertisements</small></p>
          <div class="medrec-carousel owl-carousel owl-theme" data-items="1" data-items-mobile-portrait="1" data-autoplay="true" data-autoplay-timeout="5000" data-loop="true">
            <div class="item">
              <img src="{{asset('img/real-estate/advert-1.jpg')}}" width="240" class="text-center mb-3">
            </div>
            <div class="item">
              <img src="{{asset('img/real-estate/advert-2.jpg')}}" width="240" class="text-center mb-3">
            </div>
            <div class="item">
              <img src="{{asset('img/real-estate/advert-3.jpg')}}" width="240" class="text-center mb-3">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- categories -->
  <section class="pb-0">
    <div class="container">
      <ul class="nav nav-tabs browse">
        <li><span class="type-bold">Browse by</span></li>
        <li><a data-toggle="tab" href="#type"class="active" >Type</a></li>
        <li><a data-toggle="tab" href="#price">Price</a></li>

      </ul>

      <div class="tab-content">
        <div id="make" class="tab-pane fade in active show">
          <div class="category-carousel owl-carousel owl-theme" data-items="8" data-margin="20" data-items-mobile-portrait="3" data-autoplay="true" data-loop="true" data-autoplay-timeout="3000">
            <div class="item text-center">
              <a href="#">
                <img src="{{asset('img/real-estate/cat-1.jpg')}}">

                <p class="cat-name"><small class="type-bold">Bungalow</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                  <img src="{{asset('img/real-estate/cat-2.jpg')}}">

                <p class="cat-name"><small class="type-bold">Commercial</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                  <img src="{{asset('img/real-estate/cat-3.jpg')}}">

                <p class="cat-name"><small class="type-bold">Duplex</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                  <img src="{{asset('img/real-estate/cat-4.jpg')}}">
                <p class="cat-name"><small class="type-bold">Estate</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <img src="{{asset('img/real-estate/cat-5.jpg')}}">

                <p class="cat-name"><small class="type-bold">Flat</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                  <img src="{{asset('img/real-estate/cat-6.jpg')}}">

                <p class="cat-name"><small class="type-bold">Factory</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                  <img src="{{asset('img/real-estate/cat-7.jpg')}}">

                <p class="cat-name"><small class="type-bold">Hotel</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                  <img src="{{asset('img/real-estate/cat-8.jpg')}}">

                <p class="cat-name"><small class="type-bold">Mansion</small></p>
              </a>
            </div>

          </div><!-- end carousel -->
        </div><!-- end tab-pane -->

        <div id="price" class="tab-pane fade">
          <div class="category-carousel owl-carousel owl-theme" data-items="8" data-margin="20" data-items-mobile-portrait="3" data-autoplay="true" data-loop="true" data-autoplay-timeout="3000">
            <div class="item text-center">
              <a href="#">
                <img src="{{asset('img/real-estate/cat-1.jpg')}}">

                <p class="cat-name"><small class="type-bold">Bungalow</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                  <img src="{{asset('img/real-estate/cat-2.jpg')}}">

                <p class="cat-name"><small class="type-bold">Commercial</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                  <img src="{{asset('img/real-estate/cat-3.jpg')}}">

                <p class="cat-name"><small class="type-bold">Duplex</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                  <img src="{{asset('img/real-estate/cat-4.jpg')}}">
                <p class="cat-name"><small class="type-bold">Estate</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                <img src="{{asset('img/real-estate/cat-5.jpg')}}">

                <p class="cat-name"><small class="type-bold">Flat</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                  <img src="{{asset('img/real-estate/cat-6.jpg')}}">

                <p class="cat-name"><small class="type-bold">Factory</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                  <img src="{{asset('img/real-estate/cat-7.jpg')}}">

                <p class="cat-name"><small class="type-bold">Hotel</small></p>
              </a>
            </div>
            <div class="item text-center">
              <a href="#">
                  <img src="{{asset('img/real-estate/cat-8.jpg')}}">

                <p class="cat-name"><small class="type-bold">Mansion</small></p>
              </a>
            </div>

          </div><!-- end carousel -->
        </div><!-- end tab-pane -->

      </div>
    </div>
  </section>

  <!-- featured -->
  <section class="featured-biz pb-0">
    <div class="container">

      <div class="row">
        <div class="col-md-9 col-lg-9 col-xl-9">
          <div class="row justify-content-end">
            <div class="col-7 col-md-9">
              <h5 class="type-bold mb-4">Featured Property Listings</h5>
            </div>
            <div class="col-5 col-md-3 text-right">
              <a href="">View All Properties <span class="color-dark ml-2"><i class="fas fa-angle-right"></i></span></a>
            </div>
          </div>
          <div class="row equal-container">

            @forelse ($listings as $listing)

              <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
                <a href="#">
                  <div class="featured card">
                    <span class="notify-badge type-bold">For Rent</span>
                    <img src="{{asset('img/real-estate/feat-1.jpg')}}" class="card-img-top" alt="Featured 1">
                    <div class="card-body equal">
                      <p class="card-title type-bold mb-1">Executive Three Bedroom Flat</p>
                      <p class="price type-medium mb-2">N 600,000.00</p>

                      <div class="listing-address mb-2">
                        <div class="mr-2">
                          <i class="fas fa-home"></i>
                          <span>Flat</span>
                        </div>
                        <div class="mr-2">
                            <i class="fas fa-bed"></i>
                          <span>3</span>
                        </div>
                        <div class="mr-2">
                            <i class="fas fa-bath"></i>
                          <span>3</span>
                        </div>
                        <div class="">
                            <i class="fas fa-ruler-vertical"></i>
                          <span>602 sqm</span>
                        </div>
                      </div>

                      <div class="listing-address mb-2">
                        <i class="fas fa-map-marker-alt"></i>
                        <span>Lekki, Lagos</span>
                      </div>
                    </div>
                  </div>
                </a>
              </div>

            @empty

              <p>There are currently no properties.</p>

            @endforelse

            {{-- <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
              <a href="#">
                <div class="featured card">
                  <span class="notify-badge type-bold">For Rent</span>
                  <img src="{{asset('img/real-estate/feat-2.jpg')}}" class="card-img-top" alt="Featured 1">
                  <div class="card-body equal">
                    <p class="card-title type-bold mb-1">Office And Desk Spaces</p>
                    <p class="price type-medium mb-2">N 5,000.00</p>

                    <div class="listing-address mb-2">
                      <div class="mr-2">
                        <i class="fas fa-home"></i>
                        <span>Office</span>
                      </div>
                      <div class="mr-2">
                          <i class="fas fa-bath"></i>
                        <span>3</span>
                      </div>
                      <div class="">
                          <i class="fas fa-ruler-vertical"></i>
                        <span>200 sqm</span>
                      </div>
                    </div>

                    <div class="listing-address mb-2">
                      <i class="fas fa-map-marker-alt"></i>
                      <span>Owerri, Imo</span>
                    </div>
                  </div>
                </div>
              </a>
            </div>

            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
              <a href="#">
                <div class="featured card">
                  <span class="notify-badge bg-primary-1 type-bold">For Sale</span>
                  <img src="{{asset('img/real-estate/feat-3.jpg')}}" class="card-img-top" alt="Featured 1">
                  <div class="card-body equal">
                    <p class="card-title type-bold mb-1">4 Units 3 Bedroom Flat / Apartment</p>
                    <p class="price type-medium mb-2">N 115,000,000</p>

                    <div class="listing-address mb-2">
                      <div class="mr-2">
                        <i class="fas fa-home"></i>
                        <span>Flat</span>
                      </div>
                      <div class="mr-2">
                          <i class="fas fa-bed"></i>
                        <span>3</span>
                      </div>
                      <div class="mr-2">
                          <i class="fas fa-bath"></i>
                        <span>3</span>
                      </div>
                      <div class="">
                          <i class="fas fa-ruler-vertical"></i>
                        <span>737 sqm</span>
                      </div>
                    </div>

                    <div class="listing-address mb-2">
                      <i class="fas fa-map-marker-alt"></i>
                      <span>Lekki, Lagos</span>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
              <a href="#">
                <div class="featured card">
                  <span class="notify-badge bg-primary-1 type-bold">For Sale</span>
                  <img src="{{asset('img/real-estate/feat-4.jpg')}}" class="card-img-top" alt="Featured 1">
                  <div class="card-body equal">
                    <p class="card-title type-bold mb-1">Royal Gardens City</p>
                    <p class="price type-medium mb-2">N 1,750,000</p>

                    <div class="listing-address mb-2">
                      <div class="mr-2">
                        <i class="fas fa-home"></i>
                        <span>Land</span>
                      </div>
                      <div class="">
                          <i class="fas fa-ruler-vertical"></i>
                        <span>600 sqm</span>
                      </div>
                    </div>

                    <div class="listing-address mb-2">
                      <i class="fas fa-map-marker-alt"></i>
                      <span>Mowe, Ogun</span>
                    </div>
                  </div>
                </div>
              </a>
            </div>

            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
              <a href="#">
                <div class="featured card">
                  <span class="notify-badge type-bold">For Rent</span>
                  <img src="{{asset('img/real-estate/feat-5.jpg')}}" class="card-img-top" alt="Featured 1">
                  <div class="card-body equal">
                    <p class="card-title type-bold mb-1">A Functioning Hotel</p>
                    <p class="price type-medium mb-2">N 12,000,000.00</p>

                    <div class="listing-address mb-2">
                      <div class="mr-1">
                        <i class="fas fa-home"></i>
                        <span>Hotel</span>
                      </div>
                      <div class="mr-1">
                          <i class="fas fa-bed"></i>
                        <span>21</span>
                      </div>
                      <div class="mr-1">
                          <i class="fas fa-bath"></i>
                        <span>21</span>
                      </div>
                      <div class="">
                          <i class="fas fa-ruler-vertical"></i>
                        <span>1296 sqm</span>
                      </div>
                    </div>

                    <div class="listing-address mb-2">
                      <i class="fas fa-map-marker-alt"></i>
                      <span>Egbeda, Lagos</span>
                    </div>
                  </div>
                </div>
              </a>
            </div>

            <div class="col-md-4 col-lg-4 col-xl-4 mb-3">
              <a href="#">
                <div class="featured card">
                  <span class="notify-badge bg-primary-1 type-bold">For Sale</span>
                  <img src="{{asset('img/real-estate/feat-6.jpg')}}" class="card-img-top" alt="Featured 1">
                  <div class="card-body equal">
                    <p class="card-title type-bold mb-1">Semi Detached Duplex</p>
                    <p class="price type-medium mb-2">N 35,000,000.00</p>

                    <div class="listing-address mb-2">
                      <div class="mr-1">
                        <i class="fas fa-home"></i>
                        <span>Duplex</span>
                      </div>
                      <div class="mr-1">
                          <i class="fas fa-bed"></i>
                        <span>4</span>
                      </div>
                      <div class="mr-1">
                          <i class="fas fa-bath"></i>
                        <span>5</span>
                      </div>
                      <div class="">
                          <i class="fas fa-ruler-vertical"></i>
                        <span>850 sqm</span>
                      </div>
                    </div>

                    <div class="listing-address mb-2">
                      <i class="fas fa-map-marker-alt"></i>
                      <span>Lekki, Lagos</span>
                    </div>
                  </div>
                </div>
              </a>
            </div> --}}
          </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xl-3 text-center">
          <p class="mb-4"><small>Advertisements</small></p>
          <div class="hidden-xs hidden-sm">
            <img src="{{asset('img/real-estate/advert-1.jpg')}}" width="240" class="text-center mb-3">
            <img src="{{asset('img/real-estate/advert-2.jpg')}}" width="240" class="text-center mb-3">
            <img src="{{asset('img/real-estate/advert-3.jpg')}}" width="240" class="text-center mb-3">
          </div>
          <div class="hidden-lg hidden-md">
            <div class="medrec-carousel owl-carousel owl-theme" data-items-mobile-portrait="1" data-autoplay="true" data-autoplay-timeout="5000" data-loop="true">
              <div class="item">
                <img src="{{asset('img/real-estate/advert-1.jpg')}}" width="240" class="text-center mb-3">
              </div>
              <div class="item">
                <img src="{{asset('img/real-estate/advert-2.jpg')}}" width="240" class="text-center mb-3">
              </div>
              <div class="item">
                <img src="{{asset('img/real-estate/advert-3.jpg')}}" width="240" class="text-center mb-3">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- cta -->
  <section>
    <div class="container">

      <!-- CTAs -->
    <div class="row">
    <!-- Get a quote -->
    <div class="col-md-7 col-lg-7 col-xl-7 mb-3 mb-lg-0 mb-xl-0">
      <div class="card home-card-ctas ">
        <div class="row no-gutters">


          <div class="col-md-4 col-lg-4 col-xl-4">
            <div class="imagebg h-100 second">
              <div class="background-image-holder border-tl-radius border-bl-radius">
                <img src="{{asset('img/real-estate/home-1.jpg')}}" alt="">
              </div>
            </div>
          </div><!-- end col -->

          <div class="col-md-8 col-lg-8 col-xl-8">
            <div class="card-body border-tr-radius border-br-radius bg-primary-1">
              <h4 class="card-title">Real estate news and <span class="next-line">expert reviews </span></h4>
              <p>Connect Nigeria Real Estate brings you property news, expert reviews in the real estate industries, property investment and buying advice from our experts.</p>
              <a href="#" class="btn btn-orange">Read Property News</a>
            </div>
          </div><!-- end col -->
        </div><!-- end row -->
      </div><!-- end card -->
    </div><!-- end quote cta -->

    <!-- Confirm Businesses -->
    <div class="col-md-5 col-lg-5 col-xl-5">
      <div class="card home-card-ctas h-100 ">
        <div class="imagebg image-dark">
          <div class="background-image-holder border-radius-5">
            <img src="{{asset('img/real-estate/home-2.jpg')}}" class="card-img">
          </div>
          <div class="card-img-overlay card-body">
            <div class="row">
              <div class="col-11 col-lg-7">
                <h4 class="card-title">Looking for home improvement tips?</h4>
                <p class="card-text">Searching for expert advice on home designs, home improvements and trends?  Ask our experts now!</p>
                <a href="#" class="btn btn-primary-1">Browse Services</a>
              </div>
            </div>
          </div>
        </div>

      </div><!-- end card -->
    </div><!-- end confirm cta -->
    </div><!-- end row -->
    </div>
  </section>

  <!-- Articles -->
  <section class="bg-secondary home-news" >
    <div class="container">
      <div class="row mb-4">
        <div class="col-6 col-md-6 col-lg-6 col-xl-6">
          <h5 class="type-bold">In the news</h5>
        </div>
        <div class="col-6 col-md-6 col-lg-6 col-xl-6 text-right">
          <a href="">Browse Articles <span class="color-dark ml-2"><i class="fas fa-angle-right"></i></span></a>
        </div>
      </div>
      <div class="home-news-carousel owl-carousel owl-theme" data-items="4" data-margin="15" data-items-mobile-portrait="1" data-items-tablet-portrait="2" data-loop="true" data-autoplay="true" data-autoplay-timeout="3000">

        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/real-estate/article-1.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Alex Obigdu</span></small>
                </p>
                <p class="card-title">Lagos To Introduce City Wide Video Surveillance With 13,000 CCTV</p>
              </div>
            </div>
          </a>
        </div>
        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/real-estate/article-2.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">19 Jul 2020 | <span class="meta-cat">Uzo Ajawi</span></small>
                </p>
                <p class="card-title">Lagos State Land Law Prohibiting Forceful Entry And Illegal</p>
              </div>
            </div>
          </a>
        </div>
        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/real-estate/article-3.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">20 Jul 2020 | <span class="meta-cat">Martin Andrews</span></small>
                </p>
                <p class="card-title">Real Estate Investment Secrets Every Young Nigerian Should Know</p>
              </div>
            </div>
          </a>
        </div>
        <div class="item" style="">
          <a href="#">
            <div class="card card-post">
              <img src="{{asset('img/real-estate/article-4.jpg')}}" class="card-img-top" alt="">
              <div class="card-body">
                <p class="card-text meta-posted">
                  <small class="meta-date">18 Jul 2020 | <span class="meta-cat">Kelly Abudhi</span></small>
                </p>
                <p class="card-title">5 Things Landlords In Nigeria Might Hide From Their Tenants</p>
              </div>
            </div>
          </a>
        </div>

      </div>
    </div>
  </section>
  <!-- end Articles section -->

  <section class="p-0">
    <div class="row no-gutters">
      <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="imagebg height-50 image-dark">
          <div class="background-image-holder">
            <img src="{{asset('img/cars/footer-banners-1.jpg')}}">
          </div>
          <div class="container pos-vertical-center h-100">
            <div class="cta-banner-footer">
              <div class="row w-100 mb-2">
                <div class="col-md-10 col-lg-10 col-xl-10">
                  <h2 class="title type-bold mb-2">Reach your target market faster </h2>
                  <p>with just <strong>N10,000</strong></p>
                </div>
              </div>
              <div class="d-flex mb-3">
                <div class="cta-stats mr-3">
                  <img src="{{asset('img/businesses/window-pointer.png')}}">
                  <div>
                    <p class="type-medium mb-0">250,000+</p>
                    <small>Daily Visitors</small>
                  </div>
                </div>
                <div class="cta-stats">
                  <img src="{{asset('img/businesses/briefcase.png')}}">
                  <div>
                    <p class="type-medium mb-0">500,000+</p>
                    <small>Business Listings</small>
                  </div>
                </div>
              </div>
              <div class="d-flex">
                <a href="#" class="btn btn-lg btn-primary-1 mr-4">
                  Advertise with us
                </a>
                <div class="">
                  <p class="mb-0">Call <span class="type-medium">0700-800-5000</span></p>
                  <small>grow@connectnigeria.com</small>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="imagebg height-50 image-dark">
          <div class="background-image-holder">
            <img src="{{asset('img/real-estate/footer-banners-2.jpg')}}">
          </div>
          <div class="container pos-vertical-center h-100">
            <div class="cta-banner-footer-2 d-flex">
              <div class="row align-items-center w-100 mb-2">
                <div class="col-md-8 col-lg-8 col-xl-8">
                  <h2 class="title type-bold mb-4">Be the first to know what’s happening</h2>
                  <p class="mb-4">Sign up to our newsletter get interesting news and updates delivered right to your inbox</p>
                  <a href="#" class="btn btn-lg btn-black mr-4">
                    Subscribe Now
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('includes.footer')
</div>

@endsection
