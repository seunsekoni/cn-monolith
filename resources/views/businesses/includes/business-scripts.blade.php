<script>

      $(document).ready(function () {
          fetchReviews();
      });

      // Fetch reviews
      function fetchReviews() {
          let business = "{{ request()->route('business')->id }}"
          let route = "{{ route('fetch_business_reviews', ['business' => request()->route('business')->id ]) }}";

          $.ajax({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: route,
              type: 'get',
              success: function (response) {
                let totalReview = response.data.length
                  $('#pills-home-tab').text(`Reviews (${totalReview})`)
                  buildReviewDiv(response.data);
              },
              error: function (error) {
                  let response = JSON.parse(error.responseText)
              }
          });
      }

      function buildReviewDiv(reviews)
      {
        // If there are no reviews or approved reviews
        if (reviews.length == 0) {
          var reviewDiv = `<p>There are currently no reviews for this business.</p>`
          return $('#pills-home').append(reviewDiv)
        }

        // if reviews isn't empty
        user = "{{ (auth()->user()->id ?? '') }}"
          // clear the element and repopulate
          $('#pills-home').html('')
          reviews.forEach(review => {
            //
              var reviewDiv = `
                    <div class="row mt-4 w-100">
                      <div class="col-4 text-center">
                        <img src="{{asset('img/placeholders/avatar.jpg')}}" class="image-round">
                      </div>
                      <div class="col-8">`;

                    for (let i = 0; i < 5; i++) {
                      var reviewDiv  = reviewDiv + `<i class="fas fa-star color-${review.rating <= i ? 'grey': 'yellow' } font-small"></i>`
                    }
                    var reviewDiv = reviewDiv +
                        `
                        <p><span class="type-bold color-dark">${review.user_id == user ? 'You' : review.user_firstname } said</span> ${review.comment} </p>
                      </div>
                    </div>
              `
              return $('#pills-home').append(reviewDiv)

          });
      }

      // Store reviews
      // $(document).ready(function () {
      //   $('#reviewSubmit').on('click', function (e) {
      //     var user = "{{ (auth()->user()->id ?? '') }}"
      //     let loginRoute = "{{ route('login') }}"
      //     if(!user) {
      //       toastr.warning('Please Login to make reviews')
      //       return setTimeout(function () {
      //         $(location).attr('href', loginRoute);
      //       }, 2000)
      //     }
      //       submitReviews();
      //   });
      // });

      function submitReviews(e) {
        // check if user is authenticated
        e.preventDefault()
        var user = "{{ (auth()->user()->id ?? '') }}"
        let loginRoute = "{{ route('login') }}"
        if(!user) {
          toastr.warning('Please Login to make reviews')
          return setTimeout(function () {
            $(location).attr('href', loginRoute);
          }, 2000)
        }else{
          var user = "{{ (auth()->user()->id ?? '') }}"
          let reviewText = $('#reviewText').val();
          let reviewRating =  $("#starRatingPercentage").val();
          let business = "{{ request()->route('business')->id }}"
          let route = "{{ route('store_business_review') }}";

          // get the form post object
          var reviewObj = {
            comment: reviewText,
            rating: reviewRating,
            businessReview: business,
            user_id: user
          }

          $.ajax({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: route,
              data: reviewObj,
              type: 'post',
              success: function (response) {
                fetchReviews()
                toastr.success("Review has been saved on this business.");
                $('#ratingModal').modal('hide');
              },
              error: function (error) {
                  let response = JSON.parse(error.responseText)
                  toastr.error("Unable to save review at the moment");
              }
          });
          $("#reviewForm").trigger('reset');
        }

      }

      function submitComments() {
        // check if user is authenticated
        var user = "{{ (auth()->user()->id ?? '') }}"
        let body = $('#comment').val();
        let route = "{{ route('store_business_comments') }}"
        let business = "{{ request()->route('business')->id }}"


        // get the form post object
        var commentObj = {
          body: body,
          user_id: user,
          business: business
        }

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: route,
            data: commentObj,
            type: 'post',
            success: function (response) {
              toastr.success("Comment has been saved on this business.");
              fetchComments()
            },
            error: function (error) {
                let response = JSON.parse(error.responseText)
                toastr.error("Unable to save comments at the moment");

                if(response.message == "Unauthenticated."){
                  window.location.replace("{{ route('login') }}");
                }
            }
        });
        $('#comment').val('')

      }

      $("#addComment").on('submit', function(e){
        e.preventDefault();
        submitComments();
      })

      // COMMENTS SECTION

      $(document).ready(function () {
        fetchComments();
      });

      function fetchComments() {
          let business = "{{ request()->route('business')->id }}"
          let route = "{{ route('fetch_business_comments', ['business' => request()->route('business')->id ]) }}";

          $.ajax({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: route,
              type: 'get',
              success: function (response) {
                 // Get the total amount comments and update the count accordingly
                 let comments = response.data.filter(comment => comment.parent_id == null )
                 
                 let totalComment = comments.length
                  $('#pills-comments-tab').text(`Comments (${totalComment})`)

                  // clear the comment div and repopulate
                  $('#commentDiv').html('')
                  buildCommentDiv(response.data);
              },
              error: function (error) {
                  let response = JSON.parse(error.responseText)
              }
          });
      }

      function buildCommentDiv(comments) {
        // If there are no comments or approved comments
        if (comments.length == 0) {
          $('.comment').html('')
          var commentDiv = `<p>There are currently no comments for this business.</p>`
          $('.comment').append(commentDiv)
        }
        comments.forEach(comment => {
          var commentDiv = null
          if(!comment.parent_id){
            commentDiv = `
              <div class="card p-3 comment-${comment.id}">
                <div class="d-flex justify-content-between align-items-center">
                  <div class="user d-flex flex-row align-items-center">
                    <img src="{{asset('img/placeholders/avatar.jpg')}}" width="30" class="user-img rounded-circle mr-2">
                    <span>
                      <small class="font-weight-bold text-primary commenter">${comment.user_firstname }</small> -
                      <small class="font-weight-bold">${ comment.body }</small>
                    </span>
                  </div>
                  <small>${ comment.created_at }</small>
                </div>
                <div class="action d-flex justify-content-between mt-2 align-items-center">
                  <div class="reply head px-4" id="reply${comment.id}">
                    <small onClick=getReplyForm(${comment.id})>Reply</small>
                    <div id="replyBox"></div>
                  </div>
                </div>
              </div>
            `
            $('#commentDiv').append(commentDiv)
          }else{
            commentDiv = `
              <div class="comment-reply pl-4 comment-${comment.id}">
                <div class="d-flex justify-content-between align-items-center">
                  <div class="user d-flex flex-row align-items-center">
                    <img src="{{asset('img/placeholders/avatar.jpg')}}" width="30" class="user-img rounded-circle mr-2">
                    <span>
                      <small class="font-weight-bold text-primary commenter">${comment.user_firstname }</small> -
                      <small class="font-weight-bold">${ comment.body }</small>
                    </span>
                  </div>
                  <small>${ comment.created_at }</small>
                </div>
                <div class="action d-flex justify-content-between mt-2 align-items-center">
                  <div class="reply head px-4" id="reply${comment.id}">
                    <small onClick=getReplyForm(${comment.id})>Reply</small>
                    <div id="replyBox"></div>
                  </div>
                </div>
              </div>

            `
            $(`#commentDiv .comment-${comment.parent_id}`).append(commentDiv)
          }
        })

      }

      function getReplyForm(id){
        $("#replyBox .reply-add").each(function(i, e){
          e.remove()
        })
        $(`#reply${id} #replyBox`).append(
          `
            <div class="reply-add my-3 row">
            <form method="post" onsubmit="submitReply(event, ${id})">
              @csrf
                <div class="col-12">
                  <input type="text" name="body" id="value${id}" autofocus>
                  <input type="hidden" name="business" value="{{$business->slug}}"
                </div>
                <div class="col-12 mt-2">
                    <button type="button" class="btn btn-secondary cancel" onclick="cancelReplyForm()">Cancel</button>
                    <button type="submit" class="btn btn-primary">Reply</button>
                </div>
            </form>
            </div>
          `
        )
      }


      function cancelReplyForm(){
          $(".reply-add").each(function(i, e){
              e.remove()
          })
      }

      function submitReply(e, id){
        e.preventDefault()
        // check if user is authenticated
        var user = "{{ (auth()->user()->id ?? '') }}"
        let body = $(`#value${id}`).val();
        let route = "{{ route('store_business_comments') }}"
        let business = "{{ request()->route('business')->id }}"


        // get the form post object
        var commentObj = {
          body: body,
          user_id: user,
          business: business,
          parent_id: id
        }

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: route,
            data: commentObj,
            type: 'post',
            success: function (response) {
              toastr.success("Reply has been saved.");
              fetchComments()
              $("#replyBox .reply-add").each(function(i, e){
                e.remove()
              })
            },
            error: function (error) {
                let response = JSON.parse(error.responseText)
                toastr.error("Unable to save review at the moment");

                if(response.message == "Unauthenticated."){
                  window.location.replace("{{ route('login') }}");
                }
            }
        });

      }

    </script>
