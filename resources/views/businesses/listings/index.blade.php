@extends('layouts.app')

@section('title', 'Businesses')

@section('styles')
    <link href="{{ asset('css/theme-home.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="nav-container">
        <div>
            @include('includes.nav-two')
        </div>
    </div>

    <div class="main-container">
        <!-- HERO BANNER -->
        <section class="imagebg height-40">
            <div
                class="background-image-holder"
                style="background-image: url({{ mapMonthBackground('home') }})"
            ></div>
            <div class="container pos-vertical-center">
                <div class="row align-items-center">
                    <div class="col-md-12 col-lg-9 col-xl-9">
                        <!-- SEARCH BAR -->
                        <form
                            id="searchForm"
                            class="search-bar mb-2"
                            action="{{ route('search') }}"
                            method="GET"
                        >
                            {{-- Enable search --}}
                            <input type="hidden" name="q" value="s">

                            <div class="row">
                                <div class="col-12 col-md-6 mb-2">
                                    <input
                                        type="text"
                                        name="query"
                                        value="{{ request()->get('query') }}"
                                        class="form-control"
                                        placeholder="Product or service name"
                                    >
                                </div>

                                <div class="col-12 col-md-6 mb-2">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">In</span>
                                        </div>
                                        <input
                                            type="text"
                                            name="street"
                                            value="{{ request()->get('street') }}"
                                            class="form-control"
                                            placeholder="Lagos, Nigeria"
                                        >
                                    </div>
                                </div>

                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fas fa-search mr-2"></i>
                                        Search
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!-- END SEARCH BAR -->

                        {{-- <div class="popular-searches">
                            <span class="color-primary">
                                <small class="type-bold">Popular:</small>
                            </span>
                            <span>
                                <small class="color-white">Home Repair, Beauty Salon, Delivery Services, Electrician</small>
                            </span>
                        </div> --}}
                    </div>

                    <!-- CAROUSEL BANNER -->
                    <div class="col-lg-3 col-xl-3 hidden-xs hidden-sm">
                        <p class="mb-0"><small>Advertisements</small></p>
                        <div class="medrec-carousel owl-carousel owl-theme" data-items="1" data-items-mobile-portrait="1"
                            data-autoplay="true" data-autoplay-timeout="5000" data-loop="true">
                            <div class="item">
                                <img src="{{ asset('img/businesses/ads-4.jpg') }}" width="240"
                                    class="text-center mb-3">
                            </div>
                            <div class="item">
                                <img src="{{ asset('img/businesses/ads-2.jpg') }}" width="240"
                                    class="text-center mb-3">
                            </div>
                            <div class="item">
                                <img src="{{ asset('img/businesses/ads-3.jpg') }}" width="240"
                                    class="text-center mb-3">
                            </div>
                        </div>
                    </div>
                    <!-- END CAROUSEL BANNER -->
                </div>
            </div>
        </section>
        <!-- END HERO BANNER -->

        @include('includes.category-icon-slider')

        <section class="featured-biz pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-xl-9">
                        <div class="row justify-content-end">
                            <div class="col-7 col-md-9">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <x-tabuna-breadcrumbs
                                            class="breadcrumb-item"
                                            active="active"
                                        />
                                    </ol>
                                </nav>
                            </div>
                            <div class="col-5 col-md-3 text-right">
                                {{-- <a href="{{ route('businesses.index', ['q' => 'all']) }}">
                                    View All Businesses
                                    <span class="color-dark ml-2">
                                        <i class="fas fa-angle-right"></i>
                                    </span>
                                </a> --}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div class="card">
                                    <div class="card-body p0" style="overflow: hidden">
                                        <div class="row p40">
                                            <div class="col-md-8">
                                                <h4 class="type-bold mb-3">{{ $business->name }}</h4>
                                                @if($business->featured)
                                                    <span class="badge bg-orange h6-size color-white type-normal">FEATURED</span>
                                                @endif
                                                <div class="listing-indicators mb-2">
                                                    <div class="listing-rate d-inline-block">
                                                    @for ($i = 0; $i < 5; $i++)
                                                        <i
                                                            class="{{ getAverageRating($business) <= $i ? 'far fa-star color-lime' : 'fas fa-star color-lime' }} font-small"
                                                        >
                                                        </i>
                                                    @endfor
                                                        <span>{{ $business->reviews()->count() }}</span>
                                                    </div>

                                                    <div class="listing-like d-inline-block ml-3">
                                                        <i class="fas fa-thumbs-up color-lime"></i>
                                                        <span>{{ $business->likes }}</span>
                                                    </div>
                                                    <div class="listing-connected d-inline-block ml-3">
                                                        <i class="fas fa-link color-lime"></i>
                                                        <span>{{ $business->shares }}</span>
                                                    </div>
                                                </div>

                                                <p class="color-gray mb-3">{{ $business->businessType->name }}</p>

                                                <div class="row mb-3">
                                                    @isset($business->businessLocations()->first()->street_address )
                                                    <div class="col-md-12 mb-2">
                                                        <div class="listing-address">
                                                            <i class="fas fa-map-marker-alt color-lime mr-2"></i>
                                                            <span
                                                                class="font-small">{{ $business->businessLocations()->first()->street_address ?? '' }}</span>
                                                        </div>
                                                    </div>
                                                    @endisset
                                                    @isset($business->businessContact->phone)
                                                    <div class="col-md-12 mb-2">
                                                        <div class="listing-address">
                                                            <i class="fas fa-phone-alt color-lime mr-2"></i>
                                                            <span
                                                                class="font-small">{{$business->businessContact->phone ?? ''}}</span>
                                                        </div>
                                                    </div>
                                                    @endisset
                                                    @isset($business->businessContact->email)
                                                    <div class="col-md-12 mb-2">
                                                        <div class="listing-address">
                                                            <i class="fas fa-envelope color-lime mr-2"></i>
                                                            <span class="font-small"><a
                                                                    href="mailto:{{$business->businessContact->email ?? ''}}">{{$business->businessContact->email ?? ''}}</a></span>
                                                        </div>
                                                    </div>
                                                    @endisset
                                                    @isset($business->businessLocations()->first()->website_url)
                                                    <div class="col-md-12 mb-2">
                                                        <div class="listing-address">
                                                            <i class="fas fa-mouse-pointer color-lime mr-3"></i>
                                                            <span class="font-small"><a
                                                                    href="{{ $business->businessLocations()->first()->website_url ?? '' }}"
                                                                    target="_blank">{{ $business->businessLocations()->first()->website_url ?? '' }}</a></span>
                                                        </div>
                                                    </div>
                                                    @endisset
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                                <img src="{{ $business->logo }}" class="image-inherit mb-0">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row plr-40 bg-secondary-1">
                                                <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                    <i class="fas fa-share-alt mr-1"></i> Share
                                                </a>

                                                <a href="#"
                                                   class="btn btn-cta btn-light d-inline-block mb-0 mr-2 showReviews"
                                                   onclick="setDefaultStar()">
                                                    <i class="fas fa-star-half-alt mr-1"></i> Rate & Review
                                                </a>

                                                <!-- <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                    <i class="fas fa-comments mr-1"></i> Request Quote
                                                </a> -->

                                                <a href="#" class="btn btn-cta btn-light d-inline-block mb-0 mr-2">
                                                    <i class="fas fa-thumbs-up mr-1"></i> Like
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-md-12 p-0">
                                            <div class="row p40">
                                                <div class="col-md-4">
                                                    <div class="card hours">
                                                        @if($business->business_hours)
                                                            <div class="card-header type-bold">
                                                                Business Hours
                                                            </div>
                                                            <ul class="list-group list-group-flush">
                                                                @foreach(collect(json_decode($business->business_hours)) as $business_hour)
                                                                    <li class="list-group-item bg-secondary-1 font-small">
                                                                        <dl class="row mb-0">
                                                                            <dt class="col-sm-5 ">{{substr($business_hour->day,0,3)}}</dt>
                                                                            <dd class="col-sm-7 mb-0">{{$business_hour->open}}
                                                                                - {{$business_hour->close}}</dd>
                                                                        </dl>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <iframe
                                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.546441382114!2d3.385522114494949!3d6.452222595332086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b8b0fb5756d71%3A0x7c6a0510ac25ea80!2sStallion%20Plaza!5e0!3m2!1sen!2sph!4v1597047213715!5m2!1sen!2sph"
                                                        height="260"
                                                        frameborder="0"
                                                        style="border:1px solid #bfc3c9;width: 100%"
                                                        allowfullscreen=""
                                                        aria-hidden="false"
                                                        tabindex="0"
                                                    ></iframe>
                                                </div>

                                                <div class="col-12 row pt-4">
                                                    <div class="col-md-8">
                                                        <h5 class="type-bold mb-3">About Us</h5>
                                                        <div class="text-justify" style="line-height: 1.8em;">
                                                            {{ $business->profile }}
                                                        </div>
                                                    </div>

                                                    @if ($businessListings->count())
                                                        <div class="col-md-4">
                                                            <h6 class="type-bold mb-4">Products & Services</h6>

                                                            <ul class="arrow mt-3">
                                                                @foreach ($businessListings as $listing)
                                                                    <li>
                                                                        <a href="{{ route('listings.show', ['listing' => $listing]) }}">
                                                                            {{ $listing->name }}
                                                                        </a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif
                                                </div>

                                                <div class="col-md-12 p-4">
                                                    @if($business->galleries()->count())
                                                        <div class="row pt-4">
                                                            <h6 class="type-bold mb-4">Photo Gallery</h6>
                                                            <div
                                                                class="gallery-carousel owl-carousel owl-theme"
                                                                data-items="3"
                                                                data-margin="15"
                                                                data-items-mobile-portrait="1"
                                                                data-autoplay="true"
                                                                data-autoplay-timeout="2000"
                                                                data-loop="true"
                                                            >
                                                                @foreach($business->galleries as $gallery)
                                                                    @if($gallery->type == "media")
                                                                        @foreach($gallery->getMedia(constant("\App\Enums\MediaCollection::PHOTOS")) as $photo)
                                                                            <div class="item">
                                                                                <img src="{{ $photo->getFullUrl() }}"
                                                                                     alt="">
                                                                            </div>
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>

                                                <!-- Reviews and Rating modal -->
                                                <div class="modal fade center reviewModal" id="ratingModal"
                                                     tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog modal-md" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <div class="top-strip">
                                                                    <div class="close-review-modal">
                                                                        <i class="fa fa-times"></i>
                                                                    </div>
                                                                    <!-- Review form -->
                                                                    <div class="reviewHeading">Drop a review</div>
                                                                    <form method="post" onsubmit="submitReviews(event)"
                                                                          id="reviewForm">
                                                                        @csrf
                                                                        <input type="hidden" id="businessReview">
                                                                        <textarea name="comment" id="reviewText"
                                                                                  required cols="30" rows="10"
                                                                                  placeholder="Enter your review"></textarea>
                                                                        <div class='rating-widget'>
                                                                            <!-- Rating Stars Box -->

                                                                            <!-- This input has the percentage of stars -->
                                                                            <input type="hidden"
                                                                                   name="starRatingPercentage"
                                                                                   id="starRatingPercentage">
                                                                            <div class='rating-stars text-center'>
                                                                                <ul id='stars'>
                                                                                    <li class='star' title='Poor'
                                                                                        data-value='1'>
                                                                                        <i class='fa fa-star fa-fw'></i>
                                                                                    </li>
                                                                                    <li class='star' title='Fair'
                                                                                        data-value='2'>
                                                                                        <i class='fa fa-star fa-fw'></i>
                                                                                    </li>
                                                                                    <li class='star' title='Good'
                                                                                        data-value='3'>
                                                                                        <i class='fa fa-star fa-fw'></i>
                                                                                    </li>
                                                                                    <li class='star' title='Excellent'
                                                                                        data-value='4'>
                                                                                        <i class='fa fa-star fa-fw'></i>
                                                                                    </li>
                                                                                    <li class='star' title='WOW!!!'
                                                                                        data-value='5'>
                                                                                        <i class='fa fa-star fa-fw'></i>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>

                                                                            <div class='success-box'>
                                                                                <i class="fas fa-check"></i>
                                                                                <div class='text-message'></div>
                                                                                <div class='clearfix'></div>
                                                                            </div>
                                                                        </div>
                                                                        <button type="submit" id="reviewSubmit">Submit
                                                                            your Review
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                                {{-- <div class="review-modal">
                                                                    <div class="reviewHeading">Customers' Reviews</div>


                                                                        <div class="review-card">
                                                                            <div class="meta">
                                                                                <img class="photo" src="{{asset('img/businesses/ad-1.jpg')}}"></img>
                                                                            </div>
                                                                            <div class="description">
                                                                                <h1>Great Product!!</h1>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star"></span>
                                                                                <span class="fa fa-star"></span>
                                                                                <br>
                                                                                <h2>Adittya Dey</h2>
                                                                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis!</p>
                                                                            </div>
                                                                        </div>

                                                                        <br>
                                                                        <div class="review-card">
                                                                            <div class="meta">
                                                                                <img class="photo" src="{{asset('img/businesses/ad-1.jpg')}}"></img>
                                                                            </div>
                                                                            <div class="description">
                                                                                <h1>Great Product!!</h1>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star"></span>
                                                                                <span class="fa fa-star"></span>
                                                                                <br>
                                                                                <h2>Adittya Dey</h2>
                                                                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis!</p>
                                                                            </div>
                                                                        </div>

                                                                        <br>

                                                                        <div class="review-card">
                                                                            <div class="meta">
                                                                                <img class="photo" src="{{asset('img/businesses/ad-1.jpg')}}"></img>
                                                                            </div>
                                                                            <div class="description">
                                                                                <h1>Sleek design.</h1>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star"></span>
                                                                                <span class="fa fa-star"></span>
                                                                                <br>
                                                                                <h2>Adittya Dey</h2>
                                                                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis!</p>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <div class="bottom-strip"></div> --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row pt-5 px-3">
                                                    <div class="col-md-12">
                                                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                            <li class="nav-item">
                                                                <a
                                                                    class="nav-link active"
                                                                    id="pills-home-tab"
                                                                    data-toggle="pill"
                                                                    href="#pills-home"
                                                                    role="tab"
                                                                    aria-controls="pills-home"
                                                                    aria-selected="true"
                                                                >
                                                                    Reviews ({{ $approved_reviews->count() }})
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a
                                                                    class="nav-link"
                                                                    id="pills-comments-tab"
                                                                    data-toggle="pill"
                                                                    href="#pills-comments"
                                                                    role="tab"
                                                                    aria-controls="pills-comments"
                                                                    aria-selected="false"
                                                                >
                                                                    Comments ({{ $approved_comments->count() ?? 0 }})
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content" id="pills-tabContent">
                                                            <div
                                                                class="tab-pane fade show active"
                                                                id="pills-home"
                                                                role="tabpanel"
                                                                aria-labelledby="pills-home-tab"
                                                            ><!-- being fed by AJAX --></div>

                                                            <div class="tab-pane fade" id="pills-comments"
                                                                 role="tabpanel"
                                                                 aria-labelledby="pills-comment-tab">
                                                                <div class="comment-add my-3 row">
                                                                    <form action="#" method="post" id="addComment">
                                                                        <div class="col-12">
                                                                            <input type="text" name="" id="comment">
                                                                        </div>
                                                                        <div class="col-12 mt-2">
                                                                            <button type="submit"
                                                                                    class="btn btn-primary">Add Comment
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                                <div class="comment mb-5">
                                                                    <div class="row d-flex justify-content-center">
                                                                        <div class="col-12" id="commentDiv">
                                                                            <!-- Comments are fetched with AJAX -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- END COMMENT SECTION -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row mb-4">
                                <div class="col-6 col-md-6 col-lg-6 col-xl-6">
                                    <h5 class="type-bold">You might also like...</h5>
                                </div>
                            </div>
                            @if ($related_businesses->count())
                                <div class="row">
                                    @foreach($related_businesses as $business)
                                        <x-frontend.business-card :business="$business" />
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        @include('includes.featured-businesses')

                        <div class="row">
                            <div class="container">
                                <img src="{{asset('img/businesses/ad-1.jpg')}}" class="image-inherit mb-3">
                                <img src="{{asset('img/businesses/ad-2.jpg')}}" class="image-inherit mb-3">
                                <img src="{{asset('img/businesses/ad-3.jpg')}}" class="image-inherit mb-3">
                                <img src="{{asset('img/businesses/ad-4.jpg')}}" class="image-inherit mb-3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            @include('includes.middle-cta', ['category' => $category ?? ''])
        </section>

        <!-- Articles -->
        {{--<!-- <section class="bg-secondary home-news">
            <div class="container">
                <div class="row mb-4">
                    <div class="col-6 col-md-6 col-lg-6 col-xl-6">
                        <h5 class="type-bold">In the news</h5>
                    </div>
                    <div class="col-6 col-md-6 col-lg-6 col-xl-6 text-right">
                        <a href="https://connectnigeria.com/articles" target="_blank">Browse Articles <span class="color-dark ml-2"><i class="fas fa-angle-right"></i></span></a>
                    </div>
                </div>
                <div class="home-news-carousel owl-carousel owl-theme" data-items="4" data-margin="15"
                    data-items-mobile-portrait="1" data-items-tablet-portrait="2" data-loop="true" data-autoplay="true"
                    data-autoplay-timeout="3000">

                    @forelse($news_articles as $article)
                        <div class="item">
                            <a href="{{ $article->link ?? '' }}" target="_blank">
                                <div class="card card-post">
                                    <img src="{{ $article->better_featured_image->source_url ?? ''}}" class="card-img-top" alt="{{ $article->better_featured_image->alt_text ?? '' }}">
                                    <div class="card-body">
                                        <p class="card-text meta-posted">
                                            <small class="meta-date">{{ Carbon\Carbon::parse($article->date)->format('j M Y') ??  ''}} | <span class="meta-cat">Business</span></small>
                                        </p>
                                        <p class="card-title">{{ $article->title->rendered ?? ''}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @empty
                        <p>Unable to fetch articles at the moment</p>
                    @endforelse

                </div>
            </div>
        </section> -->
        <!-- end Articles section --> --}}

        @include('includes.footer-cta', ['category' => $category ?? ''])

        @include('includes.footer')
    </div>

@endsection
@once
    @push('scripts')
        @include('businesses.includes.business-scripts')
    @endpush
@endonce
