@extends('layouts.app')

@section('title', 'Businesses')

@section('styles')
	<link href="{{ asset('css/theme-home.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
<div class="nav-container">
	<div>
		@include('includes.nav-two')
	</div>
</div>

<div class="main-container">
	<!-- HERO BANNER -->
	<section class="imagebg height-40">
		<div
			class="background-image-holder"
			style="background-image: url({{ mapMonthBackground('home') }})"
		></div>
		<div class="container pos-vertical-center">
			<div class="row align-items-center">
				<div class="col-md-12 col-lg-9 col-xl-9">
					<!-- SEARCH BAR -->
					<form
						id="searchForm"
						class="search-bar mb-2"
						action="{{ route('search') }}"
						method="GET"
					>
						{{-- Enable search --}}
						<input type="hidden" name="q" value="s">

						<div class="row">
							<div class="col-12 col-md-6 mb-2">
								<input
									type="text"
									name="query"
									value="{{ request()->get('query') }}"
									class="form-control"
									placeholder="Product or service name"
								>
							</div>

							<div class="col-12 col-md-6 mb-2">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">In</span>
									</div>
									<input
										type="text"
										name="street"
										value="{{ request()->get('street') }}"
										class="form-control"
										placeholder="Lagos, Nigeria"
									>
								</div>
							</div>

							<div class="col-12">
								<button type="submit" class="btn btn-primary">
									<i class="fas fa-search mr-2"></i>
									Search
								</button>
							</div>
						</div>
					</form>
					<!-- END SEARCH BAR -->

					{{-- <div class="popular-searches">
						<span class="color-primary">
							<small class="type-bold">Popular:</small>
						</span>
						<span>
							<small class="color-white">Home Repair, Beauty Salon, Delivery Services, Electrician</small>
						</span>
					</div> --}}
				</div>

				<!-- CAROUSEL BANNER -->
				<div class="col-lg-3 col-xl-3 hidden-xs hidden-sm">
					<p class="mb-0"><small>Advertisements</small></p>
					<div class="medrec-carousel owl-carousel owl-theme" data-items="1" data-items-mobile-portrait="1"
						data-autoplay="true" data-autoplay-timeout="5000" data-loop="true">
						<div class="item">
							<img src="{{ asset('img/businesses/ads-4.jpg') }}" width="240"
								class="text-center mb-3">
						</div>
						<div class="item">
							<img src="{{ asset('img/businesses/ads-2.jpg') }}" width="240"
								class="text-center mb-3">
						</div>
						<div class="item">
							<img src="{{ asset('img/businesses/ads-3.jpg') }}" width="240"
								class="text-center mb-3">
						</div>
					</div>
				</div>
				<!-- END CAROUSEL BANNER -->
			</div>
		</div>
	</section>
	<!-- END HERO BANNER -->

	<!-- CATEGORIES -->
	@include('includes.category-icon-slider')
	<!-- END CATEGORIES -->

	<!-- FEAT BIZ -->
	<section class="featured-biz pb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-lg-9 col-xl-9">
					<div class="row justify-content-end">
						<div class="col-7 col-md-9">
							<h5 class="type-bold mb-4">Listed Businesses</h5>
						</div>
						<div class="col-5 col-md-3 text-right">
							<a href="{{ route('businesses.index', ['b' => 'all']) }}">
								View All Businesses
								<span class="color-dark ml-2">
									<i class="fas fa-angle-right"></i>
								</span>
							</a>
						</div>
					</div>
					<div class="row equal-container">

						@forelse ($businesses as $business)
							<x-frontend.business-card :business="$business" />
						@empty
							<p>There are currently no business.</p>
						@endforelse

						<div class="d-flex justify-content-center cn-lemon">
							{{ $businesses->appends(request()->all())->links() }}
						</div>
					</div>
				</div>
				<div class="col-md-3 col-lg-3 col-xl-3 text-center">
					<p class="mb-4"><small>Advertisements</small></p>
					<div class="hidden-xs hidden-sm">
						<img src="{{asset('img/businesses/ads-1.jpg')}}" width="240" class="text-center mb-3">
						<img src="{{asset('img/businesses/ads-2.jpg')}}" width="240" class="text-center mb-3">
						<img src="{{asset('img/businesses/ads-3.jpg')}}" width="240" class="text-center mb-3">
					</div>
					<div class="hidden-lg hidden-md">
						<div class="medrec-carousel owl-carousel owl-theme" data-items-mobile-portrait="1" data-autoplay="true"
							data-autoplay-timeout="5000" data-loop="true">
							<div class="item">
								<img src="{{asset('img/businesses/ads-1.jpg')}}" width="240" class="text-center mb-3">
							</div>
							<div class="item">
								<img src="{{asset('img/businesses/ads-2.jpg')}}" width="240" class="text-center mb-3">
							</div>
							<div class="item">
								<img src="{{asset('img/businesses/ads-3.jpg')}}" width="240" class="text-center mb-3">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- END of FEATURED BIZ -->
	<section>
		@include('includes.middle-cta', ['category' => $category ?? '' ])
	</section>

	<!-- Articles -->
	{{--<!-- <section class="bg-secondary home-news">
		<div class="container">
			<div class="row mb-4">
				<div class="col-6 col-md-6 col-lg-6 col-xl-6">
					<h5 class="type-bold">In the news</h5>
				</div>
				<div class="col-6 col-md-6 col-lg-6 col-xl-6 text-right">
					<a href="https://connectnigeria.com/articles" target="_blank">Browse Articles <span class="color-dark ml-2"><i class="fas fa-angle-right"></i></span></a>
				</div>
			</div>
			<div class="home-news-carousel owl-carousel owl-theme" data-items="4" data-margin="15"
				data-items-mobile-portrait="1" data-items-tablet-portrait="2" data-loop="true" data-autoplay="true"
				data-autoplay-timeout="3000">

				@forelse($news_articles as $article)
					<div class="item">
						<a href="{{ $article->link ?? '' }}" target="_blank">
							<div class="card card-post">
								<img src="{{ $article->better_featured_image->source_url ?? ''}}" class="card-img-top" alt="{{ $article->better_featured_image->alt_text ?? '' }}">
								<div class="card-body">
									<p class="card-text meta-posted">
										<small class="meta-date">{{ Carbon\Carbon::parse($article->date)->format('j M Y') ??  ''}} | <span class="meta-cat">Business</span></small>
									</p>
									<p class="card-title">{{ $article->title->rendered ?? ''}}</p>
								</div>
							</div>
						</a>
					</div>
				@empty
					<p>Unable to fetch articles at the moment</p>
				@endforelse

			</div>
		</div>
	</section> -->
	<!-- end Articles section --> --}}

	<!-- FOOTER CTA -->
		@include('includes.footer-cta', ['category' => $category ?? ''])
	<!-- END FOOTER CTA -->

	<!-- FOOTER -->
	@include('includes.footer')
	<!-- FOOTER -->
</div>

@endsection
