<?php

use App\Enums\Distance;

return [

    Distance::class => [
        Distance::KILOMETER1 => '1 km',
        Distance::KILOMETER5 => '5 km',
        Distance::KILOMETER10 => '10 km',
        Distance::KILOMETER20 => '20 km',
    ],

];
