<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static KILOMETER1()
 * @method static static KILOMETER5()
 * @method static static KILOMETER10()
 * @method static static KILOMETER20()
 */
final class Distance extends Enum implements LocalizedEnum
{
    public const KILOMETER1 = '6';
    public const KILOMETER5 = '5';
    public const KILOMETER10 = '4';
    public const KILOMETER20 = '3';
}
