<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static BOTTOM()
 * @method static static MIDDLE()
 * @method static static TOP()
 */
final class Position extends Enum
{
    public const BOTTOM = 'bottom';
    public const MIDDLE = 'middle';
    public const TOP = 'top';
}
