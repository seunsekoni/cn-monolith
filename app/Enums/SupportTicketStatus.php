<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static OPEN()
 * @method static static PROCESSING()
 * @method static static CLOSED()
 */
final class SupportTicketStatus extends Enum
{
    public const OPEN = 'open';
    public const PROCESSING = 'processing';
    public const CLOSE = 'close';
}
