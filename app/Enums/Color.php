<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static PRIMARY()
 * @method static static INFO()
 * @method static static DANGER()
 * @method static static SECONDARY()
 * @method static static SUCCESS()
 * @method static static WARNING()
 * @method static static LIGHT()
 * @method static static DARK()
 */
final class Color extends Enum
{
    public const DANGER = 'danger';
    public const DARK = 'dark';
    public const INFO = 'info';
    public const LIGHT = 'light';
    public const PRIMARY = 'primary';
    public const SECONDARY = 'secondary';
    public const SUCCESS = 'success';
    public const WARNING = 'warning';
}
