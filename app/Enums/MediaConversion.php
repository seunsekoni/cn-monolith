<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static STANDARD()
 * @method static static THUMB()
 */
final class MediaConversion extends Enum
{
    public const STANDARD = 'standard';
    public const THUMB = 'thumb';
}
