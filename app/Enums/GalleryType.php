<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static MEDIA()
 * @method static static LINK()
 */
final class GalleryType extends Enum
{
    public const MEDIA = 'media';
    public const LINK = 'link';
}
