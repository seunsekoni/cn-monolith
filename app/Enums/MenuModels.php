<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static PAGE()
 * @method static static CATEGORY()
 */
final class MenuModels extends Enum
{
    public const PAGE = 'page';
    public const CATEGORY = 'category';
}
