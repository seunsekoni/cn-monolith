<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static LOGO()
 * @method static static AVATAR()
 * @method static static FEATUREDIMAGE()
 * @method static static ICON()
 * @method static static WHITEICON()
 * @method static static IMAGE()
 * @method static static BACKDROP()
 * @method static static PHOTOS()
 */
final class MediaCollection extends Enum
{
    public const LOGO = 'logo';
    public const AVATAR = 'avatar';
    public const FEATUREDIMAGE = 'featured_image';
    public const ICON = 'icon';
    public const WHITEICON = 'white-icon';
    public const IMAGE = 'image';
    public const BACKDROP = 'backdrop';
    public const PHOTOS = 'photos';
    public const GALLERY = 'gallery';
}
