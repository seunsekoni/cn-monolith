<?php

namespace App\Logging\GoogleChat;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class GoogleChatLogHandler extends AbstractProcessingHandler
{
    /**
     * Define the
     *
     * @param Logger|int $level
     */
    public function __construct($level = Logger::WARNING)
    {
        parent::__construct($level);
    }

    /**
     * Process the log.
     *
     * @param array $record
     * @return void
     */
    protected function write(array $record): void
    {
        if (!in_array(strtolower($record['level_name']), config('logging.channels.google_chat.levels'))) {
            return;
        }

        if ($url = config('logging.channels.google_chat.url')) {
            $context = collect($record['context']);

            $message = "{$record['message']}";
            if ($context->isNotEmpty()) {
                $context->each(function ($value, $key) use (&$message) {
                    $key = json_encode($key);
                    $value = json_encode($value);

                    $message .= "\n*{$key}:* _{$value}_";
                });
            }

            $response = Http::post($url, [
                'text' => $message,
            ]);

            if ($response->failed()) {
                Log::alert($record['message'], $record['context']);
            }
        } else {
            Log::log($record['level_name'], $record['message'], $record['context']);
        }
    }
}
