<?php

namespace App\Logging\GoogleChat;

use Monolog\Logger;

class GoogleChatLogger
{
    /**
     * Create a custom Monolog instance.
     *
     * @param array $config
     * @return \Monolog\Logger
     */
    public function __invoke(array $config)
    {
        return new Logger('google_chat', [new GoogleChatLogHandler()]);
    }
}
