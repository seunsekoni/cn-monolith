<?php

namespace App\Notifications;

use App\Models\BusinessListing;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BusinessListingUpdatedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $old_listing_details;

    public $new_listing_details;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(array $old_listing_details, BusinessListing $new_listing_details)
    {
        $this->old_listing_details = $old_listing_details;
        $this->new_listing_details = $new_listing_details;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $action = route('admin.listings.edit', ['listing' => $this->new_listing_details]);
        return (new MailMessage())
            ->line("Hello Admin, {$this->old_listing_details['name']}, has made some modification on their Business Listing")
            ->line("Old Listing Name: {$this->old_listing_details['name']}  New Listing Name: {$this->new_listing_details->name}")
            ->line("Old Description: {$this->old_listing_details['description']}")
            ->line("New Description: {$this->new_listing_details->description}")
            ->action('View Listing', $action);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [];
    }
}
