<?php

namespace App\Notifications;

use App\Models\Business;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BusinessUpdatedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $old_business_details;
    public $new_business_details;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(array $old_business_details, Business $new_business_details)
    {
        $this->old_business_details = $old_business_details;
        $this->new_business_details = $new_business_details;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $action = route('admin.businesses.show', ['business' => $this->new_business_details]);
        return (new MailMessage())
            ->line("Hello Admin, {$this->old_business_details['name']}, has made some modification on their Business")
            ->line("Old Business Name: {$this->old_business_details['name']}  New Business Name: {$this->new_business_details->name}")
            ->line("Old Profile: {$this->old_business_details['profile']}")
            ->line("New Profile: {$this->new_business_details->profile}")
            ->action('View Business', $action);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [];
    }
}
