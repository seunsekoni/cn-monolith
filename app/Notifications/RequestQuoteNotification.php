<?php

namespace App\Notifications;

use App\Models\BusinessListing;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RequestQuoteNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private BusinessListing $businessListing;
    private array $userDetail;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(BusinessListing $businessListing, array $userDetail)
    {
        $this->businessListing = $businessListing;
        $this->userDetail = $userDetail;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject('Quotation Request')
            ->from($this->userDetail['email'])

            ->line("Hello {$this->businessListing->business->name}, You Have A Quotation Request")
            ->line("The Quotation Request came from {$this->userDetail['last_name']} {$this->userDetail['first_name']}")
            ->line("with email {$this->userDetail['email']}")
            ->line("based on the listing {$this->businessListing->name}")
            ->action('View Listing', route("listings.show", $this->businessListing))
            ->line($this->userDetail['quote-desc'])
            ->line('Thank you for using ' . config('app.name'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

}
