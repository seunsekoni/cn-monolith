<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WelcomeAdmin extends Notification implements ShouldQueue
{
use Queueable;
    private $email;
    private $first_name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($email, $first_name)
    {
        //
        $this->email = $email;
        $this->first_name = $first_name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
                    ->greeting("Hello $this->first_name")
                    ->line('Welcome to ' . env('APP_NAME'))
                    ->line('you have been profiled as an admin.')
                    ->line('below are your login credentials')
                    ->line("Email: $this->email")
                    ->line('Password: ' . env('DEFAULT_ADMIN_PASSWORD'))
                    ->line('we strictly recommend that you change your password to a more secured one')
                    ->action('Login', env('APP_URL') . "/admin/login")
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
