<?php

namespace App\Observers;

use App\Models\Admin;
use App\Models\Business;
use App\Notifications\BusinessUpdatedNotification;
use Illuminate\Support\Str;

class BusinessObserver
{
    /**
     * Attributes of the model to monitor for verification.
     */
    public const VERIFIABLE_ATTRIBUTES = ['name', 'profile'];

    /**
     * Handle the business "creating" event.
     *
     * @param  \App\Models\Business $business
     * @return void
     */
    public function creating(Business $business)
    {
        $business->slug = Str::slug($business->name);
        $business->business_hours = file_get_contents(base_path() . '/default_business_hours.json');
    }

    /**
     * Handle the business "created" event.
     *
     * @param  \App\Models\Business  $business
     * @return void
     */
    public function created(Business $business)
    {
    }

    /**
     * Handle the business "updating" event.
     *
     * @param  \App\Models\Business  $business
     * @return void
     */
    public function updating(Business $business)
    {
        // If user isn't an admin set verified column to false if any of the
        // verifiable attributes was edited.
        if (auth()->check() && $business->isDirty(self::VERIFIABLE_ATTRIBUTES)) {
            $business->verified = false;

            $old_business = $business->getOriginal();

            // Get admin.
            $admin = Admin::first();
            $admin->notify((new BusinessUpdatedNotification($old_business, $business)));
        }

        // If the business has been verified/confirmed, update the verified_at column.
        if (auth()->guard('admin')->check() && $business->isDirty('verified') && !$business->getOriginal('verified')) {
            $business->verified_at = (bool) $business->verified ? now() : null;
        }

        // Update the slug.
        $business->slug = Str::slug($business->name);
    }

    /**
     * Handle the business "updated" event.
     *
     * @param  \App\Models\Business  $business
     * @return void
     */
    public function updated(Business $business)
    {
    }

    /**
     * Handle the business "deleting" event.
     *
     * @param  \App\Models\Business  $business
     * @return void
     */
    public function deleting(Business $business)
    {
        // Delete all the comments made on the listing.
        $business->comments()->delete();

        // Delete all the reviews made on the listing.
        $business->reviews()->delete();
    }

    /**
     * Handle the business "deleted" event.
     *
     * @param  \App\Models\Business  $business
     * @return void
     */
    public function deleted(Business $business)
    {
    }

    /**
     * Handle the business "restored" event.
     *
     * @param  \App\Models\Business  $business
     * @return void
     */
    public function restored(Business $business)
    {
    }

    /**
     * Handle the business "force deleted" event.
     *
     * @param  \App\Models\Business  $business
     * @return void
     */
    public function forceDeleted(Business $business)
    {
    }
}
