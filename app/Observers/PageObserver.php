<?php

namespace App\Observers;

use App\Models\Page;
use Illuminate\Support\Str;

class PageObserver
{
    /**
     * Handle the page "creating" event.
     *
     * @param Page $page
     * @return void
     */
    public function creating(Page $page)
    {
        $page->slug = Str::slug($page->title);
    }

    /**
     * Handle the page "updating" event.
     *
     * @param Page $page
     * @return void
     */
    public function updating(Page $page)
    {
        $page->slug = Str::slug($page->title);
    }

    /**
     * Handle the page "deleted" event.
     *
     * @param Page $page
     * @return void
     */
    public function deleted(Page $page)
    {
    }

    /**
     * Handle the page "restored" event.
     *
     * @param Page $page
     * @return void
     */
    public function restored(Page $page)
    {
    }

    /**
     * Handle the page "force deleted" event.
     *
     * @param Page $page
     * @return void
     */
    public function forceDeleted(Page $page)
    {
    }
}
