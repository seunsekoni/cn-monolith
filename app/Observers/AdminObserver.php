<?php

namespace App\Observers;

use App\Models\Admin;
use App\Notifications\VerifyAdminEmail;
use App\Notifications\Admin\WelcomeAdmin;

class AdminObserver
{
    /**
     * Attributes of the model to monitor for verification.
     */
    public const VERIFIABLE_ATTRIBUTES = ['email'];


    /**
     * Handle the Admin "created" event.
     *
     * @param  \App\Models\Admin  $admin
     * @return void
     */
    public function created(Admin $admin)
    {
        //
    }

    /**
     * Handle the Admin "updating" event.
     *
     * @param  \App\Models\Admin  $admin
     * @return void
     */
    public function updating(Admin $admin)
    {
        if ($admin->isDirty(self::VERIFIABLE_ATTRIBUTES)) {
            $admin->email_verified_at = null;

            $admin->notify((new VerifyAdminEmail()));
        }
    }

    /**
     * Handle the Admin "deleted" event.
     *
     * @param  \App\Models\Admin  $admin
     * @return void
     */
    public function deleted(Admin $admin)
    {
        //
    }

    /**
     * Handle the Admin "restored" event.
     *
     * @param  \App\Models\Admin  $admin
     * @return void
     */
    public function restored(Admin $admin)
    {
        //
    }

    /**
     * Handle the Admin "force deleted" event.
     *
     * @param  \App\Models\Admin  $admin
     * @return void
     */
    public function forceDeleted(Admin $admin)
    {
        //
        $admin->notify(new WelcomeAdmin($admin->email, $admin->firstname));
    }
}
