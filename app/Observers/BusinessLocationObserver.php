<?php

namespace App\Observers;

use App\Jobs\PopulateGeocodeForAddress;
use App\Models\BusinessLocation;

class BusinessLocationObserver
{
    /**
     * Handle the BusinessLocation "created" event.
     *
     * @param \App\Models\BusinessLocation $businessLocation
     * @return void
     */
    public function created(BusinessLocation $businessLocation)
    {
        PopulateGeocodeForAddress::dispatch($businessLocation);
    }

    /**
     * Handle the BusinessLocation "updated" event.
     *
     * @param \App\Models\BusinessLocation $businessLocation
     * @return void
     */
    public function updated(BusinessLocation $businessLocation)
    {
        PopulateGeocodeForAddress::dispatch($businessLocation);
    }

    /**
     * Handle the BusinessLocation "deleted" event.
     *
     * @param \App\Models\BusinessLocation $businessLocation
     * @return void
     */
    public function deleted(BusinessLocation $businessLocation)
    {
        // Trigger the listings for the business, so that the search index is updated.
        dispatch(function () use ($businessLocation) {
            $businessLocation->business->searchable();
            $businessLocation->business->businessListings()->searchable();
        })->delay(now()->addSeconds(5));
    }

    /**
     * Handle the BusinessLocation "restored" event.
     *
     * @param \App\Models\BusinessLocation $businessLocation
     * @return void
     */
    public function restored(BusinessLocation $businessLocation)
    {
        // Trigger the listings for the business, so that the search index is updated.
        dispatch(function () use ($businessLocation) {
            $businessLocation->business->searchable();
            $businessLocation->business->businessListings()->searchable();
        })->delay(now()->addSeconds(5));
    }

    /**
     * Handle the BusinessLocation "force deleted" event.
     *
     * @param \App\Models\BusinessLocation $businessLocation
     * @return void
     */
    public function forceDeleted(BusinessLocation $businessLocation)
    {
        // Trigger the listings for the business, so that the search index is updated.
        dispatch(function () use ($businessLocation) {
            $businessLocation->business->searchable();
            $businessLocation->business->businessListings()->searchable();
        })->delay(now()->addSeconds(5));
    }
}
