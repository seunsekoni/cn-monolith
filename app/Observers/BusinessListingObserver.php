<?php

namespace App\Observers;

use App\Models\Admin;
use App\Models\BusinessListing;
use App\Notifications\BusinessListingUpdatedNotification;
use Illuminate\Support\Str;

class BusinessListingObserver
{
    /**
     * Attributes of the model to monitor for verification.
     */
    public const VERIFIABLE_ATTRIBUTES = ['name', 'description'];

    /**
     * Handle the business listing "creating" event.
     *
     * @param  \App\Models\BusinessListing  $businessListing
     * @return void
     */
    public function creating(BusinessListing $businessListing)
    {
        $id = (BusinessListing::max('id') + 1);
        $businessListing->slug = Str::slug("{$businessListing->name}-{$id}");
    }

    /**
     * Handle the business listing "created" event.
     *
     * @param  \App\Models\BusinessListing  $businessListing
     * @return void
     */
    public function created(BusinessListing $businessListing)
    {
    }

    /**
     * Handle the business listing "updating" event.
     *
     * @param  \App\Models\BusinessListing  $businessListing
     * @return void
     */
    public function updating(BusinessListing $businessListing)
    {
        // If user isn't an admin set status column to false if any of the
        // verifiable attributes was edited.
        if (auth()->check() && $businessListing->isDirty(self::VERIFIABLE_ATTRIBUTES)) {
            $businessListing->status = false;

            $old_listing = $businessListing->getOriginal();

            // Get admin.
            $admin = Admin::first();

            $admin->notify((new BusinessListingUpdatedNotification($old_listing, $businessListing)));
        }

        $businessListing->slug = Str::slug("{$businessListing->name}-{$businessListing->id}");
    }

    /**
     * Handle the business listing "updated" event.
     *
     * @param  \App\Models\BusinessListing  $businessListing
     * @return void
     */
    public function updated(BusinessListing $businessListing)
    {
    }

    /**
     * Handle the business listing "deleting" event.
     *
     * @param  \App\Models\BusinessListing  $businessListing
     * @return void
     */
    public function deleting(BusinessListing $businessListing)
    {
        // Delete all the comments made on the listing.
        $businessListing->comments()->delete();

        // Delete all the reviews made on the listing.
        $businessListing->reviews()->delete();
    }

    /**
     * Handle the business listing "deleted" event.
     *
     * @param  \App\Models\BusinessListing  $businessListing
     * @return void
     */
    public function deleted(BusinessListing $businessListing)
    {
    }

    /**
     * Handle the business listing "restored" event.
     *
     * @param  \App\Models\BusinessListing  $businessListing
     * @return void
     */
    public function restored(BusinessListing $businessListing)
    {
    }

    /**
     * Handle the business listing "force deleted" event.
     *
     * @param  \App\Models\BusinessListing  $businessListing
     * @return void
     */
    public function forceDeleted(BusinessListing $businessListing)
    {
    }
}
