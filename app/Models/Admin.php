<?php

namespace App\Models;

use App\Enums\MediaCollection;
use App\Enums\MediaType;
use App\Notifications\Admin\ResetPassword;
use App\Notifications\VerifyAdminEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\CausesActivity;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Permission\Traits\HasRoles;

class Admin extends Authenticatable implements HasMedia, MustVerifyEmail
{
    use CausesActivity;
    use HasRoles;
    use InteractsWithMedia;
    use LogsActivity;
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'password',
    ];

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyAdminEmail());
    }

    /**
     * Log only specified attributes.
     *
     * @var array
     */
    protected static $logAttributes = ['first_name', 'last_name', 'email', 'phone'];

    /**
     * Modify log event name.
     *
     * @var string
     */
    protected static $logName = 'admin';

    /**
     * Modify only attributes that were actually changed after the update.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;

    /**
     * Get description for the log event.
     *
     * @param string $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return "You have {$eventName} an admin";
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Indicates custom attributes to append to model.
     *
     * @var array
     */
    public $appends = ['avatar'];

    /**
     * Registers media collections
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollection::AVATAR)
            ->useFallbackUrl('/img/placeholders/avatar.jpg')
            ->acceptsMimeTypes(MediaType::IMAGES)
            ->singleFile();
    }

    /**
     * Get full URL for the user's avatar.
     *
     * @return string
     */
    public function getAvatarAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollection::AVATAR);
    }

    /**
     * Get the admin's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Get all of the support tickets by the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function supportTickets()
    {
        return $this->morphMany(SupportTicket::class, 'supportable');
    }
}
