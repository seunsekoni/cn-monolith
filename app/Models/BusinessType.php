<?php

namespace App\Models;

use App\Enums\MediaType;
use App\Enums\MediaCollection;
use App\Enums\MediaConversion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class BusinessType extends Model implements HasMedia
{
    use SoftDeletes;
    use InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'status'];

    /**
     * Indicates custom attributes to append to model.
     *
     * @var array
     */
    public $appends = ['backdrop', 'icon'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * Registers media collections
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollection::BACKDROP)
            ->useFallbackUrl('/img/placeholders/backdrop.jpg')
            ->acceptsMimeTypes(MediaType::IMAGES)
            ->singleFile();

        $this->addMediaCollection(MediaCollection::ICON)
            ->useFallbackUrl('/img/placeholders/icon.png')
            ->acceptsMimeTypes(MediaType::ICONS)
            ->singleFile();
    }

    /**
     * Register media conversions.
     *
     * @return void
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion(MediaConversion::THUMB)
            ->width(100)
            ->height(100)
            ->sharpen(10)
            ->queued();

        $this->addMediaConversion(MediaConversion::STANDARD)
            ->width(400)
            ->height(400)
            ->sharpen(10)
            ->queued();
    }

    /**
     * Get full URL for the business type's backdrop.
     *
     * @return string
     */
    public function getBackdropAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollection::BACKDROP);
    }

    /**
     * Get full URL for the business type's icon.
     *
     * @return string
     */
    public function getIconAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollection::ICON);
    }

    /**
     * Scope a query to only include active business types.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query)
    {
        return $query->where('status', true);
    }

    /**
     * Get all the businesses for the business type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function businesses()
    {
        return $this->hasMany(Business::class);
    }
}
