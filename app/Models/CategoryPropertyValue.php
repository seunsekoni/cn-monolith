<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryPropertyValue extends Model
{
    /**
     * Get the owning category property.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo(CategoryProperty::class, 'category_property_id');
    }
}
