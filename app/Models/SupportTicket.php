<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupportTicket extends Model
{
    use SoftDeletes;

    /**
     * Get the parent supportable model .
     */
    public function supportable()
    {
        return $this->morphTo();
    }

    /**
     * Replies to the support tickets.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany(SupportTicket::class, 'parent_id')->orderBy('updated_at', 'desc');
    }

    /**
     * Get the writer of the comment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'supportable_id');
    }

    /**
     * Get the writer of the comment if admin.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class, 'supportable_id');
    }
}
