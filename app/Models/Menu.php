<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use SoftDeletes;

    /**
     * Indicates custom attributes to append to model.
     *
     * @var array
     */
    protected $appends = ['no_of_children'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_model' => 'boolean',
        'top_visible' => 'boolean',
        'bottom_visible' => 'boolean',
        'status' => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'is_model',
        'link',
        'model',
        'model_id',
        'top_visible',
        'bottom_visible',
        'target',
        'active',
        'parent_id',
        'order'
    ];

    /**
     * Get the parent for the menu.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo($this, 'parent_id', 'id');
    }

    /**
     * Get the children for the menu.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return auth()->guard('admin')->check() ?
            $this->hasMany($this, 'parent_id')->withTrashed() :
            $this->hasMany($this, 'parent_id');
    }

    /**
     * Get the count for the menu's children.
     *
     * @return string
     */
    public function getNoOfChildrenAttribute()
    {
        return $this->children->count();
    }

    /**
     * Scope a query to only include active menus.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query)
    {
        return $query->where('active', true);
    }

    /**
     * Get the owning category model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'model_id');
    }
}
