<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;

    protected $fillable = ['title', 'body', 'slug', 'keywords', 'status', 'description'];
    protected $casts = ['status' => 'boolean'];

    /**
     * Get the owning menu.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->hasOne(Menu::class, 'model_id');
    }
}
