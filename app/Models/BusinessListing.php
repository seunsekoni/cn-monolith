<?php

namespace App\Models;

use App\Enums\MediaCollection;
use App\Enums\MediaConversion;
use App\Enums\MediaType;
use App\Notifications\RequestQuoteNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Scout\Searchable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class BusinessListing extends Model implements HasMedia
{
    use Searchable;
    use SoftDeletes;
    use InteractsWithMedia;
    use HasFactory;
    use LogsActivity;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'business_id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'featured' => 'boolean',
        'status' => 'boolean',
        'discount_enabled' => 'boolean',
    ];

    /**
     * Log only specified attributes.
     *
     * @var array
     */
    protected static $logAttributes = ['*'];

    /**
     * Modify log event name.
     *
     * @var string
     */
    protected static $logName = 'businessListing';

    /**
     * Modify only attributes that were actually changed after the update.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    protected $touches = ['category'];

    /**
     * Indicates custom attributes to append to model.
     *
     * @var array
     */
    public $appends = ['featured_image'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['media'];

    /**
     * Determine if the model should be searchable.
     *
     * @return bool
     */
    public function shouldBeSearchable()
    {
        return (bool) $this->status;
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $searchableArray = $this->only([
            'id',
            'name',
            'description',
            'price',
            'featured',
        ]);

        $searchableArray['has_contact'] = optional($this->business->businessContact)->email
            && optional($this->business->businessContact)->phone;

        $searchableArray['tags'] = $this->tags->map(fn($tag) => $tag->name)->implode(', ');

        $searchableArray['category_name'] = optional($this->category)->name;
        $searchableArray['category_description'] = optional($this->category)->description;

        // @see https://github.com/meilisearch/MeiliSearch/issues/426#issuecomment-842373518
        // @see https://gitlab.com/nextdaysite/connect-nigeria/-/blob/develop/README.md
        $searchableArray['locations'] = $this->business
            ->businessLocations()
            ->active()
            ->cursor()
            ->filter(fn ($location) => $location->latitude && $location->longitude)
            ->map(function ($location) {
                return getGeoHashEncode($location->latitude, $location->longitude, 8);
            })
            ->flatten();

        $properties = $this->properties->map(function ($property) {
            $propertyName = strtolower($property->name);
            return [
                "property_{$propertyName}" => $property->optional
                    ? $property->pivot->category_property_value
                    : optional(CategoryPropertyValue::find($property->pivot->category_property_value))->name
            ];
        })->collapse();

        return collect($searchableArray)->merge($properties)->toArray();
    }

    /**
     * Registers media collections
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollection::FEATUREDIMAGE)
            ->useFallbackUrl('/img/placeholders/listing.jpg')
            ->acceptsMimeTypes(MediaType::IMAGES)
            ->singleFile();
    }

     /**
     * Register media conversions.
     *
     * @return void
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion(MediaConversion::THUMB)
            ->width(100)
            ->height(100)
            ->sharpen(10)
            ->queued();

        $this->addMediaConversion(MediaConversion::STANDARD)
            ->width(400)
            ->height(400)
            ->sharpen(10)
            ->queued();
    }

    /**
     * Scope a query to only include active business listings.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query)
    {
        return $query->where('status', true);
    }

    /**
     * Scope a query to only include featured business listings.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFeatured($query)
    {
        return $query->where('featured', true);
    }

    /**
     * Get full URL for the listing's featured image.
     *
     * @return string
     */
    public function getFeaturedImageAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollection::FEATUREDIMAGE);
    }

    /**
     * Get the owning business for the listing.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    /**
     * Get the owning category for the listing.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the owning discountType for the listing.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discountType()
    {
        return $this->belongsTo(DiscountType::class, 'discount_id');
    }

    /**
     * The tags that are associated with the listing.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphsToMany
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Get the listing's gallery.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function galleries()
    {
        return $this->morphMany(Gallery::class, 'gallerable');
    }

    /**
     * Get the listing's comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Get the listing's reviews.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function reviews()
    {
        return $this->morphMany(Review::class, 'reviewable');
    }

    /**
     * The properties of the listing.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function properties()
    {
        return $this->belongsToMany(CategoryProperty::class, 'business_listing_properties')
            ->withPivot(['category_property_value'])
            ->withTimestamps();
    }

    /**
     * send email for requesting quotation
     * @param array $userDetails
     */
    public function sendEmailForRequestingQuotation(array $userDetails)
    {
         $this->notify(new RequestQuoteNotification($this, $userDetails));
    }

    /**
     * Specifies the user's email
     *
     * @return string
     */
    public function routeNotificationForMail($notification): string
    {
        return $this->business->businessContact->email;
    }
}
