<?php

namespace App\Models;

use App\Enums\MediaType;
use App\Enums\MediaCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class CallToAction extends Model implements HasMedia
{
    use InteractsWithMedia;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'url', 'category_id', 'button_text', 'color', 'position'];

    /**
     * Indicates custom attributes to append to model.
     *
     * @var array
     */
    protected $appends = ['backdrop'];

    /**
     * Registers media collections
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollection::BACKDROP)
            ->useFallbackUrl('/img/placeholders/backdrop.jpg')
            ->acceptsMimeTypes(MediaType::IMAGES)
            ->singleFile();
    }

    /**
     * Get full URL for the callToAction's backdrop.
     *
     * @return string
     */
    public function getBackdropAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollection::BACKDROP);
    }

    /**
     * Get the owning category model
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Scope a query to only include active call to actions.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query)
    {
        return $query->where('status', true);
    }
}
