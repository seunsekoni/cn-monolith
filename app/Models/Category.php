<?php

namespace App\Models;

use App\Enums\MediaCollection;
use App\Enums\MediaConversion;
use App\Enums\MediaType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Category extends Model implements HasMedia
{
    use SoftDeletes;
    use InteractsWithMedia;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'status', 'template_id'];

    /**
     * Indicates custom attributes to append to model.
     *
     * @var array
     */
    public $appends = ['icon', 'backdrop'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['media'];

    /**
     * Registers media collections
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollection::ICON)
            ->useFallbackUrl('/img/placeholders/icon.png')
            ->acceptsMimeTypes(MediaType::ICONS)
            ->singleFile();

        $this->addMediaCollection(MediaCollection::WHITEICON)
            ->useFallbackUrl('/img/placeholders/white-icon.png')
            ->acceptsMimeTypes(MediaType::ICONS)
            ->singleFile();

        $this->addMediaCollection(MediaCollection::BACKDROP)
            ->useFallbackUrl('/img/placeholders/backdrop.jpg')
            ->acceptsMimeTypes(MediaType::IMAGES)
            ->singleFile();
    }

    /**
     * Register media conversions.
     *
     * @return void
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion(MediaConversion::THUMB)
            ->width(100)
            ->height(100)
            ->sharpen(10)
            ->queued();

        $this->addMediaConversion(MediaConversion::STANDARD)
            ->width(400)
            ->height(400)
            ->sharpen(10)
            ->queued();
    }

    /**
     * Scope a query to only include active categories.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query)
    {
        return $query->where('status', true);
    }

    /**
     * Get full URL for the category's icon.
     *
     * @return string
     */
    public function getIconAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollection::ICON);
    }

    /**
     * Get full URL for the category's white icon.
     *
     * @return string
     */
    public function getWhiteIconAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollection::WHITEICON);
    }

    /**
     * Get full URL for the category's Backdrop.
     *
     * @return string
     */
    public function getBackdropAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollection::BACKDROP);
    }


    /**
     * Get the related template for the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function template()
    {
        return $this->belongsTo(Template::class);
    }

    /**
     * Get all the listings for the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function businessListings()
    {
        return $this->hasMany(BusinessListing::class);
    }

    /**
     * Get all the properties for the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany(CategoryProperty::class);
    }
}
