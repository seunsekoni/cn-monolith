<?php

namespace App\Models;

use App\Enums\MediaCollection;
use App\Enums\MediaConversion;
use App\Enums\MediaType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Business extends Model implements HasMedia
{
    use Searchable;
    use InteractsWithMedia;
    use SoftDeletes;
    use LogsActivity;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Log only specified attributes.
     *
     * @var array
     */
    protected static $logAttributes = ['*'];

    /**
     * Modify log event name.
     *
     * @var string
     */
    protected static $logName = 'business';

    /**
     * Modify only attributes that were actually changed after the update.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'featured' => 'boolean',
        'verified' => 'boolean',
        'verified_at' => 'datetime',
        'changed_logo' => 'boolean',
    ];

    /**
     * Indicates custom attributes to append to model.
     *
     * @var array
     */
    public $appends = ['logo'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['media'];

    /**
     * Determine if the model should be searchable.
     *
     * @return bool
     */
    public function shouldBeSearchable()
    {
        return true;
        // return (bool) $this->verified;
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $searchableArray = $this->only([
            'id',
            'name',
            'profile',
            'verified',
            'featured',
            'verified',
            'changed_logo',
        ]);

        $searchableArray['has_contact'] = optional($this->businessContact)->email
            && optional($this->businessContact)->phone;

        $searchableArray['tags'] = $this->tags->map(fn($tag) => $tag->name)->implode(', ');

        // @see https://github.com/meilisearch/MeiliSearch/issues/426#issuecomment-842373518
        // @see https://gitlab.com/nextdaysite/connect-nigeria/-/blob/develop/README.md
        $searchableArray['locations'] = $this->businessLocations()
            ->active()
            ->cursor()
            ->filter(fn ($location) => $location->latitude && $location->longitude)
            ->map(function ($location) {
                return getGeoHashEncode($location->latitude, $location->longitude, 8);
            })
            ->flatten();

        return $searchableArray;
    }

    /**
     * Registers media collections
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(MediaCollection::LOGO)
            ->useFallbackUrl('/img/placeholders/logo.jpg')
            ->acceptsMimeTypes(MediaType::IMAGES)
            ->singleFile();
    }

    /**
     * Register media conversions.
     *
     * @param Media $media
     * @return void
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion(MediaConversion::THUMB)
            ->width(100)
            ->height(100)
            ->sharpen(10)
            ->queued();

        $this->addMediaConversion(MediaConversion::STANDARD)
            ->width(400)
            ->height(400)
            ->sharpen(10)
            ->queued();
    }

    /**
     * Scope a query to only include active businesses.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query)
    {
        return $query->where('verified', true);
    }

    /**
     * Scope a query to only include featured businesses.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFeatured($query)
    {
        return $query->where('featured', true);
    }

    /**
     * Get full URL for the business' logo.
     *
     * @return string
     */
    public function getLogoAttribute()
    {
        return $this->getFirstMediaUrl(MediaCollection::LOGO);
    }

    /**
     * Get the owning business type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function businessType()
    {
        return $this->belongsTo(BusinessType::class);
    }

    /**
     * Get the users associated with the business.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Get the primary location for the business.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function primaryLocation()
    {
        return $this->belongsTo(BusinessLocation::class);
    }

    /**
     * Get all the locations for the business.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function businessLocations()
    {
        return auth()->guard('admin')->check() ?
                $this->hasMany(BusinessLocation::class)->withTrashed() :
                $this->hasMany(BusinessLocation::class);
    }

    /**
     * Get all the listings for the business.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function businessListings()
    {
        return $this->hasMany(BusinessListing::class);
    }

    /**
     * Get the contact information of the business.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function businessContact()
    {
        return $this->hasOne(BusinessContact::class);
    }

    /**
     * The tags that are associated with the business.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphsToMany
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Get the business' gallery.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function galleries()
    {
        return $this->morphMany(Gallery::class, 'gallerable');
    }

    /**
     * Get the business' comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Get the business' reviews.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function reviews()
    {
        return $this->morphMany(Review::class, 'reviewable');
    }
}
