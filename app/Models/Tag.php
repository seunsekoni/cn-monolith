<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Set the tag's name.
     *
     * @param string $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

    /**
     * The businesses that belong to the tag.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphByMany
     */
    public function businesses()
    {
        return $this->morphedByMany(Business::class, 'taggable');
    }

    /**
     * The business listings that belong to the tag.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphByMany
     */
    public function businessListings()
    {
        return $this->morphedByMany(BusinessListing::class, 'taggable');
    }
}
