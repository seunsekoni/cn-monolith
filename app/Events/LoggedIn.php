<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;

class LoggedIn
{
    use Dispatchable;
    use SerializesModels;

    /**
     * @var mixed $user \App\Models\User | \App\Models\Admin
     */
    public $user;
    public Request $request;

    /**
     * Create a new event instance.
     *
     * @param Request $request
     * @param mixed $user
     * @return void
     */
    public function __construct(Request $request, $user)
    {
        $this->request = $request;
        $this->user = $user;
    }
}
