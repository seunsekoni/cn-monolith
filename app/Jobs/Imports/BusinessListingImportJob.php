<?php

namespace App\Jobs\Imports;

use App\Imports\BusinessListingImport;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class BusinessListingImportJob implements ShouldQueue
{
    use Batchable;
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (app()->environment('local')) {
            (new BusinessListingImport())->import(storage_path('existing-data/listings.xlsx'));
        } else {
            $files = Storage::files('existing-data/listings');

            foreach ($files as $file) {
                (new BusinessListingImport())->import($file);
            }
        }
    }
}
