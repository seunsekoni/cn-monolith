<?php

namespace App\Jobs;

use App\Models\BusinessLocation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * @see https://developers.google.com/maps/documentation/geocoding/overview
 */
class PopulateGeocodeForAddress implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public BusinessLocation $businessLocation;

    /**
     * Create a new job instance.
     *
     * @param BusinessLocation $businessLocation
     * @return void
     */
    public function __construct(BusinessLocation $businessLocation)
    {
        $this->businessLocation = $businessLocation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $location = $this->businessLocation;

        if ($location->latitude && $location->longitude) {
            return;
        }

        if ($data = getGeocodeByAddress($location->full_address)) {
            $location->latitude = $data['latitude'];
            $location->longitude = $data['longitude'];
            $location->saveQuietly();

            // Trigger the listings for the business, so that the search index is updated.
            dispatch(function () use ($location) {
                $location->business->searchable();
                $location->business->businessListings()->searchable();
            })->delay(now()->addSeconds(5));
        }

        return;
    }
}
