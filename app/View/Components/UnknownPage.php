<?php

namespace App\View\Components;

use Illuminate\View\Component;

class UnknownPage extends Component
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $description;


    /**
     * UnknownPage constructor.
     * @param $title
     * @param $description
     */
    public function __construct($title, $description)
    {
        $this->title = $title;
        $this->description = $description;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.unknown-page');
    }
}
