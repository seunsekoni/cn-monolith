<?php

namespace App\View\Components\Frontend;

use App\Models\BusinessListing;
use Illuminate\View\Component;

class ListingCard extends Component
{
    /**
     * The card business
     *
     * @var $business
     */
    public $listing;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(BusinessListing $listing)
    {
        $this->listing = $listing;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.frontend.listing-card');
    }
}
