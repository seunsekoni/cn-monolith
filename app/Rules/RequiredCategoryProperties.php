<?php

namespace App\Rules;

use App\Models\Category;
use Illuminate\Contracts\Validation\Rule;

class RequiredCategoryProperties implements Rule
{
    private $category_id;

    /**
     * Create a new rule instance.
     *
     * @param mixed $category_id
     * @return void
     */
    public function __construct($category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $category = Category::find($this->category_id);
        $properties = $category->properties()->approved()->get()->map(fn($property) => strtolower($property->name));
        $values = collect($value);

        $diff = $properties->diffKeys($values->keys());

        if ($diff->count() || $values->some(fn ($value) => $value === null)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.required');
    }
}
