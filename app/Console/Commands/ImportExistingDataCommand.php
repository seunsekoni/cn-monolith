<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ImportExistingDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:existing-data {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all the existing data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $model = $this->argument('model');

        Log::channel('google_chat')->alert("Starting import of {$model}");

        $class = "App\\Jobs\\Imports\\{$model}ImportJob";
        dispatch(new $class());
    }
}
