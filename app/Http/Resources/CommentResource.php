<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $filtered_comment = app('profanityFilter')->filter($this->body);
        return [
            'id' => $this->id,
            'user_firstname' => $this->user->first_name ?? '',
            'user_id' => $this->user_id,
            'body' => $filtered_comment,
            'parent_id' => $this->parent_id,
            'created_at' => $this->created_at->diffForHumans()

        ];
    }
}
