<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $filtered_comment = app('profanityFilter')->filter($this->comment);
        return [
            'id' => $this->id,
            'user_firstname' => $this->user->first_name ?? '',
            'user_id' => $this->user_id,
            'comment' => $filtered_comment,
            'rating' => $this->rating
        ];
    }
}
