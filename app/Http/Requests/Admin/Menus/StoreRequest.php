<?php

namespace App\Http\Requests\Admin\Menus;

use App\Enums\MenuModels;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'is_model' => 'bool',
            'model' => 'required_with:is_model',
//            'page' => 'required_with:is_model|required_if:model,' . MenuModels::PAGE,
//            'category' => 'required_with:is_model|required_if:model,' . MenuModels::CATEGORY,
            'link' => 'required_without:is_model',
            'position' => 'required|in:bottom,top',
            'target' => 'required|in:_parent,_blank,_self,_top',
            'order' => 'nullable|integer'
        ];
    }
}
