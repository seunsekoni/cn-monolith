<?php

namespace App\Http\Requests\Admin\City;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'state_id' => 'required|integer|exists:states,id',
            'name' => [
                'required',
                'string',
                Rule::unique('cities')
                    ->where(function ($query) {
                        return $query->where('state_id', $this->state_id);
                    })
                    ->ignoreModel($this->city),
            ],
            'status' => 'sometimes',
        ];
    }
}
