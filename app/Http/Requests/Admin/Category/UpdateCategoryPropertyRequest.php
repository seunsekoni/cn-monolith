<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateCategoryPropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                Rule::unique('category_properties')
                    ->where('category_id', $this->category->id)
                    ->ignoreModel($this->property)
            ],
            'hint' => 'nullable|string',
            'status' => 'sometimes',
            'optional' => 'sometimes',
            'filterable' => 'sometimes',
        ];
    }
}
