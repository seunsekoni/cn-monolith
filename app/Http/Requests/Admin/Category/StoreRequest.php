<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'icon' => 'nullable|image',
            'white_icon' => 'nullable|image',
            'name' => 'required|string|unique:categories,name',
            'description' => 'nullable|string|max:190',
            'status' => 'sometimes',
        ];
    }
}
