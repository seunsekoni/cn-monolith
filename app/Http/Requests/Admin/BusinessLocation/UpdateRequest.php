<?php

namespace App\Http\Requests\Admin\BusinessLocation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'city_id' => 'required|int|exists:cities,id',
            'street_address' => 'required|string',
            'address_landmark' => 'required|string',
            'phone_1' => 'nullable|string',
            'phone_2' => 'nullable|string',
            'website_url' => 'nullable|url',
            'enable_sms' => 'sometimes',
            'sms_number' => [
                'nullable',
                'string',
                Rule::requiredIf((bool) $request->enable_sms)
            ],
            'enable_feedback' => 'sometimes',
            'feedback_email' => [
                'nullable',
                'email',
                Rule::requiredIf((bool) $request->enable_feedback)
            ],
            'status' => 'sometimes',
        ];
    }
}
