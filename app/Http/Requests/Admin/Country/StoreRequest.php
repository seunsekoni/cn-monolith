<?php

namespace App\Http\Requests\Admin\Country;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'region_id' => 'required|integer|exists:regions,id',
            'name' => 'required|string|unique:countries',
            'code' => 'required|string',
            'currency' => 'required|string',
            'phone_code' => 'required|unique:countries|integer|digits_between:0,99999',
            'status' => 'sometimes'
        ];
    }
}
