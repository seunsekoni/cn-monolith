<?php

namespace App\Http\Requests\Admin\CallToAction;

use App\Enums\Color;
use App\Enums\Position;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required|exists:categories,id',
            'title' => 'required|string',
            'description' => 'required|string',
            'url' => 'required|url',
            'backdrop' => 'nullable|image',
            'button_text' => 'required|string',
            'color' => [
                'required',
                'string',
                new EnumValue(Color::class)
            ],
            'position' => [
                'required',
                'string',
                new EnumValue(Position::class)
            ]
        ];
    }
}
