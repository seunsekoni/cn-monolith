<?php

namespace App\Http\Requests\User\Listing;

use App\Rules\RequiredCategoryProperties;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'featured_image' => 'nullable|image',
            'category_id' => 'required|int|exists:categories,id',
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric',
            'status' => 'sometimes',
            'featured' => 'sometimes',
            'tags' => 'nullable|array',
            'tags.*' => 'string',
            'discount_enabled' => 'sometimes',
            'discount_type_id' => [
                'nullable',
                'int',
                'exists:discount_types,id',
                Rule::requiredIf((bool) $request->discount_enabled)
            ],
            'discount_value' => [
                'nullable',
                'int',
                Rule::requiredIf((bool) $request->discount_enabled)
            ],
            'discount_start' => [
                'nullable',
                'date',
                Rule::requiredIf((bool) $request->discount_enabled)
            ],
            'discount_end' => [
                'nullable',
                'date',
                Rule::requiredIf((bool) $request->discount_enabled)
            ],
            'properties' => ['sometimes', 'array', new RequiredCategoryProperties($this->category_id)]
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'business_id.required' => 'A business is required',
            'business_id.int' => 'Please select a business'
        ];
    }
}
