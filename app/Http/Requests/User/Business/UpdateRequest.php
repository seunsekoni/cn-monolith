<?php

namespace App\Http\Requests\User\Business;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'nullable|image',
            'business_type_id' => 'required|exists:business_types,id',
            'name' => [
                'required',
                'string',
                Rule::unique('businesses', 'name')->ignoreModel($this->user()->business)
            ],
            'profile' => 'required|string',
            'tags' => 'nullable|array',
            'tags.*' => 'string',
            'verified' => 'sometimes',
            'featured' => 'sometimes',
        ];
    }
}
