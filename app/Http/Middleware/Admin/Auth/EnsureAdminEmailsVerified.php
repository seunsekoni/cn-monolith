<?php

namespace App\Http\Middleware\Admin\Auth;

use Closure;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class EnsureAdminEmailsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $redirectToRoute
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|null
     */
    public function handle($request, Closure $next, $redirectToRoute = null)
    {
        $password = $request->user('admin')->password;
        $default_password = config('cnsettings.default_admin_password');
        $unchanged_password = Hash::check($default_password, $password);
        if (
            ! $request->user('admin') ||
            ($request->user('admin') instanceof MustVerifyEmail && ! $request->user('admin')->hasVerifiedEmail())
        ) {
            return $request->expectsJson()
                    ? abort(403, 'Your email address is not verified.')
                    : Redirect::guest(URL::route($redirectToRoute ?: 'admin.verification.notice'));
        }

        // dd($unchanged_password);
        if ($unchanged_password) {
            return redirect()->route('admin.profile.edit')->withStatus(__('Kindly update your password to continue using the application'));
        }

        return $next($request);
    }
}
