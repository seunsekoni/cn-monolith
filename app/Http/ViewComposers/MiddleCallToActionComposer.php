<?php

namespace App\Http\ViewComposers;

use App\Enums\Position;
use App\Models\CallToAction;
use Illuminate\View\View;

class MiddleCallToActionComposer
{
    public $middle_cta;

    /**
     * Create a middle cta composer.
     *
     * @return void
     */
    public function __construct(CallToAction $middle_cta)
    {
        $this->middle_cta = $middle_cta;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = $this->middle_cta->where('position', Position::MIDDLE)->inRandomOrder()->take(2)->get();
        $view->with('middle_cta', $data);
    }
}
