<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Menu;

class FooterComposer
{
    public $footerMenuList;

    /**
     * Create a footer composer.
     *
     * @return void
     */
    public function __construct(Menu $footer)
    {
        $this->footerMenuList = $footer;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = $this->footerMenuList::approved()
            ->where([
                ['bottom_visible', true],
                ['parent_id', null]
            ])
//            ->take(3)
            ->get();

        $view->with('footerMenuList', $data);
    }
}
