<?php

namespace App\Http\ViewComposers;

use App\Enums\Position;
use App\Models\CallToAction;
use Illuminate\View\View;

class BottomCallToActionComposer
{
    public $bottom_cta;

    /**
     * Create a bottom composer.
     *
     * @return void
     */
    public function __construct(CallToAction $bottom_cta)
    {
        $this->bottom_cta = $bottom_cta;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = $this->bottom_cta->where('position', Position::BOTTOM)->inRandomOrder()->take(2)->get();
        $view->with('bottom_cta', $data);
    }
}
