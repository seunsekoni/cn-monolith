<?php


namespace App\Http\ViewComposers;


use App\Models\Menu;
use Illuminate\View\View;

class TopBarMenuComposer
{
    public Menu $menu;

    /**
     * Create a topbar menu section composer.
     *
     * @return void
     */
    public function __construct(Menu $menu)
    {
        $this->menu = $menu;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $menus = Menu::approved()
            ->where([
                ['top_visible', true],
                ['parent_id', null]
            ])
            ->take(5)
            ->get();

        $view->with('menus', $menus);
    }
}
