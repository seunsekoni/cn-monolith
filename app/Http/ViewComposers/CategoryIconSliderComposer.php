<?php

namespace App\Http\ViewComposers;

use App\Models\Category;
use Illuminate\View\View;

class CategoryIconSliderComposer
{
    public $categoryIconList;

    /**
     * Create a footer composer.
     *
     * @return void
     */
    public function __construct(Category $category)
    {
        $this->categoryIconList = $category;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = $this->categoryIconList
            ->approved()
            ->has('businessListings')
            ->get();

        $view->with('categoryIconList', $data);
    }
}
