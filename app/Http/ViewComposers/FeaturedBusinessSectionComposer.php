<?php

namespace App\Http\ViewComposers;

use App\Models\Business;
use Illuminate\View\View;

class FeaturedBusinessSectionComposer
{
    public $business;

    /**
     * Create a featured business section composer.
     *
     * @return void
     */
    public function __construct(Business $business)
    {
        $this->business = $business;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = $this->business
            ->where('featured', 1)
            ->inRandomOrder()
            ->take(10)
            ->get();

        $view->with('featuredBusinesses', $data);
    }
}
