<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Models\BusinessListing;
use Illuminate\Http\Request;
use MeiliSearch\Endpoints\Indexes;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $query = $request->input('query');
        $type = $request->input('type') ?: 'business';

        $sortby = $request->input('sortby') ?: 'relevance';

        // DO NOT TOUCH; UPDATE THE RANKING FOR SORTING OF THE SEARCH
        $meilisearchCallback = function (Indexes $meilisearch, $query, $options) use ($sortby) {
            if ($sortby == 'recommended') {
                $meilisearch->updateRankingRules([
                    'desc(featured)',
                    'desc(verified)',
                    'desc(changed_logo)',
                    'desc(has_contact)',
                    'typo',
                    'words',
                    'proximity',
                    'attribute',
                    'wordsPosition',
                    'exactness',
                ]);
            } else {
                $meilisearch->resetRankingRules();
            }

            sleep(1); // MIGHT REQUIRE OPTIMIZATION

            return $meilisearch->search($query, $options);
        };
        // END RANKING FOR SORTING OF THE SEARCH

        // Set location in search.
        $location = trim($request->input('street') . ' ' . $request->input('city'));
        $distance = $request->input('distance') ?: (empty($request->input('street')) ? 3 : 4);

        if (!empty($location)) {
            if ($data = getGeocodeByAddress($location)) {
                $query .= (' ' . getGeoHashEncode(
                    $data['latitude'],
                    $data['longitude'],
                    $distance ?: 4
                ));
            }
        }

        if ($type == 'listing') {
            $data = BusinessListing::search($query, $meilisearchCallback);

            // Set category in search.
            if ($category = $request->input('category')) {
                $data = $data->where('category_name', $category);
            }
        } else {
            $data = Business::search($query, $meilisearchCallback);
        }

        $data = $data->paginate();

        return view('search-result', [
            'keyword' => $keyword,
            'type' => $type,
            'sortby' => $sortby,
            'data' => $data,
            'location' => $location,
            'distance' => $distance,
        ]);
    }
}
