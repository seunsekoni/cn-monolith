<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Show homepage.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $categories = Category::select(['id', 'name', 'slug'])
            ->approved()
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('business_listings')
                    ->where('status', '1')
                    ->whereColumn('business_listings.category_id', 'categories.id');
            })
            ->inRandomOrder()
            ->get();

        return view('index', [
            'categories' => $categories,
        ]);
    }
}
