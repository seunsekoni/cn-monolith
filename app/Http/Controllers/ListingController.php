<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommentResource;
use App\Http\Resources\ReviewResource;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Review;
use App\Models\BusinessListing;
use Illuminate\Http\Request;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $previewAll = ($request->input('l') == 'all');

        $listings = BusinessListing::approved()
            ->when(!$previewAll, function ($query) {
                $featuredQuery = clone $query;

                if ($featuredQuery->featured()->exists()) {
                    return $query->featured();
                }

                return $query;
            })
            ->inRandomOrder()
            ->paginate(6);

        return view('listings.index', [
            'listings' => $listings,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\BusinessListing $listing
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessListing $listing)
    {
        // fetch related businesses with the same tags
        $related_listings = BusinessListing::approved()
            ->whereHas('tags', function ($q) use ($listing) {
                return $q->whereIn('name', $listing->tags->pluck('name'))->limit(3);
            })
            ->where('id', '!=', $listing->id) // So you won't fetch same business listings
            ->take(3)
            ->inRandomOrder()
            ->get();

        $approved_reviews = $listing->reviews()->get();

        $approved_comments = $listing->comments()->whereNull('parent_id')->get();

        return view('listings.show', [
            'listing' => $listing,
            'related_listings' => $related_listings,
            'approved_reviews' => $approved_reviews,
            'approved_comments' => $approved_comments
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\BusinessListing $businessListing
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessListing $businessListing)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BusinessListing $businessListing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BusinessListing $businessListing)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\BusinessListing $businessListing
     * @return \Illuminate\Http\Response
     */
    public function destroy(BusinessListing $businessListing)
    {
    }

    /**
     * Fetch reviews for a specific listing.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchListingReviews(BusinessListing $listing)
    {
        $approved_reviews = $listing->reviews()->get();

        return response()->json([
            'success' => true,
            'data' => ReviewResource::collection($approved_reviews)
        ]);
    }

    /**
     * Fetch comments for a specific listing.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchListingComments($listingId)
    {
        $listing = BusinessListing::whereId($listingId)->first();
        $approved_comments = $listing->comments()->get();

        return response()->json([
            'success' => true,
            'data' => CommentResource::collection($approved_comments)
        ]);
    }

    /**
     * Store a review for a specific listing.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeListingReview(Request $request)
    {
        $review = new Review();

        $listingId = $request->listingReview;
        $listing = BusinessListing::whereId($listingId)->first();

        $review->comment = $request->comment;
        $review->rating = $request->rating;
        $review->user_id = $request->user_id;
        $review->reviewable()->associate($listing)->save();

        $approved_reviews = $listing->reviews()->get();

        // display notifications
        return response()->json([
            'success' => true,
            'data' => ReviewResource::collection($approved_reviews)
        ]);
    }

    /**
     * Store a comment or reply for a specific listing.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeListingComments(Request $request)
    {
        $comment = new Comment();

        $listingId = $request->listing;
        $listing = BusinessListing::whereId($listingId)->first();

        $comment->body = $request->body;
        $comment->user_id = auth()->user()->id;
        $comment->parent_id = $request->parent_id;
        $comment->commentable()->associate($listing)->save();

        $approved_comments = $listing->comments()->get();

        // display notifications
        return response()->json([
            'success' => true,
            'data' => CommentResource::collection($approved_comments)
        ]);
    }
}
