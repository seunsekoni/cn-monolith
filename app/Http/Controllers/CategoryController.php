<?php

namespace App\Http\Controllers;

use App\Models\BusinessListing;
use App\Models\Category;
use App\Models\CategoryProperty;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Category $category)
    {
        if ($request->input('q') == 's') {
            $listings = BusinessListing::search($request->input('query'))
                ->where('category_name', $category->name);

            // Filter by other queries.
            collect($request->all())
                ->filter(function ($value, $key) {
                    return Str::contains($key, 'property_');
                })
                ->each(function ($value, $key) use ($listings) {
                    if (!empty($value)) {
                        $listings = $listings->where($key, $value);
                    }
                });

            $listings = $listings->paginate(6);
        } else {
            $previewAll = ($request->input('l') == 'all');

            $listings = BusinessListing::where('category_id', $category->id)
                ->approved()
                ->when(!$previewAll, function ($query) {
                    $featuredQuery = clone $query;

                    if ($featuredQuery->featured()->exists()) {
                        return $query->featured();
                    }

                    return $query;
                })
                ->inRandomOrder()
                ->paginate(6);
        }

        $properties = CategoryProperty::approved()
            ->where('category_id', $category->id)
            ->where('filterable', true)
            ->with(['values'])
            ->take(2)
            ->get();

        return view('listings.index', [
            'listings' => $listings,
            'category' => $category,
            'properties' => $properties,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
