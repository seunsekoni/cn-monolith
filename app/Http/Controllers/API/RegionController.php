<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Region;
use Illuminate\Http\Request;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class RegionController extends Controller
{
    /**
     * Search region of the resource (used primarily for the select2).
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        if ($id = $request->input('id')) {
            return ResponseBuilder::asSuccess()
                ->withData([
                    'region' => Region::approved()->select(['id', 'name'])->find($id),
                ])
                ->build();
        }
        $regions = Region::approved()
            ->select(['id', 'name'])
            ->where('name', 'LIKE', "%{$request->term}%")
            ->orderBy('name')
            ->get();

        return ResponseBuilder::asSuccess()
            ->withData([
                'regions' => $regions
            ])
            ->build();
    }
}
