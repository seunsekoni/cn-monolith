<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class CountryController extends Controller
{
    /**
     * Search country of the resource (used primarily for the select2).
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        if ($id = $request->input('id')) {
            return ResponseBuilder::asSuccess()
                ->withData([
                    'country' => Country::approved()->select(['id', 'name'])->find($id),
                ])
                ->build();
        }

        $countries = Country::approved()
            ->select(['id', 'name'])
            ->where('name', 'LIKE', "%{$request->term}%")
            ->orderBy('name')
            ->get();

        return ResponseBuilder::asSuccess()
        ->withData([
            'countries' => $countries
        ])
        ->build();
    }
}
