<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ResponseBuilder
     */
    public function index()
    {
        $categories = Category::approved()
            ->with([
                'properties' => function ($query) {
                    $query->where('status', true)->with('values')->orderBy('name');
                },
            ])
            ->get();

        return ResponseBuilder::asSuccess()
            ->withData([
                'categories' => $categories
            ])
            ->build();
    }

    /**
     * Search a listing of the resource (used primarily for the select2).
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        if ($id = $request->input('id')) {
            return response()->json([
                'status' => true,
                'message' => 'Result...',
                'data' => [
                    'category' => Category::approved()
                        ->with([
                            'properties' => function ($query) {
                                $query->where('status', true)->with('values')->orderBy('name');
                            },
                        ])
                        ->where('id', $id)
                        ->orWhere('name', $id)
                        ->first(),
                ]
            ]);
        }

        $categories = Category::approved()
            ->where('name', 'LIKE', "%{$request->term}%")
            ->with([
                'properties' => function ($query) {
                    $query->where('status', true)->with('values')->orderBy('name');
                },
            ])
            ->orderBy('name')
            ->get();

        if ($request->ajax()) {
            return response()->json([
                'status' => true,
                'message' => "Result...",
                'data' => [
                    'categories' => $categories
                ]
            ]);
        }

        return $categories;
    }
}
