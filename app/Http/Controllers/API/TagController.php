<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ResponseBuilder
     */
    public function index()
    {
        $tags = Tag::all();

        return ResponseBuilder::asSuccess()
            ->withData([
                'tags' => $tags
            ])
            ->build();
    }

    /**
     * Search a listing of the resource (used primarily for the select2).
     *
     * @param Request $request
     * @return ResponseBuilder
     */
    public function search(Request $request)
    {
        if ($id = $request->input('id')) {
            return ResponseBuilder::asSuccess()
                ->withData([
                    'tag' => Tag::where('id', $id)
                        ->orWhere('name', $id)
                        ->first(),
                ])
                ->build();
        }

        $tags = Tag::where('name', 'LIKE', "%{$request->term}%")
            ->orderBy('name')
            ->get();

        return ResponseBuilder::asSuccess()
            ->withData([
                'tags' => $tags,
            ])
            ->build();
    }
}
