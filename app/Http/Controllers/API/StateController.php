<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use Illuminate\Http\Request;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class StateController extends Controller
{
    /**
     * Search city of the resource (used primarily for the select2).
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        if ($id = $request->input('id')) {
            return ResponseBuilder::asSuccess()
                ->withData([
                    'state' => State::approved()
                        ->select(['id', 'name'])
                        ->addSelect([
                            'country' => Country::select('name')
                                ->whereColumn('states.country_id', 'countries.id')
                                ->limit(1)
                        ])
                        ->find($id),
                ])
                ->build();
        }

        $states = State::approved()
            ->select(['id', 'name'])
            ->addSelect([
                'country' => Country::select('name')
                    ->whereColumn('states.country_id', 'countries.id')
                    ->limit(1)
            ])
            ->where('name', 'LIKE', "%{$request->term}%")
            ->orderBy('name')
            ->get();

        return ResponseBuilder::asSuccess()
            ->withData([
                'states' => $states,
            ])
            ->build();
    }
}
