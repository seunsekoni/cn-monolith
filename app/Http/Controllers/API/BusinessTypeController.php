<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\BusinessType;
use Illuminate\Http\Request;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class BusinessTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ResponseBuilder
     */
    public function index()
    {
        $businessTypes = BusinessType::approved()->get();

        return ResponseBuilder::asSuccess()
            ->withData([
                'business_types' => $businessTypes
            ])
            ->build();
    }

    /**
     * Search a listing of the resource (used primarily for the select2).
     *
     * @param Request $request
     * @return ResponseBuilder
     */
    public function search(Request $request)
    {
        if ($id = $request->input('id')) {
            return ResponseBuilder::asSuccess()
                ->withData([
                    'business_type' => BusinessType::approved()
                        ->where('id', $id)
                        ->orWhere('name', $id)
                        ->first(),
                ])
                ->build();
        }

        $businessTypes = BusinessType::approved()
            ->where('name', 'LIKE', "%{$request->term}%")
            ->orderBy('name')
            ->get();

        return ResponseBuilder::asSuccess()
            ->withData([
                'business_types' => $businessTypes,
            ])
            ->build();
    }
}
