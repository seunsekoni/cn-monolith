<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\City;
use Illuminate\Http\Request;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ResponseBuilder
     */
    public function index()
    {
        $cities = City::approved()->get();

        return ResponseBuilder::asSuccess()
            ->withData([
                'cities' => $cities
            ])
            ->build();
    }

    /**
     * Search a city of the resource (used primarily for the select2).
     *
     * @param Request $request
     * @return ResponseBuilder
     */
    public function search(Request $request)
    {
        if ($id = $request->input('id')) {
            return ResponseBuilder::asSuccess()
                ->withData([
                    'city' => City::approved()
                        ->where('id', $id)
                        ->orWhere('name', $id)
                        ->first(),
                ])
                ->build();
        }

        $cities = City::approved()
            ->where('name', 'LIKE', "%{$request->term}%")
            ->orderBy('name')
            ->get();

        return ResponseBuilder::asSuccess()
            ->withData([
                'cities' => $cities,
            ])
            ->build();
    }
}
