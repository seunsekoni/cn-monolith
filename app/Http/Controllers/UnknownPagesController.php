<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UnknownPagesController extends Controller
{
    public function addUsers()
    {
        return $this->viewScaffold('Add New User', 'Creation Of New Admin Users');
    }

    public function viewUsers()
    {
        return $this->viewScaffold('View Registered Users', 'See All Available Admin User');
    }

    public function pages()
    {
        return $this->viewScaffold('Available Pages', 'View And Create All Available Pages Within the Application');
    }

    public function settings()
    {
        return $this->viewScaffold('Available Settings', 'Set the configurations of certain Pages');
    }

    public function applicationIssues()
    {
        return $this->viewScaffold('Application Settings', 'All highlighted user reports across the application');
    }

    public function documentation()
    {
        return $this->viewScaffold('Documentation', 'All docs that archives the application process flow');
    }

    /**
     * this method is an attempt to maintain a DRY
     * watch for all method within the class
     * @param $title
     * @param $description
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function viewScaffold($title, $description)
    {
        $detail = ['title' => $title, 'description' => $description];
        return view('admin.pages.coming-soon', $detail);
    }
}
