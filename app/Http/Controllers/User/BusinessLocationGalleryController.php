<?php

namespace App\Http\Controllers\User;

use App\Enums\GalleryType;
use App\Enums\MediaCollection;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\BusinessLocation\StoreGalleryRequest;
use App\Models\BusinessLocation;
use App\Models\Gallery;
use Illuminate\Http\Request;

class BusinessLocationGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param BusinessLocation $location
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(BusinessLocation $location)
    {
        $gallery = $location->galleries->load('media');

        return response()->json([
            'status' => true,
            'message' => 'Business location gallery fetched successfully.',
            'data' => [
                'gallery' => $gallery
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreGalleryRequest $request
     * @param \App\Models\BusinessLocation $location
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreGalleryRequest $request, BusinessLocation $location)
    {
        $gallery = new Gallery();
        $gallery->gallerable()->associate($location);

        if ($url = $request->url) {
            $gallery->type = GalleryType::LINK;
            $gallery->url = $url;
        } else {
            $gallery->type = GalleryType::MEDIA;
            $gallery->addAllMediaFromRequest()
                ->each(function ($fileAdder) {
                    $fileAdder->toMediaCollection(MediaCollection::PHOTOS);
                });
        }

        $gallery->save();

        return response()->json([
            'status' => true,
            'message' => 'Request was successful.',
            'data' => [
                'type' => $gallery->type
            ],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\BusinessLocation $location
     * @param \App\Models\Gallery
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(BusinessLocation $location, Gallery $gallery)
    {
        $gallery->forceDelete();

        return response()->json([
            'status' => true,
            'message' => 'Gallery deleted successfully.',
            'data' => null
        ]);
    }
}
