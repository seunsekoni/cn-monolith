<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BusinessHourController extends Controller
{
    public function index()
    {
        $business_hours = collect(json_decode(auth()->user()->business->business_hours));

        return view('user.pages.business_hours', compact('business_hours'));
    }

    public function store(Request $request)
    {
        if ($request->ajax() && $request->business_hours) {
            $request->user()->business->business_hours = $request->business_hours;
            $request->user()->business->save();

            return response()->json([
            "message" => "Business Hour Updated Successfully"
            ]);
        }
    }
}
