<?php

namespace App\Http\Controllers\User;

use App\Enums\MediaCollection;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Business\StoreRequest;
use App\Http\Requests\User\Business\UpdateRequest;
use App\Models\Business;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BusinessController extends Controller
{
    /**
     * Get the authenticated user's business.
     *
     * @return Business
     */
    protected function business()
    {
        return auth()->user()->business;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.pages.business.index', ['business' => $this->business()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.pages.business.create');
    }

    /**
     * Save the authenticated user's business.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();

        $business = new Business();
        $business->businessType()->associate($request->business_type_id);
        $business->name = $request->name;
        $business->profile = $request->profile;
        $business->changed_logo = $request->logo ? true : false;
        $business->save();

        $user = User::find(auth()->user()->id);
        $user->business_id = $business->id;
        $user->save();

        // Store the logo
        if ($request->logo) {
            $business->addMediaFromRequest('logo')->toMediaCollection(MediaCollection::LOGO);
        }

        // Take care of business tags
        if ($requestTags = $request->tags) {
            $tags = collect();
            foreach ($requestTags as $requestTag) {
                $tag = Tag::firstOrCreate([
                    'name' => $requestTag
                ]);
                $tags->push($tag);
            }
            $business->tags()->sync($tags->pluck('id'));
        }

        DB::commit();

        $request->session()->flash('success', 'Business created successfully!');

        return redirect()->route('user.dashboard');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit()
    {
        $business = $this->business();
        $business->tags = $business->tags->map(fn($tag) => $tag->name);

        return view('user.pages.business.edit', [
            'business' => $business,
        ]);
    }

    /**
     * Update the user's business.
     *
     * @param Business $business
     * @param Request $request
     * @return RedirectResponse
     *
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     */
    public function update(UpdateRequest $request)
    {
        $business = $this->business();

        DB::beginTransaction();

        $business->businessType()->associate($request->business_type_id);
        $business->name = $request->name;
        $business->profile = $request->profile;
        $business->changed_logo = $request->logo ? true : false;
        $business->update();

        // Store the logo
        if ($request->logo) {
            $business->addMediaFromRequest('logo')->toMediaCollection(MediaCollection::LOGO);
        }

        if ($requestTags = $request->tags) {
            $tags = collect();
            foreach ($requestTags as $requestTag) {
                $tag = Tag::firstOrCreate([
                    'name' => $requestTag
                ]);
                $tags->push($tag);
            }
            $business->tags()->sync($tags->pluck('id'));
        }

        DB::commit();

        $request->session()->flash('success', 'Business updated successfully!');

        return redirect()->route('user.business.edit');
    }

    /**
     * Update the primary location for user's business.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function setPrimaryLocation(Request $request)
    {
        $business = $request->user()->business;

        $locations = collect($business->businessLocations()->select('id')->get())->implode('id', ',');

        $request->validate([
            'location' => "required|in:{$locations}",
        ]);

        $business->primaryLocation()->associate($request->location);
        $business->save();

        $request->session()->flash('status', 'Business primary address updated successfully!');

        return redirect()->route('user.business.locations.index');
    }
}
