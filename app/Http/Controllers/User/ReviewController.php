<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * Show all user reviews.
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        return view('user.pages.reviews.index', [
            'reviews' => auth()->user()->reviews()->latest()->get(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Review $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        return view('user.pages.reviews.show', [
            'review' => $review,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        return view('user.pages.reviews.edit', [
            'review' => $review,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Review $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        abort_if($review->approved, 403);

        $review->comment = $request->body;
        $review->approved = (bool) $request->approved ? true : false;
        $review->save();

        $request->session()->flash('status', 'Review updated successfully.');

        return redirect()->route('user.reviews.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Review $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Review $review)
    {
        abort_if($review->approved, 403);

        $review->delete();

        $request->session()->flash('status', 'Review deleted successfully!');

        return redirect()->route('user.reviews.index');
    }
}
