<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\BusinessContact\StoreRequest;
use App\Models\Business;
use App\Models\BusinessContact;
use Illuminate\Support\Facades\DB;

class BusinessContactController extends Controller
{
    /**
     * Get the authenticated user's business.
     *
     * @return Business
     */
    protected function business()
    {
        return auth()->user()->business;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.pages.business.relations.contact', [
            'business' => $this->business(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();

        BusinessContact::updateOrCreate(
            ['business_id' => $this->business()->id],
            [
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'facebook' => $request->facebook,
                'twitter' => $request->twitter,
                'instagram' => $request->instagram,
                'yookos' => $request->yookos,
                'linkedin' => $request->linkedin,
                'tiktok' => $request->tiktok,
            ]
        );

        DB::commit();

        $request->session()->flash('status', 'Business contact updated successfully!');

        return redirect()->route('user.business.contact.index');
    }
}
