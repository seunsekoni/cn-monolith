<?php

namespace App\Http\Controllers\User;

use App\Enums\GalleryType;
use App\Enums\MediaCollection;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Listing\StoreGalleryRequest;
use App\Models\BusinessListing;
use App\Models\Gallery;

class BusinessListingGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param BusinessListing $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(BusinessListing $listing)
    {
        $gallery = $listing->galleries->load('media');

        return response()->json([
            'status' => true,
            'message' => 'Business listing gallery fetched successfully.',
            'data' => [
                'gallery' => $gallery
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreGalleryRequest $request
     * @param \App\Models\BusinessListing $userListing
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreGalleryRequest $request, BusinessListing $listing)
    {
        $gallery = new Gallery();
        $gallery->gallerable()->associate($listing);

        if ($url = $request->url) {
            $gallery->type = GalleryType::LINK;
            $gallery->url = $url;
        } else {
            $gallery->type = GalleryType::MEDIA;
            $gallery->addAllMediaFromRequest()
                ->each(function ($fileAdder) {
                    $fileAdder->toMediaCollection(MediaCollection::PHOTOS);
                });
        }

        $gallery->save();

        return response()->json([
            'status' => true,
            'message' => 'Request was successful.',
            'data' => [
                'type' => $gallery->type
            ],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\BusinessListing $listing
     * @param \App\Models\Gallery
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(BusinessListing $listing, Gallery $gallery)
    {
        $gallery->forceDelete();

        return response()->json([
            'status' => true,
            'message' => 'Gallery deleted successfully.',
            'data' => null
        ]);
    }
}
