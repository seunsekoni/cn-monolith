<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\BusinessLocation\StoreRequest;
use App\Http\Requests\User\BusinessLocation\UpdateRequest;
use App\Models\BusinessLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BusinessLocationController extends Controller
{
    /**
     * Get the authenticated user's business.
     *
     * @return \App\Models\Businesses\Business
     */
    protected function business()
    {
        return auth()->user()->business;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.pages.business.relations.location', [
            'business' => $this->business()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();

        $location = new BusinessLocation();
        $location->business()->associate($this->business());
        $location->city()->associate($request->city_id);
        $location->street_address = $request->street_address;
        $location->address_landmark = $request->address_landmark;
        $location->phone_1 = $request->phone_1;
        $location->phone_2 = $request->phone_2;
        $location->website_url = $request->website_url;
        $location->enable_sms = $request->enable_sms ? true : false;
        $location->sms_number = $request->sms_number;
        $location->enable_feedback = $request->enable_feedback ? true : false;
        $location->feedback_email = $request->feedback_email;
        $location->status = $request->status ? true : false;
        $location->save();

        DB::commit();

        $request->session()->flash('status', 'Business location created successfully!');

        return redirect()->route('user.business.locations.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param BusinessLocation $location
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, BusinessLocation $location)
    {
        DB::beginTransaction();

        $location->city()->associate($request->city_id);
        $location->street_address = $request->street_address;
        $location->address_landmark = $request->address_landmark;
        $location->phone_1 = $request->phone_1;
        $location->phone_2 = $request->phone_2;
        $location->website_url = $request->website_url;
        $location->enable_sms = $request->enable_sms ? true : false;
        $location->sms_number = $request->sms_number;
        $location->enable_feedback = $request->enable_feedback ? true : false;
        $location->feedback_email = $request->feedback_email;
        $location->status = $request->status ? true : false;
        $location->save();

        DB::commit();

        $request->session()->flash('status', 'Business location updated successfully!');

        return redirect()->route('user.business.locations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param BusinessLocation $location
     * @return void
     */
    public function destroy(Request $request, BusinessLocation $location)
    {
        $location->delete();

        $request->session()->flash('status', 'Business location deleted successfully!');

        return redirect()->route('user.business.locations.index');
    }
}
