<?php

namespace App\Http\Controllers\User;

use App\Enums\SupportTicketStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Support\StoreRequest;
use App\Models\SupportTicket;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class SupportTicketController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('user.pages.support-tickets.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        $user = auth()->user();

        return $this->dataTables->of($user->supportTickets()->where('parent_id', null)->latest()->get())
            ->addIndexColumn()
            ->addColumn('subject', function ($support) {
                return truncate($support->subject, 50);
            })
            ->editColumn('message', function ($support) {
                $body = truncate($support->message);
                $body .= "<br>";
                $body .= (
                ($count = $support->replies()->count())
                    ? "<span class='text-info' style='font-size: 10px;'>
                            >> {$count} "
                    . Str::of('reply')->plural($count) .
                    "</span>"
                    : ''
                );
                return $body;
            })
            ->addColumn('show_support_url', function ($support) {
                return route('user.support-tickets.show', ['support_ticket' => $support->id]);
            })
            ->addColumn('edit_support_url', function ($support) {
                return route('user.support-tickets.edit', ['support_ticket' => $support->id]);
            })
            ->addColumn('delete_support_url', function ($support) {
                return route('user.support-tickets.destroy', ['support_ticket' => $support->id]);
            })
            ->rawColumns(['message'])
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.pages.support-tickets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\User\SupportTicket\StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $support = new SupportTicket();

        // Save support ticket into DB.
        $support->subject = $request->subject;
        $support->message = $request->message;
        $support->parent_id = $request->parent;
        $support->status = $request->status ? $request->status : SupportTicketStatus::OPEN;
        $support->supportable()->associate(auth()->user());

        $support->save();

        session()->flash('status', 'Successfully added ticket');
        return redirect()->route('user.support-tickets.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Support $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function show(SupportTicket $supportTicket)
    {
        return view('user.pages.support-tickets.show', [
            'support' => $supportTicket
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\SupportTicket $support
     * @return \Illuminate\Http\Response
     */
    public function edit(supportTicket $supportTicket)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\SupportTicket $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupportTicket $supportTicket)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\SupportTicket $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupportTicket $supportTicket)
    {
        $supportTicket->delete();

        session()->flash('status', 'Successfully deleted ticket.');

        return redirect()->route('user.support-tickets.index');
    }
}
