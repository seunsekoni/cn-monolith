<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Show all user comments.
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        return view('user.pages.comments.index', [
            'comments' => auth()->user()->comments()->latest()->get(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return view('user.pages.comments.show', [
            'comment' => $comment,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        return view('user.pages.comments.edit', [
            'comment' => $comment,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        abort_if($comment->approved, 403);

        $comment->body = $request->body;
        $comment->approved = (bool) $request->approved ? true : false;
        $comment->save();

        $request->session()->flash('status', 'Comment updated successfully.');

        return redirect()->route('user.comments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Comment $comment)
    {
        abort_if($comment->approved, 403);

        $comment->delete();

        $request->session()->flash('status', 'Comment deleted successfully!');

        return redirect()->route('user.comments.index');
    }
}
