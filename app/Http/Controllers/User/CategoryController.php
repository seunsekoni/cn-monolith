<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Search a listing of the resource (used primarily for the select2).
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        if ($id = $request->input('id')) {
            return response()->json([
                'status' => true,
                'message' => "Result...",
                'data' => [
                    'category' => Category::approved()
                        ->with([
                            'properties' => function ($query) {
                                $query->where('status', true)->with('values')->orderBy('name');
                            },
                        ])
                        ->find($id),
                ]
            ]);
        }

        $categories = Category::approved()
            ->where('name', 'LIKE', "%{$request->term}%")
            ->with([
                'properties' => function ($query) {
                    $query->where('status', true)->with('values')->orderBy('name');
                },
            ])
            ->orderBy('name')
            ->get();

        if ($request->ajax()) {
            return response()->json([
                'status' => true,
                'message' => "Result...",
                'data' => [
                    'categories' => $categories
                ]
            ]);
        }

        return $categories;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
    }
}
