<?php

namespace App\Http\Controllers\User;

use App\Enums\GalleryType;
use App\Enums\MediaCollection;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreGalleryRequest;
use App\Models\Gallery;

class BusinessGalleryController extends Controller
{
    /**
     * Get the authenticated user's business.
     *
     * @return \App\Models\Businesses\Business
     */
    protected function business()
    {
        return auth()->user()->business;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $gallery = $this->business()->galleries->load('media');

        return response()->json([
            'status' => true,
            'message' => 'Business gallery fetched successfully.',
            'data' => [
                'gallery' => $gallery
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreGalleryRequest $request
     * @param \App\Models\Business $business
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreGalleryRequest $request)
    {
        $gallery = new Gallery();
        $gallery->gallerable()->associate($this->business());

        if ($url = $request->url) {
            $gallery->type = GalleryType::LINK;
            $gallery->url = $url;
        } else {
            $gallery->type = GalleryType::MEDIA;
            $gallery->addAllMediaFromRequest()
                ->each(function ($fileAdder) {
                    $fileAdder->toMediaCollection(MediaCollection::PHOTOS);
                });
        }

        $gallery->save();

        return response()->json([
            'status' => true,
            'message' => 'Request was successful.',
            'data' => [
                'type' => $gallery->type
            ],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Gallery
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Gallery $gallery)
    {
        $gallery->forceDelete();

        return response()->json([
            'status' => true,
            'message' => 'Gallery deleted successfully.',
            'data' => null
        ]);
    }
}
