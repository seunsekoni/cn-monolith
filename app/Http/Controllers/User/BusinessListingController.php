<?php

namespace App\Http\Controllers\User;

use App\Enums\MediaCollection;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Listing\StoreRequest;
use App\Http\Requests\User\Listing\UpdateRequest;
use App\Models\CategoryProperty;
use App\Models\DiscountType;
use App\Models\BusinessListing;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class BusinessListingController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Get the authenticated user's business.
     *
     * @return \App\Models\Business
     */
    protected function business()
    {
        return auth()->user()->business;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('user.pages.business.listings.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        $business = $this->business();

        return $this->dataTables->eloquent(BusinessListing::where('business_id', $business->id))
            ->addIndexColumn()
            ->editColumn('description', function (BusinessListing $userListing) {
                return truncate($userListing->description, 50);
            })
            ->addColumn('category_name', function (BusinessListing $listing) {
                return optional($listing->category)->name;
            })
            ->addColumn('listing_gallery_url', function (BusinessListing $listing) {
                return route('user.business.listings.gallery.store', [
                    'listing' => $listing->id,
                ]);
            })
            ->addColumn('edit_listing_url', function (BusinessListing $listing) {
                return route('user.business.listings.edit', [
                    'listing' => $listing->id,
                ]);
            })
            ->addColumn('delete_listing_url', function (BusinessListing $listing) {
                return route('user.business.listings.destroy', [
                    'listing' => $listing->id,
                ]);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function create()
    {
        $discountTypes = DiscountType::orderBy('name')->get();

        return view('user.pages.business.listings.create', [
            'discountTypes' => $discountTypes,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();

        $listing = new BusinessListing();
        $listing->business()->associate($this->business());
        $listing->category()->associate($request->category_id);
        $listing->name = $request->name;
        $listing->description = $request->description;
        $listing->price = $request->price;
        $listing->discount_enabled = $request->discount_enabled ? true : false;

        if ((bool) $listing->discount_enabled) {
            $listing->discountType()->associate($request->discount_type_id);
            $listing->discount_value = $request->discount_value;
            $listing->discount_start = $request->discount_start;
            $listing->discount_end = $request->discount_end;
        }

        $listing->save();

        if ($properties = $request->properties) {
            foreach ($properties as $prop => $value) {
                $property = CategoryProperty::whereName($prop)->firstOrFail();

                $listing->properties()->attach($property->id, [
                    'category_property_value' => $value
                ]);
            }
        }

        // Store the featured image.
        if ($request->featured_image) {
            $listing->addMediaFromRequest('featured_image')->toMediaCollection(MediaCollection::FEATUREDIMAGE);
        }

        // Take care of tags.
        $tags = collect();
        if ($requestTags = $request->tags) {
            foreach ($requestTags as $requestTag) {
                $tag = Tag::firstOrCreate([
                    'name' => $requestTag
                ]);
                $tags->push($tag);
            }
        }
        $listing->tags()->sync($tags->pluck('id'));

        DB::commit();

        $request->session()->flash('status', 'Listing created successfully!');

        return redirect()->route('user.business.listings.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param BusinessListing $userListing
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessListing $listing)
    {
        $discountTypes = DiscountType::orderBy('name')->get();

        $listing->tags = $listing->tags->map(fn($tag) => $tag->name);
        $listing->load('properties');

        return view('user.pages.business.listings.edit', [
            'listing' => $listing,
            'discountTypes' => $discountTypes,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param BusinessListing $userListing
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, BusinessListing $listing)
    {
        DB::beginTransaction();

        $listing->category()->associate($request->category_id);
        $listing->name = $request->name;
        $listing->description = $request->description;
        $listing->price = $request->price;
        $listing->discount_enabled = $request->discount_enabled ? true : false;

        if ((bool) $listing->discount_enabled) {
            $listing->discountType()->associate($request->discount_type_id);
            $listing->discount_value = $request->discount_value;
            $listing->discount_start = $request->discount_start;
            $listing->discount_end = $request->discount_end;
        }

        $listing->update();


        if ($properties = $request->properties) {
            $listing->properties()->detach(); // Remove all existing properties.

            foreach ($properties as $prop => $value) {
                $property = CategoryProperty::whereName($prop)->firstOrFail();

                $listing->properties()->attach($property->id, [
                    'category_property_value' => $value
                ]);
            }
        }

        // Store the featured image.
        if ($request->featured_image) {
            $listing->addMediaFromRequest('featured_image')->toMediaCollection(MediaCollection::FEATUREDIMAGE);
        }

        // Take care of tags.
        $tags = collect();
        $request_tags = $request->tags;
        if ($request_tags) {
            foreach ($request_tags as $requestTag) {
                $tag = Tag::firstOrCreate([
                    'name' => $requestTag
                ]);
                $tags->push($tag);
            }
            $listing->tags()->sync($tags->pluck('id'));
            $listing->tags()->sync($tags->pluck('id'));
        }

        DB::commit();

        $request->session()->flash('status', 'Listing updated successfully!');

        return redirect()->route('user.business.listings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param BusinessListing $userListing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, BusinessListing $listing)
    {
        $listing->delete();

        $request->session()->flash('status', 'Listing deleted successfully!');

        return redirect()->route('user.business.listings.index');
    }
}
