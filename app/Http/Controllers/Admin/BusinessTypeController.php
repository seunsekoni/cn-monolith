<?php

namespace App\Http\Controllers\Admin;

use App\Enums\MediaCollection;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BusinessType\StoreRequest;
use App\Http\Requests\Admin\BusinessType\UpdateRequest;
use App\Models\BusinessType;
use Yajra\DataTables\DataTables;

class BusinessTypeController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.lookups.business-types.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables
            ->eloquent(
                BusinessType::withTrashed()
                    ->orderBy('deleted_at')
            )
            ->addIndexColumn()
            ->addColumn('name', function (BusinessType $businessType) {
                return $businessType->name;
            })
            ->addColumn('description', function (BusinessType $businessType) {
                return truncate($businessType->description);
            })
            ->addColumn('status', function (BusinessType $businessType) {
                return $businessType->status;
            })
            ->addColumn('edit_business_type_url', function (BusinessType $businessType) {
                return route('admin.lookups.business-types.edit', ['business_type' => $businessType]);
            })
            ->addColumn('restore_business_type_url', function (BusinessType $businessType) {
                return route('admin.lookups.business-types.restore', ['business_type' => $businessType->id]);
            })
            ->addColumn('delete_business_type_url', function (BusinessType $businessType) {
                return route('admin.lookups.business-types.destroy', ['business_type' => $businessType->id]);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.lookups.business-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\BusinessType\StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $business_type = new BusinessType();
        $business_type->name = $request->name;
        $business_type->description = $request->description;
        $business_type->status = $request->status ? true : false;
        $business_type->save();

        // Store the backdrop
        if ($request->backdrop) {
            $business_type->addMediaFromRequest('backdrop')->toMediaCollection(MediaCollection::BACKDROP);
        }
        // Store the icon
        if ($request->icon) {
            $business_type->addMediaFromRequest('icon')->toMediaCollection(MediaCollection::ICON);
        }

        session()->flash('status', 'Business Type created successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\BusinessType $businessType
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessType $businessType)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\BusinessType $business_type
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessType $businessType)
    {
        return view('admin.pages.lookups.business-types.edit', [
            'business_type' => $businessType
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\BusinessType\UpdateRequest $request
     * @param \App\Models\BusinessType $businessType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, BusinessType $businessType)
    {
        $businessType->name = $request->name;
        $businessType->description = $request->description;
        $businessType->status = $request->status ? true : false;
        $businessType->save();

        // Store the backdrop
        if ($request->backdrop) {
            $businessType->addMediaFromRequest('backdrop')->toMediaCollection(MediaCollection::BACKDROP);
        }
        // Store the icon
        if ($request->icon) {
            $businessType->addMediaFromRequest('icon')->toMediaCollection(MediaCollection::ICON);
        }

        session()->flash("status", 'Business Type updated successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\BusinessType $businessType
     * @return \Illuminate\Http\Response
     */
    public function destroy(BusinessType $businessType)
    {
        $businessType->delete();

        session()->flash('status', 'Business Type deleted successfully!');

        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $businessType
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($businessType)
    {
        BusinessType::withTrashed()->where('id', $businessType)->restore();

        session()->flash('status', 'Business Type was restored successfully.');

        return redirect()->back();
    }
}
