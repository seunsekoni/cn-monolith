<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Business;
use App\Models\Review;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class BusinessReviewsController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function index(Business $business)
    {
        if (request()->ajax()) {
            return $this->indexAjax($business);
        }

        return view('admin.pages.businesses.relations.reviews', [
            'business' => $business,
            'reviews' => $business->reviews,
        ]);
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @param Business $business
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax(Business $business)
    {
        return $this->dataTables->eloquent($business->reviews()->with('user')->select('reviews.*')->latest())
            ->addIndexColumn()
            ->addColumn('user', function (Review $review) {
                return "
                    {$review->user->full_name}
                    <br>
                    <small class='text-muted'>{$review->user->email}</small>
                ";
            })
            ->editColumn('approved', function (Review $review) use ($business) {
                return "
                    <a
                        href='#'
                        class='btn btn-sm btn-" . ($review->approved ? 'danger' : 'success') . "'
                        onclick='
                            event.preventDefault();
                            document.getElementById(\"update-review{$review->id}-form\").submit();
                        '
                    >
                        " . ($review->approved ? 'unapprove' : 'approve') . "
                    </a>
                    <form
                        id='update-review" . $review->id . "-form'
                        action=" . route('admin.businesses.reviews.update', [
                        'business' => $business->id,
                        'review' => $review->id,
                        'approved' => !$review->approved
                    ]) . "
                        method='POST'
                    >
                        <input type='hidden' name='_method' value='PUT'>
                        " . csrf_field() . "
                    </form>
                ";
            })
            ->addColumn('edit_review_url', function (Review $review) {
                return route('admin.reviews.edit', [
                    'review' => $review->id,
                ]);
            })
            ->addColumn('delete_review_url', function (Review $review) use ($business) {
                return route('admin.businesses.reviews.update', [
                    'business' => $business->id,
                    'review' => $review->id,
                ]);
            })
            ->rawColumns(['user', 'approved'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function create(Business $business)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Business $business)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param Business $business
     * @param Review $review
     * @return \Illuminate\Http\Response
     */
    public function show(Business $business, Review $review)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Business $business
     * @param Review $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business, Review $review)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Business $business
     * @param Review $review
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Business $business, Review $review)
    {
        $request->validate([
            'comment' => 'sometimes|string',
            'approved' => 'sometimes|boolean'
        ]);

        $review->comment = $request->comment ?: $review->comment;
        $review->approved = (bool) $request->approved ? true : false;
        $review->save();

        $request->session()->flash('status', 'Review updated successfully.');

        return redirect()->route('admin.businesses.reviews.index', [
            'business' => $business->id,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Business $business
     * @param Review $review
     * @return void
     */
    public function destroy(Request $request, Business $business, Review $review)
    {
        $review->delete();

        $request->session()->flash('status', 'Review deleted successfully!');

        return redirect()->route('admin.businesses.reviews.index', [
            'business' => $business->id
        ]);
    }
}
