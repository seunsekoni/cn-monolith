<?php

namespace App\Http\Controllers\Admin;

use App\Enums\MediaCollection;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\StoreRequest;
use App\Http\Requests\Admin\Category\UpdateRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.lookups.categories.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables->of(Category::withTrashed())
            ->addIndexColumn()
            ->editColumn('description', function (Category $category) {
                return truncate($category->description, 50);
            })
            ->addColumn('show_category_url', function ($category) {
                return route('admin.lookups.categories.show', ['category' => $category->id]);
            })
            ->addColumn('edit_category_url', function ($category) {
                return route('admin.lookups.categories.edit', ['category' => $category->id]);
            })
            ->addColumn('restore_category_url', function ($category) {
                return route('admin.lookups.categories.restore', ['category' => $category->id]);
            })
            ->addColumn('delete_category_url', function ($category) {
                return route('admin.lookups.categories.destroy', ['category' => $category->id]);
            })
            ->make();
    }

    /**
     * Search a listing of the resource (used primarily for the select2).
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        if ($id = $request->input('id')) {
            return response()->json([
                'status' => true,
                'message' => "Result...",
                'data' => [
                    'category' => Category::approved()
                        ->with([
                            'properties' => function ($query) {
                                $query->where('status', true)->with('values')->orderBy('name');
                            },
                        ])
                        ->find($id),
                ]
            ]);
        }

        $categories = Category::approved()
            ->where('name', 'LIKE', "%{$request->term}%")
            ->with([
                'properties' => function ($query) {
                    $query->where('status', true)->with('values')->orderBy('name');
                },
            ])
            ->orderBy('name')
            ->get();

        if ($request->ajax()) {
            return response()->json([
                'status' => true,
                'message' => "Result...",
                'data' => [
                    'categories' => $categories
                ]
            ]);
        }

        return $categories;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.lookups.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $category = Category::create([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status ? true : false,
        ]);

        // Store the backdrop
        if ($request->backdrop) {
            $category->addMediaFromRequest('backdrop')->toMediaCollection(MediaCollection::BACKDROP);
        }

        // Store the icon.
        if ($request->icon) {
            $category->addMediaFromRequest('icon')->toMediaCollection(MediaCollection::ICON);
        }

        // Store the white icon.
        if ($request->white_icon) {
            $category->addMediaFromRequest('white_icon')->toMediaCollection(MediaCollection::WHITEICON);
        }

        session()->flash('status', 'Category added successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Category $category)
    {
        return view('admin.pages.lookups.categories.show', [
            'category' => $category
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.pages.lookups.categories.edit', [
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Category $category)
    {
        $category->update([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status ? true : false,
        ]);

        // Store the backdrop
        if ($request->backdrop) {
            $category->addMediaFromRequest('backdrop')->toMediaCollection(MediaCollection::BACKDROP);
        }

        // Store the icon.
        if ($request->icon) {
            $category->addMediaFromRequest('icon')->toMediaCollection(MediaCollection::ICON);
        }

        // Store the white icon.
        if ($request->white_icon) {
            $category->addMediaFromRequest('white_icon')->toMediaCollection(MediaCollection::WHITEICON);
        }

        session()->flash('status', 'Category updated successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Category $category)
    {
        $category->delete();

        session()->flash('status', 'Category deleted successfully!');

        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($category)
    {
        Category::withTrashed()->where('id', $category)->restore();

        session()->flash('status', 'Category was restored successfully.');

        return redirect()->back();
    }
}
