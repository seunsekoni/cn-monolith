<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryProperty;
use App\Models\CategoryPropertyValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryPropertyValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CategoryProperty $property
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Category $category, CategoryProperty $property)
    {
        $propertyValues = $property->values()->latest()->get();

        return response()->json([
            'status' => true,
            'message' => 'Property values fetched successfully.',
            'data' => [
                'propertyValues' => $propertyValues
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Category $category
     * @param CategoryProperty $property
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Category $category, CategoryProperty $property)
    {
        $request->validate([
            'name' => 'required|string',
        ]);

        $propertyValue = new CategoryPropertyValue();
        $propertyValue->property()->associate($property);
        $propertyValue->name = $request->name;
        $propertyValue->save();

        return response()->json([
            'status' => true,
            'message' => 'Property value created successfully.',
            'data' => [
                'propertyValue' => $propertyValue
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\CategoryPropertyValue $categoryPropertyValue
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryPropertyValue $categoryPropertyValue)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\CategoryPropertyValue $categoryPropertyValue
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryPropertyValue $categoryPropertyValue)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param CategoryPropertyValue $value
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoryPropertyValue $value)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @param CategoryProperty $property
     * @param CategoryPropertyValue $categoryPropertyValue
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Category $category, CategoryProperty $property, CategoryPropertyValue $value)
    {
        DB::beginTransaction();

        $value->property->businessListings()->wherePivot('category_property_value', $value->id)->detach();
        $value->delete();

        DB::commit();

        return response()->json([
            'status' => true,
            'message' => 'Property value deleted successfully.',
            'data' => null,
        ]);
    }
}
