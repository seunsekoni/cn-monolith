<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Business;
use App\Models\BusinessListing;
use Illuminate\Http\Request;

class BusinessListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function index(Business $business)
    {
        $listings = $business->businessListings()->latest()->paginate(3);

        return view('admin.pages.businesses.relations.listing', [
            'business' => $business,
            'listings' => $listings,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function create(Business $business)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Business $business)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param Business $business
     * @param BusinessListing $listing
     * @return \Illuminate\Http\Response
     */
    public function show(Business $business, BusinessListing $listing)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Business $business
     * @param BusinessListing $listing
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business, BusinessListing $listing)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Business $business
     * @param BusinessListing $listing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Business $business, BusinessListing $listing)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Business $business
     * @param BusinessListing $listing
     * @return void
     */
    public function destroy(Request $request, Business $business, BusinessListing $listing)
    {
        $listing->delete();

        $request->session()->flash('status', 'Business listing deleted successfully!');

        return redirect()->route('admin.businesses.listings.index', [
            'business' => $business
        ]);
    }
}
