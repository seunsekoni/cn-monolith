<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Country\StoreRequest;
use App\Http\Requests\Admin\Country\UpdateRequest;
use App\Models\Country;
use App\Models\Region;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CountryController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.lookups.countries.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables
            ->eloquent(
                Country::withTrashed()
                    ->orderBy('deleted_at')
            )
            ->addIndexColumn()
            ->addColumn('edit_country_url', function (Country $country) {
                return route('admin.lookups.countries.edit', ['country' => $country]);
            })
            ->addColumn('restore_country_url', function (Country $country) {
                return route('admin.lookups.countries.restore', ['country' => $country]);
            })
            ->addColumn('delete_country_url', function (Country $country) {
                return route('admin.lookups.countries.destroy', ['country' => $country->id]);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.lookups.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        Country::create([
            'region_id' => $request->region_id,
            'name' => $request->name,
            'code' => strtoupper($request->code),
            'currency' => strtoupper($request->currency),
            'phone_code' => $request->phone_code,
            'status' => $request->status ? true : false
        ]);

        $request->session()->flash('status', 'Country created successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Country $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        return view('admin.pages.lookups.countries.show', [
            'country' => $country
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Country $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        return view('admin.pages.lookups.countries.edit', [
            'country' => $country,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\UpdateRequest $request
     * @param \App\Models\Admin\Country $country
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Country $country)
    {
        $country->update([
            'region_id' => $request->region_id,
            'name' => $request->name,
            'code' => $request->code,
            'currency' => $request->currency,
            'phone_code' => $request->phone_code,
            'status' => $request->status ? true : false
        ]);

        $request->session()->flash('status', 'Country updated successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Country $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Country $country)
    {
        $country->delete();

        $request->session()->flash('status', 'Country deleted successfully!');

        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $country
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($country)
    {
        Country::withTrashed()->where('id', $country)->restore();

        session()->flash('status', 'Country was restored successfully.');

        return redirect()->back();
    }
}
