<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BusinessLocation\StoreRequest;
use App\Http\Requests\Admin\BusinessLocation\UpdateRequest;
use App\Models\Business;
use App\Models\BusinessLocation;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BusinessLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function index(Business $business)
    {
        $configCities = City::orderBy('name')->get();

        return view('admin.pages.businesses.relations.location', [
            'business' => $business,
            'configCities' => $configCities
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function create(Business $business)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, Business $business)
    {
        DB::beginTransaction();

        $location = new BusinessLocation();
        $location->business()->associate($business);
        $location->city()->associate($request->city_id);
        $location->street_address = $request->street_address;
        $location->address_landmark = $request->address_landmark;
        $location->phone_1 = $request->phone_1;
        $location->phone_2 = $request->phone_2;
        $location->website_url = $request->website_url;
        $location->enable_sms = $request->enable_sms ? true : false;
        $location->sms_number = $request->sms_number;
        $location->enable_feedback = $request->enable_feedback ? true : false;
        $location->feedback_email = $request->feedback_email;
        $location->status = $request->status ? true : false;
        $location->save();

        DB::commit();

        $request->session()->flash('status', 'Business location created successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param Business $business
     * @param BusinessLocation $location
     * @return \Illuminate\Http\Response
     */
    public function show(Business $business, BusinessLocation $location)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Business $business
     * @param BusinessLocation $businessLocation
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business, BusinessLocation $location)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Business $business
     * @param BusinessLocation $location
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Business $business, BusinessLocation $location)
    {
        DB::beginTransaction();

        $location->city()->associate($request->city_id);
        $location->street_address = $request->street_address;
        $location->address_landmark = $request->address_landmark;
        $location->phone_1 = $request->phone_1;
        $location->phone_2 = $request->phone_2;
        $location->website_url = $request->website_url;
        $location->enable_sms = $request->enable_sms ? true : false;
        $location->sms_number = $request->sms_number;
        $location->enable_feedback = $request->enable_feedback ? true : false;
        $location->feedback_email = $request->feedback_email;
        $location->status = $request->status ? true : false;
        $location->save();

        DB::commit();

        $request->session()->flash('status', 'Business location updated successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Business $business
     * @param BusinessLocation $location
     * @return void
     */
    public function destroy(Request $request, Business $business, BusinessLocation $location)
    {
        $location->delete();

        $request->session()->flash('status', 'Business location deleted successfully!');

        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $location
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($location)
    {
        BusinessLocation::withTrashed()->where('id', $location)->restore();

        session()->flash('status', 'Business Location was restored successfully.');

        return redirect()->back();
    }
}
