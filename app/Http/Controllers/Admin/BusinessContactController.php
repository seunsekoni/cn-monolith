<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BusinessContact\StoreRequest;
use App\Models\Business;
use App\Models\BusinessContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BusinessContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function index(Business $business)
    {
        return view('admin.pages.businesses.relations.contact', [
            'business' => $business,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function create(Business $business)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, Business $business)
    {
        DB::beginTransaction();

        BusinessContact::updateOrCreate(
            ['business_id' => $business->id],
            [
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'facebook' => $request->facebook,
                'twitter' => $request->twitter,
                'instagram' => $request->instagram,
                'yookos' => $request->yookos,
                'linkedin' => $request->linkedin,
                'tiktok' => $request->tiktok,
            ]
        );

        DB::commit();

        $request->session()->flash('status', 'Business contact updated successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param Business $business
     * @param BusinessContact $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Business $business, BusinessContact $contact)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Business $business
     * @param BusinessContact $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business, BusinessContact $contact)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Business $business
     * @param BusinessContact $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Business $business, BusinessContact $contact)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Business $business
     * @param BusinessContact $contact
     * @return void
     */
    public function destroy(Business $business, BusinessContact $contact)
    {
    }
}
