<?php

namespace App\Http\Controllers\Admin;

use App\Models\Business;
use App\Models\BusinessListing;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin.pages.dashboard', [
            'business' => $this->fetchBusinessesStat(),
            'listing' => $this->fetchBusinessListingsStat(),
            'chart' => $this->fetchDailyConfirmedBusinessChart(),
            'without' => $this->fetchBusinessWithout()
        ]);
    }

    /**
     * Get the businesses statistics.
     *
     * @return array
     */
    public function fetchBusinessesStat(): array
    {
        $total_business = Business::count();
        $unconfirmed_business = Business::whereVerified(false)->count();
        $confirmed_business = Business::whereVerified(true)->count();

        return [
            'total_business' => $total_business,
            'unconfirmed_business' => $unconfirmed_business,
            'confirmed_business' => $confirmed_business,
        ];
    }

    /**
     * Get the business listings statistics.
     *
     * @return array
     */
    public function fetchBusinessListingsStat(): array
    {
        $total_listing = BusinessListing::count();
        $unconfirmed_listing = BusinessListing::whereStatus(false)->count();
        $confirmed_listing = BusinessListing::whereStatus(true)->count();

        return [
            'total_listing' => $total_listing,
            'unconfirmed_listing' => $unconfirmed_listing,
            'confirmed_listing' => $confirmed_listing
        ];
    }

    /**
     * Get the daily confirmed businesses chart data.
     *
     * @return array
     */
    public function fetchDailyConfirmedBusinessChart(): array
    {
        $no_of_confirmed_bus_per_day = $this->monthDayStat('d')->map(function ($conf_bus) {
            return $conf_bus->count();
        });

        return [
            'current_month_days' => array_keys($this->monthDayStat('d')->toArray()),
            'no_of_confirmed_bus_per_day' => array_values($no_of_confirmed_bus_per_day->toArray()),
            'current_month' => array_keys($this->monthDayStat('M')->toArray()) ? array_keys($this->monthDayStat('M')->toArray())[0] : null
        ];
    }

    /**
     * Get the month data analytics.
     *
     * @param string $format
     * @return mixed
     */
    public function monthDayStat(string $format)
    {
        return Business::whereVerified(true)
            ->whereMonth('verified_at', Carbon::now()->month)->orderBy('verified_at', 'asc')
            ->get()->groupBy(fn($date) => Carbon::parse($date->verified_at)->format($format));
    }

    /**
     * Fetch the businesses without specific requirements.
     *
     * @return array
     * @todo Fix whatever is causing this method to max out memory limit
     */
    public function fetchBusinessWithout(): array
    {
        return [
            'without_email' => $this->filterBusinessWithoutCertainContact('email')->count(),
            'without_phone' => $this->filterBusinessWithoutCertainContact('phone')->count(),
//            'without_website' => $this->filterBusinessWithoutCertainLocation('website_url')->count() // pulling this out cos it is not optimized
        ];
    }

    /**
     * Filter businesses without the specified contact information.
     *
     * @param string $field
     * @return \Illuminate\Support\Collection
     */
    public function filterBusinessWithoutCertainContact(string $field)
    {
        $business = Business::with(['businessContact'])->get();

        return $business->filter(function ($business) use ($field) {
            return $business->businessContact === null || $business->businessContact->{$field} === null;
        });
    }

    /**
     * Filter businesses without the specified location information.
     *
     * @param string $field
     * @return \Illuminate\Support\Collection
     */
    public function filterBusinessWithoutCertainLocation(string $field)
    {
        $hasLocationButNoField = null;
        $hasNoLocation = null;

        $totalHasNoField = [];
        $business = Business::with(['businessLocations'])->get();

        return $business->map(function ($e) {
            if ($e->businessLocations !== null) {
                $ella = $e->businessLocations->first();
                return $ella;
            }
        })->map(function ($e) use ($field, $hasLocationButNoField, $hasNoLocation, $totalHasNoField) {
            if (($e)) {
                $hasLocationButNoField = $e->{$field} ?? 'no-field';
            } else {
                $hasNoLocation = 'no-loc';
            }

            if ($hasLocationButNoField === 'no-field') {
                array_push($totalHasNoField, $hasLocationButNoField);
            }

            if ($hasNoLocation === "no-loc") {
                array_push($totalHasNoField, $hasNoLocation);
            }
            return $totalHasNoField;
        })->filter(function ($e) {
            return count($e) > 0;
        });
    }
}
