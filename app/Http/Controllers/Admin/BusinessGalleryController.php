<?php

namespace App\Http\Controllers\Admin;

use App\Enums\GalleryType;
use App\Enums\MediaCollection;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Business\StoreGalleryRequest;
use App\Models\Business;
use App\Models\Gallery;
use Illuminate\Http\Request;

class BusinessGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Business $business
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, Business $business)
    {
        $gallery = $business->galleries->load('media');

        return response()->json([
            'status' => true,
            'message' => 'Business gallery fetched successfully.',
            'data' => [
                'gallery' => $gallery
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreGalleryRequest $request
     * @param \App\Models\Business $business
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreGalleryRequest $request, Business $business)
    {
        $gallery = new Gallery();
        $gallery->gallerable()->associate($business);

        if ($url = $request->url) {
            $gallery->type = GalleryType::LINK;
            $gallery->url = $url;
        } else {
            $gallery->type = GalleryType::MEDIA;
            $gallery->addAllMediaFromRequest()
                ->each(function ($fileAdder) {
                    $fileAdder->toMediaCollection(MediaCollection::PHOTOS);
                });
        }

        $gallery->save();

        return response()->json([
            'status' => true,
            'message' => "Request was successful.",
            'data' => [
                'type' => $gallery->type
            ],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Business $business
     * @return \Illuminate\Http\Response
     */
    public function show(Business $business)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Business $business
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Business $business)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Business $business
     * @param \App\Models\Gallery
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Business $business, Gallery $gallery)
    {
        $gallery->forceDelete();

        return response()->json([
            'status' => true,
            'message' => 'Gallery deleted successfully.',
            'data' => null
        ]);
    }
}
