<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class CommentsController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.comments.index', [
            'comments' => Comment::all(),
        ]);
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables
            ->of(
                Comment::whereNull('parent_id')
                    ->orderBy('updated_at', 'desc')
                    ->with(['user', 'commentable'])
            )
            ->addIndexColumn()
            ->addColumn('relation', function (Comment $comment) {
                $model = strtolower(
                    (new \ReflectionClass($comment->commentable_type))->getShortName()
                );

                $route = '';
                if ($model == 'businesslisting') {
                    $model = 'listing';
                    $route = route('admin.businesses.listings.index', [
                        'business' => $comment->commentable->business->id,
                    ]);
                } else {
                    $route = route('admin.businesses.show', [
                        'business' => $comment->commentable_id,
                    ]);
                }

                return "
                    <div class='d-flex justify-content-between'>
                        <a href='{$route}'>{$comment->commentable->name}</a>
                        <div>
                            <span class='badge badge-" . ($model == 'listing' ? 'success' : 'danger') . "'>
                                {$model}
                            </span>
                        </div>
                    </div>
                ";
            })
            ->addColumn('user', function (Comment $comment) {
                return "
                    {$comment->user->full_name}
                    <br>
                    <small class='text-muted'>{$comment->user->email}</small>
                ";
            })
            ->editColumn('body', function (Comment $comment) {
                $body = truncate($comment->body);
                $body .= "<br>";
                $body .= (
                ($count = $comment->replies()->count())
                    ? "<span class='text-info' style='font-size: 10px;'>
                            >> {$count} "
                    . Str::of('reply')->plural($count) .
                    "</span>"
                    : ''
                );
                return $body;
            })
            ->editColumn('approved', function (Comment $comment) {
                return "
                    <a
                        href='#'
                        class='btn btn-sm btn-" . ($comment->approved ? 'danger' : 'success') . "'
                        onclick='
                            event.preventDefault();
                            document.getElementById(\"update-comment{$comment->id}-form\").submit();
                        '
                    >
                        " . ($comment->approved ? 'unapprove' : 'approve') . "
                    </a>
                    <form
                        id='update-comment" . $comment->id . "-form'
                        action=" . route('admin.comments.update', [
                        'comment' => $comment->id,
                        'approved' => !$comment->approved
                    ]) . "
                        method='POST'
                    >
                        <input type='hidden' name='_method' value='PUT'>
                        " . csrf_field() . "
                    </form>
                ";
            })
            ->addColumn('show_comment_url', function (Comment $comment) {
                return route('admin.comments.show', [
                    'comment' => $comment->id,
                ]);
            })
            ->addColumn('edit_comment_url', function (Comment $comment) {
                return route('admin.comments.edit', [
                    'comment' => $comment->id,
                ]);
            })
            ->addColumn('delete_comment_url', function (Comment $comment) {
                return route('admin.comments.destroy', [
                    'comment' => $comment->id,
                ]);
            })
            ->rawColumns(['relation', 'user', 'body', 'approved'])
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return view('admin.pages.comments.show', [
            'comment' => $comment,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        return view('admin.pages.comments.edit', [
            'comment' => $comment,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        $request->validate([
            'body' => 'sometimes|string',
            'approved' => 'sometimes'
        ]);

        $comment->body = $request->body ?: $comment->body;
        $comment->approved = (bool) $request->approved ? true : false;
        $comment->save();

        $request->session()->flash('status', 'Comment updated successfully.');

        return redirect()->route('admin.comments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Comment $comment)
    {
        $comment->delete();

        $request->session()->flash('status', 'Comment deleted successfully!');

        return redirect()->route('admin.comments.index');
    }
}
