<?php

namespace App\Http\Controllers\Admin;

use App\Enums\GalleryType;
use App\Enums\MediaCollection;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BusinessLocation\StoreGalleryRequest;
use App\Models\BusinessLocation;
use App\Models\Gallery;
use Illuminate\Http\Request;

class BusinessLocationGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param BusinessLocation $location
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, BusinessLocation $location)
    {
        $gallery = $location->galleries->load('media');

        return response()->json([
            'status' => true,
            'message' => 'Business location gallery fetched successfully.',
            'data' => [
                'gallery' => $gallery
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreGalleryRequest $request
     * @param \App\Models\BusinessLocation $location
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreGalleryRequest $request, BusinessLocation $location)
    {
        $gallery = new Gallery();
        $gallery->gallerable()->associate($location);

        if ($url = $request->url) {
            $gallery->type = GalleryType::LINK;
            $gallery->url = $url;
        } else {
            $gallery->type = GalleryType::MEDIA;
            $gallery->addAllMediaFromRequest()
                ->each(function ($fileAdder) {
                    $fileAdder->toMediaCollection(MediaCollection::PHOTOS);
                });
        }

        $gallery->save();

        return response()->json([
            'status' => true,
            'message' => "Request was successful.",
            'data' => [
                'type' => $gallery->type
            ],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\BusinessLocation $businessLocation
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessLocation $businessLocation)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\BusinessLocation $businessLocation
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessLocation $businessLocation)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BusinessLocation $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BusinessLocation $location)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\BusinessLocation $location
     * @param \App\Models\Gallery
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(BusinessLocation $location, Gallery $gallery)
    {
        $gallery->forceDelete();

        return response()->json([
            'status' => true,
            'message' => 'Gallery deleted successfully.',
            'data' => null
        ]);
    }
}
