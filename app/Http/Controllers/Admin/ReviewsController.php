<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class ReviewsController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.reviews.index', [
            'reviews' => Review::all(),
        ]);
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables
            ->of(
                Review::whereNull('parent_id')
                    ->orderBy('updated_at', 'desc')
                    ->with(['user', 'reviewable'])
            )
            ->addIndexColumn()
            ->addColumn('relation', function (Review $review) {
                $model = strtolower(
                    (new \ReflectionClass($review->reviewable_type))->getShortName()
                );

                $route = '';
                if ($model == 'businesslisting') {
                    $model = 'listing';
                    $route = route('admin.businesses.listings.index', [
                        'business' => $review->reviewable->business->id,
                    ]);
                } else {
                    $route = route('admin.businesses.show', [
                        'business' => $review->reviewable_id,
                    ]);
                }

                return "
                    <div class='d-flex justify-content-between'>
                        <a href='{$route}'>{$review->reviewable->name}</a>
                        <div>
                            <span class='badge badge-" . ($model == 'listing' ? 'success' : 'danger') . "'>
                                {$model}
                            </span>
                        </div>
                    </div>
                ";
            })
            ->addColumn('user', function (Review $review) {
                return "
                    {$review->user->full_name}
                    <br>
                    <small class='text-muted'>{$review->user->email}</small>
                ";
            })
            ->editColumn('comment', function (Review $review) {
                $body = truncate($review->comment);
                $body .= "<br>";
                $body .= (
                ($count = $review->replies()->count())
                    ? "<span class='text-info' style='font-size: 10px;'>
                            >> {$count} "
                    . Str::of('reply')->plural($count) .
                    "</span>"
                    : ''
                );
                return $body;
            })
            ->editColumn('approved', function (Review $review) {
                return "
                    <a
                        href='#'
                        class='btn btn-sm btn-" . ($review->approved ? 'danger' : 'success') . "'
                        onclick='
                            event.preventDefault();
                            document.getElementById(\"update-review{$review->id}-form\").submit();
                        '
                    >
                        " . ($review->approved ? 'unapprove' : 'approve') . "
                    </a>
                    <form
                        id='update-review" . $review->id . "-form'
                        action=" . route('admin.reviews.update', [
                        'review' => $review->id,
                        'approved' => !$review->approved
                    ]) . "
                        method='POST'
                    >
                        <input type='hidden' name='_method' value='PUT'>
                        " . csrf_field() . "
                    </form>
                ";
            })
            ->addColumn('show_review_url', function (Review $review) {
                return route('admin.reviews.show', [
                    'review' => $review->id,
                ]);
            })
            ->addColumn('edit_review_url', function (Review $review) {
                return route('admin.reviews.edit', [
                    'review' => $review->id,
                ]);
            })
            ->addColumn('delete_review_url', function (Review $review) {
                return route('admin.reviews.update', [
                    'review' => $review->id,
                ]);
            })
            ->rawColumns(['relation', 'user', 'comment', 'approved'])
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Review $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        return view('admin.pages.reviews.show', [
            'review' => $review,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Review $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        return view('admin.pages.reviews.edit', [
            'review' => $review,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Review $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        $request->validate([
            'comment' => 'sometimes|string',
            'approved' => 'sometimes'
        ]);

        $review->comment = $request->comment ?: $review->comment;
        $review->approved = (bool) $request->approved ? true : false;
        $review->save();

        $request->session()->flash('status', 'Review updated successfully.');

        return redirect()->route('admin.reviews.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Review $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Review $review)
    {
        $review->delete();

        $request->session()->flash('status', 'Review deleted successfully!');

        return redirect()->route('admin.reviews.index');
    }
}
