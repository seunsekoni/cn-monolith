<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DiscountType\StoreRequest;
use App\Http\Requests\Admin\DiscountType\UpdateRequest;
use App\Models\DiscountType;

class DiscountTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discounts = DiscountType::all();

        return view('admin.pages.lookups.discount-types.index', [
            'discounts' => $discounts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.lookups.discount-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\DiscountType\StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DiscountType::create([
            'name' => $request->name,
            'regex_format' => strtolower($request->regex_format),
            'status' => $request->status ? true : false,
        ]);

        session()->flash('status', 'Discount Type added successfully');

        return redirect()->route('admin.lookups.discount-types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\DiscountType $discountType
     * @return \Illuminate\Http\Response
     */
    public function show(DiscountType $discountType)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\DiscountType $discountType
     * @return \Illuminate\Http\Response
     */
    public function edit(DiscountType $discountType)
    {
        return view('admin.pages.lookups.discount-types.edit', [
            'discount_type' => $discountType
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\DiscountType\UpdateRequest $request
     * @param \App\Models\DiscountType $discountType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, DiscountType $discountType)
    {
        $discountType->update([
            'name' => $request->name,
            'regex_format' => strtolower($request->regex_format),
            'status' => $request->status ? true : false,
        ]);

        session()->flash('status', 'Discount Type updated successfully');

        return redirect()->route('admin.lookups.discount-types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\DiscountType $discountType
     * @return \Illuminate\Http\Response
     */
    public function destroy(DiscountType $discountType)
    {
        $discountType->delete();

        session()->flash('status', 'Discount Type deleted successfully');

        return redirect()->route('admin.lookups.discount-types.index');
    }
}
