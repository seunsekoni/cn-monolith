<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\State\StoreRequest;
use App\Http\Requests\Admin\State\UpdateRequest;
use App\Models\State;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class StateController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.lookups.states.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables
            ->eloquent(
                State::with('country')
                    ->withTrashed()
                    ->orderBy('deleted_at')
            )
            ->addIndexColumn()
            ->addColumn('edit_state_url', function (State $state) {
                return route('admin.lookups.states.edit', ['state' => $state]);
            })
            ->addColumn('restore_state_url', function (State $state) {
                return route('admin.lookups.states.restore', ['state' => $state]);
            })
            ->addColumn('delete_state_url', function (State $state) {
                return route('admin.lookups.states.destroy', ['state' => $state->id]);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.lookups.states.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\State\StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        State::create([
            'name' => $request->name,
            'status' => $request->status ? true : false,
            'country_id' => $request->country_id
        ]);

        $request->session()->flash('status', 'State created successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\State $state
     * @return \Illuminate\Http\Response
     */
    public function edit(State $state)
    {
        return view('admin.pages.lookups.states.edit', [
            'state' => $state,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\State\UpdateRequest $request
     * @param \App\Models\State $state
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, State $state)
    {
        $state->update([
            'name' => $request->name,
            'status' => $request->status ? true : false,
            'country_id' => $request->country_id
        ]);

        $request->session()->flash('status', 'State updated successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param State $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, State $state)
    {
        $state->delete();

        $request->session()->flash('status', 'State deleted successfully!');

        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $state
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($state)
    {
        State::withTrashed()->where('id', $state)->restore();

        session()->flash('status', 'State was restored successfully.');

        return redirect()->back();
    }
}
