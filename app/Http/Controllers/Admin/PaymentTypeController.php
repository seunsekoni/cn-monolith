<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PaymentType\StoreRequest;
use App\Http\Requests\Admin\PaymentType\UpdateRequest;
use App\Models\PaymentType;
use Yajra\DataTables\DataTables;

class PaymentTypeController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.lookups.payment-types.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables
            ->eloquent(
                PaymentType::withTrashed()
                    ->orderBy('deleted_at')
            )
            ->addIndexColumn()
            ->editColumn('description', function (PaymentType $paymentType) {
                return truncate($paymentType->description);
            })
            ->addColumn('edit_payment_type_url', function (PaymentType $paymentType) {
                return route('admin.lookups.payment-types.edit', ['payment_type' => $paymentType]);
            })
            ->addColumn('restore_payment_type_url', function (PaymentType $paymentType) {
                return route('admin.lookups.payment-types.restore', ['payment_type' => $paymentType]);
            })
            ->addColumn('delete_payment_type_url', function (PaymentType $paymentType) {
                return route('admin.lookups.payment-types.destroy', ['payment_type' => $paymentType->id]);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.lookups.payment-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\PaymentType\StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        PaymentType::create([
            'name' => $request->name,
            'status' => $request->status ? true : false,
            'description' => $request->description
        ]);

        $request->session()->flash('status', 'Payment Type added successfully');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param App\Models\PaymentType $paymentType
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentType $paymentType)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param App\Models\PaymentType $paymentType
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentType $paymentType)
    {
        return view('admin.pages.lookups.payment-types.edit', [
            'payment_type' => $paymentType
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\PaymentType\UpdateRequest $request
     * @param \App\Models\PaymentType $paymentType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, PaymentType $paymentType)
    {
        $paymentType->update([
            'name' => $request->name,
            'status' => $request->status ? true : false,
            'description' => $request->description
        ]);
        session()->flash('status', 'Payment Type updated successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\PaymentType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentType $paymentType)
    {
        $paymentType->delete();

        session()->flash('status', 'Payment Type deleted successfully!');

        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $paymentType
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($paymentType)
    {
        PaymentType::withTrashed()->where('id', $paymentType)->restore();

        session()->flash('status', 'Payment Type was restored successfully.');

        return redirect()->back();
    }
}
