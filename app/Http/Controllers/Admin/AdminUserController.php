<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class AdminUserController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAction();
        }

        return view('admin.pages.admin-user.index');
    }

    public function indexAction()
    {
        return $this->dataTables
            ->of(
                Admin::withTrashed()->where('email', '!=', auth('admin')->user()->email)
                    ->orderBy('deleted_at')->get()
            )
            ->addIndexColumn()
            ->addColumn('edit_admin_url', function ($admin) {
                return route('admin.admin-user.edit', ['admin_user' => $admin->id]);
            })
            ->addColumn('delete_admin_url', function ($admin) {
                return route('admin.admin-user.destroy', ['admin_user' => $admin->id]);
            })
            ->addColumn('restore_admin_url', function ($admin) {
                return route('admin.admin-user.restore', ['admin_user' => $admin->id]);
            })
            ->make();
    }

    public function create()
    {
        return view('admin.pages.admin-user.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:admins,email',
            'phone' => 'required|unique:admins,phone'
        ]);

        $admin = $request->all();
        $password = env('DEFAULT_ADMIN_PASSWORD');

        $admin['password'] = bcrypt($password);

        Admin::create($admin);

        return redirect()->route('admin.admin-user.index')->with('success', 'Admin Profiled Successfully');
    }

    /**
     * @param Admin $admin
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Admin $admin_user)
    {
        return view('admin.pages.admin-user.edit', compact('admin_user'));
    }

    /**
     * @param \App\Models\Admin $admin_user
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Admin $admin_user, Request $request): RedirectResponse
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required'
        ]);

        $admin_user->update($request->all());

        return redirect()->route('admin.admin-user.index')->with('success', 'Admin Updated Successfully');
    }

    public function destroy(Admin $admin_user)
    {
        $admin_user->delete();

        return redirect()->route('admin.admin-user.index')->with('success', 'Admin Deleted Successfully');
    }

    public function restore($admin_user)
    {
        Admin::withTrashed()->where('id', $admin_user)->restore();

        return redirect()->route('admin.admin-user.index')->with('success', 'Admin Restored Successfully');
    }
}
