<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PaymentGateway\StoreRequest;
use App\Http\Requests\Admin\PaymentGateway\UpdateRequest;
use App\Models\PaymentGateway;
use Yajra\DataTables\DataTables;

class PaymentGatewayController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.lookups.payment-gateways.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables
            ->eloquent(
                PaymentGateway::withTrashed()
                    ->orderBy('deleted_at')
            )
            ->addIndexColumn()
            ->editColumn('description', function (PaymentGateway $paymentGateway) {
                return truncate($paymentGateway->description);
            })
            ->addColumn('edit_payment_gateway_url', function (PaymentGateway $paymentGateway) {
                return route('admin.lookups.payment-gateways.edit', ['payment_gateway' => $paymentGateway]);
            })
            ->addColumn('restore_payment_gateway_url', function (PaymentGateway $paymentGateway) {
                return route('admin.lookups.payment-gateways.restore', ['payment_gateway' => $paymentGateway]);
            })
            ->addColumn('delete_payment_gateway_url', function (PaymentGateway $paymentGateway) {
                return route('admin.lookups.payment-gateways.destroy', ['payment_gateway' => $paymentGateway->id]);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.lookups.payment-gateways.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\PaymentGateway\StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        PaymentGateway::create([
            'name' => $request->name,
            'status' => $request->status ? true : false,
            'description' => $request->description
        ]);

        session()->flash('status', 'Payment Gateway added successfully!');

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Admin\PaymentGateway $paymentGateway
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentGateway $paymentGateway)
    {
        return view('admin.pages.lookups.payment-gateways.edit', [
            'payment_gateway' => $paymentGateway
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\PaymentGateway\UpdateRequest $request
     * @param \App\Models\Admin\PaymentGateway $paymentGateway
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, PaymentGateway $paymentGateway)
    {
        $paymentGateway->update([
            'name' => $request->name,
            'status' => $request->status ? true : false,
            'description' => $request->description
        ]);

        session()->flash('status', 'Payment Gateway updated successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Admin\PaymentGateway $paymentGateway
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentGateway $paymentGateway)
    {
        $paymentGateway->delete();

        session()->flash('status', 'Payment Gateway deleted successfully!');

        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $paymentGateway
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($paymentGateway)
    {
        PaymentGateway::withTrashed()->where('id', $paymentGateway)->restore();

        session()->flash('status', 'Payment Gateway was restored successfully.');

        return redirect()->back();
    }
}
