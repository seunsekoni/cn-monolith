<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Business;
use App\Models\Comment;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class BusinessCommentsController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function index(Business $business)
    {
        if (request()->ajax()) {
            return $this->indexAjax($business);
        }

        return view('admin.pages.businesses.relations.comments', [
            'business' => $business,
            'comments' => $business->comments,
        ]);
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @param Business $business
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax(Business $business)
    {
        return $this->dataTables->eloquent($business->comments()->orderBy('updated_at', 'desc')->with('user'))
            ->addIndexColumn()
            ->addColumn('user', function (Comment $comment) {
                return "
                    {$comment->user->full_name}
                    <br>
                    <small class='text-muted'>{$comment->user->email}</small>
                ";
            })
            ->editColumn('approved', function (Comment $comment) use ($business) {
                return "
                    <a
                        href='#'
                        class='btn btn-sm btn-" . ($comment->approved ? 'danger' : 'success') . "'
                        onclick='
                            event.preventDefault();
                            document.getElementById(\"update-comment{$comment->id}-form\").submit();
                        '
                    >
                        " . ($comment->approved ? 'unapprove' : 'approve') . "
                    </a>
                    <form
                        id='update-comment" . $comment->id . "-form'
                        action=" . route('admin.businesses.comments.update', [
                        'business' => $business->id,
                        'comment' => $comment->id,
                        'approved' => !$comment->approved
                    ]) . "
                        method='POST'
                    >
                        <input type='hidden' name='_method' value='PUT'>
                        " . csrf_field() . "
                    </form>
                ";
            })
            ->addColumn('edit_comment_url', function (Comment $comment) {
                return route('admin.comments.edit', [
                    'comment' => $comment->id,
                ]);
            })
            ->addColumn('delete_comment_url', function (Comment $comment) use ($business) {
                return route('admin.businesses.comments.update', [
                    'business' => $business->id,
                    'comment' => $comment->id,
                ]);
            })
            ->rawColumns(['user', 'approved'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function create(Business $business)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Business $business)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param Business $business
     * @param Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Business $business, Comment $comment)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Business $business
     * @param Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business, Comment $comment)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Business $business
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function update(Request $request, Business $business, Comment $comment)
    {
        $request->validate([
            'body' => 'sometimes|string',
            'approved' => 'sometimes|boolean'
        ]);

        $comment->body = $request->body ?: $comment->body;
        $comment->approved = (bool) $request->approved ? true : false;
        $comment->save();

        $request->session()->flash('status', 'Comment updated successfully.');

        return redirect()->route('admin.businesses.comments.index', [
            'business' => $business->id,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Business $business
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function destroy(Request $request, Business $business, Comment $comment)
    {
        $comment->delete();

        $request->session()->flash('status', 'Comment deleted successfully!');

        return redirect()->route('admin.businesses.comments.index', [
            'business' => $business
        ]);
    }
}
