<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    /**
     * fetch states according to the selected country
     * @param int $country_id
     * @return response
     */
    public function getStates($country_id)
    {
        $states = State::where('country_id', $country_id)->get();
        return response()->json(['states' => $states]);
    }

    /**
     * fetch cities according to the selected state
     * @param int $state_id
     * @return response
     */
    public function getCities($state_id)
    {
        $cities = Country::where('state_id', $state_id)->get();
        return response()->json(['cities' => $cities]);
    }
}
