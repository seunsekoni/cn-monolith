<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Region\StoreRequest;
use App\Http\Requests\Admin\Region\UpdateRequest;
use App\Models\Region;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RegionController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.lookups.regions.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables
            ->eloquent(
                Region::withTrashed()
                    ->orderBy('deleted_at')
            )
            ->addIndexColumn()
            ->addColumn('edit_region_url', function (Region $region) {
                return route('admin.lookups.regions.edit', ['region' => $region]);
            })
            ->addColumn('restore_region_url', function (Region $region) {
                return route('admin.lookups.regions.restore', ['region' => $region->id]);
            })
            ->addColumn('delete_region_url', function (Region $region) {
                return route('admin.lookups.regions.destroy', ['region' => $region->id]);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.lookups.regions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        Region::create([
            'name' => $request->name,
            'status' => $request->status ? true : false
        ]);

        $request->session()->flash('status', 'Region created successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Admin\Region $region
     * @return \Illuminate\Http\Response
     */
    public function show(Region $region)
    {
        return view('admin.pages.lookups.regions.show', [
            'region' => $region
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Admin\Region $region
     * @return \Illuminate\Http\Response
     */
    public function edit(Region $region)
    {
        return view('admin.pages.lookups.regions.edit', [
            'region' => $region
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param \App\Models\Admin\Region $region
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Region $region)
    {
        $region->update([
            'name' => $request->name,
            'status' => $request->status ? true : false
        ]);

        $request->session()->flash('status', 'Region updated successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param \App\Models\Admin\Region $region
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Region $region)
    {
        $region->delete();

        $request->session()->flash('status', 'Region deleted successfully!');

        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $region
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($region)
    {
        Region::withTrashed()->where('id', $region)->restore();

        session()->flash('status', 'Region was restored successfully.');

        return redirect()->back();
    }
}
