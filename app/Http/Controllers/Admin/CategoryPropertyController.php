<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\StoreCategoryPropertyRequest;
use App\Http\Requests\Admin\Category\UpdateCategoryPropertyRequest;
use App\Models\Category;
use App\Models\CategoryProperty;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CategoryPropertyController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        if (request()->ajax()) {
            return $this->indexAjax($category);
        }

        return view('admin.pages.lookups.categories.relations.property', [
            'category' => $category->load(['properties']),
        ]);
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax(Category $category)
    {
        return $this->dataTables->of(
            $category
                ->properties()
                ->withTrashed()
                ->with(['category'])
                ->withCount(['values'])
                ->orderBy('deleted_at')
        )
            ->addIndexColumn()
            ->addColumn('property_value_url', function ($property) use ($category) {
                return route('admin.categories.properties.values.index', [
                    'category' => $category->id,
                    'property' => $property->id
                ]);
            })
            ->addColumn('edit_property_url', function ($property) use ($category) {
                return route('admin.lookups.categories.properties.update', [
                    'category' => $category->id,
                    'property' => $property->id
                ]);
            })
            ->addColumn('restore_property_url', function ($property) use ($category) {
                return route('admin.lookups.categories.properties.restore', [
                    'category' => $category->id,
                    'property' => $property->id
                ]);
            })
            ->addColumn('delete_property_url', function ($property) use ($category) {
                return route('admin.lookups.categories.properties.destroy', [
                    'category' => $category->id,
                    'property' => $property->id
                ]);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCategoryPropertyRequest $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCategoryPropertyRequest $request, Category $category)
    {
        $property = new CategoryProperty();
        $property->category()->associate($category);
        $property->name = $request->name;
        $property->hint = $request->hint;
        $property->optional = $request->optional ? true : false;
        $property->status = $request->optional ? ($request->status ? true : false) : false;
        $property->filterable = $request->filterable ? true : false;
        $property->save();

        $request->session()->flash('status', 'Category property was created successfully.');

        return redirect()->route('admin.lookups.categories.properties.index', [
            'category' => $category,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\CategoryProperty $categoryProperty
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryProperty $categoryProperty)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\CategoryProperty $categoryProperty
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryProperty $categoryProperty)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCategoryPropertyRequest $request
     * @param Category $category
     * @param CategoryProperty $categoryProperty
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryPropertyRequest $request, Category $category, CategoryProperty $property)
    {
        $property->name = $request->name;
        $property->hint = $request->hint;
        $property->optional = $request->optional ? true : false;
        $property->status = $request->status ? true : false;
        $property->filterable = $request->filterable ? true : false;
        $property->save();

        $request->session()->flash('status', 'Category property was updated successfully.');

        return redirect()->route('admin.lookups.categories.properties.index', [
            'category' => $category,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Category $category
     * @param string $property
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Category $category, $property)
    {
        $property = CategoryProperty::withTrashed()->where('id', $property)->firstOrFail();

        $property->businessListings()->detach();

        if ((bool) $request->force) {
            $property->forceDelete();
        } else {
            $property->delete();
        }

        $request->session()->flash('status', 'Category property was deleted successfully.');

        return redirect()->route('admin.lookups.categories.properties.index', [
            'category' => $category,
        ]);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param Request $request
     * @param Category $category
     * @param string $property
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore(Request $request, Category $category, $property)
    {
        CategoryProperty::withTrashed()->where('id', $property)->restore();

        $request->session()->flash('status', 'Category property was restored successfully.');

        return redirect()->route('admin.lookups.categories.properties.index', [
            'category' => $category,
        ]);
    }
}
