<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\City\StoreRequest;
use App\Http\Requests\Admin\City\UpdateRequest;
use App\Models\City;
use App\Models\State;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CityController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.lookups.cities.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables
            ->eloquent(
                City::with(['state.country'])
                    ->withTrashed()
                    ->orderBy('deleted_at')
            )
            ->addIndexColumn()
            ->addColumn('edit_city_url', function (City $city) {
                return route('admin.lookups.cities.edit', ['city' => $city->id]);
            })
            ->addColumn('restore_city_url', function (City $city) {
                return route('admin.lookups.cities.restore', ['city' => $city->id]);
            })
            ->addColumn('delete_city_url', function (City $city) {
                return route('admin.lookups.cities.destroy', ['city' => $city->id]);
            })
            ->make();
    }

    /**
     * Search a city of the resource (used primarily for the select2).
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        if ($id = $request->input('id')) {
            return response()->json([
                'status' => true,
                'data' => [
                    'city' => City::approved()->select(['id', 'name'])->find($id),
                ]
            ]);
        }

        $cities = City::approved()
            ->select(['id', 'name'])
            ->where('name', 'LIKE', "%{$request->term}%")
            ->orderBy('name')
            ->get();

        if ($request->ajax()) {
            return response()->json([
                'status' => true,
                'data' => [
                    'cities' => $cities
                ]
            ]);
        }

        return $cities;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.lookups.cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\City\StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        City::create([
            'name' => $request->name,
            'status' => $request->status ? true : false,
            'state_id' => $request->state_id,
        ]);

        $request->session()->flash('status', 'City created successfully!');

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\City $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        return view('admin.pages.lookups.cities.edit', [
            'city' => $city,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\City\UpdateRequest $request
     * @param \App\Models\City $city
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, City $city)
    {
        $city->update([
            'name' => $request->name,
            'status' => $request->status ? true : false,
            'state_id' => $request->state_id,
        ]);

        $request->session()->flash('status', 'City updated successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\City $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, City $city)
    {
        $city->delete();

        $request->session()->flash('status', 'City deleted successfully!');

        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $city
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($city)
    {
        City::withTrashed()->where('id', $city)->restore();

        session()->flash('status', 'City was restored successfully.');

        return redirect()->back();
    }
}
