<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Support\Collection;
use Yajra\DataTables\DataTables;

class AuditTrailController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.audit-trails.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        $admins = Admin::all();
        $data = [];
        foreach ($admins as $admin) {
            $actions = $admin->actions;
            $clonedActions = clone $actions;
            $obj = new \stdClass();
            $obj->full_name = $admin->full_name;
            $obj->created_business_count = $clonedActions
                                                ->where('log_name', 'business')
                                                ->where('description', 'created')
                                                ->where('subject_type', 'App\Models\Business')
                                                ->count();
            $obj->updated_business_count = $clonedActions
                                                ->where('log_name', 'business')
                                                ->where('description', 'updated')
                                                ->count();
            $obj->created_listing_count = $clonedActions
                                                ->where('log_name', 'businessListing')
                                                ->where('description', 'created')
                                                ->where('subject_type', 'App\Models\BusinessListing')
                                                ->count();
            $obj->updated_listing_count = $clonedActions
                                                ->where('log_name', 'businessListing')
                                                ->where('description', 'updated')
                                                ->where('subject_type', 'App\Models\BusinessListing')
                                                ->count();
            $data[] = $obj;
        }
        $users = new Collection($data);
        return $this->dataTables->of($users)
        ->addIndexColumn()
        ->make(true);
    }
}
