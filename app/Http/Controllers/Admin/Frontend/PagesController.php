<?php

namespace App\Http\Controllers\Admin\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Pages\StoreRequest;
use App\Models\Page;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Yajra\DataTables\DataTables;

class PagesController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjaxAction();
        }

        return view('admin.pages.frontend-management.pages.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjaxAction()
    {
        return $this->dataTables->of(
            Page::query()
                ->withTrashed()
                ->orderBy('deleted_at')
        )
            ->addIndexColumn()
            ->addColumn('show_page_url', function ($page) {
                return route('admin.pages.show', ['page' => $page->id]);
            })
            ->addColumn('edit_page_url', function ($page) {
                return route('admin.pages.edit', ['page' => $page->id]);
            })
            ->addColumn('restore_page_url', function ($page) {
                return route('admin.pages.restore', ['page' => $page->id]);
            })
            ->addColumn('delete_page_url', function ($page) {
                return route('admin.pages.destroy', ['page' => $page->id]);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('admin.pages.frontend-management.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRequest $request): RedirectResponse
    {
        Page::create([
            'title' => $request->title,
            'body' => $request->body,
            'keywords' => implode(',', $request->keywords),
            'status' => (bool) $request->status,
            'description' => $request->description
        ]);

        session()->flash('status', 'Page was created successfully.');

        return redirect()->route('admin.pages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Page $page
     * @return Application|Factory|View
     */
    public function show(Page $page)
    {
        return view('admin.pages.frontend-management.pages.show', ['page' => $page]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Page $page
     * @return View
     */
    public function edit(Page $page)
    {
        return view('admin.pages.frontend-management.pages.edit', ['page' => $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Page $page
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Page $page, Request $request)
    {
        $page->update([
            'title' => $request->title,
            'body' => $request->body,
            'keywords' => implode(',', $request->keywords),
            'status' => (bool) $request->status,
            'description' => $request->description
        ]);

        session()->flash('status', 'Page was updated successfully.');

        return redirect()->route('admin.pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Page $page
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Page $page)
    {
        $page->delete();

        session()->flash('status', 'Page was deleted successfully.');

        return redirect()->route('admin.pages.index');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $page
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($page)
    {
        Page::withTrashed()->where('id', $page)->restore();

        session()->flash('status', 'Page was restored successfully.');

        return redirect()->route('admin.pages.index');
    }
}
