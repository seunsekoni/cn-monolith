<?php

namespace App\Http\Controllers\Admin\Frontend;

use App\Enums\MenuModels;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Menus\StoreRequest;
use App\Models\Category;
use App\Models\Menu;
use App\Models\Page;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Yajra\DataTables\DataTables;

class MenuController extends Controller
{
    private DataTables $dataTables;

    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $pages = Page::all();
        $categories = Category::all();
        $is_child_menu = false;

        if (request()->ajax()) {
            return $this->indexAjaxAction(false);
        }
        return view('admin.pages.frontend-management.menu.index', [
            'pages' => $pages,
            'categories' => $categories,
            'is_child_menu' => $is_child_menu
        ]);
    }

    public function indexAjaxAction($is_child, $parent_id = null)
    {
        return $this->dataTables->of(
            $is_child && $parent_id ?
            Menu::find($parent_id)->children :
            Menu::doesntHave('parent')->orderBy('id', 'desc')->withTrashed()->get()
        )
            ->addIndexColumn()
            ->addColumn('show_menu_url', function ($page) {
                return route('admin.menus.show', ['menu' => $page->id]);
            })
            ->addColumn('edit_menu_url', function ($page) {
                return route('admin.menus.edit', ['menu' => $page->id]);
            })
            ->addColumn('restore_menu_url', function ($page) {
                return route('admin.menus.restore', ['menu' => $page->id]);
            })
            ->addColumn('delete_menu_url', function ($page) {
                return route('admin.menus.destroy', ['menu' => $page->id]);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws \Throwable
     */
    public function store(StoreRequest $request): RedirectResponse
    {
            Menu::create([
                'order' => $request->order,
                'title' => $request->title,
                'parent_id' => $request->parent_id ?? null,
                'is_model' => (bool) $request->is_model,
                'link' => $request->link,
                'model' => $request->model,
                'model_id' => (bool) $request->is_model ? $request->model_id : null,
                'top_visible' => $request->position === 'top',
                'bottom_visible' => $request->position === 'bottom',
                'target' => $request->target,
                'active' => (bool) $request->status
            ]);

        session()->flash('status', 'Menu was created successfully.');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|Response|View
     */
    public function show(Menu $menu)
    {
        $pages = Page::all();
        $categories = Category::all();
        $is_child_menu = true;

        if (request()->ajax()) {
            return $this->indexAjaxAction(true, $menu->id);
        }
        return view('admin.pages.frontend-management.menu.index', [
            'pages' => $pages,
            'categories' => $categories,
            'is_child_menu' => $is_child_menu,
            'menu' => $menu
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(Menu $menu)
    {
        $pages = Page::all();
        $categories = Category::all();
        $menuModels = MenuModels::toSelectArray();

        // dd($menu);

        return view('admin.pages.frontend-management.menu.edit', [
            'pages' => $pages,
            'categories' => $categories,
            'menu' => $menu,
            'menuModels' => $menuModels
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(StoreRequest $request, Menu $menu)
    {
//        dd($request->all());
        $menu->update([
            'order' => $request->order,
            'title' => $request->title,
            'is_model' => (bool) $request->is_model,
            'link' => $request->link,
            'model' => $request->is_model ? $request->model : null,
            'model_id' => $request->is_model ? $request->model_id : null,
            'top_visible' => $request->position === 'top',
            'bottom_visible' => $request->position === 'bottom',
            'target' => $request->target,
            'active' => (bool) $request->status
        ]);

        session()->flash('status', 'Menu was updated successfully.');

        return redirect()->route('admin.menus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();

        session()->flash('status', 'Menu was deleted successfully.');

        return redirect()->route('admin.menus.index');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $menu
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($menu)
    {
        Menu::withTrashed()->where('id', $menu)->restore();

        session()->flash('status', 'Menu was restored successfully.');

        return redirect()->route('admin.menus.index');
    }
}
