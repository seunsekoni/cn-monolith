<?php

namespace App\Http\Controllers\Admin\Frontend;

use App\Enums\Color;
use App\Enums\MediaCollection;
use App\Enums\Position;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CallToAction\StoreRequest;
use App\Http\Requests\Admin\CallToAction\UpdateRequest;
use App\Models\CallToAction;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class CallToActionController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjaxAction();
        }

        return view('admin.pages.frontend-management.call-to-action.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     * @throws \Exception
     */
    public function indexAjaxAction()
    {
        return $this->dataTables->of(
            CallToAction::with('category')
            ->withTrashed()
            ->orderBy('deleted_at')
        )
            ->addIndexColumn()
            ->editColumn('description', function (CallToAction $callToAction) {
                return truncate($callToAction->description, 50);
            })
            ->addColumn('edit_call_to_action_url', function ($callToAction) {
                return route('admin.call-to-actions.edit', ['call_to_action' => $callToAction->id]);
            })
            ->addColumn('restore_call_to_action_url', function ($callToAction) {
                return route('admin.call-to-actions.restore', ['call_to_action' => $callToAction->id]);
            })
            ->addColumn('delete_call_to_action_url', function ($callToAction) {
                return route('admin.call-to-actions.destroy', ['call_to_action' => $callToAction->id]);
            })
            ->make();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::approved()->get();
        $colors = Color::toSelectArray();
        $button_positions = Position::toSelectArray();

        return view('admin.pages.frontend-management.call-to-action.create', [
            'categories' => $categories,
            'colors' => $colors,
            'button_positions' => $button_positions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\CallToAction\StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();

        $call_to_action = CallToAction::create([
            'category_id' => $request->category_id,
            'title' => $request->title,
            'description' => $request->description,
            'url' => $request->url,
            'button_text' => $request->button_text,
            'color' => $request->color,
            'position' => $request->position
        ]);

        if ($request->backdrop) {
            $call_to_action->addMediaFromRequest('backdrop')->toMediaCollection(MediaCollection::BACKDROP);
        }

        DB::commit();

        session()->flash('status', 'Call To Action was created successfully');

        return redirect()->route('admin.call-to-actions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CallToAction  $callToAction
     * @return \Illuminate\Http\Response
     */
    public function show(CallToAction $callToAction)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CallToAction  $callToAction
     * @return \Illuminate\Http\Response
     */
    public function edit(CallToAction $callToAction)
    {
        $categories = Category::approved()->get();
        $colors = Color::toSelectArray();
        $button_positions = Position::toSelectArray();

        return view('admin.pages.frontend-management.call-to-action.edit', [
            'call_to_action' => $callToAction,
            'categories' => $categories,
            'colors' => $colors,
            'button_positions' => $button_positions,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param \App\Models\CallToAction $callToAction
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, CallToAction $callToAction)
    {
        DB::beginTransaction();

        $callToAction->update([
            'category_id' => $request->category_id,
            'title' => $request->title,
            'description' => $request->description,
            'url' => $request->url,
            'button_text' => $request->button_text,
            'color' => $request->color,
            'position' => $request->position
        ]);

        if ($request->backdrop) {
            $callToAction->addMediaFromRequest('backdrop')->toMediaCollection(MediaCollection::BACKDROP);
        }

        DB::commit();

        session()->flash('status', 'Call To Action was updated successfully');

        return redirect()->route('admin.call-to-actions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CallToAction  $callToAction
     * @return \Illuminate\Http\Response
     */
    public function destroy(CallToAction $callToAction)
    {
        $callToAction->delete();

        session()->flash('status', 'Call To Action Deleted Successfully');

        return back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $callToAction
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($callToAction)
    {
        CallToAction::withTrashed()->where('id', $callToAction)->restore();

        session()->flash('status', 'Call To Action was restored successfully.');

        return redirect()->route('admin.call-to-actions.index');
    }
}
