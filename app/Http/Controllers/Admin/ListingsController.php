<?php

namespace App\Http\Controllers\Admin;

use App\Enums\MediaCollection;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Listing\StoreRequest;
use App\Http\Requests\Admin\Listing\UpdateRequest;
use App\Models\BusinessListing;
use App\Models\CategoryProperty;
use App\Models\DiscountType;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ListingsController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.listings.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables
            ->eloquent(
                BusinessListing::with(['business', 'category'])
                    ->withTrashed()
                    ->orderBy('deleted_at')
            )
            ->addIndexColumn()
            ->editColumn('description', function (BusinessListing $listing) {
                return truncate($listing->description, 50);
            })
            ->editColumn('business.name', function (BusinessListing $listing) {
                return '<a href="' . route('admin.businesses.listings.index', [
                    'business' => $listing->business->id,
                ]) . '">' . $listing->business->name . '</a>';
            })
            ->addColumn('listing_gallery_url', function (BusinessListing $listing) {
                return route('admin.listings.galleries.store', [
                    'listing' => $listing->id,
                ]);
            })
            ->addColumn('edit_listing_url', function (BusinessListing $listing) {
                return route('admin.listings.edit', [
                    'listing' => $listing->id,
                ]);
            })
            ->addColumn('restore_listing_url', function (BusinessListing $listing) {
                return route('admin.listings.restore', [
                    'listing' => $listing->id,
                ]);
            })
            ->addColumn('delete_listing_url', function (BusinessListing $listing) {
                return route('admin.listings.destroy', [
                    'listing' => $listing->id,
                ]);
            })
            ->rawColumns(['business.name'])
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $discountTypes = DiscountType::orderBy('name')->get();

        return view('admin.pages.listings.create', [
            'discountTypes' => $discountTypes,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();

        $listing = new BusinessListing();
        $listing->business()->associate($request->business_id);
        $listing->category()->associate($request->category_id);
        $listing->name = $request->name;
        $listing->description = $request->description;
        $listing->price = $request->price;
        $listing->status = $request->status ? true : false;
        $listing->featured = $request->featured ? true : false;
        $listing->discount_enabled = $request->discount_enabled ? true : false;

        if ((bool) $listing->discount_enabled) {
            $listing->discountType()->associate($request->discount_type_id);
            $listing->discount_value = $request->discount_value;
            $listing->discount_start = $request->discount_start;
            $listing->discount_end = $request->discount_end;
        }

        $listing->save();

        if ($properties = $request->properties) {
            foreach ($properties as $prop => $value) {
                $property = CategoryProperty::whereName($prop)->firstOrFail();

                $listing->properties()->attach($property->id, [
                    'category_property_value' => $value
                ]);
            }
        }

        // Store the featured image.
        if ($request->featured_image) {
            $listing->addMediaFromRequest('featured_image')->toMediaCollection(MediaCollection::FEATUREDIMAGE);
        }

        // Take care of tags.
        if ($requestTags = $request->tags) {
            $tags = collect();
            foreach ($requestTags as $requestTag) {
                $tag = Tag::firstOrCreate([
                    'name' => $requestTag
                ]);
                $tags->push($tag);
            }
            $listing->tags()->sync($tags->pluck('id'));
        }

        DB::commit();

        $request->session()->flash('status', 'Listing created successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param BusinessListing $listing
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessListing $listing)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param BusinessListing $listing
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessListing $listing)
    {
        $discountTypes = DiscountType::orderBy('name')->get();

        $listing->tags = $listing->tags->map(fn($tag) => $tag->name);
        $listing->load('properties');

        return view('admin.pages.listings.edit', [
            'listing' => $listing,
            'discountTypes' => $discountTypes,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param BusinessListing $listing
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, BusinessListing $listing)
    {
        DB::beginTransaction();

        $listing->business()->associate($request->business_id);
        $listing->category()->associate($request->category_id);
        $listing->name = $request->name;
        $listing->description = $request->description;
        $listing->price = $request->price;
        $listing->status = $request->status ? true : false;
        $listing->featured = $request->featured ? true : false;
        $listing->discount_enabled = $request->discount_enabled ? true : false;

        if ((bool) $listing->discount_enabled) {
            $listing->discountType()->associate($request->discount_type_id);
            $listing->discount_value = $request->discount_value;
            $listing->discount_start = $request->discount_start;
            $listing->discount_end = $request->discount_end;
        }

        $listing->update();

        if ($properties = $request->properties) {
            $listing->properties()->detach(); // remove all existing properties

            foreach ($properties as $prop => $value) {
                $property = CategoryProperty::whereName($prop)->firstOrFail();

                $listing->properties()->attach($property->id, [
                    'category_property_value' => $value
                ]);
            }
        }

        // Store the featured image.
        if ($request->featured_image) {
            $listing->addMediaFromRequest('featured_image')->toMediaCollection(MediaCollection::FEATUREDIMAGE);
        }

        // Take care of tags.
        if ($requestTags = $request->tags) {
            $tags = collect();
            foreach ($request->tags as $requestTag) {
                $tag = Tag::firstOrCreate([
                    'name' => $requestTag
                ]);
                $tags->push($tag);
            }
            $listing->tags()->sync($tags->pluck('id'));
        }

        DB::commit();

        $request->session()->flash('status', 'Listing updated successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param BusinessListing $listing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, BusinessListing $listing)
    {
        $listing->delete();

        $request->session()->flash('status', 'Listing deleted successfully!');

        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $listing
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($business)
    {
        BusinessListing::withTrashed()->where('id', $business)->restore();

        session()->flash('status', 'Listing was restored successfully.');

        return redirect()->back();
    }
}
