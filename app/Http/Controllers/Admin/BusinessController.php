<?php

namespace App\Http\Controllers\Admin;

use App\Enums\MediaCollection;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Business\StoreRequest;
use App\Http\Requests\Admin\Business\UpdateRequest;
use App\Models\Business;
use App\Models\BusinessLocation;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class BusinessController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }

        return view('admin.pages.businesses.index');
    }

    /**
     * Search a listing of the resource (used primarily for the select2).
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        if ($id = $request->input('id')) {
            return response()->json([
                'status' => true,
                'message' => 'Result...',
                'data' => [
                    'business' => Business::select(['id', 'name'])->find($id),
                ]
            ]);
        }

        $businesses = Business::approved()
            ->select(['id', 'name'])
            ->where('name', 'LIKE', "%{$request->term}%")
            ->orderBy('name')
            ->get();

        if ($request->ajax()) {
            return response()->json([
                'status' => true,
                'message' => 'Result...',
                'data' => [
                    'businesses' => $businesses
                ]
            ]);
        }

        return $businesses;
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables->eloquent(Business::query()->withTrashed()->orderBy('deleted_at'))
            ->addIndexColumn()
            ->editColumn('profile', function (Business $business) {
                return truncate($business->profile, 50);
            })
            ->addColumn('show_business_url', function ($business) {
                return route('admin.businesses.show', ['business' => $business->id]);
            })
            ->addColumn('edit_business_url', function ($business) {
                return route('admin.businesses.edit', ['business' => $business->id]);
            })
            ->addColumn('restore_business_url', function ($business) {
                return route('admin.businesses.restore', ['business' => $business->id]);
            })
            ->addColumn('delete_business_url', function ($business) {
                return route('admin.businesses.destroy', ['business' => $business->id]);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $businessTypes = DB::table('business_types')->select(['id', 'name'])->get();

        return view('admin.pages.businesses.create', [
            'businessTypes' => $businessTypes,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();

        $business = new Business();
        $business->businessType()->associate($request->business_type_id);
        $business->name = $request->name;
        $business->profile = $request->profile;
        $business->verified = $request->verified ? true : false;
        $business->verified_at = (bool) $request->verified ? now() : null;
        $business->featured = $request->featured ? true : false;
        $business->changed_logo = $request->logo ? true : false;
        $business->save();

        // Store the logo.
        if ($request->logo) {
            $business->addMediaFromRequest('logo')->toMediaCollection(MediaCollection::LOGO);
        }

        // Take care of business tags.
        if ($requestTags = $request->tags) {
            $tags = collect();
            foreach ($requestTags as $requestTag) {
                $tag = Tag::firstOrCreate([
                    'name' => $requestTag
                ]);
                $tags->push($tag);
            }
            $business->tags()->sync($tags->pluck('id'));
        }

        DB::commit();

        $request->session()->flash('status', 'Business created successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Business $business
     * @return \Illuminate\Http\Response
     */
    public function show(Business $business)
    {
        return view('admin.pages.businesses.show', [
            'business' => $business
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Business $business
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business)
    {
        $businessTypes = DB::table('business_types')->select(['id', 'name'])->get();

        $business->tags = $business->tags->map(fn($tag) => $tag->name);

        return view('admin.pages.businesses.edit', [
            'business' => $business,
            'businessTypes' => $businessTypes,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param \App\Models\Business $business
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Business $business)
    {
        DB::beginTransaction();

        $business->businessType()->associate($request->business_type_id);
        $business->name = $request->name;
        $business->profile = $request->profile;
        $business->verified = $request->verified ? true : false;
        $business->featured = $request->featured ? true : false;
        $business->changed_logo = $request->logo ? true : false;
        $business->update();

        // Store the logo.
        if ($request->logo) {
            $business->addMediaFromRequest('logo')->toMediaCollection(MediaCollection::LOGO);
        }

        // Take care of business tags.
        if ($requestTags = $request->tags) {
            $tags = collect();
            foreach ($requestTags as $requestTag) {
                $tag = Tag::firstOrCreate([
                    'name' => $requestTag
                ]);
                $tags->push($tag);
            }
            $business->tags()->sync($tags->pluck('id'));
        }

        DB::commit();

        $request->session()->flash('status', 'Business updated successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param \App\Models\Business $business
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Business $business)
    {
        $business->delete();

        $request->session()->flash('status', 'Business deleted successfully!');

        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $business
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($business)
    {
        Business::withTrashed()->where('id', $business)->restore();

        session()->flash('status', 'Business was restored successfully.');

        return redirect()->back();
    }

    /**
     * Update the primary location for the specified resource in storage.
     *
     * @param Request $request
     * @param Business $business
     * @return \Illuminate\Http\Response
     */
    public function setPrimaryLocation(Request $request, Business $business)
    {
        $locations = collect($business->businessLocations()->select('id')->get())->implode('id', ',');

        $request->validate([
            'location' => "required|in:{$locations}",
        ]);

        $business->primaryLocation()->associate($request->location);
        $business->save();

        $request->session()->flash('status', 'Business primary address updated successfully!');

        return redirect()->back();
    }
}
