<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\KycRequirement\StoreRequest;
use App\Http\Requests\Admin\KycRequirement\UpdateRequest;
use App\Models\KycRequirement;
use Yajra\DataTables\DataTables;

class KycRequirementController extends Controller
{
    private DataTables $dataTables;

    /**
     * Create a new controller instance.
     *
     * @param DataTables $dataTables
     * @return void
     */
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->indexAjax();
        }
        return view('admin.pages.lookups.kyc-requirements.index');
    }

    /**
     * Display a listing of the resource with DataTables.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function indexAjax()
    {
        return $this->dataTables->eloquent(KycRequirement::query()->withTrashed())
            ->addIndexColumn()
            ->editColumn('description', function (KycRequirement $kycRequirement) {
                return truncate($kycRequirement->description);
            })
            ->addColumn('edit_kyc_requirement_url', function (KycRequirement $kycRequirement) {
                return route('admin.lookups.kyc-requirements.edit', ['kyc_requirement' => $kycRequirement]);
            })
            ->addColumn('restore_kyc_requirement_url', function (KycRequirement $kycRequirement) {
                return route('admin.lookups.kyc-requirements.restore', ['kyc_requirement' => $kycRequirement->id]);
            })
            ->addColumn('delete_kyc_requirement_url', function (KycRequirement $kycRequirement) {
                return route('admin.lookups.kyc-requirements.destroy', ['kyc_requirement' => $kycRequirement->id]);
            })
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.lookups.kyc-requirements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\KycRequirement\StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {

        KycRequirement::create([
            'name' => $request->name,
            'status' => $request->status ? true : false,
            'description' => $request->description
        ]);

        session()->flash('status', 'KYC Requirement added successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\KycRequirement $kycRequirement
     * @return \Illuminate\Http\Response
     */
    public function show(KycRequirement $kycRequirement)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\KycRequirement $kycRequirement
     * @return \Illuminate\Http\Response
     */
    public function edit(KycRequirement $kycRequirement)
    {
        return view('admin.pages.lookups.kyc-requirements.edit', [
            'kyc_requirement' => $kycRequirement,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\KycRequirement\UpdateRequest $request
     * @param \App\Models\KycRequirement $kycRequirement
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, KycRequirement $kycRequirement)
    {
        $kycRequirement->update([
            'name' => $request->name,
            'status' => $request->status ? true : false,
            'description' => $request->description
        ]);

        session()->flash('status', 'KYC requirement updated successfully');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\KycRequirement $kycRequirement
     * @return \Illuminate\Http\Response
     */
    public function destroy(KycRequirement $kycRequirement)
    {
        $kycRequirement->delete();

        session()->flash('status', 'KYC requirement deleted successfully');

        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param mixed $kycRequirement
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($kycRequirement)
    {
        KycRequirement::withTrashed()->where('id', $kycRequirement)->restore();

        session()->flash('status', 'KYC Requirement was restored successfully.');

        return redirect()->back();
    }
}
