<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Profile\PasswordRequest;
use App\Http\Requests\Admin\Profile\StoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Show the form for updating the password.
     *
     * @param Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function updatePassword(PasswordRequest $request)
    {
        auth()->guard('admin')->user()->update(['password' => Hash::make($request->get('password'))]);

        return back()->withStatus(__('Password successfully updated.'));
    }

    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        $admin = auth()->guard('admin')->user();
        return view('admin.profile.edit', [
            'admin' => $admin
        ]);
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\Admin\Profile\StoreRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreRequest $request)
    {
        auth()->guard('admin')->user()->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'email' => $request->email,
        ]);

        return back()->withStatus(__('Profile successfully updated.'));
    }
}
