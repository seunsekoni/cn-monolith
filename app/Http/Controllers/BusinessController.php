<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommentResource;
use App\Http\Resources\ReviewResource;
use App\Models\Business;
use App\Models\BusinessListing;
use App\Models\Comment;
use App\Models\Review;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class BusinessController extends Controller
{
    /**
     * Get a collection of businesses
     *
     * @return View
     */
    public function index(Request $request)
    {
        $previewAll = ($request->input('b') == 'all');

        $businesses = Business::when(!$previewAll, function ($query) {
            $featuredQuery = clone $query;

            if ($featuredQuery->featured()->exists()) {
                return $query->featured();
            }

            return $query;
        })
            ->inRandomOrder()
            ->paginate(6);

        return view('businesses.index', [
            'businesses' => $businesses,
        ]);
    }

    /**
     * Fetch reviews for a specific business.
     *
     * @param Business $business
     * @return JsonResponse
     */
    public function fetchBusinessReviews(Business $business)
    {
        $approved_reviews = $business->reviews()->get();

        return response()->json([
            'success' => true,
            'data' => ReviewResource::collection($approved_reviews)
        ]);
    }

    /**
     * Show the specified resource.
     *
     * @param Business $business
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|View
     */
    public function showListing(Business $business)
    {
        $businessListings = $business->businessListings()
            ->select(['id', 'name', 'slug'])
            ->where('status', 1)
            ->inRandomOrder()
            ->limit(10)
            ->get();

        // fetch related businesses with the same tags
        $related_businesses = Business::whereHas('tags', function ($q) use ($business) {
            return $q->whereIn('name', $business->tags->pluck('name'))->limit(3);
        })
            ->where('id', '!=', $business->id) // So you won't fetch same business
            ->take(3)
            ->inRandomOrder()
            ->get();

        $approved_reviews = $business->reviews()->get();

        $approved_comments = $business->comments()
            ->whereNull('parent_id')
            ->get();

        return view('businesses.listings.index', [
            'business' => $business,
            'businessListings' => $businessListings,
            'approved_reviews' => $approved_reviews,
            'approved_comments' => $approved_comments,
            'related_businesses' => $related_businesses
        ]);
    }

    /**
     * Fetch comments for a specific business.
     *
     * @param Business $business
     * @return JsonResponse
     */
    public function fetchBusinessComments(Business $business)
    {
        $approved_comments = $business->comments()->get();

        return response()->json([
            'success' => true,
            'data' => CommentResource::collection($approved_comments)
        ]);
    }

    /**
     * Get a collection of all businesses
     *
     * @return View
     */
    public function listings()
    {
        $businesses = Business::inRandomOrder()->paginate(6);

        return view('businesses.show')->with('businesses', $businesses);
    }

    /**
     * Store a review for a specific business.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeBusinessReview(Request $request)
    {
        $review = new Review();

        $business = $request->businessReview;
        $business = Business::whereId($business)->first();

        $review->comment = $request->comment;
        $review->rating = $request->rating;
        $review->user_id = $request->user_id;
        $review->reviewable()->associate($business)->save();

        $approved_reviews = $business->reviews()->get();

        // display notifications
        return response()->json([
            'success' => true,
            'data' => ReviewResource::collection($approved_reviews)
        ]);
    }

    /**
     * Store a comment or reply for a specific business.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeBusinessComments(Request $request)
    {
        $comment = new Comment();

        $businessId = $request->business;
        $business = Business::whereId($businessId)->first();

        $comment->body = $request->body;
        $comment->user_id = auth()->user()->id;
        $comment->parent_id = $request->parent_id;
        $comment->commentable()->associate($business)->save();

        $approved_comments = $business->comments()->get();

        // display notifications
        return response()->json([
            'success' => true,
            'data' => CommentResource::collection($approved_comments)
        ]);
    }

    /**
     * this method is intended to return the listings
     * associated to a particular business
     * @param Business $business
     * @return JsonResponse
     */
    public function showABusinessListing(Business $business): JsonResponse
    {
        return response()->json([
            "data" => $business->businessListings
        ]);
    }

    public function requestQuote(BusinessListing $listing, Request $request)
    {
            $request->validate([
            "first_name" => "required",
            "last_name" => "required",
            "phone" => "required",
            "email" => ["required", "email"],
            "listing_id" => ["required", "exists:business_listings,id"]
        ]);

        $listing->sendEmailForRequestingQuotation($request->all());

        return response()->json([
            "message" => "request quote sent successfully, kindly expect a feedback from the business owner"
        ]);
    }
}
