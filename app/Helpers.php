<?php

use App\Models\Business;
use App\Models\BusinessListing;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

if (!function_exists('getGeocodeByAddress')) {
    function getGeocodeByAddress($address)
    {
        $apiKey = config('google.geocode_api_key');

        try {
            $response = Http::retry(2, 100)
                ->get("https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key={$apiKey}");

            if ($response->successful()) {
                $data = json_decode($response->body());

                if ($data->status == 'OK') {
                    $location = $data->results[0]->geometry->location;

                    return [
                        'latitude' => $location->lat,
                        'longitude' => $location->lng,
                    ];
                }

                return false;
            }

            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}

if (!function_exists('getGeoHashEncode')) {
    function getGeoHashEncode($latitude, $longitude, $precision = 4)
    {
        return \GeoHash::encode($latitude, $longitude, $precision);
    }
}

if (!function_exists('getGeoHashDecode')) {
    function getGeoHashDecode($geohash)
    {
        return \GeoHash::decode($geohash);
    }
}

if (!function_exists('mapBackground')) {
    function mapMonthBackground($section = 'home')
    {
        $month = strtolower(date('F'));

        if (Storage::exists("backgrounds/{$section}/{$month}.jpg")) {
            return Storage::url("backgrounds/{$section}/{$month}.jpg");
        }

        return asset("img/backgrounds/{$section}/{$month}.jpg");
    }
}

if (!function_exists('truncate')) {
    function truncate($statement, $limit = 20)
    {
        return Str::of($statement)->limit($limit);
    }
}

if (!function_exists('pluralize')) {
    function pluralize($word, $length = 2)
    {
        return Str::of($word)->plural($length);
    }
}

if (!function_exists('singularize')) {
    function singularize($word)
    {
        return Str::singular($word);
    }
}

if (!function_exists('presentPrice')) {
    function presentPrice($price)
    {
        return '₦' . number_format($price, 2);
    }
}

if (!function_exists('presentDate')) {
    function presentDate($date)
    {
        return Carbon::parse($date)->format('M d, Y');
    }
}

if (!function_exists('presentStates')) {
    function presentStates()
    {
        return App\Models\State::where('status', 1)->orderBy('name', 'asc')->get();
    }
}

if (!function_exists('presentBusinessType')) {
    function presentBusinessType()
    {
        return App\Models\BusinessType::all();
    }
}

if (!function_exists('presentCategories')) {
    function presentCategories()
    {
        return App\Models\Category::where('status', 1)->get();
    }
}

if (!function_exists('presentCategoryProperties')) {
    function presentCategoryProperties()
    {
        return App\Models\CategoryProperty::where('status', 1)->get();
    }
}

if (!function_exists('presentCategoryPropertyValues')) {
    function presentCategoryPropertyValues()
    {
        return App\Models\CategoryPropertyValue::where('status', 1)->get();
    }
}

if (!function_exists('presentCountries')) {
    function presentCountries()
    {
        return App\Models\Country::where('status', 1)->get();
    }
}

if (!function_exists('presentCities')) {
    function presentCities()
    {
        return App\Models\City::where('status', 1)->get();
    }
}

if (!function_exists('presentPages')) {
    function presentPages()
    {
        return App\Models\Page::where('status', 1)->get();
    }
}

if (!function_exists('presentReviews')) {
    function presentReviews()
    {
        return App\Models\Review::all();
    }
}

if (!function_exists('presentComments')) {
    function presentComments()
    {
        return App\Models\Comment::all();
    }
}

if (!function_exists('formatTelephone')) {
    function formatTelephone($number)
    {
        $countryCode = '+234';

        if (preg_match("~^0\d+$~", $number)) {
            $internationalNumber = preg_replace('/^0/', $countryCode, $number);
        } else {
            $internationalNumber = $countryCode . $number;
        }

        return $internationalNumber;
    }
}

// if (!function_exists('sendMessage')) {
// function sendMessage($message, $recipients)
// {
// $account_sid = getenv("TWILIO_SID");
// $auth_token = getenv("TWILIO_AUTH_TOKEN");
// $twilio_number = getenv("TWILIO_NUMBER");
// $client = new Client($account_sid, $auth_token);
// $client->messages->create(formatTelephone($recipients),
// ['from' => $twilio_number, 'body' => $message] );
// }
// }
if (!function_exists('presentAvatar')) {
    function presentAvatar($user_id)
    {
        foreach (scandir(public_path() . '/storage/avatar/') as $key => $file) {
            if (preg_match('/[\s\S]+\.(png|jpg|jpeg|tiff|gif|bmp)/iu', $file)) {
                $arr = explode('.', $file);

                if ($arr[0] == $user_id) {
                    return $file;
                }
            }
        }
    }
}

if (!function_exists('getAverageRating')) {
    function getAverageRating($model)
    {
        $modelTotalFiveStars = $model->reviews()->where('rating', 5)->count();
        $modelTotalFourStars = $model->reviews()->where('rating', 4)->count();
        $modelTotalThreeStars = $model->reviews()->where('rating', 3)->count();
        $modelTotalTwoStars = $model->reviews()->where('rating', 2)->count();
        $modelTotalOneStar = $model->reviews()->where('rating', 1)->count();

        $totalCount = $model->reviews()->sum('rating');

        try {
            $result = ((5 * $modelTotalFiveStars) + (4 * $modelTotalFourStars) + (3 * $modelTotalThreeStars) +
            (2 * $modelTotalTwoStars) + (1 * $modelTotalOneStar))  / intval($totalCount);
            return round($result, 1);
        } catch (\Throwable $th) {
            return 0;
        }
    }
}
