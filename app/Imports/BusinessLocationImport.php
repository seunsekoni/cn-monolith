<?php

namespace App\Imports;

use App\Models\Business;
use App\Models\BusinessLocation;
use App\Models\City;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\ImportFailed;
use Maatwebsite\Excel\Validators\Failure;

class BusinessLocationImport implements
    ToModel,
    WithHeadingRow,
    WithBatchInserts,
    WithChunkReading,
    WithValidation,
    WithEvents,
    ShouldQueue,
    SkipsEmptyRows,
    SkipsOnFailure,
    SkipsOnError
{
    use Importable;
    use SkipsFailures;
    use SkipsErrors;

    /**
     * Save the business location.
     *
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Model|null|void
     */
    public function model(array $row)
    {
        try {
            dispatch(function () use ($row) {
                $business = Business::withTrashed()->whereName($row['business_name'])->firstOrFail();

                $businessLocation = new BusinessLocation();
                $businessLocation->business()->associate($business);

                if ($city = $row['city_name']) {
                    $businessLocation->city()->associate(City::withTrashed()->whereName($city)->firstOrFail());
                }

                $businessLocation->street_address = $row['street_address'];
                $businessLocation->address_landmark = $row['address_landmark'];
                $businessLocation->phone_1 = $row['phone_1'] ?: null;
                $businessLocation->phone_2 = $row['phone_2'] ?: null;
                $businessLocation->website_url = $row['website_link'] ?: null;
                $businessLocation->enable_sms = $row['enable_sms'] == 'yes' ? true : false;
                $businessLocation->sms_number = $row['sms_number'] ?: null;
                $businessLocation->enable_feedback = $row['enable_feedback'] == 'yes' ? true : false;
                $businessLocation->feedback_email = $row['feedback_email'] ?: null;
                $businessLocation->status = $row['active'] == 'yes' ? true : false;
                $businessLocation->save();

                $business->primaryLocation()->associate($businessLocation);
                $business->save();
            });

            return;
        } catch (\PDOException $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        } catch (\Exception $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        }
    }

    /**
     * Determine how many models will be inserted into the database in one time.
     *
     * @return int
     */
    public function batchSize(): int
    {
        return 500;
    }

    /**
     * Put memory usage into consideration when reading the file.
     *
     * @return int
     */
    public function chunkSize(): int
    {
        return 500;
    }

    /**
     * Prepare the data for validation.
     *
     * @param array $row
     * @return void
     */
    public function prepareForValidation(array $row)
    {
        foreach ($row as $key => $value) {
            $row[$key] = trim($value);
        }

        return $row;
    }

    /**
     * Configure the validation rules
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'business_name' => 'required|string',
            'city_name' => 'nullable|string',
            'street_address' => 'nullable|string',
            'address_landmark' => 'nullable|string',
            'phone_1' => 'nullable|string',
            'phone_2' => 'nullable|string',
            'website_link' => 'nullable|string',
            'enable_sms' => 'nullable|string',
            'sms_number' => 'nullable|string',
            'enable_feedback' => 'nullable|string',
            'feedback_email' => 'nullable|string',
        ];
    }

    /**
     * Manage failures in import to avoid rollback.
     *
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        foreach ($failures as $failure) {
            Log::channel('google_chat')->warning('Failures occurred during locations import.', [
                'Row that went wrong' => $failure->row(),
                'Actual error messages from Laravel validator' => $failure->errors(),
            ]);
        }
    }

    /**
     * Manage errors in import to avoid rollback.
     *
     * @param \Throwable $e
     */
    public function onError(\Throwable $e)
    {
        Log::channel('google_chat')->warning($e->getMessage());
    }

    /**
     * Register import events.
     *
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function (ImportFailed $event) {
                Log::channel('google_chat')->warning($event->getException()->getMessage());
            },
        ];
    }
}
