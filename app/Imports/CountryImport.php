<?php

namespace App\Imports;

use App\Models\Country;
use App\Models\Region;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\ImportFailed;
use Maatwebsite\Excel\Validators\Failure;

class CountryImport implements
    ToModel,
    WithHeadingRow,
    WithBatchInserts,
    WithChunkReading,
    WithUpserts,
    WithValidation,
    WithEvents,
    SkipsEmptyRows,
    SkipsOnFailure,
    SkipsOnError
{
    use Importable;
    use SkipsFailures;
    use SkipsErrors;

    /**
     * Save the country.
     *
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $region = Region::whereName($row['region'])->first();

        return new Country([
            'region_id' => $region->id,
            'name' => $row['name'],
            'code' => $row['code'],
            'currency' => $row['currency'],
            'phone_code' => $row['phone_code'],
            'status' => $row['active'] == 'yes' ? true : false,
        ]);
    }

    /**
     * Set the unique attributes.
     *
     * @return string|array
     */
    public function uniqueBy()
    {
        return 'name';
    }

    /**
     * Determine how many models will be inserted into the database in one time.
     *
     * @return int
     */
    public function batchSize(): int
    {
        return 1000;
    }

    /**
     * Put memory usage into consideration when reading the file.
     *
     * @return int
     */
    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * Configure the validation rules
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'region' => 'required|string|exists:regions,name',
            'name' => 'required|string',
            'code' => 'required|string',
            'currency' => 'nullable|string',
            'phone_code' => 'required|int',
        ];
    }

    /**
     * Manage failures in import to avoid rollback.
     *
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        foreach ($failures as $failure) {
            Log::channel('google_chat')->warning('Failures occurred during countries import.', [
                'Row that went wrong' => $failure->row(),
                'Actual error messages from Laravel validator' => $failure->errors(),
            ]);
        }
    }

    /**
     * Manage errors in import to avoid rollback.
     *
     * @param \Throwable $e
     */
    public function onError(\Throwable $e)
    {
        Log::channel('google_chat')->warning($e->getMessage());
    }

    /**
     * Register import events.
     *
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function (ImportFailed $event) {
                Log::channel('google_chat')->warning($event->getException()->getMessage());
            },
        ];
    }
}
