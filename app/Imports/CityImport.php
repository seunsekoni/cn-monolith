<?php

namespace App\Imports;

use App\Models\City;
use App\Models\State;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\ImportFailed;
use Maatwebsite\Excel\Validators\Failure;

class CityImport implements
    ToModel,
    WithHeadingRow,
    WithBatchInserts,
    WithChunkReading,
    WithUpserts,
    WithValidation,
    WithEvents,
    SkipsEmptyRows,
    SkipsOnFailure,
    SkipsOnError
{
    use Importable;
    use SkipsFailures;
    use SkipsErrors;

    /**
     * Save the city.
     *
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $state = State::whereName($row['state'])->first();

        return new City([
            'state_id' => $state->id,
            'name' => $row['name'],
            'status' => $row['active'] == 'yes' ? true : false,
        ]);
    }

    /**
     * Set the unique attributes.
     *
     * @return string|array
     */
    public function uniqueBy()
    {
        return ['name', 'state_id'];
    }

    /**
     * Determine how many models will be inserted into the database in one time.
     *
     * @return int
     */
    public function batchSize(): int
    {
        return 2000;
    }

    /**
     * Put memory usage into consideration when reading the file.
     *
     * @return int
     */
    public function chunkSize(): int
    {
        return 2000;
    }

    /**
     * Prepare the data for validation.
     *
     * @param array $row
     * @return void
     */
    public function prepareForValidation(array $row)
    {
        foreach ($row as $key => $value) {
            $row[$key] = trim($value);
        }

        return $row;
    }

    /**
     * Configure the validation rules
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'state' => 'required|string|exists:states,name',
            'name' => 'required|string',
        ];
    }

    /**
     * Manage failures in import to avoid rollback.
     *
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        foreach ($failures as $failure) {
            Log::channel('google_chat')->warning('Failures occurred during cities import.', [
                'Row that went wrong' => $failure->row(),
                'Actual error messages from Laravel validator' => $failure->errors(),
            ]);
        }
    }

    /**
     * Manage errors in import to avoid rollback.
     *
     * @param \Throwable $e
     */
    public function onError(\Throwable $e)
    {
        Log::channel('google_chat')->warning($e->getMessage());
    }

    /**
     * Register import events.
     *
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function (ImportFailed $event) {
                Log::channel('google_chat')->warning($event->getException()->getMessage());
            },
        ];
    }
}
