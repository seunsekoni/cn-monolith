<?php

namespace App\Imports;

use App\Enums\MediaCollection;
use App\Models\Business;
use App\Models\BusinessContact;
use App\Models\BusinessType;
use App\Models\Tag;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\ImportFailed;
use Maatwebsite\Excel\Validators\Failure;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileUnacceptableForCollection;
use Spatie\MediaLibrary\MediaCollections\Exceptions\UnreachableUrl;

class BusinessImport implements
    ToModel,
    WithHeadingRow,
    WithBatchInserts,
    WithChunkReading,
    WithUpserts,
    WithValidation,
    WithEvents,
    ShouldQueue,
    SkipsEmptyRows,
    SkipsOnFailure,
    SkipsOnError
{
    use Importable;
    use SkipsFailures;
    use SkipsErrors;

    /**
     * Save the business.
     *
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Model|null|void
     */
    public function model(array $row)
    {
        try {
            $business = Business::firstOrNew([
                'name' => $row['name']
            ]);
            $business->businessType()->associate(
                BusinessType::firstOrCreate([
                    'name' => $row['business_type'] ?: 'Uncategorized'
                ])
            );
            $business->profile = ucfirst(strip_tags(html_entity_decode($row['profile'])));
            $business->featured = $row['featured'] == 'yes' ? true : false;
            $business->verified = $row['verified'] == 'yes' ? true : false;
            $business->verified_at = (bool) $business->verified ? now() : null;
            $business->save();

            dispatch(function () use ($business, $row) {
                $this->saveTags($business, $row['tags']);
            })->afterCommit();

            dispatch(function () use ($business, $row) {
                $this->saveBusinessContact($business, $row);
            })->afterCommit();

            // Store the logo
            dispatch(function () use ($business, $row) {
                if ($logo = $row['logo']) {
                    $business->addMediaFromUrl($logo)->toMediaCollection(MediaCollection::LOGO);
                }
            })->afterCommit();

            return;
        } catch (FileUnacceptableForCollection $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        } catch (\PDOException $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        } catch (UnreachableUrl $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        } catch (\Exception $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        }
    }

    /**
     * Set the unique attributes.
     *
     * @return string|array
     */
    public function uniqueBy()
    {
        return 'name';
    }

    /**
     * Determine how many models will be inserted into the database in one time.
     *
     * @return int
     */
    public function batchSize(): int
    {
        return 1000;
    }

    /**
     * Put memory usage into consideration when reading the file.
     *
     * @return int
     */
    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * Prepare the data for validation.
     *
     * @param array $row
     * @return void
     */
    public function prepareForValidation(array $row)
    {
        foreach ($row as $key => $value) {
            $row[$key] = trim($value);
        }

        return $row;
    }

    /**
     * Configure the validation rules
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'business_type' => 'nullable|string',
            'name' => 'required|string',
            'profile' => 'nullable|string',
            'logo' => 'nullable|string',
            'verified' => 'nullable|string|in:yes,no,YES,NO',
            'featured' => 'nullable|string|in:yes,no,YES,NO',
            'tags' => 'nullable|string',
            'contact_name' => 'nullable|string',
            'contact_email' => 'nullable|string',
            'contact_phone' => 'nullable|string',
            'facebook' => 'nullable|string',
            'twitter' => 'nullable|string',
            'instagram' => 'nullable|string',
            'yookos' => 'nullable|string',
            'linkedin' => 'nullable|string',
            'tiktok' => 'nullable|string',
            'skype' => 'nullable|string',
        ];
    }

    /**
     * Save the business tags.
     *
     * @param Business $business
     * @param string $rowTags
     * @return Business
     */
    public function saveTags(Business $business, string $rowTags)
    {
        $tags = collect();
        foreach (explode(',', $rowTags) as $requestTag) {
            $tag = Tag::firstOrCreate([
                'name' => trim($requestTag)
            ]);
            $tags->push($tag);
        }
        $business->tags()->sync($tags->pluck('id'));

        return $business;
    }

    /**
     * Save the business contact information.
     *
     * @param Business $business
     * @param array $row
     * @return BusinessContact|void
     */
    public function saveBusinessContact(Business $business, array $row)
    {
        $businessContact = BusinessContact::firstOrNew([
            'business_id' => $business->id
        ]);
        $businessContact->name = $row['contact_name'];
        $businessContact->email = $row['contact_email'];
        $businessContact->phone = $row['contact_phone'];
        $businessContact->facebook = $row['twitter'];
        $businessContact->twitter = $row['twitter'];
        $businessContact->instagram = $row['instagram'];
        $businessContact->yookos = $row['yookos'];
        $businessContact->linkedin = $row['linkedin'];
        $businessContact->tiktok = $row['tiktok'];
        $businessContact->skype = $row['skype'];

        // Make sure empty records are not inserted
        if (
            $businessContact->name == null
            && $businessContact->email == null
            && $businessContact->phone == null
            && $businessContact->facebook == null
            && $businessContact->twitter == null
            && $businessContact->instagram == null
            && $businessContact->yookos == null
            && $businessContact->linkedin == null
            && $businessContact->tiktok == null
            && $businessContact->skype == null
        ) {
            return;
        }

        $businessContact->save();
    }

    /**
     * Manage failures in import to avoid rollback.
     *
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        foreach ($failures as $failure) {
            Log::channel('google_chat')->warning('Failures occurred during businesses import.', [
                'Row that went wrong' => $failure->row(),
                'Actual error messages from Laravel validator' => $failure->errors(),
            ]);
        }
    }

    /**
     * Manage errors in import to avoid rollback.
     *
     * @param \Throwable $e
     */
    public function onError(\Throwable $e)
    {
        Log::channel('google_chat')->warning($e->getMessage());
    }

    /**
     * Register import events.
     *
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function (ImportFailed $event) {
                Log::channel('google_chat')->warning($event->getException()->getMessage());
            },
        ];
    }
}
