<?php

namespace App\Imports;

use App\Enums\MediaCollection;
use App\Models\Category;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\ImportFailed;
use Maatwebsite\Excel\Validators\Failure;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileUnacceptableForCollection;
use Spatie\MediaLibrary\MediaCollections\Exceptions\UnreachableUrl;

class CategoryImport implements
    ToModel,
    WithHeadingRow,
    WithBatchInserts,
    WithChunkReading,
    WithUpserts,
    WithValidation,
    WithEvents,
    ShouldQueue,
    SkipsEmptyRows,
    SkipsOnFailure,
    SkipsOnError
{
    use Importable;
    use SkipsFailures;
    use SkipsErrors;

    /**
     * Save the category.
     *
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        try {
            $category = Category::firstOrNew([
                'name' => $row['name']
            ]);
            $category->description = ucfirst(strip_tags(html_entity_decode($row['description'])));
            $category->status = true;
            $category->save();

            // Store the icon
            if ($icon = $row['icon']) {
                $category->addMediaFromUrl($icon)->toMediaCollection(MediaCollection::ICON);
            }

            return;
        } catch (FileUnacceptableForCollection $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        } catch (\PDOException $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        } catch (UnreachableUrl $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        } catch (\Exception $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        }
    }

    /**
     * Set the unique attributes.
     *
     * @return string|array
     */
    public function uniqueBy()
    {
        return 'name';
    }

    /**
     * Determine how many models will be inserted into the database in one time.
     *
     * @return int
     */
    public function batchSize(): int
    {
        return 50;
    }

    /**
     * Put memory usage into consideration when reading the file.
     *
     * @return int
     */
    public function chunkSize(): int
    {
        return 50;
    }

    /**
     * Configure the validation rules
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'description' => 'nullable|string',
            'icon' => 'nullable|string|url',
        ];
    }

    /**
     * Manage failures in import to avoid rollback.
     *
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        foreach ($failures as $failure) {
            Log::channel('google_chat')->warning('Failures occurred during category import.', [
                'Row that went wrong' => $failure->row(),
                'Actual error messages from Laravel validator' => $failure->errors(),
            ]);
        }
    }

    /**
     * Manage errors in import to avoid rollback.
     *
     * @param \Throwable $e
     */
    public function onError(\Throwable $e)
    {
        Log::channel('google_chat')->warning($e->getMessage());
    }

    /**
     * Register import events.
     *
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function (ImportFailed $event) {
                Log::channel('google_chat')->warning($event->getException()->getMessage());
            },
        ];
    }
}
