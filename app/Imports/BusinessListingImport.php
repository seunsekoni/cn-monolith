<?php

namespace App\Imports;

use App\Enums\MediaCollection;
use App\Models\Business;
use App\Models\BusinessListing;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\ImportFailed;
use Maatwebsite\Excel\Validators\Failure;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileUnacceptableForCollection;
use Spatie\MediaLibrary\MediaCollections\Exceptions\UnreachableUrl;

class BusinessListingImport implements
    ToModel,
    WithHeadingRow,
    WithBatchInserts,
    WithChunkReading,
    WithUpserts,
    WithValidation,
    WithEvents,
    ShouldQueue,
    SkipsEmptyRows,
    SkipsOnFailure,
    SkipsOnError
{
    use Importable;
    use SkipsFailures;
    use SkipsErrors;

    /**
     * Save the business listing.
     *
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Model|null|void
     */
    public function model(array $row)
    {
        try {
            $listingNames = explode(',', $row['listing_name']);

            $business = Business::withTrashed()
                ->whereName($row['business_name'])
                ->firstOrFail();

            foreach ($listingNames as $name) {
                if (!empty($name)) {
                    dispatch(function () use ($business, $row, $name) {
                        $businessListing = BusinessListing::withTrashed()
                            ->firstOrNew([
                                'name' => trim($name),
                                'business_id' => $business->id,
                            ]);
                        $businessListing->category()->associate(
                            Category::withTrashed()
                                ->firstOrCreate([
                                    'name' => $row['category_name'] ?: 'Uncategorized'
                                ])
                        );
                        $businessListing->description = ucfirst(strip_tags(html_entity_decode($row['description'])));
                        $businessListing->price = (float) $row['price'];
                        $businessListing->status = $row['verified'] == 'yes' ? true : false;
                        $businessListing->featured = $row['featured'] == 'yes' ? true : false;
                        $businessListing->save();

                        dispatch(function () use ($businessListing, $row) {
                            $this->saveTags($businessListing, $row['tags']);
                        })->afterCommit();

                        // Store the featured_image
                        dispatch(function () use ($row, $businessListing) {
                            if ($featured_image = $row['featured_image']) {
                                $businessListing->addMediaFromUrl($featured_image)
                                    ->toMediaCollection(MediaCollection::FEATUREDIMAGE);
                            }
                        })->afterCommit();
                    });
                }
            }

            return;
        } catch (FileUnacceptableForCollection $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        } catch (\PDOException $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        } catch (UnreachableUrl $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        } catch (\Exception $e) {
            Log::channel('google_chat')->warning($e->getMessage());
        }
    }

    /**
     * Set the unique attributes.
     *
     * @return string|array
     */
    public function uniqueBy()
    {
        return 'name';
    }

    /**
     * Determine how many models will be inserted into the database in one time.
     *
     * @return int
     */
    public function batchSize(): int
    {
        return 500;
    }

    /**
     * Put memory usage into consideration when reading the file.
     *
     * @return int
     */
    public function chunkSize(): int
    {
        return 500;
    }

    /**
     * Prepare the data for validation.
     *
     * @param array $row
     * @return void
     */
    public function prepareForValidation(array $row)
    {
        foreach ($row as $key => $value) {
            $row[$key] = trim($value);
        }

        return $row;
    }

    /**
     * Configure the validation rules
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'business_name' => 'required|string',
            'category_name' => 'nullable|string',
            'listing_name' => 'required|string',
            'description' => 'nullable|string',
            'price' => 'nullable|numeric',
            'featured_image' => 'nullable|string',
            'verified' => 'nullable|string|in:yes,no,YES,NO',
            'featured' => 'nullable|string|in:yes,no,YES,NO',
            'tags' => 'nullable|string',
        ];
    }

    /**
     * Save the business listing tags.
     *
     * @param BusinessListing $business
     * @param string $rowTags
     * @return BusinessListing
     */
    public function saveTags(BusinessListing $businessListing, string $rowTags)
    {
        $tags = collect();
        foreach (explode(',', $rowTags) as $requestTag) {
            $tag = Tag::firstOrCreate([
                'name' => trim($requestTag)
            ]);
            $tags->push($tag);
        }
        $businessListing->tags()->sync($tags->pluck('id'));

        return $businessListing;
    }

    /**
     * Manage failures in import to avoid rollback.
     *
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        foreach ($failures as $failure) {
            Log::channel('google_chat')->warning('Failures occurred during listings import.', [
                'Row that went wrong' => $failure->row(),
                'Actual error messages from Laravel validator' => $failure->errors(),
            ]);
        }
    }

    /**
     * Manage errors in import to avoid rollback.
     *
     * @param \Throwable $e
     */
    public function onError(\Throwable $e)
    {
        Log::channel('google_chat')->warning($e->getMessage());
    }

    /**
     * Register import events.
     *
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function (ImportFailed $event) {
                Log::channel('google_chat')->warning($event->getException()->getMessage());
            },
        ];
    }
}
