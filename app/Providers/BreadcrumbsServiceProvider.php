<?php

namespace App\Providers;

use App\Models\Business;
use App\Models\BusinessListing;
use Illuminate\Support\ServiceProvider;
use Tabuna\Breadcrumbs\Breadcrumbs;
use Tabuna\Breadcrumbs\Trail;

class BreadcrumbsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Breadcrumbs::for('admin.dashboard', fn (Trail $trail) =>
        $trail->push('Dashboard', route('admin.dashboard')));

        // Business breadcrumbs
        Breadcrumbs::for('admin.business.index', fn (Trail $trail) =>
        $trail->parent('admin.dashboard')
            ->push('Businesses', route('admin.business.index')));

        Breadcrumbs::for('admin.business.create', fn (Trail $trail) =>
        $trail
            ->parent('admin.business.index', route('admin.business.index'))
            ->push('Add new business', route('admin.dashboard')));

        Breadcrumbs::for('admin.business.edit', fn (Trail $trail) =>
        $trail
            ->parent('admin.business.index', route('admin.business.index'))
            ->push('Update business', route('admin.dashboard')));

        Breadcrumbs::for('admin.business.show', fn (Trail $trail) =>
            $trail
                ->parent('admin.business.index', route('admin.business.index'))
                ->push('Business details', route('admin.dashboard')));

        // General Status breadcrumbs
        Breadcrumbs::for('admin.status-general.index', fn (Trail $trail) =>
             $trail->parent('admin.dashboard')
             ->push('General Status', route('admin.status-general.index')));

        Breadcrumbs::for('admin.status-general.create', fn (Trail $trail) =>
            $trail
                ->parent('admin.status-general.index', route('admin.status-general.index'))
                ->push('Add new General Status', route('admin.dashboard')));

        Breadcrumbs::for('admin.status-general.edit', fn (Trail $trail) =>
            $trail
                ->parent('admin.status-general.index', route('admin.status-general.index'))
                ->push('Update General Status', route('admin.dashboard')));

        Breadcrumbs::for('admin.status-general.show', fn (Trail $trail) =>
            $trail
                ->parent('admin.status-general.index', route('admin.status-general.index'))
                ->push('General Status details', route('admin.dashboard')));

        // Listings.
        Breadcrumbs::for('listings.index', fn (Trail $trail) =>
            $trail->push('All Listings', route('listings.index')));

        Breadcrumbs::for('listings.show', fn (Trail $trail, BusinessListing $businessListing) =>
            $trail
                ->parent('listings.index', route('listings.index'))
                ->push($businessListing->name, route('listings.show', $businessListing)));

        // Businesses.
        Breadcrumbs::for('businesses.index', fn (Trail $trail) =>
            $trail
                ->push('All Businesses', route('businesses.index')));
        Breadcrumbs::for('businesses.show', fn (Trail $trail, Business $business) =>
            $trail
                ->parent('businesses.index', route('businesses.index'))
                ->push($business->name, route('businesses.show', $business)));
    }
}
