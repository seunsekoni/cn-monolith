<?php

namespace App\Providers;

use App\Http\ViewComposers\BottomCallToActionComposer;
use App\Http\ViewComposers\CategoryIconSliderComposer;
use App\Http\ViewComposers\FeaturedBusinessSectionComposer;
use App\Http\ViewComposers\FooterComposer;
use App\Http\ViewComposers\MiddleCallToActionComposer;
use App\Http\ViewComposers\TopBarMenuComposer;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            'includes.footer-cta',
            BottomCallToActionComposer::class
        );

        view()->composer(
            'includes.middle-cta',
            MiddleCallToActionComposer::class
        );

        view()->composer(
            'includes.category-icon-slider',
            CategoryIconSliderComposer::class
        );

        view()->composer(
            '*',
            FeaturedBusinessSectionComposer::class
        );

        view()->composer(
            'includes.footer',
            FooterComposer::class
        );

        view()->composer(
            'components.top-bar-menu',
            TopBarMenuComposer::class
        );
    }
}
