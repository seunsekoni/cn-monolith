<?php

namespace App\Providers;

use App\Events\LoggedIn;
use App\Listeners\SaveLastLoginData;
use App\Models\Admin;
use App\Observers\AdminObserver;
use App\Models\Business;
use App\Models\BusinessListing;
use App\Models\BusinessLocation;
use App\Models\Category;
use App\Models\Page;
use App\Observers\BusinessListingObserver;
use App\Observers\BusinessLocationObserver;
use App\Observers\BusinessObserver;
use App\Observers\CategoryObserver;
use App\Observers\PageObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        LoggedIn::class => [
            SaveLastLoginData::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Admin::observe(AdminObserver::class);
        Page::observe(PageObserver::class);
        Business::observe(BusinessObserver::class);
        BusinessListing::observe(BusinessListingObserver::class);
        BusinessLocation::observe(BusinessLocationObserver::class);
        Category::observe(CategoryObserver::class);
    }
}
