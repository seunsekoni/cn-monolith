<?php

use App\Http\Controllers\API\BusinessTypeController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\CityController;
use App\Http\Controllers\API\CountryController;
use App\Http\Controllers\API\RegionController;
use App\Http\Controllers\API\StateController;
use App\Http\Controllers\API\TagController;
use Illuminate\Support\Facades\Route;

/*
    |--------------------------------------------------------------------------
    | API Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register API routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | is assigned the "api" middleware group. Enjoy building your API!
    |
*/

Route::get('cities', [CityController::class, 'index'])->name('api.cities');
Route::get('cities/search', [CityController::class, 'search'])->name('api.cities.search');

Route::get('categories', [CategoryController::class, 'index'])->name('api.categories');
Route::get('categories/search', [CategoryController::class, 'search'])->name('api.categories.search');

Route::get('business-types', [BusinessTypeController::class, 'index'])->name('api.business-types');
Route::get('business-types/search', [BusinessTypeController::class, 'search'])->name('api.business-types.search');

Route::get('tags', [TagController::class, 'index'])->name('api.tags');
Route::get('tags/search', [TagController::class, 'search'])->name('api.tags.search');

Route::get('states/search', [StateController::class, 'search'])->name('api.states.search');
Route::get('countries/search', [CountryController::class, 'search'])->name('api.countries.search');
Route::get('regions/search', [RegionController::class, 'search'])->name('api.regions.search');
