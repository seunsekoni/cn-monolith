<?php

use App\Http\Controllers\BusinessController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ListingController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['verify' => true]);

Route::get('/', [HomeController::class, 'index'])->name('site.index');
Route::get('/search', [SearchController::class, 'index'])->name('search');

Route::prefix('businesses')->group(function () {
    Route::get('/', [BusinessController::class, 'index'])->name('businesses.index');
    Route::get('/{business:slug}', [BusinessController::class, 'showListing'])->name('businesses.show');
    Route::get('/listings', [BusinessController::class, 'listings'])->name('businesses.listings');
    Route::get('/reviews/{business}', [BusinessController::class, 'fetchBusinessReviews'])
        ->name('fetch_business_reviews');
    Route::get('/comments/{business}', [BusinessController::class, 'fetchBusinessComments'])
        ->name('fetch_business_comments');
    Route::get('/{business}/listings', [BusinessController::class, 'showABusinessListing'])
        ->name('businesses_listings');
    Route::post('/{listing}/request-quote', [BusinessController::class, 'requestQuote'])
        ->name('request_quote');
    Route::middleware(['auth'])->group(function () {
        Route::post('business/review', [BusinessController::class, 'storeBusinessReview'])
            ->name('store_business_review');
        Route::post('business/comment', [BusinessController::class, 'storeBusinessComments'])
            ->name('store_business_comments');
    });
});

Route::prefix('listings')->group(function () {
    Route::get('/', [ListingController::class, 'index'])->name('listings.index');
    Route::get('/{listing:slug}', [ListingController::class, 'show'])->name('listings.show');
    Route::get('/reviews/{listing}', [ListingController::class, 'fetchListingReviews'])
        ->name('fetch_listing_reviews');
    Route::get('/comments/{listing}', [ListingController::class, 'fetchListingComments'])
        ->name('fetch_listing_comments');

    Route::middleware(['auth'])->group(function () {
        Route::post('listing/review', [ListingController::class, 'storeListingReview'])->name('store_listing_review');
        Route::post('listing/comment', [ListingController::class, 'storeListingComments'])
            ->name('store_listing_comments');
    });
});

Route::get('categories/{category:slug}', [CategoryController::class, 'index'])->name('categories.index');

Route::resource('pages', PageController::class);
