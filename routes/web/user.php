<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\User\BusinessContactController;
use App\Http\Controllers\User\BusinessController;
use App\Http\Controllers\User\BusinessGalleryController;
use App\Http\Controllers\User\BusinessHourController;
use App\Http\Controllers\User\BusinessListingController;
use App\Http\Controllers\User\BusinessListingGalleryController;
use App\Http\Controllers\User\BusinessLocationController;
use App\Http\Controllers\User\BusinessLocationGalleryController;
use App\Http\Controllers\User\CommentController;
use App\Http\Controllers\User\ReviewController;
use App\Http\Controllers\User\SupportTicketController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth', 'verified'], 'as' => 'user.'], function () {
    Route::get('/', [DashboardController::class, 'dashboard'])->name('dashboard');

    Route::group(['prefix' => 'business', 'as' => 'business.'], function () {
        Route::get('', [BusinessController::class, 'index'])->name('index');
        Route::get('create', [BusinessController::class, 'create'])->name('create');
        Route::post('', [BusinessController::class, 'store'])->name('store');
        Route::get('edit', [BusinessController::class, 'edit'])->name('edit');
        Route::put('update', [BusinessController::class, 'update'])->name('update');
        Route::post('primary-location', [BusinessController::class, 'setPrimaryLocation'])->name('primary-location');

        Route::get('galleries', [BusinessGalleryController::class, 'index'])->name('gallery.index');
        Route::post('galleries', [BusinessGalleryController::class, 'store'])->name('gallery.store');
        Route::delete('galleries/{gallery}', [BusinessGalleryController::class, 'destroy'])->name('gallery.destroy');

        Route::get('contact', [BusinessContactController::class, 'index'])->name('contact.index');
        Route::post('contact', [BusinessContactController::class, 'store'])->name('contact.store');

        Route::prefix('locations')->group(function () {
            Route::get('', [BusinessLocationController::class, 'index'])->name('locations.index');
            Route::post('', [BusinessLocationController::class, 'store'])->name('locations.store');
            Route::put('{location}', [BusinessLocationController::class, 'update'])->name('locations.update');
            Route::delete('{location}', [BusinessLocationController::class, 'destroy'])->name('locations.destroy');

            Route::get('{location}/galleries', [BusinessLocationGalleryController::class, 'index'])
                ->name('locations.gallery.index');
            Route::post('{location}/galleries', [BusinessLocationGalleryController::class, 'store'])
                ->name('locations.gallery.store');
            Route::delete('{location}/galleries/{gallery}', [BusinessLocationGalleryController::class, 'destroy'])
                ->name('locations.gallery.delete');
        });

        Route::group(['prefix' => 'listings', 'as' => 'listings.'], function () {
            Route::get('', [BusinessListingController::class, 'index'])->name('index');
            Route::get('create', [BusinessListingController::class, 'create'])->name('create');
            Route::post('', [BusinessListingController::class, 'store'])->name('store');
            Route::get('{listing}/edit', [BusinessListingController::class, 'edit'])->name('edit');
            Route::put('{listing}/update', [BusinessListingController::class, 'update'])->name('update');
            Route::delete('{listing}', [BusinessListingController::class, 'destroy'])->name('destroy');

            Route::get('{listing}/galleries', [BusinessListingGalleryController::class, 'index'])
                ->name('gallery.index');
            Route::post('{listing}/galleries', [BusinessListingGalleryController::class, 'store'])
                ->name('gallery.store');
            Route::delete('{listing}/galleries/{gallery}', [BusinessListingGalleryController::class, 'destroy'])
                ->name('gallery.delete');
        });
    });

    Route::resource('reviews', ReviewController::class);
    Route::resource('comments', CommentController::class);
    Route::resource('support-tickets', SupportTicketController::class);
    Route::resource('business-hours', BusinessHourController::class)->only([
        'index', 'store'
    ]);
});
