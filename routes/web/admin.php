<?php

use App\Http\Controllers\Admin\AdminUserController;
use App\Http\Controllers\Admin\AjaxController;
use App\Http\Controllers\Admin\AuditTrailController;
use App\Http\Controllers\Admin\Auth\ForgotPasswordController;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\Auth\ResetPasswordController;
use App\Http\Controllers\Admin\Auth\VerificationController as AuthVerificationController;
use App\Http\Controllers\Admin\BusinessCommentsController;
use App\Http\Controllers\Admin\BusinessContactController;
use App\Http\Controllers\Admin\BusinessController;
use App\Http\Controllers\Admin\BusinessGalleryController;
use App\Http\Controllers\Admin\BusinessListingController;
use App\Http\Controllers\Admin\BusinessListingGalleryController;
use App\Http\Controllers\Admin\BusinessLocationController;
use App\Http\Controllers\Admin\BusinessLocationGalleryController;
use App\Http\Controllers\Admin\BusinessReviewsController;
use App\Http\Controllers\Admin\BusinessTypeController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CategoryPropertyController;
use App\Http\Controllers\Admin\CategoryPropertyValueController;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Admin\CommentsController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\DiscountTypeController;
use App\Http\Controllers\Admin\Frontend\CallToActionController;
use App\Http\Controllers\Admin\Frontend\MenuController;
use App\Http\Controllers\Admin\Frontend\PagesController;
use App\Http\Controllers\Admin\KycRequirementController;
use App\Http\Controllers\Admin\ListingsController;
use App\Http\Controllers\Admin\PaymentGatewayController;
use App\Http\Controllers\Admin\PaymentTypeController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\RegionController;
use App\Http\Controllers\Admin\ReviewsController;
use App\Http\Controllers\Admin\StateController;
use App\Http\Controllers\Admin\SupportTicketController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\UnknownPagesController;
use Illuminate\Support\Facades\Route;

/*
    |--------------------------------------------------------------------------
    | ADMIN WEB ROUTES
    |--------------------------------------------------------------------------
    |
    | Here is where you can register all "admin/*" routes.
    |
*/

Route::get('/login', [LoginController::class, 'showLoginForm'])->name('admin.login');
Route::post('/login', [LoginController::class, 'login'])->name('admin.auth');

Route::group(['prefix' => 'password', 'as' => 'admin.password.'], function () {
    Route::get('/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('request');
    Route::post('/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('email');
    Route::get('/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('reset');
    Route::post('/reset', [ResetPasswordController::class, 'reset'])->name('update');
});

Route::get(
    'email/verify/{id}/{hash}',
    [AuthVerificationController::class, 'verify']
)->middleware('signed')->name('admin.verification.verify');

Route::group(['middleware' => ['auth:admin' ]], function () {
    Route::post('email/verify/resend', [AuthVerificationController::class, 'resend'])->name('admin.verification.resend');
    Route::get('email/verify/notice', [AuthVerificationController::class, 'show'])->name('admin.verification.notice');
    Route::post('/logout', [LoginController::class, 'logout'])->name('admin.logout');
    Route::get('getstates/{id}', [AjaxController::class, 'getStates']);
    Route::get('getcities/{id}', [AjaxController::class, 'getCities']);
    Route::get('profile', [ProfileController::class, 'edit'])->name('admin.profile.edit');
    Route::put(
        'profile/update-password',
        [ProfileController::class, 'updatePassword']
    )->name('admin.profile.update.password');

    // prefix route name with 'admin.*'
    Route::group(['middleware' => ['admin.verified'], 'as' => 'admin.'], function () {
        Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
        Route::group(['as' => 'lookups.'], function () {
            // Regions
            Route::post(
                'regions/restore/{region}',
                [RegionController::class, 'restore']
            )->name('regions.restore');
            Route::resource('regions', RegionController::class);

            // Countries.
            Route::post(
                'countries/restore/{country}',
                [CountryController::class, 'restore']
            )->name('countries.restore');
            Route::resource('countries', CountryController::class);

            // States.
            Route::post(
                'states/restore/{state}',
                [StateController::class, 'restore']
            )->name('states.restore');
            Route::resource('states', StateController::class);

            // Cities
            Route::post(
                'cities/restore/{city}',
                [CityController::class, 'restore']
            )->name('cities.restore');
            Route::resource('cities', CityController::class);

            // Payment Gateways.
            Route::post(
                'payment-gateways/restore/{payment_gateway}',
                [PaymentGatewayController::class, 'restore']
            )->name('payment-gateways.restore');
            Route::resource('payment-gateways', PaymentGatewayController::class);

            // Payment Types.
            Route::post(
                'payment-types/restore/{payment_type}',
                [PaymentTypeController::class, 'restore']
            )->name('payment-types.restore');
            Route::resource('payment-types', PaymentTypeController::class);
            Route::resource('discount-types', DiscountTypeController::class);

            // KYC Requirements
            Route::post(
                'kyc-requirements/restore/{kyc_requirement}',
                [KycRequirementController::class, 'restore']
            )->name('kyc-requirements.restore');
            Route::resource('kyc-requirements', KycRequirementController::class);

            // Business Types
            Route::post(
                'business-types/restore/{business_type}',
                [BusinessTypeController::class, 'restore']
            )->name('business-types.restore');
            Route::resource('business-types', BusinessTypeController::class);

            // Categories
            Route::post(
                'categories/restore/{category}',
                [CategoryController::class, 'restore']
            )->name('categories.restore');
            Route::resource('categories', CategoryController::class);

            // Category properties
            Route::resource('categories.properties', CategoryPropertyController::class)->only([
                'index', 'store', 'update', 'destroy'
            ]);
            Route::post(
                'categories/{category}/properties/{property}',
                [CategoryPropertyController::class, 'restore']
            )->name('categories.properties.restore');
        });

        // Businesses.
        Route::post(
            'businesses/{business}/primary-location',
            [BusinessController::class, 'setPrimaryLocation']
        )->name('businesses.primary-location');
        Route::post(
            'businesses/restore/{business}',
            [BusinessController::class, 'restore']
        )->name('businesses.restore');
        Route::resource('businesses', BusinessController::class);

        // Call to Action.
        Route::post(
            'call-to-actions/restore/{call_to_action}',
            [CallToActionController::class, 'restore']
        )->name('call-to-actions.restore');
        Route::resource('call-to-actions', CallToActionController::class);

        // Listings
        Route::post(
            'listings/restore/{listing}',
            [ListingsController::class, 'restore']
        )->name('listings.restore');
        Route::resource('listings', ListingsController::class)->except([
            'show'
        ]);
        Route::resource('reviews', ReviewsController::class)->except([
            'store'
        ]);
        Route::resource('comments', CommentsController::class)->except([
            'store'
        ]);
        Route::resource('businesses.contacts', BusinessContactController::class)->only([
            'index', 'store'
        ]);

        // Business Locations.
        Route::post(
            'businesses.locations/restore/{location}',
            [BusinessLocationController::class, 'restore']
        )->name('businesses.locations.restore');
        Route::resource('businesses.locations', BusinessLocationController::class)->only([
            'index', 'store', 'update', 'destroy'
        ]);
        Route::resource('businesses.listings', BusinessListingController::class)->only([
            'index', 'destroy'
        ]);
        Route::resource('businesses.reviews', BusinessReviewsController::class)->only([
            'index', 'update', 'destroy'
        ]);
        Route::resource('businesses.comments', BusinessCommentsController::class)->only([
            'index', 'update', 'destroy'
        ]);

        // Pages.
        Route::post(
            'pages/restore/{page}',
            [PagesController::class, 'restore']
        )->name('pages.restore');
        Route::resource('pages', PagesController::class);

        // Menus.
        Route::post(
            'menus/restore/{menu}',
            [MenuController::class, 'restore']
        )->name('menus.restore');
        Route::resource('menus', MenuController::class);

        Route::put('profile', [ProfileController::class, 'update'])->name('profile.update');
        // Admin User.
        Route::post(
            'admin-user/restore/{admin_user}',
            [AdminUserController::class, 'restore']
        )->name('admin-user.restore');
        Route::resource('admin-user', AdminUserController::class);

        // Act as AJAX routes
        Route::prefix('ajax')->group(function () {
            Route::get('businesses/search', [BusinessController::class, 'search'])->name('ajax.businesses.search');
            Route::resource('locations.galleries', BusinessLocationGalleryController::class)->only([
                'index', 'store', 'destroy'
            ]);
            Route::resource('listings.galleries', BusinessListingGalleryController::class)->only([
                'index', 'store', 'destroy'
            ]);
            Route::resource('businesses.galleries', BusinessGalleryController::class)->only([
                'index', 'store', 'destroy'
            ]);
            Route::resource('categories.properties.values', CategoryPropertyValueController::class)->only([
                'index', 'store', 'destroy'
            ]);
        });
        // End AJAX routes
        // Beginning Of Coming Soon Routes
        Route::get('add-users', [UnknownPagesController::class, 'addUsers'])->name('add_users');
        Route::get('view-users', [UnknownPagesController::class, 'viewUsers'])->name('view_users');
        Route::get('settings', [UnknownPagesController::class, 'settings'])->name('settings');
        Route::get('application-issues', [
            UnknownPagesController::class,
            'applicationIssues'
        ])->name('application_issues');
        Route::get('documentation', [UnknownPagesController::class, 'documentation'])->name('documentation');
        // Ending Of Coming Soon Routes

        Route::resource('support-tickets', SupportTicketController::class);

        Route::get('audit-trails/filter', [AuditTrailController::class, 'filter'])->name('audit-trails.filter');
        Route::resource('audit-trails', AuditTrailController::class);
    });
});
